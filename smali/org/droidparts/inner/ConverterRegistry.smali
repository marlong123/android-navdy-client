.class public Lorg/droidparts/inner/ConverterRegistry;
.super Ljava/lang/Object;
.source "ConverterRegistry.java"


# static fields
.field private static final converters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/droidparts/inner/converter/Converter",
            "<*>;>;"
        }
    .end annotation
.end field

.field private static final map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lorg/droidparts/inner/converter/Converter",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ConverterRegistry;->converters:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ConverterRegistry;->map:Ljava/util/HashMap;

    .line 50
    new-instance v0, Lorg/droidparts/inner/converter/BooleanConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/BooleanConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 51
    new-instance v0, Lorg/droidparts/inner/converter/ByteConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/ByteConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 52
    new-instance v0, Lorg/droidparts/inner/converter/CharacterConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/CharacterConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 53
    new-instance v0, Lorg/droidparts/inner/converter/DoubleConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/DoubleConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 54
    new-instance v0, Lorg/droidparts/inner/converter/FloatConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/FloatConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 55
    new-instance v0, Lorg/droidparts/inner/converter/IntegerConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/IntegerConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 56
    new-instance v0, Lorg/droidparts/inner/converter/LongConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/LongConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 57
    new-instance v0, Lorg/droidparts/inner/converter/ShortConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/ShortConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 58
    new-instance v0, Lorg/droidparts/inner/converter/StringConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/StringConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 59
    new-instance v0, Lorg/droidparts/inner/converter/EnumConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/EnumConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 60
    new-instance v0, Lorg/droidparts/inner/converter/DateConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/DateConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 61
    new-instance v0, Lorg/droidparts/inner/converter/UUIDConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/UUIDConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 62
    new-instance v0, Lorg/droidparts/inner/converter/UriConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/UriConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 63
    new-instance v0, Lorg/droidparts/inner/converter/ByteArrayConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/ByteArrayConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 64
    new-instance v0, Lorg/droidparts/inner/converter/JSONObjectConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/JSONObjectConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 65
    new-instance v0, Lorg/droidparts/inner/converter/JSONArrayConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/JSONArrayConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 66
    new-instance v0, Lorg/droidparts/inner/converter/BitmapConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/BitmapConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 67
    new-instance v0, Lorg/droidparts/inner/converter/ModelConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/ModelConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 68
    new-instance v0, Lorg/droidparts/inner/converter/EntityConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/EntityConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 69
    new-instance v0, Lorg/droidparts/inner/converter/ArrayCollectionConverter;

    invoke-direct {v0}, Lorg/droidparts/inner/converter/ArrayCollectionConverter;-><init>()V

    invoke-static {v0}, Lorg/droidparts/inner/ConverterRegistry;->registerConverter(Lorg/droidparts/inner/converter/Converter;)V

    .line 70
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method public static getConverter(Ljava/lang/Class;)Lorg/droidparts/inner/converter/Converter;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lorg/droidparts/inner/converter/Converter",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v3, Lorg/droidparts/inner/ConverterRegistry;->map:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/droidparts/inner/converter/Converter;

    .line 80
    .local v1, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    if-nez v1, :cond_1

    .line 81
    sget-object v3, Lorg/droidparts/inner/ConverterRegistry;->converters:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/droidparts/inner/converter/Converter;

    .line 82
    .local v0, "conv":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    invoke-virtual {v0, p0}, Lorg/droidparts/inner/converter/Converter;->canHandle(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    move-object v1, v0

    .line 84
    sget-object v3, Lorg/droidparts/inner/ConverterRegistry;->map:Ljava/util/HashMap;

    invoke-virtual {v3, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    .end local v0    # "conv":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz v1, :cond_2

    .line 90
    return-object v1

    .line 92
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No converter for \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static registerConverter(Lorg/droidparts/inner/converter/Converter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/inner/converter/Converter",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "converter":Lorg/droidparts/inner/converter/Converter;, "Lorg/droidparts/inner/converter/Converter<*>;"
    sget-object v0, Lorg/droidparts/inner/ConverterRegistry;->converters:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method
