.class public final Lorg/droidparts/inner/ReflectionUtils;
.super Ljava/lang/Object;
.source "ReflectionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v1, "hierarhy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Class<*>;>;"
    const/4 v0, 0x0

    .line 130
    .local v0, "enteredDroidParts":Z
    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v1, v3, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 131
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "org.droidparts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    .line 132
    .local v2, "inDroidParts":Z
    if-eqz v0, :cond_1

    if-nez v2, :cond_1

    .line 139
    :goto_0
    return-object v1

    .line 135
    :cond_1
    move v0, v2

    .line 136
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    .line 138
    if-nez p0, :cond_0

    goto :goto_0
.end method

.method public static classForName(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p0, "clsName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 105
    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static getArrayComponentType(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "arrCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v1, [B

    if-ne p0, v1, :cond_0

    .line 144
    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    .line 163
    :goto_0
    return-object v1

    .line 145
    :cond_0
    const-class v1, [S

    if-ne p0, v1, :cond_1

    .line 146
    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 147
    :cond_1
    const-class v1, [I

    if-ne p0, v1, :cond_2

    .line 148
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 149
    :cond_2
    const-class v1, [J

    if-ne p0, v1, :cond_3

    .line 150
    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 151
    :cond_3
    const-class v1, [F

    if-ne p0, v1, :cond_4

    .line 152
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 153
    :cond_4
    const-class v1, [D

    if-ne p0, v1, :cond_5

    .line 154
    sget-object v1, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 155
    :cond_5
    const-class v1, [Z

    if-ne p0, v1, :cond_6

    .line 156
    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 157
    :cond_6
    const-class v1, [C

    if-ne p0, v1, :cond_7

    .line 158
    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    goto :goto_0

    .line 161
    :cond_7
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "clsName":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-static {v0}, Lorg/droidparts/inner/ReflectionUtils;->classForName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    goto :goto_0
.end method

.method public static getFieldGenericArgs(Ljava/lang/reflect/Field;)[Ljava/lang/Class;
    .locals 8
    .param p0, "field"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            ")[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 168
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 169
    .local v2, "genericType":Ljava/lang/reflect/Type;
    instance-of v6, v2, Ljava/lang/reflect/ParameterizedType;

    if-eqz v6, :cond_0

    .line 170
    check-cast v2, Ljava/lang/reflect/ParameterizedType;

    .end local v2    # "genericType":Ljava/lang/reflect/Type;
    invoke-interface {v2}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v5

    .line 172
    .local v5, "typeArr":[Ljava/lang/reflect/Type;
    array-length v6, v5

    new-array v0, v6, [Ljava/lang/Class;

    .line 173
    .local v0, "argsArr":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, v5

    if-ge v3, v6, :cond_1

    .line 175
    aget-object v6, v5, v3

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 176
    .local v4, "nameParts":[Ljava/lang/String;
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v1, v4, v6

    .line 177
    .local v1, "clsName":Ljava/lang/String;
    invoke-static {v1}, Lorg/droidparts/inner/ReflectionUtils;->classForName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    aput-object v6, v0, v3

    .line 173
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "argsArr":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    .end local v1    # "clsName":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "nameParts":[Ljava/lang/String;
    .end local v5    # "typeArr":[Ljava/lang/reflect/Type;
    .restart local v2    # "genericType":Ljava/lang/reflect/Type;
    :cond_0
    const/4 v6, 0x0

    new-array v0, v6, [Ljava/lang/Class;

    .end local v2    # "genericType":Ljava/lang/reflect/Type;
    :cond_1
    return-object v0
.end method

.method public static getFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/lang/Object;
    .locals 6
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "field"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Field;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    .line 44
    .local v1, "ft":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x0

    :try_start_0
    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isBoolean(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 63
    :goto_0
    return-object v2

    .line 46
    :cond_0
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isInteger(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .local v2, "val":Ljava/lang/Integer;
    goto :goto_0

    .line 48
    .end local v2    # "val":Ljava/lang/Integer;
    :cond_1
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isLong(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 49
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getLong(Ljava/lang/Object;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .local v2, "val":Ljava/lang/Long;
    goto :goto_0

    .line 50
    .end local v2    # "val":Ljava/lang/Long;
    :cond_2
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isFloat(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 51
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getFloat(Ljava/lang/Object;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .local v2, "val":Ljava/lang/Float;
    goto :goto_0

    .line 52
    .end local v2    # "val":Ljava/lang/Float;
    :cond_3
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isDouble(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 53
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getDouble(Ljava/lang/Object;)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .local v2, "val":Ljava/lang/Double;
    goto :goto_0

    .line 54
    .end local v2    # "val":Ljava/lang/Double;
    :cond_4
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isByte(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 55
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getByte(Ljava/lang/Object;)B

    move-result v3

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    .local v2, "val":Ljava/lang/Byte;
    goto :goto_0

    .line 56
    .end local v2    # "val":Ljava/lang/Byte;
    :cond_5
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isShort(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 57
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getShort(Ljava/lang/Object;)S

    move-result v3

    invoke-static {v3}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    .local v2, "val":Ljava/lang/Short;
    goto :goto_0

    .line 58
    .end local v2    # "val":Ljava/lang/Short;
    :cond_6
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lorg/droidparts/inner/TypeHelper;->isCharacter(Ljava/lang/Class;Z)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 59
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->getChar(Ljava/lang/Object;)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    .local v2, "val":Ljava/lang/Character;
    goto :goto_0

    .line 61
    .end local v2    # "val":Ljava/lang/Character;
    :cond_7
    invoke-virtual {p1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .local v2, "val":Ljava/lang/Object;
    goto :goto_0

    .line 64
    .end local v2    # "val":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public static newEnum(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
    .locals 2
    .param p1, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Enum",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "enumClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-class v1, Ljava/lang/Enum;

    invoke-virtual {p0, v1}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, p1}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    .line 123
    .local v0, "en":Ljava/lang/Enum;
    return-object v0
.end method

.method public static newInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static setFieldVal(Ljava/lang/Object;Ljava/lang/reflect/Field;Ljava/lang/Object;)V
    .locals 9
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "field"    # Ljava/lang/reflect/Field;
    .param p2, "val"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 71
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    .line 73
    .local v3, "ft":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isBoolean(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    move-object v0, p2

    check-cast v0, Ljava/lang/Boolean;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 100
    :goto_0
    return-void

    .line 75
    :cond_0
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isInteger(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    move-object v0, p2

    check-cast v0, Ljava/lang/Integer;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    :catch_0
    move-exception v2

    .line 93
    .local v2, "e":Ljava/lang/Exception;
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "valClsName":Ljava/lang/String;
    :goto_1
    const-string v5, "Error assigning <%s> %s to (%s) field %s#%s: %s."

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v8

    const/4 v7, 0x1

    aput-object p2, v6, v7

    const/4 v7, 0x2

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 77
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "valClsName":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isLong(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 78
    move-object v0, p2

    check-cast v0, Ljava/lang/Long;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, p0, v6, v7}, Ljava/lang/reflect/Field;->setLong(Ljava/lang/Object;J)V

    goto :goto_0

    .line 79
    :cond_2
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isFloat(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 80
    move-object v0, p2

    check-cast v0, Ljava/lang/Float;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setFloat(Ljava/lang/Object;F)V

    goto :goto_0

    .line 81
    :cond_3
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isDouble(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 82
    move-object v0, p2

    check-cast v0, Ljava/lang/Double;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {p1, p0, v6, v7}, Ljava/lang/reflect/Field;->setDouble(Ljava/lang/Object;D)V

    goto/16 :goto_0

    .line 83
    :cond_4
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isByte(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 84
    move-object v0, p2

    check-cast v0, Ljava/lang/Byte;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setByte(Ljava/lang/Object;B)V

    goto/16 :goto_0

    .line 85
    :cond_5
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isShort(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 86
    move-object v0, p2

    check-cast v0, Ljava/lang/Short;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Short;->shortValue()S

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setShort(Ljava/lang/Object;S)V

    goto/16 :goto_0

    .line 87
    :cond_6
    const/4 v5, 0x0

    invoke-static {v3, v5}, Lorg/droidparts/inner/TypeHelper;->isCharacter(Ljava/lang/Class;Z)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 88
    move-object v0, p2

    check-cast v0, Ljava/lang/Character;

    move-object v5, v0

    invoke-virtual {v5}, Ljava/lang/Character;->charValue()C

    move-result v5

    invoke-virtual {p1, p0, v5}, Ljava/lang/reflect/Field;->setChar(Ljava/lang/Object;C)V

    goto/16 :goto_0

    .line 90
    :cond_7
    invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 93
    .restart local v2    # "e":Ljava/lang/Exception;
    :cond_8
    const-string v4, "?"

    goto/16 :goto_1
.end method

.method public static varArgsHack([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .param p0, "varArgs"    # [Ljava/lang/Object;

    .prologue
    .line 186
    if-eqz p0, :cond_0

    array-length v2, p0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 187
    const/4 v2, 0x0

    aget-object v0, p0, v2

    .line 188
    .local v0, "firstArg":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 190
    .local v1, "firstArgCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 191
    invoke-static {v0}, Lorg/droidparts/util/Arrays2;->toObjectArray(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    .line 195
    .end local v0    # "firstArg":Ljava/lang/Object;
    .end local v1    # "firstArgCls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    return-object p0
.end method
