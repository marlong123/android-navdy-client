.class public Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;
.super Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;
.source "PasswordBasedEncrypter.java"

# interfaces
.implements Lcom/nimbusds/jose/JWEEncrypter;


# annotations
.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# static fields
.field public static final MIN_RECOMMENDED_ITERATION_COUNT:I = 0x3e8

.field public static final MIN_SALT_LENGTH:I = 0x8


# instance fields
.field private final iterationCount:I

.field private final saltLength:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "saltLength"    # I
    .param p3, "iterationCount"    # I

    .prologue
    .line 114
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;-><init>([BII)V

    .line 115
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "password"    # [B
    .param p2, "saltLength"    # I
    .param p3, "iterationCount"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;-><init>([B)V

    .line 86
    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The minimum salt length (p2s) is 8 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iput p2, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->saltLength:I

    .line 92
    const/16 v0, 0x3e8

    if-ge p3, v0, :cond_1

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The minimum recommended iteration count (p2c) is 1000"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_1
    iput p3, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->iterationCount:I

    .line 97
    return-void
.end method


# virtual methods
.method public encrypt(Lcom/nimbusds/jose/JWEHeader;[B)Lcom/nimbusds/jose/JWECryptoParts;
    .locals 11
    .param p1, "header"    # Lcom/nimbusds/jose/JWEHeader;
    .param p2, "clearText"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getAlgorithm()Lcom/nimbusds/jose/JWEAlgorithm;

    move-result-object v0

    .line 123
    .local v0, "alg":Lcom/nimbusds/jose/JWEAlgorithm;
    invoke-virtual {p1}, Lcom/nimbusds/jose/JWEHeader;->getEncryptionMethod()Lcom/nimbusds/jose/EncryptionMethod;

    move-result-object v2

    .line 125
    .local v2, "enc":Lcom/nimbusds/jose/EncryptionMethod;
    iget v9, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->saltLength:I

    new-array v7, v9, [B

    .line 126
    .local v7, "salt":[B
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 127
    invoke-static {v0, v7}, Lcom/nimbusds/jose/crypto/PBKDF2;->formatSalt(Lcom/nimbusds/jose/JWEAlgorithm;[B)[B

    move-result-object v4

    .line 128
    .local v4, "formattedSalt":[B
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getMACProvider()Ljava/security/Provider;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/nimbusds/jose/crypto/PRFParams;->resolve(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/security/Provider;)Lcom/nimbusds/jose/crypto/PRFParams;

    move-result-object v5

    .line 129
    .local v5, "prfParams":Lcom/nimbusds/jose/crypto/PRFParams;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getPassword()[B

    move-result-object v9

    iget v10, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->iterationCount:I

    invoke-static {v9, v4, v10, v5}, Lcom/nimbusds/jose/crypto/PBKDF2;->deriveKey([B[BILcom/nimbusds/jose/crypto/PRFParams;)Ljavax/crypto/SecretKey;

    move-result-object v6

    .line 132
    .local v6, "psKey":Ljavax/crypto/SecretKey;
    new-instance v9, Lcom/nimbusds/jose/JWEHeader$Builder;

    invoke-direct {v9, p1}, Lcom/nimbusds/jose/JWEHeader$Builder;-><init>(Lcom/nimbusds/jose/JWEHeader;)V

    .line 133
    invoke-static {v7}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/nimbusds/jose/JWEHeader$Builder;->pbes2Salt(Lcom/nimbusds/jose/util/Base64URL;)Lcom/nimbusds/jose/JWEHeader$Builder;

    move-result-object v9

    .line 134
    iget v10, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->iterationCount:I

    invoke-virtual {v9, v10}, Lcom/nimbusds/jose/JWEHeader$Builder;->pbes2Count(I)Lcom/nimbusds/jose/JWEHeader$Builder;

    move-result-object v9

    .line 135
    invoke-virtual {v9}, Lcom/nimbusds/jose/JWEHeader$Builder;->build()Lcom/nimbusds/jose/JWEHeader;

    move-result-object v8

    .line 137
    .local v8, "updatedHeader":Lcom/nimbusds/jose/JWEHeader;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->generateCEK(Lcom/nimbusds/jose/EncryptionMethod;Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 140
    .local v1, "cek":Ljavax/crypto/SecretKey;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-virtual {v9}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getKeyEncryptionProvider()Ljava/security/Provider;

    move-result-object v9

    invoke-static {v1, v6, v9}, Lcom/nimbusds/jose/crypto/AESKW;->wrapCEK(Ljavax/crypto/SecretKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v9

    invoke-static {v9}, Lcom/nimbusds/jose/util/Base64URL;->encode([B)Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 142
    .local v3, "encryptedKey":Lcom/nimbusds/jose/util/Base64URL;
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v9

    invoke-static {v8, p2, v1, v3, v9}, Lcom/nimbusds/jose/crypto/ContentCryptoProvider;->encrypt(Lcom/nimbusds/jose/JWEHeader;[BLjavax/crypto/SecretKey;Lcom/nimbusds/jose/util/Base64URL;Lcom/nimbusds/jose/jca/JWEJCAContext;)Lcom/nimbusds/jose/JWECryptoParts;

    move-result-object v9

    return-object v9
.end method

.method public getIterationCount()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->iterationCount:I

    return v0
.end method

.method public bridge synthetic getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->getJCAContext()Lcom/nimbusds/jose/jca/JWEJCAContext;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPassword()[B
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->getPassword()[B

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPasswordString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->getPasswordString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSaltLength()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/nimbusds/jose/crypto/PasswordBasedEncrypter;->saltLength:I

    return v0
.end method

.method public bridge synthetic supportedEncryptionMethods()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->supportedEncryptionMethods()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic supportedJWEAlgorithms()Ljava/util/Set;
    .locals 1

    .prologue
    .line 1
    invoke-super {p0}, Lcom/nimbusds/jose/crypto/PasswordBasedCryptoProvider;->supportedJWEAlgorithms()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
