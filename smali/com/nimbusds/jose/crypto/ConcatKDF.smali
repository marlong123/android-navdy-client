.class Lcom/nimbusds/jose/crypto/ConcatKDF;
.super Ljava/lang/Object;
.source "ConcatKDF.java"

# interfaces
.implements Lcom/nimbusds/jose/jca/JCAAware;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/nimbusds/jose/jca/JCAAware",
        "<",
        "Lcom/nimbusds/jose/jca/JCAContext;",
        ">;"
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/ThreadSafe;
.end annotation


# instance fields
.field private final jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

.field private final jcaHashAlg:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "jcaHashAlg"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/nimbusds/jose/jca/JCAContext;

    invoke-direct {v0}, Lcom/nimbusds/jose/jca/JCAContext;-><init>()V

    iput-object v0, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    .line 57
    if-nez p1, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The JCA hash algorithm must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaHashAlg:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public static composeOtherInfo([B[B[B[B[B)[B
    .locals 2
    .param p0, "algID"    # [B
    .param p1, "partyUInfo"    # [B
    .param p2, "partyVInfo"    # [B
    .param p3, "suppPubInfo"    # [B
    .param p4, "suppPrivInfo"    # [B

    .prologue
    .line 184
    const/4 v0, 0x5

    new-array v0, v0, [[B

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    invoke-static {v0}, Lcom/nimbusds/jose/util/ByteUtils;->concat([[B)[B

    move-result-object v0

    return-object v0
.end method

.method public static computeDigestCycles(II)I
    .locals 6
    .param p0, "digestLength"    # I
    .param p1, "keyLength"    # I

    .prologue
    .line 224
    int-to-double v2, p1

    int-to-double v4, p0

    div-double v0, v2, v4

    .line 225
    .local v0, "digestCycles":D
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    return v2
.end method

.method public static encodeDataWithLength(Lcom/nimbusds/jose/util/Base64URL;)[B
    .locals 2
    .param p0, "data"    # Lcom/nimbusds/jose/util/Base64URL;

    .prologue
    .line 292
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/nimbusds/jose/util/Base64URL;->decode()[B

    move-result-object v0

    .line 293
    .local v0, "bytes":[B
    :goto_0
    invoke-static {v0}, Lcom/nimbusds/jose/crypto/ConcatKDF;->encodeDataWithLength([B)[B

    move-result-object v1

    return-object v1

    .line 292
    .end local v0    # "bytes":[B
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static encodeDataWithLength([B)[B
    .locals 4
    .param p0, "data"    # [B

    .prologue
    const/4 v3, 0x0

    .line 276
    if-eqz p0, :cond_0

    move-object v0, p0

    .line 277
    .local v0, "bytes":[B
    :goto_0
    array-length v2, v0

    invoke-static {v2}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v1

    .line 278
    .local v1, "length":[B
    const/4 v2, 0x2

    new-array v2, v2, [[B

    aput-object v1, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v2}, Lcom/nimbusds/jose/util/ByteUtils;->concat([[B)[B

    move-result-object v2

    return-object v2

    .line 276
    .end local v0    # "bytes":[B
    .end local v1    # "length":[B
    :cond_0
    new-array v0, v3, [B

    goto :goto_0
.end method

.method public static encodeIntData(I)[B
    .locals 1
    .param p0, "data"    # I

    .prologue
    .line 249
    invoke-static {p0}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v0

    return-object v0
.end method

.method public static encodeNoData()[B
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    new-array v0, v0, [B

    return-object v0
.end method

.method public static encodeStringData(Ljava/lang/String;)[B
    .locals 2
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 262
    if-eqz p0, :cond_0

    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 263
    .local v0, "bytes":[B
    :goto_0
    invoke-static {v0}, Lcom/nimbusds/jose/crypto/ConcatKDF;->encodeDataWithLength([B)[B

    move-result-object v1

    return-object v1

    .line 262
    .end local v0    # "bytes":[B
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMessageDigest()Ljava/security/MessageDigest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/nimbusds/jose/crypto/ConcatKDF;->getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/nimbusds/jose/jca/JCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v1

    .line 203
    .local v1, "provider":Ljava/security/Provider;
    if-nez v1, :cond_0

    .line 204
    :try_start_0
    iget-object v2, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaHashAlg:Ljava/lang/String;

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 206
    :goto_0
    return-object v2

    :cond_0
    iget-object v2, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaHashAlg:Ljava/lang/String;

    invoke-static {v2, v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Lcom/nimbusds/jose/JOSEException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t get message digest for KDF: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method


# virtual methods
.method public deriveKey(Ljavax/crypto/SecretKey;I[B)Ljavax/crypto/SecretKey;
    .locals 10
    .param p1, "sharedSecret"    # Ljavax/crypto/SecretKey;
    .param p2, "keyLength"    # I
    .param p3, "otherInfo"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 99
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 101
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-direct {p0}, Lcom/nimbusds/jose/crypto/ConcatKDF;->getMessageDigest()Ljava/security/MessageDigest;

    move-result-object v6

    .line 103
    .local v6, "md":Ljava/security/MessageDigest;
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_0
    invoke-virtual {v6}, Ljava/security/MessageDigest;->getDigestLength()I

    move-result v7

    invoke-static {v7, p2}, Lcom/nimbusds/jose/crypto/ConcatKDF;->computeDigestCycles(II)I

    move-result v7

    if-le v4, v7, :cond_0

    .line 121
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 123
    .local v2, "derivedKeyMaterial":[B
    invoke-static {p2}, Lcom/nimbusds/jose/util/ByteUtils;->byteLength(I)I

    move-result v5

    .line 125
    .local v5, "keyLengthBytes":I
    array-length v7, v2

    if-ne v7, v5, :cond_2

    .line 127
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const-string v8, "AES"

    invoke-direct {v7, v2, v8}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 130
    :goto_1
    return-object v7

    .line 105
    .end local v2    # "derivedKeyMaterial":[B
    .end local v5    # "keyLengthBytes":I
    :cond_0
    invoke-static {v4}, Lcom/nimbusds/jose/util/IntegerUtils;->toBytes(I)[B

    move-result-object v1

    .line 107
    .local v1, "counterBytes":[B
    invoke-virtual {v6, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 108
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/security/MessageDigest;->update([B)V

    .line 110
    if-eqz p3, :cond_1

    .line 111
    invoke-virtual {v6, p3}, Ljava/security/MessageDigest;->update([B)V

    .line 115
    :cond_1
    :try_start_0
    invoke-virtual {v6}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 116
    :catch_0
    move-exception v3

    .line 117
    .local v3, "e":Ljava/io/IOException;
    new-instance v7, Lcom/nimbusds/jose/JOSEException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Couldn\'t write derived key: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v3}, Lcom/nimbusds/jose/JOSEException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7

    .line 130
    .end local v1    # "counterBytes":[B
    .end local v3    # "e":Ljava/io/IOException;
    .restart local v2    # "derivedKeyMaterial":[B
    .restart local v5    # "keyLengthBytes":I
    :cond_2
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    const/4 v8, 0x0

    invoke-static {v2, v8, v5}, Lcom/nimbusds/jose/util/ByteUtils;->subArray([BII)[B

    move-result-object v8

    const-string v9, "AES"

    invoke-direct {v7, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto :goto_1
.end method

.method public deriveKey(Ljavax/crypto/SecretKey;I[B[B[B[B[B)Ljavax/crypto/SecretKey;
    .locals 2
    .param p1, "sharedSecret"    # Ljavax/crypto/SecretKey;
    .param p2, "keyLength"    # I
    .param p3, "algID"    # [B
    .param p4, "partyUInfo"    # [B
    .param p5, "partyVInfo"    # [B
    .param p6, "suppPubInfo"    # [B
    .param p7, "suppPrivInfo"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/JOSEException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-static {p3, p4, p5, p6, p7}, Lcom/nimbusds/jose/crypto/ConcatKDF;->composeOtherInfo([B[B[B[B[B)[B

    move-result-object v0

    .line 161
    .local v0, "otherInfo":[B
    invoke-virtual {p0, p1, p2, v0}, Lcom/nimbusds/jose/crypto/ConcatKDF;->deriveKey(Ljavax/crypto/SecretKey;I[B)Ljavax/crypto/SecretKey;

    move-result-object v1

    return-object v1
.end method

.method public getHashAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaHashAlg:Ljava/lang/String;

    return-object v0
.end method

.method public getJCAContext()Lcom/nimbusds/jose/jca/JCAContext;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/ConcatKDF;->jcaContext:Lcom/nimbusds/jose/jca/JCAContext;

    return-object v0
.end method
