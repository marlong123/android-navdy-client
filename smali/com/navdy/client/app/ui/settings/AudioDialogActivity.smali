.class public Lcom/navdy/client/app/ui/settings/AudioDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "AudioDialogActivity.java"


# static fields
.field public static final ACTION_HFP_NOT_CONNECTED:Ljava/lang/String; = "TEST_HFP_NOT_CONNECTED"

.field public static final ACTION_TEST_AUDIO_STATUS:Ljava/lang/String; = "TEST_AUDIO_STATUS"

.field public static final DELAY_MILLIS:I = 0x7d0

.field public static final EXTRA_TITLE:Ljava/lang/String; = "EXTRA_TITLE"


# instance fields
.field private audioManager:Landroid/media/AudioManager;

.field bus:Lcom/squareup/otto/Bus;

.field private finishActivityRunnable:Ljava/lang/Runnable;

.field private firstEvent:Z

.field private handler:Landroid/os/Handler;

.field isMuted:Z

.field isShowingAudioStatus:Z

.field outputDeviceName:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100134
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field statusImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100133
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field statusTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10007f
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field

.field volumeProgress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100135
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 58
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isShowingAudioStatus:Z

    .line 59
    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isMuted:Z

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->firstEvent:Z

    return-void
.end method

.method public static showHFPNotConnected(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 223
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "TEST_HFP_NOT_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 224
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 225
    return-void
.end method

.method public static startAudioStatusActivity(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->startAudioStatusActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method public static startAudioStatusActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 213
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 214
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "TEST_AUDIO_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const-string v1, "EXTRA_TITLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 219
    return-void
.end method

.method private updateAudioStatus(Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;)V
    .locals 7
    .param p1, "audioStatus"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 130
    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    if-nez v2, :cond_3

    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->waitingForHfp:Z

    if-nez v2, :cond_3

    .line 131
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->firstEvent:Z

    if-eqz v2, :cond_2

    .line 132
    iput-boolean v5, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->firstEvent:Z

    .line 133
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finishActivityRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 177
    :cond_0
    :goto_0
    iget v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    if-eqz v2, :cond_1

    .line 178
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->volumeProgress:Landroid/widget/ProgressBar;

    iget v3, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamMaxVolume:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 179
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->volumeProgress:Landroid/widget/ProgressBar;

    iget v3, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 181
    :cond_1
    return-void

    .line 135
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finishActivityRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isMuted:Z

    if-nez v2, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finish()V

    goto :goto_0

    .line 141
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finishActivityRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 142
    iput-boolean v5, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->firstEvent:Z

    .line 143
    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    if-eqz v2, :cond_4

    iget v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->currentStreamVolume:I

    if-nez v2, :cond_4

    .line 144
    iput-boolean v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isMuted:Z

    .line 145
    const v2, 0x7f030047

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->setContentView(I)V

    .line 147
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->audioManager:Landroid/media/AudioManager;

    iget v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    const/4 v4, 0x4

    if-ne v2, v4, :cond_6

    const/4 v2, 0x3

    :goto_1
    invoke-virtual {v3, v2, v5, v6}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 148
    const v2, 0x7f10012e

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 149
    .local v1, "closeButton":Landroid/widget/ImageButton;
    new-instance v2, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$2;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v2, 0x7f100130

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 156
    .local v0, "btnOkay":Landroid/widget/Button;
    new-instance v2, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$3;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$3;-><init>(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    .end local v0    # "btnOkay":Landroid/widget/Button;
    .end local v1    # "closeButton":Landroid/widget/ImageButton;
    :cond_4
    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isMuted:Z

    if-nez v2, :cond_1

    .line 166
    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughBluetooth:Z

    if-eqz v2, :cond_7

    .line 167
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusImage:Landroid/widget/ImageView;

    const v3, 0x7f020149

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 168
    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->playingTTS:Z

    if-eqz v2, :cond_5

    iget-boolean v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->throughHFp:Z

    if-eqz v2, :cond_5

    .line 170
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v5, v6}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 175
    :cond_5
    :goto_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->outputDeviceName:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->outputDeviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 147
    :cond_6
    iget v2, p1, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;->streamType:I

    goto :goto_1

    .line 173
    :cond_7
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusImage:Landroid/widget/ImageView;

    const v3, 0x7f02010e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method


# virtual methods
.method public onAudioStatus(Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;)V
    .locals 1
    .param p1, "ttsAudioStatus"    # Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isShowingAudioStatus:Z

    if-eqz v0, :cond_0

    .line 204
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->updateAudioStatus(Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSAudioStatus;)V

    .line 206
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100384,
            0x7f100385
        }
    .end annotation

    .annotation build Lbutterknife/Optional;
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 198
    :goto_0
    return-void

    .line 188
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 189
    .local v0, "intentOpenBluetoothSettings":Landroid/content/Intent;
    const-string v1, "android.settings.BLUETOOTH_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 193
    .end local v0    # "intentOpenBluetoothSettings":Landroid/content/Intent;
    :pswitch_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finish()V

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x7f100384
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCloseClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->onBackPressed()V

    .line 127
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f030048

    .line 68
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->handler:Landroid/os/Handler;

    .line 70
    const-string v6, "audio"

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->audioManager:Landroid/media/AudioManager;

    .line 71
    new-instance v6, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/AudioDialogActivity;)V

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->finishActivityRunnable:Ljava/lang/Runnable;

    .line 77
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v6

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->bus:Lcom/squareup/otto/Bus;

    .line 78
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 79
    .local v3, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_3

    .line 80
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "action":Ljava/lang/String;
    const-string v6, "TEST_AUDIO_STATUS"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 82
    const v6, 0x7f030049

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->setContentView(I)V

    .line 83
    const v6, 0x1020002

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 84
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 85
    .local v2, "grp":Landroid/view/ViewGroup;
    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    invoke-virtual {v5, v8}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 87
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/app/Activity;)V

    .line 88
    const/4 v4, 0x0

    .line 89
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 90
    .local v1, "activityIntent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    const-string v6, "EXTRA_TITLE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 91
    const-string v6, "EXTRA_TITLE"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 95
    :goto_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->statusTitle:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->volumeProgress:Landroid/widget/ProgressBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 97
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isShowingAudioStatus:Z

    .line 107
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "activityIntent":Landroid/content/Intent;
    .end local v2    # "grp":Landroid/view/ViewGroup;
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "view":Landroid/view/View;
    :goto_1
    return-void

    .line 93
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "activityIntent":Landroid/content/Intent;
    .restart local v2    # "grp":Landroid/view/ViewGroup;
    .restart local v4    # "title":Ljava/lang/String;
    .restart local v5    # "view":Landroid/view/View;
    :cond_0
    const v6, 0x7f08041c

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 98
    .end local v1    # "activityIntent":Landroid/content/Intent;
    .end local v2    # "grp":Landroid/view/ViewGroup;
    .end local v4    # "title":Ljava/lang/String;
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    const-string v6, "TEST_HFP_NOT_CONNECTED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 99
    const v6, 0x7f0300ee

    invoke-virtual {p0, v6}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->setContentView(I)V

    .line 100
    invoke-static {p0, p0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/app/Activity;)V

    goto :goto_1

    .line 102
    :cond_2
    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->setContentView(I)V

    goto :goto_1

    .line 105
    .end local v0    # "action":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v7}, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->setContentView(I)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onPause()V

    .line 120
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isShowingAudioStatus:Z

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->unregister(Ljava/lang/Object;)V

    .line 123
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 112
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->isShowingAudioStatus:Z

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioDialogActivity;->bus:Lcom/squareup/otto/Bus;

    invoke-virtual {v0, p0}, Lcom/squareup/otto/Bus;->register(Ljava/lang/Object;)V

    .line 115
    :cond_0
    return-void
.end method

.method public requiresBus()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    return v0
.end method
