.class public Lcom/navdy/client/app/ui/search/DropPinActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "DropPinActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;


# static fields
.field private static final ADDRESS_LOOKUP_DELAY:I = 0x1f4

.field public static final NB_CAMERA_CHANGE_B4_SHOW_ADDRESS:I = 0x0

.field private static final SHOW_DETAILS_SCREEN:I = 0x2a


# instance fields
.field private cardRow:Landroid/view/View;

.field private currentDestination:Lcom/navdy/client/app/framework/models/Destination;

.field private distanceText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

.field private dropPin:Landroid/widget/ImageView;

.field private firstLine:Landroid/widget/TextView;

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private illustration:Landroid/widget/ImageView;

.field private mapHasMoved:I

.field private navButton:Landroid/widget/ImageView;

.field private final navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field private final onCardViewClick:Landroid/view/View$OnClickListener;

.field private secondLine:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 49
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .line 50
    new-instance v0, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->mapHasMoved:I

    .line 63
    new-instance v0, Lcom/navdy/client/app/ui/search/DropPinActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/search/DropPinActivity$1;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->onCardViewClick:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/ui/search/DropPinActivity;Lcom/navdy/client/app/framework/models/Destination;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->currentDestination:Lcom/navdy/client/app/framework/models/Destination;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/ui/search/DropPinActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->showUpdatingLocation()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->firstLine:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->secondLine:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->distanceText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->onCardViewClick:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->cardRow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/search/DropPinActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->setUpPinDrop()V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/search/DropPinActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->showDroppedPin()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/search/DropPinActivity;)Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/search/DropPinActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->mapHasMoved:I

    return v0
.end method

.method static synthetic access$608(Lcom/navdy/client/app/ui/search/DropPinActivity;)I
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->mapHasMoved:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->mapHasMoved:I

    return v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->dropPin:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->illustration:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/app/ui/search/DropPinActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/DropPinActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method private setUpPinDrop()V
    .locals 3

    .prologue
    .line 267
    const v1, 0x7f10014a

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 268
    .local v0, "container":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 269
    new-instance v1, Lcom/navdy/client/app/ui/search/DropPinActivity$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/DropPinActivity$4;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-nez v1, :cond_1

    .line 282
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Calling setUpPinDrop with a null googleMapFragment"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 393
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v2, Lcom/navdy/client/app/ui/search/DropPinActivity$5;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/search/DropPinActivity$5;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    goto :goto_0
.end method

.method private showDroppedPin()V
    .locals 1

    .prologue
    .line 396
    const v0, 0x7f080148

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->showThis(I)V

    .line 397
    return-void
.end method

.method private showThis(I)V
    .locals 2
    .param p1, "stringRes"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 404
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->firstLine:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 405
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->secondLine:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 407
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->cardRow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->distanceText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setVisibility(I)V

    .line 409
    return-void
.end method

.method private showUpdatingLocation()V
    .locals 1

    .prologue
    .line 400
    const v0, 0x7f0804e4

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->showThis(I)V

    .line 401
    return-void
.end method

.method private updateOfflineBannerVisibility()V
    .locals 3

    .prologue
    .line 425
    const v2, 0x7f1000aa

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 426
    .local v1, "offlineBanner":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 427
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    .line 428
    .local v0, "canReachInternet":Z
    if-eqz v0, :cond_1

    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 430
    .end local v0    # "canReachInternet":Z
    :cond_0
    return-void

    .line 428
    .restart local v0    # "canReachInternet":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public centerOnMyLocation(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 93
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 95
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v3, 0x41600000    # 14.0f

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    .line 98
    :cond_0
    return-void
.end method

.method public handleReachabilityStateChange(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;)V
    .locals 0
    .param p1, "reachabilityEvent"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 415
    if-eqz p1, :cond_0

    .line 420
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->updateOfflineBannerVisibility()V

    .line 422
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 104
    const/16 v0, 0x2a

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 107
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f08033c

    const/16 v5, 0x8

    .line 111
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 112
    const v3, 0x7f030054

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->setContentView(I)V

    .line 115
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    invoke-virtual {v3, v6}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 118
    const v3, 0x7f100149

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->dropPin:Landroid/widget/ImageView;

    .line 119
    const v3, 0x7f1000b1

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->illustration:Landroid/widget/ImageView;

    .line 120
    const v3, 0x7f100153

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->firstLine:Landroid/widget/TextView;

    .line 121
    const v3, 0x7f100154

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->secondLine:Landroid/widget/TextView;

    .line 122
    const v3, 0x7f1002f5

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    .line 123
    const v3, 0x7f100151

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->cardRow:Landroid/view/View;

    .line 124
    const v3, 0x7f1002f4

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->distanceText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    .line 126
    const v3, 0x7f1002f6

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/DropPinActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 128
    .local v2, "thirdLine":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->dropPin:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 129
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->dropPin:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 131
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->illustration:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 132
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->illustration:Landroid/widget/ImageView;

    const v4, 0x7f0201a0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->firstLine:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 135
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->firstLine:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(I)V

    .line 137
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->secondLine:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    .line 138
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->secondLine:Landroid/widget/TextView;

    const v4, 0x7f080140

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 140
    :cond_3
    if-eqz v2, :cond_4

    .line 141
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    :cond_4
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    if-eqz v3, :cond_5

    .line 144
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 145
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    const v4, 0x7f020080

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 147
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navButton:Landroid/widget/ImageView;

    new-instance v4, Lcom/navdy/client/app/ui/search/DropPinActivity$2;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/search/DropPinActivity$2;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    :cond_5
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->cardRow:Landroid/view/View;

    if-eqz v3, :cond_6

    .line 177
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->cardRow:Landroid/view/View;

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->onCardViewClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f100147

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 183
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v3, :cond_7

    .line 184
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->hide()V

    .line 185
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 187
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v0, :cond_7

    .line 188
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v5, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iget-object v5, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v5}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    const/high16 v5, 0x41600000    # 14.0f

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    .line 193
    .end local v0    # "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    :cond_7
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v1

    .line 194
    .local v1, "setDestinationTracker":Lcom/navdy/client/app/tracking/SetDestinationTracker;
    const-string v3, "Drop_Pin"

    invoke-virtual {v1, v3}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setSourceValue(Ljava/lang/String;)V

    .line 195
    const-string v3, "Drop_Pin"

    invoke-virtual {v1, v3}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 196
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 216
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 217
    return-void
.end method

.method public onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pendingRoute"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 227
    return-void
.end method

.method public onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 222
    return-void
.end method

.method public onRefreshConnectivityClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 433
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->checkForNetwork()V

    .line 434
    return-void
.end method

.method public onReroute()V
    .locals 0

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 255
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 201
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->updateOfflineBannerVisibility()V

    .line 202
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/search/DropPinActivity$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/DropPinActivity$3;-><init>(Lcom/navdy/client/app/ui/search/DropPinActivity;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->show(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/DropPinActivity;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    .line 211
    return-void
.end method

.method public onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 258
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 237
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    if-ne p1, v0, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 240
    :cond_0
    return-void
.end method

.method public onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 231
    invoke-static {p0}, Lcom/navdy/client/app/ui/routing/RoutingActivity;->startRoutingActivity(Landroid/app/Activity;)V

    .line 232
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 233
    return-void
.end method

.method public onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "route"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 245
    return-void
.end method

.method public onStopRoute()V
    .locals 0

    .prologue
    .line 261
    return-void
.end method

.method public onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "progress"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/DropPinActivity;->finish()V

    .line 250
    return-void
.end method
