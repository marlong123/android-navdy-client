.class public Lcom/navdy/client/app/service/DataCollectionService;
.super Ljava/lang/Object;
.source "DataCollectionService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/service/DataCollectionService$Data;,
        Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat;,
        Lcom/navdy/client/app/service/DataCollectionService$InstanceHolder;
    }
.end annotation


# static fields
.field private static final FAST_REFRESH_RATE:I = 0x14585

.field private static final FILE_EXTENSION:Ljava/lang/String; = ".json.gz"

.field private static final LOG_ALL_SENSOR_EVENTS:Z = false

.field private static final LOG_FREQUENCY:I = 0x3e8

.field private static final MAX_FOLDER_SIZE:I = 0x3200000

.field private static final MAX_S3_UPLOAD_RETRY:I = 0x3

.field private static final MIN_FREE_SPACE:I = 0x500000

.field private static final REQUEST_CODE:I = 0x9fe7e

.field private static final S3_BUCKET_NAME:Ljava/lang/String; = "navdy-trip-data"

.field private static final SENSOR_DATA_FOLDER:Ljava/lang/String; = "SensorData"

.field private static final SLOW_REFRESH_RATE:I = 0x3e8

.field private static final SUPER_FAST_REFRESH_RATE:I = 0x28b0a

.field private static final UPLOAD_DELAY:J

.field private static failCount:I

.field private static volatile folder:Ljava/io/File;

.field private static formatter:Ljava/text/DecimalFormat;

.field private static logger:Lcom/navdy/service/library/log/Logger;

.field private static volatile rootFolder:Ljava/io/File;

.field private static sendInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static transferObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

.field private static useNanoTime:Ljava/lang/Boolean;


# instance fields
.field private accelerometer:Landroid/hardware/Sensor;

.field private bgHandler:Landroid/os/Handler;

.field public data:Lcom/navdy/client/app/service/DataCollectionService$Data;

.field private final dataLock:Ljava/lang/Object;

.field private googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private gravity:Landroid/hardware/Sensor;

.field private gyroscope:Landroid/hardware/Sensor;

.field private isCollecting:Z

.field private isConnected:Z

.field private isDriving:Z

.field private lastGravityTimeStamp:J

.field private lastGyroscopeTimeStamp:J

.field private lastLinearAccelerationTimeStamp:J

.field private lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

.field private lastMagFieldAccuracy:I

.field private lastMagneticFieldTimeStamp:J

.field private lastOrientationTimeStamp:J

.field private lastRotationVectorTimeStamp:J

.field private linearAcceleration:Landroid/hardware/Sensor;

.field private locationUpdatesAtRegularInterval:Ljava/lang/Runnable;

.field private magneticField:Landroid/hardware/Sensor;

.field private rotationVector:Landroid/hardware/Sensor;

.field private sensorManager:Landroid/hardware/SensorManager;

.field private transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

.field private uploadScheduler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 183
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/service/DataCollectionService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    .line 193
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/service/DataCollectionService;->UPLOAD_DELAY:J

    .line 196
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/service/DataCollectionService;->useNanoTime:Ljava/lang/Boolean;

    .line 567
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.00000000000000"

    new-instance v2, Ljava/text/DecimalFormatSymbols;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    sput-object v0, Lcom/navdy/client/app/service/DataCollectionService;->formatter:Ljava/text/DecimalFormat;

    .line 1423
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/navdy/client/app/service/DataCollectionService;->sendInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1425
    sput v4, Lcom/navdy/client/app/service/DataCollectionService;->failCount:I

    return-void
.end method

.method constructor <init>()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-direct {v1}, Lcom/navdy/client/app/service/DataCollectionService$Data;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    .line 208
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    .line 210
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    .line 212
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    .line 213
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    .line 214
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    .line 215
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    .line 216
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    .line 217
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    .line 219
    iput-boolean v3, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    .line 220
    iput-boolean v3, p0, Lcom/navdy/client/app/service/DataCollectionService;->isDriving:Z

    .line 221
    iput-boolean v3, p0, Lcom/navdy/client/app/service/DataCollectionService;->isConnected:Z

    .line 223
    iput v3, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagFieldAccuracy:I

    .line 224
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLinearAccelerationTimeStamp:J

    .line 225
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGravityTimeStamp:J

    .line 226
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagneticFieldTimeStamp:J

    .line 227
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGyroscopeTimeStamp:J

    .line 228
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastRotationVectorTimeStamp:J

    .line 229
    iput-wide v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastOrientationTimeStamp:J

    .line 980
    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .line 982
    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$6;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$6;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    iput-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->locationUpdatesAtRegularInterval:Ljava/lang/Runnable;

    .line 577
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->createS3Client()Lcom/amazonaws/services/s3/AmazonS3Client;

    move-result-object v0

    .line 578
    .local v0, "amazonS3Client":Lcom/amazonaws/services/s3/AmazonS3Client;
    new-instance v1, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;-><init>(Lcom/amazonaws/services/s3/AmazonS3;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    .line 579
    return-void
.end method

.method private static EraseThisFileAndSendNextFile(Ljava/io/File;)V
    .locals 3
    .param p0, "file"    # Ljava/io/File;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1560
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1561
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while trying to delete file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1565
    :goto_0
    return-void

    .line 1563
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->sendToS3()V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->useNanoTime:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 180
    sput-object p0, Lcom/navdy/client/app/service/DataCollectionService;->useNanoTime:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->addLastLocationToData()V

    return-void
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/service/DataCollectionService;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/navdy/client/app/service/DataCollectionService;Lcom/navdy/client/app/service/DataCollectionService$Data$Location;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->locationUpdatesAtRegularInterval:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/service/DataCollectionService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->bgHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/navdy/client/app/service/DataCollectionService;Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/navdy/client/app/service/DataCollectionService;->handleTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->moveAnyTempFileToUploadFolderAndSendIfWiFi()V

    return-void
.end method

.method static synthetic access$1600(Lcom/navdy/client/app/service/DataCollectionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->resetTheData()V

    return-void
.end method

.method static synthetic access$1800()I
    .locals 1

    .prologue
    .line 180
    sget v0, Lcom/navdy/client/app/service/DataCollectionService;->failCount:I

    return v0
.end method

.method static synthetic access$1802(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 180
    sput p0, Lcom/navdy/client/app/service/DataCollectionService;->failCount:I

    return p0
.end method

.method static synthetic access$1808()I
    .locals 2

    .prologue
    .line 180
    sget v0, Lcom/navdy/client/app/service/DataCollectionService;->failCount:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/navdy/client/app/service/DataCollectionService;->failCount:I

    return v0
.end method

.method static synthetic access$1900()V
    .locals 0

    .prologue
    .line 180
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->sendToS3()V

    return-void
.end method

.method static synthetic access$2000(Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Ljava/io/File;

    .prologue
    .line 180
    invoke-static {p0}, Lcom/navdy/client/app/service/DataCollectionService;->EraseThisFileAndSendNextFile(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$202(Lcom/navdy/client/app/service/DataCollectionService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # Z

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/navdy/client/app/service/DataCollectionService;->isConnected:Z

    return p1
.end method

.method static synthetic access$300(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->updateHudUuid()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->startCollecting()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->checkForStop()V

    return-void
.end method

.method static synthetic access$602(Lcom/navdy/client/app/service/DataCollectionService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # Z

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/navdy/client/app/service/DataCollectionService;->isDriving:Z

    return p1
.end method

.method static synthetic access$700()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/service/DataCollectionService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # J

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/service/DataCollectionService;->writeDataToFileAndStartNewData(J)V

    return-void
.end method

.method static synthetic access$900(Lcom/navdy/client/app/service/DataCollectionService;Landroid/hardware/SensorEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/service/DataCollectionService;
    .param p1, "x1"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/navdy/client/app/service/DataCollectionService;->handleSensorEvent(Landroid/hardware/SensorEvent;)V

    return-void
.end method

.method private addLastLocationToData()V
    .locals 3

    .prologue
    .line 990
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$7;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$7;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1003
    return-void
.end method

.method private static availableSdCardSpace()J
    .locals 8

    .prologue
    .line 1169
    new-instance v4, Landroid/os/StatFs;

    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1170
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 1171
    .local v0, "availBlocks":J
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 1172
    .local v2, "blockSize":J
    mul-long v6, v0, v2

    return-wide v6
.end method

.method private checkForStop()V
    .locals 2

    .prologue
    .line 866
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 867
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isDriving:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isConnected:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    if-eqz v0, :cond_0

    .line 868
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->stopCollecting()V

    .line 870
    :cond_0
    monitor-exit v1

    .line 871
    return-void

    .line 870
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static ensureFolderExists()V
    .locals 8

    .prologue
    .line 1062
    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    .line 1063
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1084
    .local v0, "appVersionCode":Ljava/lang/String;
    .local v1, "context":Landroid/content/Context;
    .local v2, "folderPath":Ljava/lang/String;
    .local v3, "internalStorage":Ljava/io/File;
    .local v4, "rootFolderPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1068
    .end local v0    # "appVersionCode":Ljava/lang/String;
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "folderPath":Ljava/lang/String;
    .end local v3    # "internalStorage":Ljava/io/File;
    .end local v4    # "rootFolderPath":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 1069
    .restart local v1    # "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    .line 1070
    .restart local v3    # "internalStorage":Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->getAppVersionCode()Ljava/lang/String;

    move-result-object v0

    .line 1072
    .restart local v0    # "appVersionCode":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SensorData"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1073
    .restart local v4    # "rootFolderPath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v5, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    .line 1075
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1076
    .restart local v2    # "folderPath":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v5, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    .line 1079
    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1080
    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1081
    sget-object v5, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to create the output folder for the sensor data: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static fileSize(Ljava/io/File;)J
    .locals 8
    .param p0, "fileOrFolder"    # Ljava/io/File;

    .prologue
    .line 1152
    const-wide/16 v2, 0x0

    .line 1153
    .local v2, "result":J
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1154
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1155
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 1156
    .local v1, "fileList":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 1157
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v1, v4

    .line 1158
    .local v0, "file":Ljava/io/File;
    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->fileSize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v2, v6

    .line 1157
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1162
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "fileList":[Ljava/io/File;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 1165
    :cond_1
    return-wide v2
.end method

.method private static finishSendingToS3(Ljava/io/File;)V
    .locals 5
    .param p0, "folder"    # Ljava/io/File;

    .prologue
    const/4 v4, 0x0

    .line 1466
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendToS3: Empty data folder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1467
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1468
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "pending_sensor_data_exist"

    .line 1469
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1470
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1471
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->sendInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1472
    return-void
.end method

.method private static getAppVersionCode()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1537
    const/4 v2, 0x0

    .line 1538
    .local v2, "pInfo":Landroid/content/pm/PackageInfo;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1540
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1545
    :goto_0
    if-eqz v2, :cond_0

    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    if-lez v3, :cond_0

    .line 1546
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 1550
    :goto_1
    return-object v3

    .line 1541
    :catch_0
    move-exception v1

    .line 1542
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1547
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    if-eqz v2, :cond_1

    iget-object v3, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1548
    iget-object v3, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_1

    .line 1550
    :cond_1
    const v3, 0x7f0804da

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method static getDoubleString(D)Ljava/lang/String;
    .locals 2
    .param p0, "d"    # D

    .prologue
    .line 571
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->formatter:Ljava/text/DecimalFormat;

    invoke-virtual {v0, p0, p1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getHudUuid()Ljava/lang/String;
    .locals 4

    .prologue
    .line 716
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->getHudUuidFromAppInstance()Ljava/lang/String;

    move-result-object v0

    .line 717
    .local v0, "hudUuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 721
    .end local v0    # "hudUuid":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 720
    .restart local v0    # "hudUuid":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 721
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v2, "last_hud_uuid"

    const-string v3, "UNKNOWN"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getHudUuidFromAppInstance()Ljava/lang/String;
    .locals 4

    .prologue
    .line 693
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 694
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 695
    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;

    move-result-object v0

    .line 696
    .local v0, "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 697
    iget-object v3, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v3

    .line 698
    :try_start_0
    iget-object v2, v0, Lcom/navdy/service/library/events/DeviceInfo;->deviceUuid:Ljava/lang/String;

    monitor-exit v3

    .line 702
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :goto_0
    return-object v2

    .line 699
    .restart local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 702
    .end local v0    # "deviceInfo":Lcom/navdy/service/library/events/DeviceInfo;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/navdy/client/app/service/DataCollectionService;
    .locals 1

    .prologue
    .line 242
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService$InstanceHolder;->instance:Lcom/navdy/client/app/service/DataCollectionService;

    return-object v0
.end method

.method private getPendingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 791
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 792
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/client/app/service/ActivityRecognizedCallbackService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 793
    .local v1, "intent":Landroid/content/Intent;
    const v2, 0x9fe7e

    const/high16 v3, 0x8000000

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method static getTimestampString(J)Ljava/lang/String;
    .locals 8
    .param p0, "timestamp"    # J

    .prologue
    const-wide/16 v6, 0x3e8

    .line 562
    div-long v2, p0, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 563
    .local v0, "str1":Ljava/lang/String;
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%03d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    rem-long v6, p0, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 564
    .local v1, "str2":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private handleSensorEvent(Landroid/hardware/SensorEvent;)V
    .locals 14
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const-wide/32 v12, 0x4f78f88

    const v10, 0xb71b0

    .line 893
    new-instance v3, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;

    invoke-direct {v3, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;-><init>(Landroid/hardware/SensorEvent;)V

    .line 895
    .local v3, "sensorData":Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;
    iget-object v5, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v5

    .line 896
    :try_start_0
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_3

    .line 897
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    iput-object v6, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->lastAccelerometerData:[F

    .line 967
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->orientation:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v10, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->linearAcceleration:Ljava/util/LinkedList;

    .line 968
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v10, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gravity:Ljava/util/LinkedList;

    .line 969
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v10, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->magneticField:Ljava/util/LinkedList;

    .line 970
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v10, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gyroscope:Ljava/util/LinkedList;

    .line 971
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-gt v4, v10, :cond_1

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->rotationVector:Ljava/util/LinkedList;

    .line 972
    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-le v4, v10, :cond_2

    .line 973
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-wide v6, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    invoke-direct {p0, v6, v7}, Lcom/navdy/client/app/service/DataCollectionService;->writeDataToFileAndStartNewData(J)V

    .line 975
    :cond_2
    monitor-exit v5

    .line 976
    return-void

    .line 898
    :cond_3
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_5

    .line 899
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLinearAccelerationTimeStamp:J

    sub-long/2addr v6, v8

    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    .line 900
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->linearAcceleration:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_4

    .line 901
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LINEAR_ACCELERATION: nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->linearAcceleration:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 904
    :cond_4
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLinearAccelerationTimeStamp:J

    .line 905
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->linearAcceleration:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 975
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 907
    :cond_5
    :try_start_1
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_7

    .line 908
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGravityTimeStamp:J

    sub-long/2addr v6, v8

    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    .line 909
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gravity:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_6

    .line 910
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GRAVITY            : nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->gravity:Ljava/util/LinkedList;

    .line 911
    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 910
    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 914
    :cond_6
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGravityTimeStamp:J

    .line 915
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gravity:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 917
    :cond_7
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_a

    .line 918
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagneticFieldTimeStamp:J

    sub-long/2addr v6, v8

    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    .line 919
    iget v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagFieldAccuracy:I

    iput v4, v3, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    .line 920
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->magneticField:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_8

    .line 921
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MAGNETIC_FIELD     : nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->magneticField:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 924
    :cond_8
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagneticFieldTimeStamp:J

    .line 925
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->magneticField:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 928
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->lastAccelerometerData:[F

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->lastAccelerometerData:[F

    array-length v4, v4

    const/4 v6, 0x2

    if-le v4, v6, :cond_0

    .line 929
    const/16 v4, 0x9

    new-array v1, v4, [F

    .line 930
    .local v1, "R":[F
    const/16 v4, 0x9

    new-array v0, v4, [F

    .line 931
    .local v0, "I":[F
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->lastAccelerometerData:[F

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-static {v1, v0, v4, v6}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 932
    new-instance v2, Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;

    invoke-direct {v2, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;-><init>(Landroid/hardware/SensorEvent;)V

    .line 933
    .local v2, "orientationData":Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;
    iget-object v4, v2, Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;->values:[F

    invoke-static {v1, v4}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 934
    iget-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastOrientationTimeStamp:J

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-lez v4, :cond_9

    .line 935
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastOrientationTimeStamp:J

    sub-long/2addr v6, v8

    long-to-float v4, v6

    const v6, 0x49742400    # 1000000.0f

    div-float/2addr v4, v6

    iput v4, v2, Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;->dt:F

    .line 937
    :cond_9
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastOrientationTimeStamp:J

    .line 938
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->orientation:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 939
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->orientation:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_0

    .line 940
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ORIENTATION        : nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->orientation:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 946
    .end local v0    # "I":[F
    .end local v1    # "R":[F
    .end local v2    # "orientationData":Lcom/navdy/client/app/service/DataCollectionService$Data$OrientationData;
    :cond_a
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_c

    .line 947
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGyroscopeTimeStamp:J

    sub-long/2addr v6, v8

    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    .line 948
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gyroscope:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_b

    .line 949
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GYROSCOPE          : nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->gyroscope:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 952
    :cond_b
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGyroscopeTimeStamp:J

    .line 953
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->gyroscope:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 955
    :cond_c
    iget-object v4, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    if-ne v4, v6, :cond_0

    .line 956
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-wide v8, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastRotationVectorTimeStamp:J

    sub-long/2addr v6, v8

    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    .line 957
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->rotationVector:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    rem-int/lit16 v4, v4, 0x3e8

    if-nez v4, :cond_d

    .line 958
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ROTATION_VECTOR    : nbRecords="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v7, v7, Lcom/navdy/client/app/service/DataCollectionService$Data;->rotationVector:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " \tdata="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 961
    :cond_d
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iput-wide v6, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastRotationVectorTimeStamp:J

    .line 962
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->hasW:Z

    .line 963
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->rotationVector:Ljava/util/LinkedList;

    invoke-virtual {v4, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method private handleTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 4
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 1022
    if-nez p1, :cond_0

    .line 1057
    :goto_0
    return-void

    .line 1026
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    if-nez v0, :cond_1

    .line 1027
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->resetTheData()V

    .line 1030
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-boolean v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->isPersistedToFile:Z

    if-eqz v0, :cond_2

    .line 1031
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-virtual {v1}, Lcom/navdy/client/app/service/DataCollectionService$Data;->getLastTripDistance()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 1033
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->removeAnyTemporaryFiles()V

    .line 1034
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->isPersistedToFile:Z

    .line 1042
    :cond_2
    :goto_1
    new-instance v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    invoke-direct {v0}, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .line 1044
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v0, :cond_4

    .line 1045
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->accuracy:Ljava/lang/Float;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->accuracy:Ljava/lang/Float;

    .line 1047
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->altitude:Ljava/lang/Double;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->altitude:Ljava/lang/Double;

    .line 1048
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->bearing:Ljava/lang/Float;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->heading:Ljava/lang/Float;

    .line 1049
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->latitude:Ljava/lang/Double;

    .line 1050
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->longitude:Ljava/lang/Double;

    .line 1051
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->speed:Ljava/lang/Float;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->speed:Ljava/lang/Float;

    .line 1052
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v1, v1, Lcom/navdy/service/library/events/location/Coordinate;->timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->timestamp:Ljava/lang/Long;

    .line 1056
    :goto_2
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    iget-object v1, p1, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->tripDistance:I

    goto :goto_0

    .line 1037
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->moveAnyTempFileToUploadFolderAndSendIfWiFi()V

    .line 1038
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->resetTheData()V

    goto :goto_1

    .line 1054
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->timestamp:Ljava/lang/Long;

    goto :goto_2
.end method

.method private initDataIfNotAlready()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1125
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    if-nez v1, :cond_0

    .line 1126
    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-direct {v1}, Lcom/navdy/client/app/service/DataCollectionService$Data;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    .line 1129
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-wide v2, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->startTimestamp:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 1130
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->startTimestamp:J

    .line 1132
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->timeZoneOffset:I

    if-nez v1, :cond_2

    .line 1133
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    .line 1134
    .local v0, "tz":Ljava/util/TimeZone;
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    iput v2, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->timeZoneOffset:I

    .line 1136
    .end local v0    # "tz":Ljava/util/TimeZone;
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-wide v2, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 1137
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    .line 1139
    :cond_3
    return-void
.end method

.method private declared-synchronized initGoogleApi(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 637
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->API:Lcom/google/android/gms/common/api/Api;

    .line 638
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 639
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addConnectionCallbacks(Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 640
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    .line 641
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 642
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    monitor-exit p0

    return-void

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static isAboveTheLimitOfFolderSize()Z
    .locals 4

    .prologue
    .line 1145
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->fileSize(Ljava/io/File;)J

    move-result-wide v0

    const-wide/32 v2, 0x3200000

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->availableSdCardSpace()J

    move-result-wide v0

    const-wide/32 v2, 0x500000

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private moveAnyPendingFileToUploadFolder()V
    .locals 9

    .prologue
    .line 1235
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    if-eqz v4, :cond_0

    .line 1236
    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1239
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->ensureFolderExists()V

    .line 1241
    const/4 v2, 0x0

    .line 1242
    .local v2, "pendingFilesExist":Z
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    .line 1243
    .local v0, "files":[Ljava/io/File;
    if-nez v0, :cond_2

    .line 1244
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to list files in rootFolder: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1265
    :cond_1
    :goto_0
    return-void

    .line 1247
    :cond_2
    array-length v5, v0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_5

    aget-object v1, v0, v4

    .line 1248
    .local v1, "from":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1249
    new-instance v3, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/navdy/client/app/service/DataCollectionService;->folder:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1250
    .local v3, "to":Ljava/io/File;
    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Moving "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1251
    invoke-virtual {v1, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1252
    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to move "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " to upload folder: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1247
    .end local v3    # "to":Ljava/io/File;
    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1254
    .restart local v3    # "to":Ljava/io/File;
    :cond_4
    const/4 v2, 0x1

    goto :goto_2

    .line 1259
    .end local v1    # "from":Ljava/io/File;
    .end local v3    # "to":Ljava/io/File;
    :cond_5
    if-eqz v2, :cond_1

    .line 1260
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 1261
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "pending_sensor_data_exist"

    const/4 v6, 0x1

    .line 1262
    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 1263
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method private moveAnyTempFileToUploadFolderAndSendIfWiFi()V
    .locals 0

    .prologue
    .line 1229
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->moveAnyPendingFileToUploadFolder()V

    .line 1230
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->sendDataToS3IfOnWiFi()V

    .line 1231
    return-void
.end method

.method private declared-synchronized removeActivityRecognitionUpdates()V
    .locals 3

    .prologue
    .line 812
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "GoogleApiClient : removing activity updates"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 813
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v0

    .line 815
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    if-eqz v1, :cond_0

    .line 816
    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/location/ActivityRecognitionApi;->removeActivityUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 820
    .end local v0    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    monitor-exit p0

    return-void

    .line 812
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private removeAnyTemporaryFiles()V
    .locals 7

    .prologue
    .line 1269
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 1270
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1273
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->ensureFolderExists()V

    .line 1274
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    if-eqz v2, :cond_1

    .line 1275
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 1276
    .local v1, "files":[Ljava/io/File;
    if-nez v1, :cond_2

    .line 1277
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to list files in rootFolder: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1290
    .end local v1    # "files":[Ljava/io/File;
    :cond_1
    return-void

    .line 1280
    .restart local v1    # "files":[Ljava/io/File;
    :cond_2
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 1281
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1282
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1283
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Removed "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1280
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1285
    :cond_4
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to erase temporary file: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private declared-synchronized requestActivityRecognitionUpdates()V
    .locals 6

    .prologue
    .line 797
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "GoogleApiClient : requesting activity updates"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 798
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 799
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "GoogleApiClient is already connected, requesting the updates"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 800
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v0

    .line 801
    .local v0, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    if-eqz v1, :cond_0

    .line 802
    sget-object v1, Lcom/google/android/gms/location/ActivityRecognition;->ActivityRecognitionApi:Lcom/google/android/gms/location/ActivityRecognitionApi;

    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->googleApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    const-wide/16 v4, 0x3e8

    invoke-interface {v1, v2, v4, v5, v0}, Lcom/google/android/gms/location/ActivityRecognitionApi;->requestActivityUpdates(Lcom/google/android/gms/common/api/GoogleApiClient;JLandroid/app/PendingIntent;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    .end local v0    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 807
    :cond_1
    :try_start_1
    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "The GoogleApiClient is not connected yet. Waiting for it to be connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 797
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private resetTheData()V
    .locals 4

    .prologue
    .line 1107
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1108
    :try_start_0
    new-instance v0, Lcom/navdy/client/app/service/DataCollectionService$Data;

    invoke-direct {v0}, Lcom/navdy/client/app/service/DataCollectionService$Data;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    .line 1110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLocation:Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .line 1112
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagFieldAccuracy:I

    .line 1113
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastLinearAccelerationTimeStamp:J

    .line 1114
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGravityTimeStamp:J

    .line 1115
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagneticFieldTimeStamp:J

    .line 1116
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastGyroscopeTimeStamp:J

    .line 1117
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastRotationVectorTimeStamp:J

    .line 1118
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastOrientationTimeStamp:J

    .line 1120
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->initDataIfNotAlready()V

    .line 1121
    monitor-exit v1

    .line 1122
    return-void

    .line 1121
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private scheduleForUpload()V
    .locals 4

    .prologue
    .line 1197
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$9;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$9;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    sget-wide v2, Lcom/navdy/client/app/service/DataCollectionService;->UPLOAD_DELAY:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1203
    return-void
.end method

.method public static sendDataToS3IfOnWiFi()V
    .locals 3

    .prologue
    .line 1415
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1416
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1417
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->sendToS3IfNotAlreadySending()V

    .line 1421
    :cond_0
    :goto_0
    return-void

    .line 1418
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    .line 1419
    invoke-static {}, Lcom/navdy/client/app/framework/service/UnmeteredNetworkConnectivityJobService;->scheduleNetworkServiceForNougatOrAbove()V

    goto :goto_0
.end method

.method private static sendToS3()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1437
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Sending sensor data to S3"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1439
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    if-eqz v4, :cond_0

    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1440
    :cond_0
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendToS3: Unable to find data rootFolder: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1441
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->sendInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1463
    .local v2, "subFolders":[Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 1445
    .end local v2    # "subFolders":[Ljava/io/File;
    :cond_2
    sget-object v4, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1447
    .restart local v2    # "subFolders":[Ljava/io/File;
    if-eqz v2, :cond_3

    array-length v4, v2

    if-gtz v4, :cond_4

    .line 1448
    :cond_3
    sget-object v3, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-static {v3}, Lcom/navdy/client/app/service/DataCollectionService;->finishSendingToS3(Ljava/io/File;)V

    goto :goto_0

    .line 1452
    :cond_4
    const/4 v0, 0x0

    .line 1453
    .local v0, "calledSend":Z
    array-length v4, v2

    :goto_1
    if-ge v3, v4, :cond_5

    aget-object v1, v2, v3

    .line 1454
    .local v1, "subFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1455
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/navdy/client/app/service/DataCollectionService;->sendToS3(Ljava/io/File;Ljava/lang/String;)V

    .line 1456
    const/4 v0, 0x1

    .line 1460
    .end local v1    # "subFolder":Ljava/io/File;
    :cond_5
    if-nez v0, :cond_1

    .line 1461
    sget-object v3, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-static {v3}, Lcom/navdy/client/app/service/DataCollectionService;->finishSendingToS3(Ljava/io/File;)V

    goto :goto_0

    .line 1453
    .restart local v1    # "subFolder":Ljava/io/File;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static sendToS3(Ljava/io/File;Ljava/lang/String;)V
    .locals 10
    .param p0, "folder"    # Ljava/io/File;
    .param p1, "appVersionCode"    # Ljava/lang/String;

    .prologue
    .line 1476
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 1477
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    array-length v6, v2

    if-gtz v6, :cond_2

    .line 1478
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/service/DataCollectionService;->EraseThisFileAndSendNextFile(Ljava/io/File;)V

    .line 1534
    :cond_1
    :goto_0
    return-void

    .line 1483
    :cond_2
    const/4 v6, 0x0

    aget-object v1, v2, v6

    .line 1485
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_1

    .line 1489
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1490
    :cond_3
    invoke-static {v1}, Lcom/navdy/client/app/service/DataCollectionService;->EraseThisFileAndSendNextFile(Ljava/io/File;)V

    goto :goto_0

    .line 1494
    :cond_4
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v4, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1495
    .local v4, "photoDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1496
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 1498
    .local v5, "yearMonth":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insurance-logs/android/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1500
    .local v3, "key":Ljava/lang/String;
    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Length of the file being uploaded: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " bytes"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1501
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->getInstance()Lcom/navdy/client/app/service/DataCollectionService;

    move-result-object v6

    iget-object v6, v6, Lcom/navdy/client/app/service/DataCollectionService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    const-string v7, "navdy-trip-data"

    invoke-virtual {v6, v7, v3, v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->upload(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    move-result-object v6

    sput-object v6, Lcom/navdy/client/app/service/DataCollectionService;->transferObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    .line 1502
    sget-object v6, Lcom/navdy/client/app/service/DataCollectionService;->transferObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    new-instance v7, Lcom/navdy/client/app/service/DataCollectionService$11;

    invoke-direct {v7, v1, v2}, Lcom/navdy/client/app/service/DataCollectionService$11;-><init>(Ljava/io/File;[Ljava/io/File;)V

    invoke-virtual {v6, v7}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->setTransferListener(Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferListener;)V

    goto/16 :goto_0
.end method

.method public static sendToS3IfNotAlreadySending()V
    .locals 2

    .prologue
    .line 1428
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->sendInProgress:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1429
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Sending is already in progress"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1434
    :goto_0
    return-void

    .line 1433
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->sendToS3()V

    goto :goto_0
.end method

.method private static shouldNotCollectData()Z
    .locals 1

    .prologue
    .line 763
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isBetaUser()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->isAboveTheLimitOfFolderSize()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startCollecting()V
    .locals 5

    .prologue
    const v4, 0x14585

    .line 728
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 729
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->shouldNotCollectData()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    if-eqz v0, :cond_1

    .line 730
    :cond_0
    monitor-exit v1

    .line 760
    :goto_0
    return-void

    .line 732
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    .line 733
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Starting sensor data collection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 736
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->initDataIfNotAlready()V

    .line 738
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->bgHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->locationUpdatesAtRegularInterval:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 740
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    if-eqz v0, :cond_2

    .line 741
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    const v2, 0x28b0a

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 743
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    if-eqz v0, :cond_3

    .line 744
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 746
    :cond_3
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    if-eqz v0, :cond_4

    .line 747
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 749
    :cond_4
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    if-eqz v0, :cond_5

    .line 750
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 752
    :cond_5
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    if-eqz v0, :cond_6

    .line 753
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 755
    :cond_6
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    if-eqz v0, :cond_7

    .line 756
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 759
    :cond_7
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->requestActivityRecognitionUpdates()V

    goto :goto_0

    .line 733
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private stopCollecting()V
    .locals 2

    .prologue
    .line 769
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Stopping sensor data collection"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 772
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->bgHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 775
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 778
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->removeActivityRecognitionUpdates()V

    .line 781
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->writeDataToZipFileAndScheduleForUpload()V

    .line 783
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 784
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    .line 785
    monitor-exit v1

    .line 786
    return-void

    .line 785
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateHudUuid()V
    .locals 4

    .prologue
    .line 706
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->getHudUuidFromAppInstance()Ljava/lang/String;

    move-result-object v0

    .line 707
    .local v0, "hudUuid":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 708
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 709
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "last_hud_uuid"

    .line 710
    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 711
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 713
    .end local v1    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method private writeActivityData(Landroid/util/JsonWriter;)V
    .locals 3
    .param p1, "writer"    # Landroid/util/JsonWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1378
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1379
    const-string v1, "android_drivings"

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1380
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 1381
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;

    .line 1382
    .local v0, "activity":Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;->writeToJson(Landroid/util/JsonWriter;)V

    goto :goto_0

    .line 1384
    .end local v0    # "activity":Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 1386
    :cond_1
    return-void
.end method

.method private writeDataToFileAndStartNewData(J)V
    .locals 3
    .param p1, "tripUuid"    # J

    .prologue
    .line 1087
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1089
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->isSplitData:Z

    .line 1090
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->writeDataToZipFileAndScheduleForUpload()V

    .line 1091
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->moveAnyTempFileToUploadFolderAndSendIfWiFi()V

    .line 1092
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->resetTheData()V

    .line 1094
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->isSplitData:Z

    .line 1096
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->shouldNotCollectData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1098
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->stopCollecting()V

    .line 1101
    :cond_0
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Starting new sensor data collection"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1102
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iput-wide p1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    .line 1103
    monitor-exit v1

    .line 1104
    return-void

    .line 1103
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private writeDataToZipFile()Ljava/lang/String;
    .locals 24
    .annotation build Landroid/support/annotation/CheckResult;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1295
    const/4 v13, 0x0

    .line 1298
    .local v13, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20

    .line 1300
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v19, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/navdy/client/app/service/DataCollectionService$Data;->endTimestamp:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1302
    const/16 v16, 0x0

    .line 1304
    .local v16, "writer":Landroid/util/JsonWriter;
    :try_start_1
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->ensureFolderExists()V

    .line 1307
    new-instance v14, Ljava/text/SimpleDateFormat;

    const-string v19, "yyyy-MM-dd\'-\'HH-mm-ss"

    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v14, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1308
    .local v14, "photoDateFormat":Ljava/text/SimpleDateFormat;
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 1309
    .local v4, "currentTime":Ljava/util/Date;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/service/DataCollectionService;->getHudUuid()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, "-"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v14, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1310
    .local v15, "uniqueName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v21, ".json.gz"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1311
    .local v7, "fileName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v21, Lcom/navdy/client/app/service/DataCollectionService;->rootFolder:Ljava/io/File;

    invoke-virtual/range {v21 .. v21}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1313
    sget-object v19, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Writing sensor data to file: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 1316
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 1317
    .local v8, "fileOut":Ljava/io/FileOutputStream;
    new-instance v9, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v9, v8}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1318
    .local v9, "gzipOutputStream":Ljava/util/zip/GZIPOutputStream;
    new-instance v12, Ljava/io/OutputStreamWriter;

    const-string v19, "UTF-8"

    move-object/from16 v0, v19

    invoke-direct {v12, v9, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1319
    .local v12, "outputStreamWriter":Ljava/io/OutputStreamWriter;
    new-instance v17, Landroid/util/JsonWriter;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1321
    .end local v16    # "writer":Landroid/util/JsonWriter;
    .local v17, "writer":Landroid/util/JsonWriter;
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 1323
    const-string v19, "version"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    const-wide/16 v22, 0x1

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 1324
    const-string v19, "app_version"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->appVersion:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1325
    const-string v19, "device"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->device:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1326
    const-string v19, "os_version"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->osVersion:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1327
    const-string v19, "platform"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->platform:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1328
    const-string v19, "startTimestamp"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->startTimestamp:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/navdy/client/app/service/DataCollectionService;->getTimestampString(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1329
    const-string v19, "endTimestamp"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->endTimestamp:J

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Lcom/navdy/client/app/service/DataCollectionService;->getTimestampString(J)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1330
    const-string v19, "time_zone_offset"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->timeZoneOffset:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 1331
    const-string v19, "trip_uuid"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 1333
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    .line 1334
    .local v5, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v19, "vehicle-year"

    const-string v21, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1335
    .local v18, "yearString":Ljava/lang/String;
    const-string v19, "vehicle-make"

    const-string v21, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1336
    .local v10, "makeString":Ljava/lang/String;
    const-string v19, "vehicle-model"

    const-string v21, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1338
    .local v11, "modelString":Ljava/lang/String;
    const-string v19, "vehicle-year"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1339
    const-string v19, "vehicle-make"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1340
    const-string v19, "vehicle-model"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1341
    const-string v19, "is_split_data"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->isSplitData:Z

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Z)Landroid/util/JsonWriter;

    .line 1343
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/navdy/client/app/service/DataCollectionService;->writeActivityData(Landroid/util/JsonWriter;)V

    .line 1345
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/navdy/client/app/service/DataCollectionService;->writeLocationData(Landroid/util/JsonWriter;)V

    .line 1347
    const-string v19, "android_sensors"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1348
    invoke-virtual/range {v17 .. v17}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 1349
    const-string v19, "gravities"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->gravity:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1350
    const-string v19, "gyroscopes"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->gyroscope:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1351
    const-string v19, "linear_accelerations"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->linearAcceleration:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1352
    const-string v19, "magnetic_fields"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->magneticField:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1353
    const-string v19, "orientations"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->orientation:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1354
    const-string v19, "rotation_vectors"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->rotationVector:Ljava/util/LinkedList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 1355
    invoke-virtual/range {v17 .. v17}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 1357
    invoke-virtual/range {v17 .. v17}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 1359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    move-object/from16 v19, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v19

    iput-boolean v0, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->isPersistedToFile:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1365
    if-eqz v17, :cond_0

    .line 1367
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Landroid/util/JsonWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1373
    :cond_0
    :goto_0
    :try_start_4
    monitor-exit v20

    move-object/from16 v16, v17

    .end local v17    # "writer":Landroid/util/JsonWriter;
    .restart local v16    # "writer":Landroid/util/JsonWriter;
    move-object/from16 v19, v13

    .line 1374
    .end local v4    # "currentTime":Ljava/util/Date;
    .end local v5    # "customerPrefs":Landroid/content/SharedPreferences;
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v8    # "fileOut":Ljava/io/FileOutputStream;
    .end local v9    # "gzipOutputStream":Ljava/util/zip/GZIPOutputStream;
    .end local v10    # "makeString":Ljava/lang/String;
    .end local v11    # "modelString":Ljava/lang/String;
    .end local v12    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    .end local v14    # "photoDateFormat":Ljava/text/SimpleDateFormat;
    .end local v15    # "uniqueName":Ljava/lang/String;
    .end local v18    # "yearString":Ljava/lang/String;
    :goto_1
    return-object v19

    .line 1368
    .end local v16    # "writer":Landroid/util/JsonWriter;
    .restart local v4    # "currentTime":Ljava/util/Date;
    .restart local v5    # "customerPrefs":Landroid/content/SharedPreferences;
    .restart local v7    # "fileName":Ljava/lang/String;
    .restart local v8    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v9    # "gzipOutputStream":Ljava/util/zip/GZIPOutputStream;
    .restart local v10    # "makeString":Ljava/lang/String;
    .restart local v11    # "modelString":Ljava/lang/String;
    .restart local v12    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    .restart local v14    # "photoDateFormat":Ljava/text/SimpleDateFormat;
    .restart local v15    # "uniqueName":Ljava/lang/String;
    .restart local v17    # "writer":Landroid/util/JsonWriter;
    .restart local v18    # "yearString":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 1369
    .local v6, "e":Ljava/io/IOException;
    sget-object v19, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "Unable to close the file writer for sensor data"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1373
    .end local v4    # "currentTime":Ljava/util/Date;
    .end local v5    # "customerPrefs":Landroid/content/SharedPreferences;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v8    # "fileOut":Ljava/io/FileOutputStream;
    .end local v9    # "gzipOutputStream":Ljava/util/zip/GZIPOutputStream;
    .end local v10    # "makeString":Ljava/lang/String;
    .end local v11    # "modelString":Ljava/lang/String;
    .end local v12    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    .end local v14    # "photoDateFormat":Ljava/text/SimpleDateFormat;
    .end local v15    # "uniqueName":Ljava/lang/String;
    .end local v17    # "writer":Landroid/util/JsonWriter;
    .end local v18    # "yearString":Ljava/lang/String;
    :catchall_0
    move-exception v19

    monitor-exit v20
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v19

    .line 1360
    .restart local v16    # "writer":Landroid/util/JsonWriter;
    :catch_1
    move-exception v6

    .line 1361
    .restart local v6    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    sget-object v19, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v21, "Unable to write sensor data to file. Erasing it."

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1362
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v13}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1363
    const/16 v19, 0x0

    .line 1365
    if-eqz v16, :cond_1

    .line 1367
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Landroid/util/JsonWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1370
    :cond_1
    :goto_3
    :try_start_7
    monitor-exit v20

    goto :goto_1

    .line 1368
    :catch_2
    move-exception v6

    .line 1369
    sget-object v21, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Unable to close the file writer for sensor data"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 1365
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v19

    :goto_4
    if-eqz v16, :cond_2

    .line 1367
    :try_start_8
    invoke-virtual/range {v16 .. v16}, Landroid/util/JsonWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1370
    :cond_2
    :goto_5
    :try_start_9
    throw v19

    .line 1368
    :catch_3
    move-exception v6

    .line 1369
    .restart local v6    # "e":Ljava/io/IOException;
    sget-object v21, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v22, "Unable to close the file writer for sensor data"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 1365
    .end local v6    # "e":Ljava/io/IOException;
    .end local v16    # "writer":Landroid/util/JsonWriter;
    .restart local v4    # "currentTime":Ljava/util/Date;
    .restart local v7    # "fileName":Ljava/lang/String;
    .restart local v8    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v9    # "gzipOutputStream":Ljava/util/zip/GZIPOutputStream;
    .restart local v12    # "outputStreamWriter":Ljava/io/OutputStreamWriter;
    .restart local v14    # "photoDateFormat":Ljava/text/SimpleDateFormat;
    .restart local v15    # "uniqueName":Ljava/lang/String;
    .restart local v17    # "writer":Landroid/util/JsonWriter;
    :catchall_2
    move-exception v19

    move-object/from16 v16, v17

    .end local v17    # "writer":Landroid/util/JsonWriter;
    .restart local v16    # "writer":Landroid/util/JsonWriter;
    goto :goto_4

    .line 1360
    .end local v16    # "writer":Landroid/util/JsonWriter;
    .restart local v17    # "writer":Landroid/util/JsonWriter;
    :catch_4
    move-exception v6

    move-object/from16 v16, v17

    .end local v17    # "writer":Landroid/util/JsonWriter;
    .restart local v16    # "writer":Landroid/util/JsonWriter;
    goto :goto_2
.end method

.method private writeDataToZipFileAndScheduleForUpload()V
    .locals 6

    .prologue
    .line 1177
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->writeDataToZipFile()Ljava/lang/String;

    move-result-object v1

    .line 1179
    .local v1, "path":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1194
    :cond_0
    :goto_0
    return-void

    .line 1184
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1185
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3

    .line 1186
    :cond_2
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable find output file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doesn\'t exist or is 0 length"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1187
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1188
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to erase data file: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 1193
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->scheduleForUpload()V

    goto :goto_0
.end method

.method private writeLocationData(Landroid/util/JsonWriter;)V
    .locals 3
    .param p1, "writer"    # Landroid/util/JsonWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1389
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->locations:Ljava/util/LinkedList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->locations:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1390
    const-string v1, "locations"

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1391
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 1392
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->locations:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .line 1393
    .local v0, "location":Lcom/navdy/client/app/service/DataCollectionService$Data$Location;
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$Location;->writeToJson(Landroid/util/JsonWriter;)V

    goto :goto_0

    .line 1395
    .end local v0    # "location":Lcom/navdy/client/app/service/DataCollectionService$Data$Location;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 1397
    :cond_1
    return-void
.end method

.method private writeThisSensorData(Landroid/util/JsonWriter;Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 3
    .param p1, "writer"    # Landroid/util/JsonWriter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "sensorName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/LinkedList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "Ljava/lang/String;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1402
    .local p3, "sensorData":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;>;"
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1403
    invoke-virtual {p1, p2}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 1404
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 1405
    invoke-virtual {p3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;

    .line 1406
    .local v0, "sd":Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;
    invoke-virtual {v0, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->writeToJson(Landroid/util/JsonWriter;)V

    goto :goto_0

    .line 1408
    .end local v0    # "sd":Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;
    :cond_0
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    .line 1410
    :cond_1
    return-void
.end method


# virtual methods
.method public cancelAnyOngoingUpload()V
    .locals 2

    .prologue
    .line 1554
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->transferObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    if-eqz v0, :cond_0

    .line 1555
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->transferUtility:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;

    sget-object v1, Lcom/navdy/client/app/service/DataCollectionService;->transferObserver:Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;

    invoke-virtual {v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferObserver;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amazonaws/mobileconnectors/s3/transferutility/TransferUtility;->cancel(I)Z

    .line 1557
    :cond_0
    return-void
.end method

.method handleDetectedActivities(ZI)V
    .locals 3
    .param p1, "isDriving"    # Z
    .param p2, "confidence"    # I

    .prologue
    .line 842
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/navdy/client/app/service/DataCollectionService$4;-><init>(Lcom/navdy/client/app/service/DataCollectionService;IZ)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 863
    return-void
.end method

.method public handleLowMemory()V
    .locals 3

    .prologue
    .line 1209
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$10;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$10;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1226
    return-void
.end method

.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 1
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 877
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    if-ne p1, v0, :cond_0

    .line 878
    iput p2, p0, Lcom/navdy/client/app/service/DataCollectionService;->lastMagFieldAccuracy:I

    .line 880
    :cond_0
    return-void
.end method

.method public declared-synchronized onConnected(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 824
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "GoogleApiClient : connected "

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 825
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is already connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 827
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->requestActivityRecognitionUpdates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 829
    :cond_0
    monitor-exit p0

    return-void

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 838
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient : onConnectionFailed , Connection result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 839
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 3
    .param p1, "i"    # I

    .prologue
    .line 833
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient : onConnectionSuspended , Connection :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 834
    return-void
.end method

.method public onDeviceConnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 3
    .param p1, "deviceConnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 651
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDeviceConnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 652
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$1;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 662
    return-void
.end method

.method public onDeviceDisconnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 3
    .param p1, "deviceDisconnectedEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 676
    sget-object v0, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDeviceDisconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 677
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$3;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 686
    return-void
.end method

.method public onDeviceInfoEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;)V
    .locals 3
    .param p1, "deviceInfoEvent"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 666
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/service/DataCollectionService$2;-><init>(Lcom/navdy/client/app/service/DataCollectionService;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 672
    return-void
.end method

.method public onNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 0
    .param p1, "event"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 690
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 884
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$5;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/service/DataCollectionService$5;-><init>(Lcom/navdy/client/app/service/DataCollectionService;Landroid/hardware/SensorEvent;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 890
    return-void
.end method

.method public onTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 3
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 1007
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService;->dataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1008
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService;->isCollecting:Z

    if-nez v0, :cond_0

    .line 1010
    monitor-exit v1

    .line 1019
    :goto_0
    return-void

    .line 1012
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1013
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/service/DataCollectionService$8;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/service/DataCollectionService$8;-><init>(Lcom/navdy/client/app/service/DataCollectionService;Lcom/navdy/service/library/events/TripUpdate;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0

    .line 1012
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public startService(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 582
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->ensureFolderExists()V

    .line 586
    invoke-direct {p0}, Lcom/navdy/client/app/service/DataCollectionService;->moveAnyTempFileToUploadFolderAndSendIfWiFi()V

    .line 588
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->shouldNotCollectData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 634
    :goto_0
    return-void

    .line 592
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 594
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "BgHandlerThread"

    invoke-direct {v0, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 595
    .local v0, "bgHandlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 596
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->bgHandler:Landroid/os/Handler;

    .line 598
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "DataCollectionHandler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 599
    .local v1, "uploadThread":Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 600
    new-instance v2, Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->uploadScheduler:Landroid/os/Handler;

    .line 602
    const-string v2, "sensor"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    .line 603
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    if-nez v2, :cond_1

    .line 604
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Unable to get sensor manager!!!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 608
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    .line 609
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->accelerometer:Landroid/hardware/Sensor;

    if-nez v2, :cond_2

    .line 610
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have an accelerometer :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 612
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    .line 613
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->linearAcceleration:Landroid/hardware/Sensor;

    if-nez v2, :cond_3

    .line 614
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have a linearAcceleration :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 616
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    .line 617
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gravity:Landroid/hardware/Sensor;

    if-nez v2, :cond_4

    .line 618
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have a gravity :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 620
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    .line 621
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->magneticField:Landroid/hardware/Sensor;

    if-nez v2, :cond_5

    .line 622
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have a magneticField :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 624
    :cond_5
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    .line 625
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->gyroscope:Landroid/hardware/Sensor;

    if-nez v2, :cond_6

    .line 626
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have a gyroscope :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 628
    :cond_6
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->sensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    .line 629
    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService;->rotationVector:Landroid/hardware/Sensor;

    if-nez v2, :cond_7

    .line 630
    sget-object v2, Lcom/navdy/client/app/service/DataCollectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The phone doesn\'t seem to have a rotationVector :\'("

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 633
    :cond_7
    invoke-direct {p0, p1}, Lcom/navdy/client/app/service/DataCollectionService;->initGoogleApi(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
