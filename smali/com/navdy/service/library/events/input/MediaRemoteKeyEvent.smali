.class public final Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;
.super Lcom/squareup/wire/Message;
.source "MediaRemoteKeyEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTION:Lcom/navdy/service/library/events/input/KeyEvent;

.field public static final DEFAULT_KEY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/navdy/service/library/events/input/KeyEvent;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final key:Lcom/navdy/service/library/events/input/MediaRemoteKey;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKey;->MEDIA_REMOTE_KEY_SIRI:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->DEFAULT_KEY:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 15
    sget-object v0, Lcom/navdy/service/library/events/input/KeyEvent;->KEY_DOWN:Lcom/navdy/service/library/events/input/KeyEvent;

    sput-object v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->DEFAULT_ACTION:Lcom/navdy/service/library/events/input/KeyEvent;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V
    .locals 0
    .param p1, "key"    # Lcom/navdy/service/library/events/input/MediaRemoteKey;
    .param p2, "action"    # Lcom/navdy/service/library/events/input/KeyEvent;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    .line 25
    iput-object p2, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    .line 26
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;

    .prologue
    .line 29
    iget-object v0, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    iget-object v1, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKey;Lcom/navdy/service/library/events/input/KeyEvent;)V

    .line 30
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;-><init>(Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    if-ne p1, p0, :cond_1

    .line 39
    :cond_0
    :goto_0
    return v1

    .line 36
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 37
    check-cast v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;

    .line 38
    .local v0, "o":Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    iget-object v4, v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    iget-object v4, v0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    .line 39
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 44
    iget v0, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->hashCode:I

    .line 45
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 46
    iget-object v2, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->key:Lcom/navdy/service/library/events/input/MediaRemoteKey;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/input/MediaRemoteKey;->hashCode()I

    move-result v0

    .line 47
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->action:Lcom/navdy/service/library/events/input/KeyEvent;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/input/KeyEvent;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 48
    iput v0, p0, Lcom/navdy/service/library/events/input/MediaRemoteKeyEvent;->hashCode:I

    .line 50
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 46
    goto :goto_0
.end method
