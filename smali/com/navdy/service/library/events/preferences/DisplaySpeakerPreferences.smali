.class public final Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
.super Lcom/squareup/wire/Message;
.source "DisplaySpeakerPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FEEDBACK_SOUND:Ljava/lang/Boolean;

.field public static final DEFAULT_MASTER_SOUND:Ljava/lang/Boolean;

.field public static final DEFAULT_NOTIFICATION_SOUND:Ljava/lang/Boolean;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_VOLUME_LEVEL:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final feedback_sound:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final master_sound:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final notification_sound:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final volume_level:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 29
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 30
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->DEFAULT_MASTER_SOUND:Ljava/lang/Boolean;

    .line 31
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->DEFAULT_VOLUME_LEVEL:Ljava/lang/Integer;

    .line 32
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->DEFAULT_FEEDBACK_SOUND:Ljava/lang/Boolean;

    .line 33
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->DEFAULT_NOTIFICATION_SOUND:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;

    .prologue
    .line 75
    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->serial_number:Ljava/lang/Long;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->master_sound:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->volume_level:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->feedback_sound:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;->notification_sound:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 76
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;-><init>(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;
    .param p2, "master_sound"    # Ljava/lang/Boolean;
    .param p3, "volume_level"    # Ljava/lang/Integer;
    .param p4, "feedback_sound"    # Ljava/lang/Boolean;
    .param p5, "notification_sound"    # Ljava/lang/Boolean;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    .line 68
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    .line 69
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    .line 70
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    .line 71
    iput-object p5, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    .line 72
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 82
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 83
    check-cast v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 84
    .local v0, "o":Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    .line 85
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    .line 86
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    .line 87
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    .line 88
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 93
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->hashCode:I

    .line 94
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 95
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 96
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->master_sound:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 97
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->volume_level:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 98
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->feedback_sound:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 99
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->notification_sound:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 100
    iput v0, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->hashCode:I

    .line 102
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 95
    goto :goto_0

    :cond_3
    move v2, v1

    .line 96
    goto :goto_1

    :cond_4
    move v2, v1

    .line 97
    goto :goto_2

    :cond_5
    move v2, v1

    .line 98
    goto :goto_3
.end method
