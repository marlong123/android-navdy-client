.class public final Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
.super Lcom/squareup/wire/Message;
.source "PhoneBatteryStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;,
        Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CHARGING:Ljava/lang/Boolean;

.field public static final DEFAULT_LEVEL:Ljava/lang/Integer;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final charging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final level:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->BATTERY_OK:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->DEFAULT_STATUS:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 18
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->DEFAULT_LEVEL:Ljava/lang/Integer;

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->DEFAULT_CHARGING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;
    .param p2, "level"    # Ljava/lang/Integer;
    .param p3, "charging"    # Ljava/lang/Boolean;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    .line 41
    iput-object p2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    .line 42
    iput-object p3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    .line 43
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;

    .prologue
    .line 46
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->level:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;->charging:Ljava/lang/Boolean;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    .line 55
    .local v0, "o":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    .line 56
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    .line 57
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->hashCode:I

    .line 63
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 64
    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->status:Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus$BatteryStatus;->hashCode()I

    move-result v0

    .line 65
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->level:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 66
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->charging:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 67
    iput v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;->hashCode:I

    .line 69
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 64
    goto :goto_0

    :cond_3
    move v2, v1

    .line 65
    goto :goto_1
.end method
