.class public final Lcom/navdy/service/library/events/ui/ShowScreen;
.super Lcom/squareup/wire/Message;
.source "ShowScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ARGUMENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SCREEN:Lcom/navdy/service/library/events/ui/Screen;

.field private static final serialVersionUID:J


# instance fields
.field public final arguments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/glances/KeyValue;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;"
        }
    .end annotation
.end field

.field public final screen:Lcom/navdy/service/library/events/ui/Screen;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_DASHBOARD:Lcom/navdy/service/library/events/ui/Screen;

    sput-object v0, Lcom/navdy/service/library/events/ui/ShowScreen;->DEFAULT_SCREEN:Lcom/navdy/service/library/events/ui/Screen;

    .line 19
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/ui/ShowScreen;->DEFAULT_ARGUMENTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/ui/Screen;Ljava/util/List;)V
    .locals 1
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/ui/Screen;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/glances/KeyValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    .local p2, "arguments":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/glances/KeyValue;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 29
    invoke-static {p2}, Lcom/navdy/service/library/events/ui/ShowScreen;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    .line 30
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/ui/ShowScreen$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    .prologue
    .line 33
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v1, p1, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->arguments:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/ui/ShowScreen;-><init>(Lcom/navdy/service/library/events/ui/Screen;Ljava/util/List;)V

    .line 34
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/ui/ShowScreen;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/ui/ShowScreen$Builder;Lcom/navdy/service/library/events/ui/ShowScreen$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/ui/ShowScreen$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/ui/ShowScreen$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/ui/ShowScreen;-><init>(Lcom/navdy/service/library/events/ui/ShowScreen$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 15
    invoke-static {p0}, Lcom/navdy/service/library/events/ui/ShowScreen;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    if-ne p1, p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v1

    .line 40
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/ui/ShowScreen;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 41
    check-cast v0, Lcom/navdy/service/library/events/ui/ShowScreen;

    .line 42
    .local v0, "o":Lcom/navdy/service/library/events/ui/ShowScreen;
    iget-object v3, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iget-object v4, v0, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    .line 43
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/ui/ShowScreen;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 48
    iget v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->hashCode:I

    .line 49
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 50
    iget-object v1, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/ui/Screen;->hashCode()I

    move-result v0

    .line 51
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->arguments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v2, v1

    .line 52
    iput v0, p0, Lcom/navdy/service/library/events/ui/ShowScreen;->hashCode:I

    .line 54
    :cond_0
    return v0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 51
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method
