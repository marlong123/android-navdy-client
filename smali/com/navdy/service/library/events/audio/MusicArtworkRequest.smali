.class public final Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
.super Lcom/squareup/wire/Message;
.source "MusicArtworkRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALBUM:Ljava/lang/String; = ""

.field public static final DEFAULT_AUTHOR:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SIZE:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final album:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final author:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final size:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 19
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->DEFAULT_SIZE:Ljava/lang/Integer;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;

    .prologue
    .line 64
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->album:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->author:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->size:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v7, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;->collectionId:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;Lcom/navdy/service/library/events/audio/MusicArtworkRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicArtworkRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;-><init>(Lcom/navdy/service/library/events/audio/MusicArtworkRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;)V
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "album"    # Ljava/lang/String;
    .param p4, "author"    # Ljava/lang/String;
    .param p5, "size"    # Ljava/lang/Integer;
    .param p6, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .param p7, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 55
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    .line 59
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 60
    iput-object p7, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 79
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 72
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;

    .line 73
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicArtworkRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    .line 77
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 84
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->hashCode:I

    .line 85
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 86
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v0

    .line 87
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 88
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->album:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 89
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->author:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->size:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->collectionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 93
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkRequest;->hashCode:I

    .line 95
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 86
    goto :goto_0

    :cond_3
    move v2, v1

    .line 87
    goto :goto_1

    :cond_4
    move v2, v1

    .line 88
    goto :goto_2

    :cond_5
    move v2, v1

    .line 89
    goto :goto_3

    :cond_6
    move v2, v1

    .line 90
    goto :goto_4

    :cond_7
    move v2, v1

    .line 91
    goto :goto_5
.end method
