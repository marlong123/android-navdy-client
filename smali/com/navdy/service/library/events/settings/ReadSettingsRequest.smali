.class public final Lcom/navdy/service/library/events/settings/ReadSettingsRequest;
.super Lcom/squareup/wire/Message;
.source "ReadSettingsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_SETTINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final settings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->DEFAULT_SETTINGS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;

    .prologue
    .line 31
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;->settings:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Ljava/util/List;)V

    .line 32
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;Lcom/navdy/service/library/events/settings/ReadSettingsRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/settings/ReadSettingsRequest$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;-><init>(Lcom/navdy/service/library/events/settings/ReadSettingsRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p1, "settings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    .line 28
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 13
    invoke-static {p0}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 37
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 39
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 38
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    check-cast p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->hashCode:I

    .line 45
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/settings/ReadSettingsRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
