.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
.super Lcom/squareup/wire/Message;
.source "NavigationSessionRouteChange.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;,
        Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_OLDROUTEID:Ljava/lang/String; = ""

.field public static final DEFAULT_REASON:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

.field private static final serialVersionUID:J


# instance fields
.field public final newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
    .end annotation
.end field

.field public final oldRouteId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->NAV_SESSION_OFF_REROUTE:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->DEFAULT_REASON:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;

    .prologue
    .line 43
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->oldRouteId:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;)V
    .locals 0
    .param p1, "oldRouteId"    # Ljava/lang/String;
    .param p2, "newRoute"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p3, "reason"    # Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 39
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 51
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;

    .line 52
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    .line 54
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->hashCode:I

    .line 60
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 61
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->oldRouteId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 62
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->newRoute:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 63
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->reason:Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange$RerouteReason;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 64
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionRouteChange;->hashCode:I

    .line 66
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 61
    goto :goto_0

    :cond_3
    move v2, v1

    .line 62
    goto :goto_1
.end method
