.class public final Lcom/navdy/service/library/events/places/SuggestedDestination;
.super Lcom/squareup/wire/Message;
.source "SuggestedDestination.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;,
        Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DURATION_TRAFFIC:Ljava/lang/Integer;

.field public static final DEFAULT_TYPE:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

.field private static final serialVersionUID:J


# instance fields
.field public final destination:Lcom/navdy/service/library/events/destination/Destination;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
    .end annotation
.end field

.field public final duration_traffic:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->DEFAULT_DURATION_TRAFFIC:Ljava/lang/Integer;

    .line 18
    sget-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_RECOMMENDATION:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    sput-object v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->DEFAULT_TYPE:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;
    .param p2, "duration_traffic"    # Ljava/lang/Integer;
    .param p3, "type"    # Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 37
    iput-object p2, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    .line 38
    iput-object p3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .line 39
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;

    .prologue
    .line 42
    iget-object v0, p1, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->duration_traffic:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/SuggestedDestination;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;Lcom/navdy/service/library/events/places/SuggestedDestination$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/SuggestedDestination$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/places/SuggestedDestination$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/SuggestedDestination;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/navdy/service/library/events/places/SuggestedDestination;

    .line 51
    .local v0, "o":Lcom/navdy/service/library/events/places/SuggestedDestination;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    .line 52
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/SuggestedDestination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    iget v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->hashCode:I

    .line 59
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 60
    iget-object v2, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/destination/Destination;->hashCode()I

    move-result v0

    .line 61
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->duration_traffic:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 62
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->type:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 63
    iput v0, p0, Lcom/navdy/service/library/events/places/SuggestedDestination;->hashCode:I

    .line 65
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    :cond_3
    move v2, v1

    .line 61
    goto :goto_1
.end method
