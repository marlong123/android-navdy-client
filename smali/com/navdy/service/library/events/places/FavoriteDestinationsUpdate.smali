.class public final Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;
.super Lcom/squareup/wire/Message;
.source "FavoriteDestinationsUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_DESTINATIONS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final destinations:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/destination/Destination;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 27
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 28
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->DEFAULT_DESTINATIONS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "statusDetail"    # Ljava/lang/String;
    .param p3, "serial_number"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p4, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 58
    iput-object p2, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    .line 60
    invoke-static {p4}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    .line 61
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;

    .prologue
    .line 64
    iget-object v0, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->statusDetail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->serial_number:Ljava/lang/Long;

    iget-object v3, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->destinations:Ljava/util/List;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$1;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 22
    invoke-static {p0}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 72
    check-cast v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .line 73
    .local v0, "o":Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    iget v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->hashCode:I

    .line 82
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 83
    iget-object v2, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 84
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 85
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 86
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v2, v1

    .line 87
    iput v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->hashCode:I

    .line 89
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 83
    goto :goto_0

    :cond_3
    move v2, v1

    .line 84
    goto :goto_1

    .line 86
    :cond_4
    const/4 v1, 0x1

    goto :goto_2
.end method
