.class public Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;
.super Ljava/lang/Object;
.source "PushRegistrationRequestWrapper.java"


# instance fields
.field private pushRegistrationRequest:Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "push_notification_device"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setPushRegistrationRequest(Lcom/zendesk/sdk/model/push/PushRegistrationRequest;)V
    .locals 0
    .param p1, "pushRegistrationRequest"    # Lcom/zendesk/sdk/model/push/PushRegistrationRequest;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;->pushRegistrationRequest:Lcom/zendesk/sdk/model/push/PushRegistrationRequest;

    .line 21
    return-void
.end method
