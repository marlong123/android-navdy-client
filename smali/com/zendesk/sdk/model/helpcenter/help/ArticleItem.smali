.class public Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;
.super Ljava/lang/Object;
.source "ArticleItem.java"

# interfaces
.implements Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;


# instance fields
.field private id:Ljava/lang/Long;

.field private name:Ljava/lang/String;

.field private sectionId:Ljava/lang/Long;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "section_id"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p0, p1, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 60
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;

    .line 62
    .local v0, "that":Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    iget-object v4, v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    if-nez v3, :cond_4

    .line 63
    :cond_6
    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    iget-object v2, v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_7
    iget-object v3, v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getChildren()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getParentId()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    return-object v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x3

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v2, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->id:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 70
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->sectionId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 71
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 69
    goto :goto_0
.end method
