.class Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;
.super Ljava/lang/Object;
.source "ZendeskPicassoTransformationFactory.java"

# interfaces
.implements Lcom/squareup/picasso/Transformation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResizeTransformWidth"
.end annotation


# instance fields
.field private final maxWidth:I

.field final synthetic this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;I)V
    .locals 0
    .param p2, "maxWidth"    # I

    .prologue
    .line 41
    iput-object p1, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;->this$0:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p2, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;->maxWidth:I

    .line 43
    return-void
.end method


# virtual methods
.method public key()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "max-width-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;->maxWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 50
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget v6, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;->maxWidth:I

    if-le v5, v6, :cond_1

    .line 51
    iget v4, p0, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory$ResizeTransformWidth;->maxWidth:I

    .line 52
    .local v4, "targetWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v8, v5

    div-double v0, v6, v8

    .line 53
    .local v0, "aspectRatio":D
    int-to-double v6, v4

    mul-double/2addr v6, v0

    double-to-int v3, v6

    .line 59
    .end local v0    # "aspectRatio":D
    .local v3, "targetHeight":I
    :goto_0
    const/4 v5, 0x0

    invoke-static {p1, v4, v3, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 61
    .local v2, "result":Landroid/graphics/Bitmap;
    if-eq v2, p1, :cond_0

    .line 62
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 65
    :cond_0
    return-object v2

    .line 55
    .end local v2    # "result":Landroid/graphics/Bitmap;
    .end local v3    # "targetHeight":I
    .end local v4    # "targetWidth":I
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 56
    .restart local v3    # "targetHeight":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .restart local v4    # "targetWidth":I
    goto :goto_0
.end method
