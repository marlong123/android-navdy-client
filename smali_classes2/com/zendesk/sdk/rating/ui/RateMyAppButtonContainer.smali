.class public Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;
.super Landroid/widget/LinearLayout;
.source "RateMyAppButtonContainer.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;,
        Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;
    }
.end annotation


# static fields
.field private static final FALLBACK_DIVIDER_HEIGHT_DIP:I = 0x1

.field private static final FALLBACK_TOP_PADDING_DIP:I = 0x10

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final TEXTVIEW_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field private final mButtons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppButton;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mDismissableListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

.field private mRateMyAppSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const-class v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->LOG_TAG:Ljava/lang/String;

    .line 34
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->TEXTVIEW_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    .line 38
    sget-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->TEXTVIEW_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/rating/RateMyAppButton;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p2, "buttons":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/rating/RateMyAppButton;>;"
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 88
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->LOG_TAG:Ljava/lang/String;

    const-string v1, "No rate my app buttons supplied, dialog will be empty"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    :cond_1
    invoke-static {p2}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mButtons:Ljava/util/List;

    .line 93
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mContext:Landroid/content/Context;

    .line 95
    invoke-direct {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->initialise()V

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mDismissableListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mRateMyAppSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    return-object v0
.end method

.method private addActionButton(IIILcom/zendesk/sdk/rating/RateMyAppButton;)Z
    .locals 5
    .param p1, "horizontalPadding"    # I
    .param p2, "topPadding"    # I
    .param p3, "bottomPadding"    # I
    .param p4, "button"    # Lcom/zendesk/sdk/rating/RateMyAppButton;

    .prologue
    const/4 v1, 0x0

    .line 167
    if-eqz p4, :cond_0

    invoke-interface {p4}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p4}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v2

    if-nez v2, :cond_1

    .line 168
    :cond_0
    sget-object v2, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Can\'t add a null button or a button with no label or listener"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    :goto_0
    return v1

    .line 172
    :cond_1
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p4}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getStyleAttributeId()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 174
    .local v0, "tv":Landroid/widget/TextView;
    sget-object v1, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->TEXTVIEW_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    invoke-interface {p4}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {v0, p1, p2, p1, p3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 177
    invoke-interface {p4}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 179
    new-instance v1, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;

    invoke-direct {v1, p0, p4, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;-><init>(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;Lcom/zendesk/sdk/rating/RateMyAppButton;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addView(Landroid/view/View;)V

    .line 198
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private addDividerView(I)V
    .locals 5
    .param p1, "dividerHeight"    # I

    .prologue
    .line 202
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    sget v4, Lcom/zendesk/sdk/R$attr;->RateMyAppDividerStyle:I

    invoke-direct {v0, v2, v3, v4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 204
    .local v0, "divider":Landroid/view/View;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 205
    .local v1, "dividerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addView(Landroid/view/View;)V

    .line 208
    return-void
.end method

.method private initialise()V
    .locals 13

    .prologue
    const/4 v9, -0x1

    const/high16 v11, 0x41800000    # 16.0f

    const/4 v12, 0x1

    .line 122
    invoke-virtual {p0, v12}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setOrientation(I)V

    .line 124
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 125
    .local v3, "containerLayoutParams":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/zendesk/sdk/R$dimen;->activity_horizontal_margin:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 129
    .local v5, "horizontalPadding":I
    sget v9, Lcom/zendesk/sdk/R$attr;->RateMyAppPaddingTop:I

    iget-object v10, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mContext:Landroid/content/Context;

    .line 130
    invoke-static {v9, v10, v12, v11}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToPixels(ILandroid/content/Context;IF)I

    move-result v7

    .line 134
    .local v7, "topPadding":I
    sget v9, Lcom/zendesk/sdk/R$attr;->RateMyAppPaddingBottom:I

    iget-object v10, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mContext:Landroid/content/Context;

    .line 135
    invoke-static {v9, v10, v12, v11}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToPixels(ILandroid/content/Context;IF)I

    move-result v1

    .line 139
    .local v1, "bottomPadding":I
    sget v9, Lcom/zendesk/sdk/R$attr;->RateMyAppDividerHeight:I

    iget-object v10, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mContext:Landroid/content/Context;

    const/high16 v11, 0x3f800000    # 1.0f

    .line 140
    invoke-static {v9, v10, v12, v11}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToPixels(ILandroid/content/Context;IF)I

    move-result v4

    .line 145
    .local v4, "dividerHeight":I
    iget-object v9, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mButtons:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    int-to-float v8, v9

    .line 146
    .local v8, "weightSum":F
    invoke-virtual {p0, v8}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->setWeightSum(F)V

    .line 148
    new-instance v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->getContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    sget v11, Lcom/zendesk/sdk/R$attr;->RateMyAppTitleStyle:I

    invoke-direct {v6, v9, v10, v11}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    .local v6, "title":Landroid/widget/TextView;
    sget-object v9, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->TEXTVIEW_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    sget v9, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_title_label:I

    invoke-virtual {v6, v9}, Landroid/widget/TextView;->setText(I)V

    .line 151
    invoke-virtual {v6, v5, v7, v5, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 153
    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addView(Landroid/view/View;)V

    .line 154
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addDividerView(I)V

    .line 156
    iget-object v9, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mButtons:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/rating/RateMyAppButton;

    .line 157
    .local v2, "button":Lcom/zendesk/sdk/rating/RateMyAppButton;
    invoke-direct {p0, v5, v7, v1, v2}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addActionButton(IIILcom/zendesk/sdk/rating/RateMyAppButton;)Z

    move-result v0

    .line 159
    .local v0, "added":Z
    if-eqz v0, :cond_0

    .line 160
    invoke-direct {p0, v4}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addDividerView(I)V

    goto :goto_0

    .line 163
    .end local v0    # "added":Z
    .end local v2    # "button":Lcom/zendesk/sdk/rating/RateMyAppButton;
    :cond_1
    return-void
.end method


# virtual methods
.method public setDismissableListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;)V
    .locals 0
    .param p1, "dismissableListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mDismissableListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

    .line 107
    return-void
.end method

.method public setRateMyAppSelectionListener(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;)V
    .locals 0
    .param p1, "selectionListener"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->mRateMyAppSelectionListener:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    .line 118
    return-void
.end method
