.class Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;
.super Ljava/lang/Object;
.source "RateMyAppButtonContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->addActionButton(IIILcom/zendesk/sdk/rating/RateMyAppButton;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

.field final synthetic val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

.field final synthetic val$tv:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;Lcom/zendesk/sdk/rating/RateMyAppButton;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    iput-object p3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$tv:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$tv:Landroid/widget/TextView;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->access$000(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/RateMyAppButton;->shouldDismissDialog()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->access$000(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$DismissableListener;->dismissDialog()V

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->access$100(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    invoke-interface {v0}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 192
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;->access$100(Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer;)Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$1;->val$button:Lcom/zendesk/sdk/rating/RateMyAppButton;

    invoke-interface {v1}, Lcom/zendesk/sdk/rating/RateMyAppButton;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/rating/ui/RateMyAppButtonContainer$RateMyAppSelectionListener;->selectionMade(I)V

    .line 194
    :cond_2
    return-void
.end method
