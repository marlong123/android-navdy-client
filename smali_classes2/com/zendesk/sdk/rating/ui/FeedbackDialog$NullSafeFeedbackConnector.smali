.class Lcom/zendesk/sdk/rating/ui/FeedbackDialog$NullSafeFeedbackConnector;
.super Ljava/lang/Object;
.source "FeedbackDialog.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/FeedbackConnector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/rating/ui/FeedbackDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NullSafeFeedbackConnector"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 4
    .param p1, "feedback"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 234
    .local p2, "attachments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CreateRequest;>;"
    const-string v0, "Connector is null, cannot send feedback!"

    .line 235
    .local v0, "error":Ljava/lang/String;
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Connector is null, cannot send feedback!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 236
    if-eqz p3, :cond_0

    .line 237
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Connector is null, cannot send feedback!"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 239
    :cond_0
    return-void
.end method
