.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/HelpCenterProvider;


# static fields
.field private static final EMPTY_JSON_BODY:Ljava/lang/String; = "{}"

.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskHelpCenterProvider"


# instance fields
.field private final baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

.field private final helpCenterService:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

.field private final helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;Lcom/zendesk/sdk/storage/HelpCenterSessionCache;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;
    .param p2, "zendeskHelpCenterService"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;
    .param p3, "helpCenterSessionCache"    # Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 74
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->helpCenterService:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .line 75
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->helpCenterService:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->helpCenterSessionCache:Lcom/zendesk/sdk/storage/HelpCenterSessionCache;

    return-object v0
.end method


# virtual methods
.method asFlatArticleList(Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;)Ljava/util/List;
    .locals 13
    .param p1, "articlesListResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/FlatArticle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 641
    if-nez p1, :cond_0

    .line 642
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 679
    :goto_0
    return-object v6

    .line 645
    :cond_0
    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getCategories()Ljava/util/List;

    move-result-object v2

    .line 646
    .local v2, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getSections()Ljava/util/List;

    move-result-object v9

    .line 647
    .local v9, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getArticles()Ljava/util/List;

    move-result-object v1

    .line 649
    .local v1, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 650
    .local v4, "categoryMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 653
    .local v8, "sectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 655
    .local v6, "flatArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/FlatArticle;>;"
    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 656
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 657
    .local v3, "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/Category;->getId()Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v4, v11, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 660
    .end local v3    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    :cond_1
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 661
    .local v7, "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/Section;->getId()Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v8, v11, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 664
    .end local v7    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 665
    .local v0, "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/Article;->getSectionId()Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 666
    .restart local v7    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/Section;->getCategoryId()Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 668
    .restart local v3    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    new-instance v5, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;

    invoke-direct {v5, v3, v7, v0}, Lcom/zendesk/sdk/model/helpcenter/FlatArticle;-><init>(Lcom/zendesk/sdk/model/helpcenter/Category;Lcom/zendesk/sdk/model/helpcenter/Section;Lcom/zendesk/sdk/model/helpcenter/Article;)V

    .line 669
    .local v5, "flatArticle":Lcom/zendesk/sdk/model/helpcenter/FlatArticle;
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 673
    .end local v0    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    .end local v3    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    .end local v5    # "flatArticle":Lcom/zendesk/sdk/model/helpcenter/FlatArticle;
    .end local v7    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    :cond_3
    const-string v10, "ZendeskHelpCenterProvider"

    const-string v11, "There are no articles contained in this account"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v10, v11, v12}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 674
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 677
    :cond_4
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method asSearchArticleList(Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;)Ljava/util/List;
    .locals 19
    .param p1, "result"    # Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 575
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 577
    .local v8, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    if-nez p1, :cond_1

    .line 636
    :cond_0
    return-object v8

    .line 581
    :cond_1
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 582
    .local v10, "sectionIdToSection":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 583
    .local v6, "categoryIdToCategory":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 585
    .local v13, "userIdToUser":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/User;>;"
    invoke-interface/range {p1 .. p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getArticles()Ljava/util/List;

    move-result-object v15

    invoke-static {v15}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 586
    .local v3, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    invoke-interface/range {p1 .. p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getSections()Ljava/util/List;

    move-result-object v15

    invoke-static {v15}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 587
    .local v11, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    invoke-interface/range {p1 .. p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getCategories()Ljava/util/List;

    move-result-object v15

    invoke-static {v15}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 588
    .local v4, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    invoke-interface/range {p1 .. p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesResponse;->getUsers()Ljava/util/List;

    move-result-object v15

    invoke-static {v15}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    .line 590
    .local v14, "users":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/User;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_2
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 591
    .local v9, "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    invoke-virtual {v9}, Lcom/zendesk/sdk/model/helpcenter/Section;->getId()Ljava/lang/Long;

    move-result-object v16

    if-eqz v16, :cond_2

    .line 592
    invoke-virtual {v9}, Lcom/zendesk/sdk/model/helpcenter/Section;->getId()Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v10, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 596
    .end local v9    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_4
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 597
    .local v5, "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/Category;->getId()Ljava/lang/Long;

    move-result-object v16

    if-eqz v16, :cond_4

    .line 598
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/Category;->getId()Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v6, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 602
    .end local v5    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    :cond_5
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_6
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_7

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/zendesk/sdk/model/helpcenter/User;

    .line 603
    .local v12, "user":Lcom/zendesk/sdk/model/helpcenter/User;
    invoke-virtual {v12}, Lcom/zendesk/sdk/model/helpcenter/User;->getId()Ljava/lang/Long;

    move-result-object v16

    if-eqz v16, :cond_6

    .line 604
    invoke-virtual {v12}, Lcom/zendesk/sdk/model/helpcenter/User;->getId()Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v13, v0, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 608
    .end local v12    # "user":Lcom/zendesk/sdk/model/helpcenter/User;
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 610
    .local v2, "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    const/4 v9, 0x0

    .line 611
    .restart local v9    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    const/4 v5, 0x0

    .line 613
    .restart local v5    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/Article;->getSectionId()Ljava/lang/Long;

    move-result-object v15

    if-eqz v15, :cond_8

    .line 614
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/Article;->getSectionId()Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v10, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    check-cast v9, Lcom/zendesk/sdk/model/helpcenter/Section;

    .line 619
    .restart local v9    # "section":Lcom/zendesk/sdk/model/helpcenter/Section;
    :goto_4
    if-eqz v9, :cond_9

    invoke-virtual {v9}, Lcom/zendesk/sdk/model/helpcenter/Section;->getCategoryId()Ljava/lang/Long;

    move-result-object v15

    if-eqz v15, :cond_9

    .line 620
    invoke-virtual {v9}, Lcom/zendesk/sdk/model/helpcenter/Section;->getCategoryId()Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v6, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    check-cast v5, Lcom/zendesk/sdk/model/helpcenter/Category;

    .line 625
    .restart local v5    # "category":Lcom/zendesk/sdk/model/helpcenter/Category;
    :goto_5
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthorId()Ljava/lang/Long;

    move-result-object v15

    if-eqz v15, :cond_a

    .line 626
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/Article;->getAuthorId()Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v13, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/zendesk/sdk/model/helpcenter/User;

    invoke-virtual {v2, v15}, Lcom/zendesk/sdk/model/helpcenter/Article;->setAuthor(Lcom/zendesk/sdk/model/helpcenter/User;)V

    .line 632
    :goto_6
    new-instance v7, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;

    invoke-direct {v7, v2, v9, v5}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;-><init>(Lcom/zendesk/sdk/model/helpcenter/Article;Lcom/zendesk/sdk/model/helpcenter/Section;Lcom/zendesk/sdk/model/helpcenter/Category;)V

    .line 633
    .local v7, "searchArticle":Lcom/zendesk/sdk/model/helpcenter/SearchArticle;
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 616
    .end local v7    # "searchArticle":Lcom/zendesk/sdk/model/helpcenter/SearchArticle;
    :cond_8
    const-string v15, "ZendeskHelpCenterProvider"

    const-string v17, "Unable to determine section as section id was null."

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v15, v0, v1}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 622
    :cond_9
    const-string v15, "ZendeskHelpCenterProvider"

    const-string v17, "Unable to determine category as section was null."

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v15, v0, v1}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 628
    :cond_a
    const-string v15, "ZendeskHelpCenterProvider"

    const-string v17, "Unable to determine author as author id was null."

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v15, v0, v1}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public deleteVote(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "voteId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    :goto_0
    return-void

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public downvoteArticle(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "articleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticleVote;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticleVote;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    :goto_0
    return-void

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$13;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$13;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getArticle(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "articleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Article;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$8;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$8;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getArticles(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "sectionId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Article;>;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$4;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getAttachments(Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/AttachmentType;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "articleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "attachmentType"    # Lcom/zendesk/sdk/model/helpcenter/AttachmentType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/sdk/model/helpcenter/AttachmentType;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Attachment;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 348
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Attachment;>;>;"
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    invoke-virtual {p0, p3, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    :goto_0
    return-void

    .line 352
    :cond_0
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$11;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$11;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;Lcom/zendesk/sdk/model/helpcenter/AttachmentType;)V

    invoke-interface {v6, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method getBestLocale(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Ljava/util/Locale;
    .locals 2
    .param p1, "settings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 508
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getHelpCenterLocale()Ljava/lang/String;

    move-result-object v0

    .line 509
    .local v0, "helpCenterLocale":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    :goto_1
    return-object v1

    .line 508
    .end local v0    # "helpCenterLocale":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 509
    .restart local v0    # "helpCenterLocale":Ljava/lang/String;
    :cond_1
    invoke-static {v0}, Lcom/zendesk/util/LocaleUtil;->forLanguageTag(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v1

    goto :goto_1
.end method

.method public getCategories(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Category;>;>;"
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$2;

    invoke-direct {v1, p0, p1, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getCategory(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "categoryId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Category;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 331
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Category;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 343
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$10;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$10;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getHelp(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "request"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 105
    return-void
.end method

.method public getSection(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "sectionId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 314
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/Section;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$9;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$9;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getSections(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "categoryId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Section;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/Section;>;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$3;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getSuggestedArticles(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "suggestedArticleSearch"    # Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 441
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleResponse;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    :goto_0
    return-void

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public listArticles(Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "query"    # Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    :goto_0
    return-void

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$5;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public listArticlesFlat(Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "query"    # Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/FlatArticle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/FlatArticle;>;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$6;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/ListArticleQuery;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method varargs sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z
    .locals 6
    .param p2, "nonNullArgs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<*>;[",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<*>;"
    const/4 v3, 0x0

    .line 524
    if-eqz p2, :cond_3

    .line 525
    const/4 v1, 0x1

    .line 526
    .local v1, "nonNull":Z
    array-length v5, p2

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v2, p2, v4

    .line 527
    .local v2, "o":Ljava/lang/Object;
    if-nez v2, :cond_0

    .line 528
    const/4 v1, 0x0

    .line 526
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 532
    .end local v2    # "o":Ljava/lang/Object;
    :cond_1
    if-nez v1, :cond_3

    .line 533
    const-string v0, "One or more provided parameters are null."

    .line 534
    .local v0, "message":Ljava/lang/String;
    const-string v4, "ZendeskHelpCenterProvider"

    const-string v5, "One or more provided parameters are null."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v4, v5, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 536
    if-eqz p1, :cond_2

    .line 537
    new-instance v3, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v4, "One or more provided parameters are null."

    invoke-direct {v3, v4}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 540
    :cond_2
    const/4 v3, 0x1

    .line 544
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "nonNull":Z
    :cond_3
    return v3
.end method

.method sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z
    .locals 5
    .param p2, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<*>;",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<*>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 549
    invoke-virtual {p2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->hasHelpCenterSettings()Z

    move-result v3

    if-nez v3, :cond_1

    .line 550
    const-string v0, "Help Center settings are null. Can not continue with the call"

    .line 551
    .local v0, "message":Ljava/lang/String;
    const-string v3, "ZendeskHelpCenterProvider"

    const-string v4, "Help Center settings are null. Can not continue with the call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 553
    if-eqz p1, :cond_0

    .line 554
    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "Help Center settings are null. Can not continue with the call"

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 571
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 560
    :cond_1
    invoke-virtual {p2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isHelpCenterEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 561
    const-string v0, "Help Center is disabled in your app\'s settings. Can not continue with the call"

    .line 562
    .restart local v0    # "message":Ljava/lang/String;
    const-string v3, "ZendeskHelpCenterProvider"

    const-string v4, "Help Center is disabled in your app\'s settings. Can not continue with the call"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v4, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 564
    if-eqz p1, :cond_0

    .line 565
    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "Help Center is disabled in your app\'s settings. Can not continue with the call"

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0

    .end local v0    # "message":Ljava/lang/String;
    :cond_2
    move v1, v2

    .line 571
    goto :goto_0
.end method

.method public searchArticles(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "search"    # Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$7;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public submitRecordArticleView(Ljava/lang/Long;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "articleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 470
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$16;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;Ljava/util/Locale;)V

    invoke-interface {v6, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 499
    return-void
.end method

.method public upvoteArticle(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "articleId"    # Ljava/lang/Long;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/ArticleVote;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 366
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/helpcenter/ArticleVote;>;"
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheck(Lcom/zendesk/service/ZendeskCallback;[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    :goto_0
    return-void

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;

    invoke-direct {v1, p0, p2, p2, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$12;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method
