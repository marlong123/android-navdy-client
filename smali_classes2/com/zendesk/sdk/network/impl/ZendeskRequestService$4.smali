.class Lcom/zendesk/sdk/network/impl/ZendeskRequestService$4;
.super Ljava/lang/Object;
.source "ZendeskRequestService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->addComment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;",
        "Lcom/zendesk/sdk/model/request/Request;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public extract(Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;)Lcom/zendesk/sdk/model/request/Request;
    .locals 1
    .param p1, "updateRequestWrapper"    # Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;

    .prologue
    .line 124
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;->getRequest()Lcom/zendesk/sdk/model/request/Request;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 121
    check-cast p1, Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService$4;->extract(Lcom/zendesk/sdk/model/request/UpdateRequestWrapper;)Lcom/zendesk/sdk/model/request/Request;

    move-result-object v0

    return-object v0
.end method
