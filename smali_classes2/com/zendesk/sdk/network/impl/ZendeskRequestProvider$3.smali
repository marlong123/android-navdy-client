.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->getRequests(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$status:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$status:Ljava/lang/String;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 186
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 189
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->areConversationsEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$status:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAuthenticationType()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->getAllRequestsInternal(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->answerCallbackOnConversationsDisabled(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
