.class final Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider$1;
.super Lcom/squareup/picasso/UrlConnectionDownloader;
.source "ZendeskPicassoProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/picasso/UrlConnectionDownloader;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected openConnection(Landroid/net/Uri;)Ljava/net/HttpURLConnection;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 82
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "uriScheme":Ljava/lang/String;
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "http"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 84
    invoke-static {}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Loading image: %s - http scheme detected, enforcing https"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "https"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 88
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/picasso/UrlConnectionDownloader;->openConnection(Landroid/net/Uri;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 90
    .local v1, "connection":Ljava/net/HttpURLConnection;
    sget-object v3, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 91
    invoke-virtual {v3}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v3

    invoke-interface {v3}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v3

    invoke-interface {v3}, Lcom/zendesk/sdk/storage/IdentityStorage;->getStoredAccessTokenAsBearerToken()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "bearerToken":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 94
    const-string v3, "Authorization"

    invoke-virtual {v1, v3, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_1
    return-object v1
.end method
