.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->deleteVote(Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$voteId:Ljava/lang/Long;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/Long;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->val$voteId:Ljava/lang/Long;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 423
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->val$voteId:Ljava/lang/Long;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14$1;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v3, p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->deleteVote(Ljava/lang/String;Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V

    .line 434
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 420
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$14;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
