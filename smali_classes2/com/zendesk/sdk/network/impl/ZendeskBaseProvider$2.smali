.class Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskBaseProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;->getAccessToken(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/access/AccessToken;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/access/AccessToken;)V
    .locals 1
    .param p1, "accessToken"    # Lcom/zendesk/sdk/model/access/AccessToken;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 124
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 118
    check-cast p1, Lcom/zendesk/sdk/model/access/AccessToken;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskBaseProvider$2;->onSuccess(Lcom/zendesk/sdk/model/access/AccessToken;)V

    return-void
.end method
