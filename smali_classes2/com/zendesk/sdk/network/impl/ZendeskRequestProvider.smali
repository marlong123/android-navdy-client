.class final Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
.super Ljava/lang/Object;
.source "ZendeskRequestProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/RequestProvider;


# static fields
.field private static final ALL_REQUEST_STATUSES:Ljava/lang/String; = "new,open,pending,hold,solved,closed"

.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskRequestProvider"

.field private static final SIDE_LOAD_PUBLIC_UPDATED_AT:Ljava/lang/String; = "public_updated_at"


# instance fields
.field private final baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

.field private final identity:Lcom/zendesk/sdk/model/access/Identity;

.field private final requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

.field private final requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskRequestService;Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/sdk/storage/RequestStorage;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;
    .param p2, "requestService"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestService;
    .param p3, "identity"    # Lcom/zendesk/sdk/model/access/Identity;
    .param p4, "requestStorage"    # Lcom/zendesk/sdk/storage/RequestStorage;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 51
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    .line 52
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    .line 53
    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;)Lcom/zendesk/sdk/storage/RequestStorage;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->getComments(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;)Lcom/zendesk/sdk/network/impl/ZendeskRequestService;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    return-object v0
.end method

.method private getComments(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CommentsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 203
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CommentsResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    invoke-virtual {v0, p1, p2, p3}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->getComments(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 204
    return-void
.end method


# virtual methods
.method public addComment(Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "endUserComment"    # Lcom/zendesk/sdk/model/request/EndUserComment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/EndUserComment;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Comment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Comment;>;"
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$6;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$6;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v6, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 276
    return-void
.end method

.method addCommentInternal(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "endUserComment"    # Lcom/zendesk/sdk/model/request/EndUserComment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/EndUserComment;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Comment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 232
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Comment;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;

    invoke-direct {v1, p0, p4, p4}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$5;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->addComment(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/EndUserComment;Lcom/zendesk/service/ZendeskCallback;)V

    .line 252
    return-void
.end method

.method answerCallbackOnConversationsDisabled(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "callback"    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 327
    const-string v0, "ZendeskRequestProvider"

    const-string v1, "Conversations disabled, this feature is not available on your plan or was disabled."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 328
    if-eqz p1, :cond_0

    .line 329
    new-instance v0, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v1, "Access Denied"

    invoke-direct {v0, v1}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 331
    :cond_0
    return-void
.end method

.method areConversationsEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z
    .locals 1
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 317
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isConversationsEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public createRequest(Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 5
    .param p1, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CreateRequest;>;"
    const/4 v1, 0x1

    .line 68
    .local v1, "isValid":Z
    if-nez p1, :cond_0

    .line 69
    const/4 v1, 0x0

    .line 72
    :cond_0
    if-nez v1, :cond_2

    .line 73
    const-string v0, "configuration is invalid: request null"

    .line 74
    .local v0, "error":Ljava/lang/String;
    const-string v2, "ZendeskRequestProvider"

    const-string v3, "configuration is invalid: request null"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    if-eqz p2, :cond_1

    .line 77
    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "configuration is invalid: request null"

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 88
    .end local v0    # "error":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 82
    :cond_2
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;

    invoke-direct {v3, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v2, v3}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getAllRequests(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .param p1    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->getRequests(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 166
    return-void
.end method

.method getAllRequestsInternal(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 8
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "status"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "authentication"    # Lcom/zendesk/sdk/model/access/AuthenticationType;
    .param p4    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/access/AuthenticationType;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;>;"
    invoke-static {p2}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string p2, "new,open,pending,hold,solved,closed"

    .line 142
    :cond_0
    const-string v6, "public_updated_at"

    .line 143
    .local v6, "include":Ljava/lang/String;
    sget-object v0, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne p3, v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestStorage:Lcom/zendesk/sdk/storage/RequestStorage;

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/RequestStorage;->getStoredRequestIds()Ljava/util/List;

    move-result-object v7

    .line 146
    .local v7, "listOfRequestIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v7}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    const-string v0, "ZendeskRequestProvider"

    const-string v1, "getAllRequestsInternal: There are no requests to fetch"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    if-eqz p4, :cond_1

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p4, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 161
    .end local v7    # "listOfRequestIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    .line 155
    .restart local v7    # "listOfRequestIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    invoke-static {v7}, Lcom/zendesk/util/StringUtils;->toCsvString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "public_updated_at"

    move-object v1, p1

    move-object v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->getAllRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0

    .line 159
    .end local v7    # "listOfRequestIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    const-string v1, "public_updated_at"

    invoke-virtual {v0, p1, p2, v1, p4}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->getAllRequests(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public getComments(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CommentsResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CommentsResponse;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 220
    return-void
.end method

.method public getRequest(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Request;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 306
    return-void
.end method

.method public getRequests(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "status"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/request/Request;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 191
    return-void
.end method

.method internalCreateRequest(Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 8
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "request"    # Lcom/zendesk/sdk/model/request/CreateRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "authentication"    # Lcom/zendesk/sdk/model/access/AuthenticationType;
    .param p4    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            "Lcom/zendesk/sdk/model/access/AuthenticationType;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/request/CreateRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p4, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/CreateRequest;>;"
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/service/ZendeskCallback;)V

    .line 111
    .local v0, "zendeskCallback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/request/Request;>;"
    sget-object v1, Lcom/zendesk/sdk/model/access/AuthenticationType;->ANONYMOUS:Lcom/zendesk/sdk/model/access/AuthenticationType;

    if-ne p3, v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    instance-of v1, v1, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    if-eqz v1, :cond_0

    .line 112
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    check-cast v6, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 113
    .local v6, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getSdkGuid()Ljava/lang/String;

    move-result-object v7

    .line 114
    .local v7, "sdkGuid":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    invoke-virtual {v1, p1, v7, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->createRequest(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V

    .line 120
    .end local v6    # "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    .end local v7    # "sdkGuid":Ljava/lang/String;
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider;->requestService:Lcom/zendesk/sdk/network/impl/ZendeskRequestService;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p2, v0}, Lcom/zendesk/sdk/network/impl/ZendeskRequestService;->createRequest(Ljava/lang/String;Ljava/lang/String;Lcom/zendesk/sdk/model/request/CreateRequest;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method
