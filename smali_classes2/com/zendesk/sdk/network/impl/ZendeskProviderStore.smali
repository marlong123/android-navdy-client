.class Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;
.super Ljava/lang/Object;
.source "ZendeskProviderStore.java"

# interfaces
.implements Lcom/zendesk/sdk/network/impl/ProviderStore;


# instance fields
.field private final helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

.field private final networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

.field private final pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

.field private final requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

.field private final sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

.field private final settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

.field private final uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

.field private final userProvider:Lcom/zendesk/sdk/network/UserProvider;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/UserProvider;Lcom/zendesk/sdk/network/HelpCenterProvider;Lcom/zendesk/sdk/network/PushRegistrationProvider;Lcom/zendesk/sdk/network/RequestProvider;Lcom/zendesk/sdk/network/UploadProvider;Lcom/zendesk/sdk/network/SdkSettingsProvider;Lcom/zendesk/sdk/network/NetworkInfoProvider;Lcom/zendesk/sdk/network/SettingsHelper;)V
    .locals 0
    .param p1, "userProvider"    # Lcom/zendesk/sdk/network/UserProvider;
    .param p2, "helpCenterProvider"    # Lcom/zendesk/sdk/network/HelpCenterProvider;
    .param p3, "pushRegistrationProvider"    # Lcom/zendesk/sdk/network/PushRegistrationProvider;
    .param p4, "requestProvider"    # Lcom/zendesk/sdk/network/RequestProvider;
    .param p5, "uploadProvider"    # Lcom/zendesk/sdk/network/UploadProvider;
    .param p6, "sdkSettingsProvider"    # Lcom/zendesk/sdk/network/SdkSettingsProvider;
    .param p7, "networkInfoProvider"    # Lcom/zendesk/sdk/network/NetworkInfoProvider;
    .param p8, "settingsHelper"    # Lcom/zendesk/sdk/network/SettingsHelper;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->userProvider:Lcom/zendesk/sdk/network/UserProvider;

    .line 31
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    .line 32
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

    .line 33
    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    .line 34
    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    .line 35
    iput-object p6, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    .line 36
    iput-object p7, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    .line 37
    iput-object p8, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    .line 38
    return-void
.end method


# virtual methods
.method public helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->helpCenterProvider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    return-object v0
.end method

.method public networkInfoProvider()Lcom/zendesk/sdk/network/NetworkInfoProvider;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    return-object v0
.end method

.method public pushRegistrationProvider()Lcom/zendesk/sdk/network/PushRegistrationProvider;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->pushRegistrationProvider:Lcom/zendesk/sdk/network/PushRegistrationProvider;

    return-object v0
.end method

.method public requestProvider()Lcom/zendesk/sdk/network/RequestProvider;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->requestProvider:Lcom/zendesk/sdk/network/RequestProvider;

    return-object v0
.end method

.method public sdkSettingsProvider()Lcom/zendesk/sdk/network/SdkSettingsProvider;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->sdkSettingsProvider:Lcom/zendesk/sdk/network/SdkSettingsProvider;

    return-object v0
.end method

.method public uiSettingsHelper()Lcom/zendesk/sdk/network/SettingsHelper;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->settingsHelper:Lcom/zendesk/sdk/network/SettingsHelper;

    return-object v0
.end method

.method public uploadProvider()Lcom/zendesk/sdk/network/UploadProvider;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->uploadProvider:Lcom/zendesk/sdk/network/UploadProvider;

    return-object v0
.end method

.method public userProvider()Lcom/zendesk/sdk/network/UserProvider;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskProviderStore;->userProvider:Lcom/zendesk/sdk/network/UserProvider;

    return-object v0
.end method
