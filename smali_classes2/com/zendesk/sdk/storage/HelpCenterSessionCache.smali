.class public interface abstract Lcom/zendesk/sdk/storage/HelpCenterSessionCache;
.super Ljava/lang/Object;
.source "HelpCenterSessionCache.java"


# virtual methods
.method public abstract getLastSearch()Lcom/zendesk/sdk/model/helpcenter/LastSearch;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract isUniqueSearchResultClick()Z
.end method

.method public abstract setLastSearch(Ljava/lang/String;I)V
.end method

.method public abstract unsetUniqueSearchResultClick()V
.end method
