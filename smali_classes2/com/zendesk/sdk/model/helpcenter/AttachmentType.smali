.class public final enum Lcom/zendesk/sdk/model/helpcenter/AttachmentType;
.super Ljava/lang/Enum;
.source "AttachmentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/AttachmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

.field public static final enum BLOCK:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

.field public static final enum INLINE:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;


# instance fields
.field private attachmentType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    const-string v1, "INLINE"

    const-string v2, "inline"

    invoke-direct {v0, v1, v3, v2}, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->INLINE:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    .line 16
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    const-string v1, "BLOCK"

    const-string v2, "block"

    invoke-direct {v0, v1, v4, v2}, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->BLOCK:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    .line 13
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->INLINE:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->BLOCK:Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "attachmentType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput-object p3, p0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->attachmentType:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/AttachmentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/model/helpcenter/AttachmentType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->$VALUES:[Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/model/helpcenter/AttachmentType;

    return-object v0
.end method


# virtual methods
.method public getAttachmentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/AttachmentType;->attachmentType:Ljava/lang/String;

    return-object v0
.end method
