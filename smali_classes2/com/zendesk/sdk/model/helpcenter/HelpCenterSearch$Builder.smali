.class public Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
.super Ljava/lang/Object;
.source "HelpCenterSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private categoryIds:Ljava/lang/String;

.field private include:[Ljava/lang/String;

.field private labelNames:[Ljava/lang/String;

.field private locale:Ljava/util/Locale;

.field private page:Ljava/lang/Integer;

.field private perPage:Ljava/lang/Integer;

.field private query:Ljava/lang/String;

.field private sectionIds:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    return-void
.end method


# virtual methods
.method public build()Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    .locals 2

    .prologue
    .line 289
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;-><init>(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$1;)V

    .line 291
    .local v0, "helpCenterSearch":Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->query:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$102(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 292
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->locale:Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$202(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/util/Locale;)Ljava/util/Locale;

    .line 293
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->include:[Ljava/lang/String;

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$302(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 294
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->labelNames:[Ljava/lang/String;

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$402(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 295
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->categoryIds:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$502(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 296
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->sectionIds:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$602(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 297
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->page:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$702(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 298
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->perPage:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;->access$802(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 300
    return-object v0
.end method

.method public forLocale(Ljava/util/Locale;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->locale:Ljava/util/Locale;

    .line 185
    return-object p0
.end method

.method public page(Ljava/lang/Integer;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "page"    # Ljava/lang/Integer;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->page:Ljava/lang/Integer;

    .line 268
    return-object p0
.end method

.method public perPage(Ljava/lang/Integer;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "perPage"    # Ljava/lang/Integer;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->perPage:Ljava/lang/Integer;

    .line 279
    return-object p0
.end method

.method public withCategoryId(Ljava/lang/Long;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 2
    .param p1, "categoryId"    # Ljava/lang/Long;

    .prologue
    .line 219
    if-eqz p1, :cond_0

    .line 220
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->categoryIds:Ljava/lang/String;

    .line 222
    :cond_0
    return-object p0
.end method

.method public withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;"
        }
    .end annotation

    .prologue
    .line 232
    .local p1, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvStringNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->categoryIds:Ljava/lang/String;

    .line 233
    return-object p0
.end method

.method public varargs withIncludes([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "includes"    # [Ljava/lang/String;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->include:[Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method public varargs withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "labelNames"    # [Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->labelNames:[Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public withQuery(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->query:Ljava/lang/String;

    .line 173
    return-object p0
.end method

.method public withSectionId(Ljava/lang/Long;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 2
    .param p1, "sectionId"    # Ljava/lang/Long;

    .prologue
    .line 243
    if-eqz p1, :cond_0

    .line 244
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->sectionIds:Ljava/lang/String;

    .line 246
    :cond_0
    return-object p0
.end method

.method public withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "sectionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvStringNumber(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->sectionIds:Ljava/lang/String;

    .line 257
    return-object p0
.end method
