.class public Lcom/zendesk/sdk/model/AppVersion;
.super Ljava/lang/Object;
.source "AppVersion.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mAppVersionCode:I

.field private mAppVersionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/zendesk/sdk/model/AppVersion;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/model/AppVersion;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v2, ""

    iput-object v2, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionName:Ljava/lang/String;

    .line 18
    iput v5, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionCode:I

    .line 29
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 31
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v2, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionName:Ljava/lang/String;

    .line 32
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v2, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/zendesk/sdk/model/AppVersion;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Unable to find the package name"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v0, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getAppVersionCode()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionCode:I

    return v0
.end method

.method public getAppVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/model/AppVersion;->mAppVersionName:Ljava/lang/String;

    return-object v0
.end method
