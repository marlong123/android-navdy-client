.class final enum Lcom/zendesk/sdk/requests/ImageLoader;
.super Ljava/lang/Enum;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/requests/ImageLoader$Result;,
        Lcom/zendesk/sdk/requests/ImageLoader$TaskData;,
        Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/requests/ImageLoader;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/requests/ImageLoader;

.field public static final enum INSTANCE:Lcom/zendesk/sdk/requests/ImageLoader;

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mTarget:Lcom/squareup/picasso/Target;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/zendesk/sdk/requests/ImageLoader;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/requests/ImageLoader;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/requests/ImageLoader;->INSTANCE:Lcom/zendesk/sdk/requests/ImageLoader;

    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/requests/ImageLoader;

    sget-object v1, Lcom/zendesk/sdk/requests/ImageLoader;->INSTANCE:Lcom/zendesk/sdk/requests/ImageLoader;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/requests/ImageLoader;->$VALUES:[Lcom/zendesk/sdk/requests/ImageLoader;

    .line 37
    const-class v0, Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/requests/ImageLoader;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/zendesk/sdk/requests/ImageLoader;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/requests/ImageLoader;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/requests/ImageLoader;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/requests/ImageLoader;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/zendesk/sdk/requests/ImageLoader;->$VALUES:[Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/requests/ImageLoader;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/requests/ImageLoader;

    return-object v0
.end method


# virtual methods
.method declared-synchronized loadAndShowImage(Lcom/zendesk/sdk/model/request/Attachment;Landroid/content/Context;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;)V
    .locals 7
    .param p1, "attachment"    # Lcom/zendesk/sdk/model/request/Attachment;
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "belvedere"    # Lcom/zendesk/belvedere/Belvedere;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/Attachment;",
            "Landroid/content/Context;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/zendesk/belvedere/Belvedere;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Landroid/net/Uri;>;"
    monitor-enter p0

    :try_start_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%s-%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Attachment;->getId()Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Attachment;->getFileName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v2}, Lcom/zendesk/belvedere/Belvedere;->getFileRepresentation(Ljava/lang/String;)Lcom/zendesk/belvedere/BelvedereResult;

    move-result-object v0

    .line 68
    .local v0, "localAttachment":Lcom/zendesk/belvedere/BelvedereResult;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 69
    if-eqz p3, :cond_0

    .line 70
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :goto_0
    monitor-exit p0

    return-void

    .line 75
    :cond_0
    :try_start_1
    invoke-static {p2}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    .line 76
    .local v1, "requestCreator":Lcom/squareup/picasso/RequestCreator;
    new-instance v2, Lcom/zendesk/sdk/requests/ImageLoader$1;

    invoke-direct {v2, p0, p3, p4, p1}, Lcom/zendesk/sdk/requests/ImageLoader$1;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;Lcom/zendesk/sdk/model/request/Attachment;)V

    iput-object v2, p0, Lcom/zendesk/sdk/requests/ImageLoader;->mTarget:Lcom/squareup/picasso/Target;

    .line 94
    iget-object v2, p0, Lcom/zendesk/sdk/requests/ImageLoader;->mTarget:Lcom/squareup/picasso/Target;

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    .end local v0    # "localAttachment":Lcom/zendesk/belvedere/BelvedereResult;
    .end local v1    # "requestCreator":Lcom/squareup/picasso/RequestCreator;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
