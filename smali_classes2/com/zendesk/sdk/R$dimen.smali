.class public final Lcom/zendesk/sdk/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f0b002b

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f0b002c

.field public static final abc_action_bar_default_height_material:I = 0x7f0b0002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f0b002d

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f0b002e

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0b0040

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0b0041

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0b0042

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0b0003

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0b0043

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0b0044

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0b0045

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0b0046

.field public static final abc_action_button_min_height_material:I = 0x7f0b0047

.field public static final abc_action_button_min_width_material:I = 0x7f0b0048

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0b0049

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0b0001

.field public static final abc_button_inset_horizontal_material:I = 0x7f0b004a

.field public static final abc_button_inset_vertical_material:I = 0x7f0b004b

.field public static final abc_button_padding_horizontal_material:I = 0x7f0b004c

.field public static final abc_button_padding_vertical_material:I = 0x7f0b004d

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f0b004e

.field public static final abc_config_prefDialogWidth:I = 0x7f0b0007

.field public static final abc_control_corner_material:I = 0x7f0b004f

.field public static final abc_control_inset_material:I = 0x7f0b0050

.field public static final abc_control_padding_material:I = 0x7f0b0051

.field public static final abc_dialog_fixed_height_major:I = 0x7f0b0008

.field public static final abc_dialog_fixed_height_minor:I = 0x7f0b0009

.field public static final abc_dialog_fixed_width_major:I = 0x7f0b000a

.field public static final abc_dialog_fixed_width_minor:I = 0x7f0b000b

.field public static final abc_dialog_min_width_major:I = 0x7f0b000c

.field public static final abc_dialog_min_width_minor:I = 0x7f0b000d

.field public static final abc_dialog_padding_material:I = 0x7f0b0054

.field public static final abc_dialog_padding_top_material:I = 0x7f0b0055

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0b0057

.field public static final abc_disabled_alpha_material_light:I = 0x7f0b0058

.field public static final abc_dropdownitem_icon_width:I = 0x7f0b0059

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0b005a

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0b005b

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0b005c

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0b005d

.field public static final abc_edit_text_inset_top_material:I = 0x7f0b005e

.field public static final abc_floating_window_z:I = 0x7f0b005f

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0b0060

.field public static final abc_panel_menu_list_width:I = 0x7f0b0061

.field public static final abc_progress_bar_height_material:I = 0x7f0b0062

.field public static final abc_search_view_preferred_height:I = 0x7f0b0063

.field public static final abc_search_view_preferred_width:I = 0x7f0b0064

.field public static final abc_seekbar_track_background_height_material:I = 0x7f0b0065

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f0b0066

.field public static final abc_select_dialog_padding_start_material:I = 0x7f0b0067

.field public static final abc_switch_padding:I = 0x7f0b0038

.field public static final abc_text_size_body_1_material:I = 0x7f0b0068

.field public static final abc_text_size_body_2_material:I = 0x7f0b0069

.field public static final abc_text_size_button_material:I = 0x7f0b006a

.field public static final abc_text_size_caption_material:I = 0x7f0b006b

.field public static final abc_text_size_display_1_material:I = 0x7f0b006c

.field public static final abc_text_size_display_2_material:I = 0x7f0b006d

.field public static final abc_text_size_display_3_material:I = 0x7f0b006e

.field public static final abc_text_size_display_4_material:I = 0x7f0b006f

.field public static final abc_text_size_headline_material:I = 0x7f0b0070

.field public static final abc_text_size_large_material:I = 0x7f0b0071

.field public static final abc_text_size_medium_material:I = 0x7f0b0072

.field public static final abc_text_size_menu_header_material:I = 0x7f0b0073

.field public static final abc_text_size_menu_material:I = 0x7f0b0074

.field public static final abc_text_size_small_material:I = 0x7f0b0075

.field public static final abc_text_size_subhead_material:I = 0x7f0b0076

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0b0004

.field public static final abc_text_size_title_material:I = 0x7f0b0077

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0b0005

.field public static final activity_horizontal_margin:I = 0x7f0b003d

.field public static final activity_vertical_margin:I = 0x7f0b0079

.field public static final article_attachment_list_divider_height:I = 0x7f0b007a

.field public static final article_attachment_row_filename_margin_left:I = 0x7f0b007b

.field public static final article_attachment_row_filename_margin_right:I = 0x7f0b007c

.field public static final article_attachment_row_filesize_margin_right:I = 0x7f0b007d

.field public static final article_attachment_row_minimum_height:I = 0x7f0b007e

.field public static final attachment_container_host_horizontal_height:I = 0x7f0b007f

.field public static final attachment_container_host_horizontal_margin:I = 0x7f0b0080

.field public static final attachment_container_host_vertical_margin:I = 0x7f0b0081

.field public static final attachment_container_image_margin:I = 0x7f0b0082

.field public static final attachments_horizontal_margin:I = 0x7f0b0083

.field public static final attachments_vertical_margin:I = 0x7f0b0084

.field public static final contact_fragment_separator_height:I = 0x7f0b008e

.field public static final design_appbar_elevation:I = 0x7f0b0094

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f0b009e

.field public static final design_fab_border_width:I = 0x7f0b00a0

.field public static final design_fab_elevation:I = 0x7f0b00a1

.field public static final design_fab_image_size:I = 0x7f0b00a2

.field public static final design_fab_size_mini:I = 0x7f0b00a3

.field public static final design_fab_size_normal:I = 0x7f0b00a4

.field public static final design_fab_translation_z_pressed:I = 0x7f0b00a5

.field public static final design_navigation_elevation:I = 0x7f0b00a6

.field public static final design_navigation_icon_padding:I = 0x7f0b00a7

.field public static final design_navigation_icon_size:I = 0x7f0b00a8

.field public static final design_navigation_max_width:I = 0x7f0b002f

.field public static final design_navigation_padding_bottom:I = 0x7f0b00a9

.field public static final design_navigation_separator_vertical_padding:I = 0x7f0b00aa

.field public static final design_snackbar_action_inline_max_width:I = 0x7f0b0030

.field public static final design_snackbar_background_corner_radius:I = 0x7f0b0031

.field public static final design_snackbar_elevation:I = 0x7f0b00ab

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f0b0032

.field public static final design_snackbar_max_width:I = 0x7f0b0033

.field public static final design_snackbar_min_width:I = 0x7f0b0034

.field public static final design_snackbar_padding_horizontal:I = 0x7f0b00ac

.field public static final design_snackbar_padding_vertical:I = 0x7f0b00ad

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f0b0035

.field public static final design_snackbar_text_size:I = 0x7f0b00ae

.field public static final design_tab_max_width:I = 0x7f0b00af

.field public static final design_tab_scrollable_min_width:I = 0x7f0b0036

.field public static final design_tab_text_size:I = 0x7f0b00b0

.field public static final design_tab_text_size_2line:I = 0x7f0b00b1

.field public static final disabled_alpha_material_dark:I = 0x7f0b00b2

.field public static final disabled_alpha_material_light:I = 0x7f0b00b3

.field public static final fab_margin:I = 0x7f0b00b8

.field public static final help_recycler_view_bottom_padding:I = 0x7f0b00bc

.field public static final help_row_action_height:I = 0x7f0b00bd

.field public static final help_row_action_padding_bottom:I = 0x7f0b00be

.field public static final help_row_action_padding_end:I = 0x7f0b00bf

.field public static final help_row_action_padding_start:I = 0x7f0b00c0

.field public static final help_row_action_padding_top:I = 0x7f0b00c1

.field public static final help_row_article_min_height:I = 0x7f0b00c2

.field public static final help_row_article_padding_bottom:I = 0x7f0b00c3

.field public static final help_row_article_padding_end:I = 0x7f0b00c4

.field public static final help_row_article_padding_start:I = 0x7f0b00c5

.field public static final help_row_article_padding_top:I = 0x7f0b00c6

.field public static final help_row_category_min_height:I = 0x7f0b00c7

.field public static final help_row_category_padding_bottom:I = 0x7f0b00c8

.field public static final help_row_category_padding_end:I = 0x7f0b00c9

.field public static final help_row_category_padding_start:I = 0x7f0b00ca

.field public static final help_row_category_padding_top:I = 0x7f0b00cb

.field public static final help_row_section_min_height:I = 0x7f0b00cc

.field public static final help_row_section_padding_bottom:I = 0x7f0b00cd

.field public static final help_row_section_padding_end:I = 0x7f0b00ce

.field public static final help_row_section_padding_start:I = 0x7f0b00cf

.field public static final help_row_section_padding_top:I = 0x7f0b00d0

.field public static final help_search_row_min_height:I = 0x7f0b00d1

.field public static final help_search_row_padding_bottom:I = 0x7f0b00d2

.field public static final help_search_row_padding_end:I = 0x7f0b00d3

.field public static final help_search_row_padding_start:I = 0x7f0b00d4

.field public static final help_search_row_padding_top:I = 0x7f0b00d5

.field public static final highlight_alpha_material_colored:I = 0x7f0b00d8

.field public static final highlight_alpha_material_dark:I = 0x7f0b00d9

.field public static final highlight_alpha_material_light:I = 0x7f0b00da

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0b00e2

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0b00e3

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f0b00e4

.field public static final no_network_view_bottom_padding:I = 0x7f0b00f7

.field public static final no_network_view_separator_height:I = 0x7f0b00f8

.field public static final no_network_view_top_padding:I = 0x7f0b00f9

.field public static final notification_large_icon_height:I = 0x7f0b00fd

.field public static final notification_large_icon_width:I = 0x7f0b00fe

.field public static final notification_subtext_size:I = 0x7f0b0102

.field public static final request_list_divider_height:I = 0x7f0b010f

.field public static final request_list_error_container_horizontal_padding:I = 0x7f0b0110

.field public static final request_list_error_container_vertical_padding:I = 0x7f0b0111

.field public static final request_list_row_date_padding_top:I = 0x7f0b0112

.field public static final request_list_row_date_text_size:I = 0x7f0b0113

.field public static final request_list_row_description_text_size:I = 0x7f0b0114

.field public static final request_list_row_horizontal_padding:I = 0x7f0b0115

.field public static final request_list_row_minimum_height:I = 0x7f0b0116

.field public static final request_list_row_vertical_padding:I = 0x7f0b0117

.field public static final request_list_unread_indicator_size:I = 0x7f0b0118

.field public static final retry_view_bottom_padding:I = 0x7f0b0119

.field public static final retry_view_button_layout_height:I = 0x7f0b011a

.field public static final retry_view_button_layout_width:I = 0x7f0b011b

.field public static final retry_view_button_margin_bottom:I = 0x7f0b011c

.field public static final retry_view_button_margin_right:I = 0x7f0b011d

.field public static final retry_view_button_margin_top:I = 0x7f0b011e

.field public static final retry_view_button_textsize:I = 0x7f0b011f

.field public static final retry_view_left_padding:I = 0x7f0b0120

.field public static final retry_view_right_padding:I = 0x7f0b0121

.field public static final retry_view_separator_height:I = 0x7f0b0122

.field public static final retry_view_textview_textsize:I = 0x7f0b0123

.field public static final retry_view_top_padding:I = 0x7f0b0124

.field public static final rma_button_text:I = 0x7f0b0125

.field public static final rma_divider_height:I = 0x7f0b0126

.field public static final rma_edit_text:I = 0x7f0b0127

.field public static final rma_progress_height:I = 0x7f0b0128

.field public static final rma_progress_width:I = 0x7f0b0129

.field public static final rma_subtitle_text:I = 0x7f0b012a

.field public static final rma_title_text:I = 0x7f0b012b

.field public static final view_horizontal_margin:I = 0x7f0b0138

.field public static final view_request_comment_avatar_size:I = 0x7f0b0139

.field public static final view_request_comment_list_separator_height:I = 0x7f0b013a

.field public static final view_request_comment_send_button_size:I = 0x7f0b013b

.field public static final view_request_content_margin:I = 0x7f0b013c

.field public static final view_request_horizontal_margin:I = 0x7f0b013d

.field public static final view_request_separator_height:I = 0x7f0b013e

.field public static final view_request_vertical_margin:I = 0x7f0b013f

.field public static final view_vertical_margin:I = 0x7f0b0140

.field public static final zd_dark_icon_active_opacity:I = 0x7f0b0141

.field public static final zd_dark_icon_inactive_opacity:I = 0x7f0b0142

.field public static final zd_light_icon_active_opacity:I = 0x7f0b0143

.field public static final zd_light_icon_inactive_opacity:I = 0x7f0b0144

.field public static final zd_progress_bar_normal:I = 0x7f0b0145

.field public static final zd_progress_bar_small:I = 0x7f0b0146

.field public static final zd_toolbar_elevation:I = 0x7f0b0147


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
