.class public final Lcom/zendesk/sdk/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f100319

.field public static final action_bar:I = 0x7f100099

.field public static final action_bar_activity_content:I = 0x7f100000

.field public static final action_bar_container:I = 0x7f100098

.field public static final action_bar_root:I = 0x7f100094

.field public static final action_bar_spinner:I = 0x7f100001

.field public static final action_bar_subtitle:I = 0x7f100077

.field public static final action_bar_title:I = 0x7f100076

.field public static final action_context_bar:I = 0x7f10009a

.field public static final action_divider:I = 0x7f10031d

.field public static final action_menu_divider:I = 0x7f100002

.field public static final action_menu_presenter:I = 0x7f100003

.field public static final action_mode_bar:I = 0x7f100096

.field public static final action_mode_bar_stub:I = 0x7f100095

.field public static final action_mode_close_button:I = 0x7f100078

.field public static final activity_chooser_view_content:I = 0x7f100079

.field public static final activity_contact_zendesk_root:I = 0x7f1000c5

.field public static final activity_network_no_connectivity:I = 0x7f1002ef

.field public static final activity_request_fragment:I = 0x7f1000d4

.field public static final activity_request_list_add_icon:I = 0x7f100409

.field public static final activity_request_view_root:I = 0x7f1000de

.field public static final add:I = 0x7f10002b

.field public static final alertTitle:I = 0x7f10008d

.field public static final always:I = 0x7f100043

.field public static final article_attachment_row_filename_text:I = 0x7f10034a

.field public static final article_attachment_row_filesize_text:I = 0x7f10034b

.field public static final article_title:I = 0x7f100349

.field public static final attachment_delete:I = 0x7f1003f8

.field public static final attachment_image:I = 0x7f1003f6

.field public static final attachment_inline_container:I = 0x7f1003f9

.field public static final attachment_inline_image:I = 0x7f1003fa

.field public static final attachment_inline_progressbar:I = 0x7f1003fb

.field public static final attachment_progress:I = 0x7f1003f7

.field public static final auto:I = 0x7f100032

.field public static final beginning:I = 0x7f10005a

.field public static final belvedere_dialog_listview:I = 0x7f1000fb

.field public static final belvedere_dialog_row_image:I = 0x7f1000fc

.field public static final belvedere_dialog_row_text:I = 0x7f1000fd

.field public static final bottom:I = 0x7f100033

.field public static final buttonPanel:I = 0x7f100080

.field public static final cancel_action:I = 0x7f10031a

.field public static final category_title:I = 0x7f10034c

.field public static final center:I = 0x7f100034

.field public static final center_horizontal:I = 0x7f100035

.field public static final center_vertical:I = 0x7f100036

.field public static final checkbox:I = 0x7f100090

.field public static final chronometer:I = 0x7f100322

.field public static final clip_horizontal:I = 0x7f10003f

.field public static final clip_vertical:I = 0x7f100040

.field public static final collapseActionView:I = 0x7f100061

.field public static final contact_fragment_attachments:I = 0x7f10022b

.field public static final contact_fragment_container:I = 0x7f100228

.field public static final contact_fragment_description:I = 0x7f10022a

.field public static final contact_fragment_email:I = 0x7f100229

.field public static final contact_fragment_progress:I = 0x7f10022c

.field public static final contact_us_button:I = 0x7f1000d7

.field public static final contentPanel:I = 0x7f100083

.field public static final custom:I = 0x7f10008a

.field public static final customPanel:I = 0x7f100089

.field public static final decor_content_parent:I = 0x7f100097

.field public static final default_activity_button:I = 0x7f10007c

.field public static final design_bottom_sheet:I = 0x7f100114

.field public static final design_menu_item_action_area:I = 0x7f10011b

.field public static final design_menu_item_action_area_stub:I = 0x7f10011a

.field public static final design_menu_item_text:I = 0x7f100119

.field public static final design_navigation_view:I = 0x7f100118

.field public static final disableHome:I = 0x7f100020

.field public static final edit_query:I = 0x7f10009b

.field public static final end:I = 0x7f100037

.field public static final end_padder:I = 0x7f100329

.field public static final enterAlways:I = 0x7f100026

.field public static final enterAlwaysCollapsed:I = 0x7f100027

.field public static final exitUntilCollapsed:I = 0x7f100028

.field public static final expand_activities_button:I = 0x7f10007a

.field public static final expanded_menu:I = 0x7f10008f

.field public static final fill:I = 0x7f100041

.field public static final fill_horizontal:I = 0x7f100042

.field public static final fill_vertical:I = 0x7f100038

.field public static final fixed:I = 0x7f100074

.field public static final fragment_contact_zendesk_attachment:I = 0x7f100416

.field public static final fragment_contact_zendesk_menu_done:I = 0x7f100417

.field public static final fragment_container:I = 0x7f1000d5

.field public static final fragment_help_menu_contact:I = 0x7f100419

.field public static final fragment_help_menu_search:I = 0x7f100418

.field public static final help_section_action_button:I = 0x7f100342

.field public static final help_section_loading_progress:I = 0x7f100343

.field public static final home:I = 0x7f100004

.field public static final homeAsUp:I = 0x7f100021

.field public static final icon:I = 0x7f10007e

.field public static final ifRoom:I = 0x7f100062

.field public static final image:I = 0x7f10007b

.field public static final info:I = 0x7f100323

.field public static final item_touch_helper_previous_elevation:I = 0x7f100005

.field public static final left:I = 0x7f100039

.field public static final line1:I = 0x7f100327

.field public static final line3:I = 0x7f100328

.field public static final listMode:I = 0x7f10001d

.field public static final list_item:I = 0x7f10007d

.field public static final loading_view:I = 0x7f1000d6

.field public static final media_actions:I = 0x7f10031c

.field public static final middle:I = 0x7f10005b

.field public static final mini:I = 0x7f100046

.field public static final multiply:I = 0x7f10002c

.field public static final navigation_header_container:I = 0x7f100117

.field public static final never:I = 0x7f100045

.field public static final none:I = 0x7f10001c

.field public static final normal:I = 0x7f10001e

.field public static final padding:I = 0x7f100148

.field public static final parallax:I = 0x7f10003d

.field public static final parentPanel:I = 0x7f100082

.field public static final pin:I = 0x7f10003e

.field public static final progress_circular:I = 0x7f100006

.field public static final progress_horizontal:I = 0x7f100007

.field public static final radio:I = 0x7f100092

.field public static final recyclerView:I = 0x7f100251

.field public static final request_list_fragment_progress:I = 0x7f100273

.field public static final retry_view_button:I = 0x7f1002f2

.field public static final retry_view_container:I = 0x7f1002f0

.field public static final retry_view_text:I = 0x7f1002f1

.field public static final right:I = 0x7f10003a

.field public static final rma_dialog_root:I = 0x7f100008

.field public static final rma_dont_ask_again:I = 0x7f100009

.field public static final rma_feedback_button:I = 0x7f10000a

.field public static final rma_feedback_issue_cancel_button:I = 0x7f100284

.field public static final rma_feedback_issue_edittext:I = 0x7f100282

.field public static final rma_feedback_issue_progress:I = 0x7f100283

.field public static final rma_feedback_issue_send_button:I = 0x7f100285

.field public static final rma_store_button:I = 0x7f10000b

.field public static final row_request_container:I = 0x7f100353

.field public static final row_request_date:I = 0x7f100355

.field public static final row_request_description:I = 0x7f100354

.field public static final row_request_unread_indicator:I = 0x7f100352

.field public static final screen:I = 0x7f10002d

.field public static final scroll:I = 0x7f100029

.field public static final scrollIndicatorDown:I = 0x7f100088

.field public static final scrollIndicatorUp:I = 0x7f100084

.field public static final scrollView:I = 0x7f100085

.field public static final scrollable:I = 0x7f100075

.field public static final search_badge:I = 0x7f10009d

.field public static final search_bar:I = 0x7f10009c

.field public static final search_button:I = 0x7f10009e

.field public static final search_close_btn:I = 0x7f1000a3

.field public static final search_edit_frame:I = 0x7f10009f

.field public static final search_go_btn:I = 0x7f1000a5

.field public static final search_mag_icon:I = 0x7f1000a0

.field public static final search_plate:I = 0x7f1000a1

.field public static final search_src_text:I = 0x7f1000a2

.field public static final search_voice_btn:I = 0x7f1000a6

.field public static final section_group:I = 0x7f100356

.field public static final section_title:I = 0x7f100357

.field public static final select_dialog_listview:I = 0x7f1000a7

.field public static final shortcut:I = 0x7f100091

.field public static final showCustom:I = 0x7f100022

.field public static final showHome:I = 0x7f100023

.field public static final showTitle:I = 0x7f100024

.field public static final snackbar_action:I = 0x7f100116

.field public static final snackbar_text:I = 0x7f100115

.field public static final snap:I = 0x7f10002a

.field public static final spacer:I = 0x7f100081

.field public static final split_action_bar:I = 0x7f10000c

.field public static final src_atop:I = 0x7f10002e

.field public static final src_in:I = 0x7f10002f

.field public static final src_over:I = 0x7f100030

.field public static final start:I = 0x7f10003b

.field public static final status_bar_latest_event_content:I = 0x7f10031b

.field public static final submenuarrow:I = 0x7f100093

.field public static final submit_area:I = 0x7f1000a4

.field public static final subtitle:I = 0x7f100181

.field public static final tabMode:I = 0x7f10001f

.field public static final text:I = 0x7f10000d

.field public static final text2:I = 0x7f100051

.field public static final textSpacerNoButtons:I = 0x7f100087

.field public static final time:I = 0x7f100321

.field public static final title:I = 0x7f10007f

.field public static final title_template:I = 0x7f10008c

.field public static final top:I = 0x7f10003c

.field public static final topPanel:I = 0x7f10008b

.field public static final touch_outside:I = 0x7f100113

.field public static final up:I = 0x7f100012

.field public static final useLogo:I = 0x7f100025

.field public static final view_article_attachment_list:I = 0x7f1000dd

.field public static final view_article_content_webview:I = 0x7f1000db

.field public static final view_article_progress:I = 0x7f1000dc

.field public static final view_article_toolbar_holder:I = 0x7f1000d3

.field public static final view_offset_helper:I = 0x7f100013

.field public static final view_request_agent_avatar_imageview:I = 0x7f100345

.field public static final view_request_agent_comment_date:I = 0x7f100344

.field public static final view_request_agent_name_textview:I = 0x7f100346

.field public static final view_request_agent_response_attachment_container:I = 0x7f100348

.field public static final view_request_agent_response_textview:I = 0x7f100347

.field public static final view_request_attachment_container:I = 0x7f10028b

.field public static final view_request_comment_attachment_bth:I = 0x7f10028f

.field public static final view_request_comment_attachment_container:I = 0x7f100289

.field public static final view_request_comment_container:I = 0x7f10028c

.field public static final view_request_comment_edittext:I = 0x7f10028d

.field public static final view_request_comment_list:I = 0x7f100287

.field public static final view_request_comment_send_bth:I = 0x7f10028e

.field public static final view_request_end_user_avatar_imageview:I = 0x7f10034e

.field public static final view_request_end_user_comment_date:I = 0x7f10034d

.field public static final view_request_end_user_name_textview:I = 0x7f10034f

.field public static final view_request_end_user_response_attachment_container:I = 0x7f100351

.field public static final view_request_end_user_response_textview:I = 0x7f100350

.field public static final view_request_fragment_progress:I = 0x7f100290

.field public static final view_request_separator_1:I = 0x7f10028a

.field public static final view_request_separator_2:I = 0x7f100288

.field public static final withText:I = 0x7f100063

.field public static final wrap_content:I = 0x7f100031

.field public static final zd_toolbar:I = 0x7f1003fe

.field public static final zd_toolbar_container:I = 0x7f1003fd

.field public static final zd_toolbar_shadow:I = 0x7f1003ff


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
