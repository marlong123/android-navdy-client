.class public Lcom/zendesk/sdk/attachment/AttachmentHelper;
.super Ljava/lang/Object;
.source "AttachmentHelper.java"


# static fields
.field private static final DEFAULT_MIMETYPE:Ljava/lang/String; = "application/octet-stream"

.field private static final LOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/zendesk/sdk/attachment/AttachmentHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/attachment/AttachmentHelper;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->getMimeType(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getMimeType(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Landroid/net/Uri;

    .prologue
    .line 161
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 162
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "application/octet-stream"

    goto :goto_0
.end method

.method public static isAttachmentSupportEnabled(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z
    .locals 1
    .param p0, "storedSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 40
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isAttachmentsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFileEligibleForUpload(Ljava/io/File;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .param p1, "storedSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    const/4 v0, 0x0

    .line 52
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 53
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getMaxAttachmentSize()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    .line 55
    :cond_0
    return v0
.end method

.method public static processAndUploadSelectedFiles(Ljava/util/List;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Landroid/content/Context;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 10
    .param p1, "imageUploadHelper"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "attachmentContainerHost"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;
    .param p4, "storedSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;",
            "Lcom/zendesk/sdk/attachment/ImageUploadHelper;",
            "Landroid/content/Context;",
            "Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "selectedFiles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 126
    invoke-virtual {p1, p0}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->removeDuplicateFilesFromList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 128
    .local v3, "uniqueFiles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-eq v4, v5, :cond_0

    .line 129
    sget v4, Lcom/zendesk/sdk/R$string;->attachment_upload_error_file_already_added:I

    invoke-virtual {p2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2, v4, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 130
    sget-object v4, Lcom/zendesk/sdk/attachment/AttachmentHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Files already added"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/belvedere/BelvedereResult;

    .line 134
    .local v0, "file":Lcom/zendesk/belvedere/BelvedereResult;
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 136
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-static {v5, p4}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->isFileEligibleForUpload(Ljava/io/File;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 137
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->getMimeType(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v0, v5}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadImage(Lcom/zendesk/belvedere/BelvedereResult;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {p3, v5}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->addAttachment(Ljava/io/File;)V

    goto :goto_0

    .line 142
    :cond_1
    const-string v2, ""

    .line 143
    .local v2, "fileSize":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 144
    invoke-virtual {p4}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getMaxAttachmentSize()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, Lcom/zendesk/util/FileUtils;->humanReadableFileSize(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v2

    .line 147
    :cond_2
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget v6, Lcom/zendesk/sdk/R$string;->attachment_upload_error_file_too_big:I

    invoke-virtual {p2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v2, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 148
    sget-object v5, Lcom/zendesk/sdk/attachment/AttachmentHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File is too big: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 152
    .end local v2    # "fileSize":Ljava/lang/String;
    :cond_3
    sget v5, Lcom/zendesk/sdk/R$string;->attachment_upload_error_file_not_found:I

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 154
    if-nez v0, :cond_4

    const-string v1, "no filename"

    .line 155
    .local v1, "fileName":Ljava/lang/String;
    :goto_1
    sget-object v5, Lcom/zendesk/sdk/attachment/AttachmentHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File not found: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    .end local v1    # "fileName":Ljava/lang/String;
    :cond_4
    invoke-virtual {v0}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 158
    .end local v0    # "file":Lcom/zendesk/belvedere/BelvedereResult;
    :cond_5
    return-void
.end method

.method public static showAttachmentTryAgainDialog(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereResult;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "file"    # Lcom/zendesk/belvedere/BelvedereResult;
    .param p2, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;
    .param p3, "imageUploadHelper"    # Lcom/zendesk/sdk/attachment/ImageUploadHelper;
    .param p4, "attachmentContainerHost"    # Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    .prologue
    const/4 v5, 0x0

    .line 75
    sget-object v1, Lcom/zendesk/sdk/attachment/AttachmentHelper;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Attachment failed to upload: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 78
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    sget v1, Lcom/zendesk/sdk/R$string;->attachment_upload_error_upload_failed:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 81
    sget v1, Lcom/zendesk/sdk/R$string;->attachment_upload_error_cancel:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;

    invoke-direct {v2, p4, p1}, Lcom/zendesk/sdk/attachment/AttachmentHelper$1;-><init>(Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;Lcom/zendesk/belvedere/BelvedereResult;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 91
    sget v1, Lcom/zendesk/sdk/R$string;->attachment_upload_error_try_again:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;

    invoke-direct {v2, p3, p1, p0, p4}, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;-><init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/belvedere/BelvedereResult;Landroid/content/Context;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 102
    return-void
.end method
