.class Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ZendeskFeedbackConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;->sendFeedback(Ljava/lang/String;Ljava/util/List;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/CreateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$request:Lcom/zendesk/sdk/model/request/CreateRequest;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/request/CreateRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->this$0:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector;

    iput-object p2, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p3, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 183
    const-string v0, "ZendeskFeedbackConnector"

    invoke-static {v0, p1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Lcom/zendesk/service/ErrorResponse;)V

    .line 185
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v0, p1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 188
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V
    .locals 3
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/CreateRequest;

    .prologue
    .line 174
    const-string v0, "ZendeskFeedbackConnector"

    const-string v1, "Feedback submitted successfully."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->val$request:Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 179
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConnector$1;->onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V

    return-void
.end method
