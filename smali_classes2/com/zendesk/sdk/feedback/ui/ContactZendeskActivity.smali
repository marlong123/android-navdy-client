.class public Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;
.super Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;
.source "ContactZendeskActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;
    }
.end annotation


# static fields
.field public static final EXTRA_CONTACT_CONFIGURATION:Ljava/lang/String; = "extra_contact_configuration"

.field private static final FRAGMENT_TAG:Ljava/lang/String;

.field private static final LOG_TAG:Ljava/lang/String; = "ContactZendeskActivity"

.field public static final RESULT_ERROR_IS_NETWORK_ERROR:Ljava/lang/String; = "extra_is_nw_error"

.field public static final RESULT_ERROR_REASON:Ljava/lang/String; = "extra_error_reason"

.field public static final RESULT_ERROR_STATUS_CODE:Ljava/lang/String; = "extra_status_code"


# instance fields
.field private contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

.field private final mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;-><init>()V

    .line 194
    new-instance v0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$1;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;)V

    iput-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;
    .param p1, "x1"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 218
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_is_nw_error"

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->isNetworkError()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 220
    const-string v1, "extra_error_reason"

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 221
    const-string v1, "extra_status_code"

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 222
    return-object v0
.end method

.method public static startActivity(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 86
    if-nez p0, :cond_0

    .line 87
    const-string v3, "ContactZendeskActivity"

    const-string v4, "Context is null, cannot start the context."

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :goto_0
    return-void

    .line 91
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v2, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    instance-of v3, p1, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    if-nez v3, :cond_1

    const/4 v1, 0x1

    .line 96
    .local v1, "configurationRequiresWrapping":Z
    :cond_1
    if-eqz v1, :cond_2

    .line 97
    new-instance v0, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    invoke-direct {v0, p1}, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    .end local p1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .local v0, "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    move-object p1, v0

    .line 100
    .end local v0    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .restart local p1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    :cond_2
    const-string v3, "extra_contact_configuration"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 107
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 108
    sget v9, Lcom/zendesk/sdk/R$layout;->activity_contact_zendesk:I

    invoke-virtual {p0, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->setContentView(I)V

    .line 109
    invoke-static {p0}, Lcom/zendesk/sdk/ui/ToolbarSherlock;->installToolBar(Landroid/support/v7/app/AppCompatActivity;)V

    .line 111
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 112
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-nez v0, :cond_0

    .line 113
    const-string v2, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    .line 114
    .local v2, "errorMsg":Ljava/lang/String;
    const-string v9, "ContactZendeskActivity"

    const-string v10, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    new-instance v9, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v10, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    invoke-direct {v9, v10}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->setResult(ILandroid/content/Intent;)V

    .line 116
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->finish()V

    .line 156
    .end local v2    # "errorMsg":Ljava/lang/String;
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-virtual {v0, v6}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 122
    sget-object v9, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v9}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v9

    invoke-interface {v9}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v9

    invoke-interface {v9}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v7

    .line 123
    .local v7, "identity":Lcom/zendesk/sdk/model/access/Identity;
    if-nez v7, :cond_1

    .line 124
    const-string v2, "No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity..."

    .line 125
    .restart local v2    # "errorMsg":Ljava/lang/String;
    const-string v9, "ContactZendeskActivity"

    const-string v10, "No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity..."

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    new-instance v9, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v10, "No identity is present. Did you call ZendeskConfig.INSTANCE.setIdentity()?, finishing activity..."

    invoke-direct {v9, v10}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {p0, v8, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->setResult(ILandroid/content/Intent;)V

    .line 127
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->finish()V

    goto :goto_0

    .line 133
    .end local v2    # "errorMsg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "extra_contact_configuration"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 134
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "extra_contact_configuration"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    instance-of v9, v9, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    if-eqz v9, :cond_2

    .line 135
    .local v6, "hasSuppliedContactConfiguration":Z
    :goto_1
    if-eqz v6, :cond_3

    .line 136
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v9, "extra_contact_configuration"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .line 143
    .local v1, "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    :goto_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    .line 144
    .local v4, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    sget-object v8, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 146
    .local v3, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v3, :cond_4

    instance-of v8, v3, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    if-eqz v8, :cond_4

    .line 147
    check-cast v3, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .end local v3    # "fragment":Landroid/support/v4/app/Fragment;
    iput-object v3, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .line 155
    :goto_3
    iget-object v8, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    iget-object v9, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->mSubmissionListener:Lcom/zendesk/sdk/network/SubmissionListener;

    invoke-virtual {v8, v9}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->setFeedbackListener(Lcom/zendesk/sdk/network/SubmissionListener;)V

    goto :goto_0

    .end local v1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .end local v4    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v6    # "hasSuppliedContactConfiguration":Z
    :cond_2
    move v6, v8

    .line 134
    goto :goto_1

    .line 138
    .restart local v6    # "hasSuppliedContactConfiguration":Z
    :cond_3
    const-string v2, "Contact configuration was not provided. Will use default configuration..."

    .line 139
    .restart local v2    # "errorMsg":Ljava/lang/String;
    const-string v9, "ContactZendeskActivity"

    const-string v10, "Contact configuration was not provided. Will use default configuration..."

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    new-instance v1, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity$DefaultContactConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;)V

    .restart local v1    # "configuration":Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    goto :goto_2

    .line 149
    .end local v2    # "errorMsg":Ljava/lang/String;
    .restart local v3    # "fragment":Landroid/support/v4/app/Fragment;
    .restart local v4    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_4
    invoke-static {v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->newInstance(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    move-result-object v8

    iput-object v8, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .line 150
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    .line 151
    .local v5, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    sget v8, Lcom/zendesk/sdk/R$id;->activity_contact_zendesk_root:I

    iget-object v9, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    sget-object v10, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v5, v8, v9, v10}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 152
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 160
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onDestroy()V

    .line 162
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "ContactZendeskActivity"

    const-string v1, "Deleting unused attachments"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->deleteUnusedAttachmentsBeforeShutdown()V

    .line 166
    :cond_0
    return-void
.end method

.method public onNetworkAvailable()V
    .locals 1

    .prologue
    .line 238
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkAvailable()V

    .line 239
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->onNetworkAvailable()V

    .line 240
    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkUnavailable()V

    .line 245
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->contactZendeskFragment:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->onNetworkUnavailable()V

    .line 246
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 228
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 229
    invoke-virtual {p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->onBackPressed()V

    .line 230
    const/4 v0, 0x1

    .line 233
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
