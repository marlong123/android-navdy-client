.class Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ContactZendeskFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RequestCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/CreateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    .prologue
    .line 649
    iput-object p1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 3
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    const/4 v2, 0x1

    .line 671
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 673
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 674
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionError(Lcom/zendesk/service/ErrorResponse;)V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$700(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 678
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1800(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 679
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$600(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/ui/EmailAddressAutoCompleteTextView;->setEnabled(Z)V

    .line 681
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$900(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$900(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1800(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x66

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 685
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1300(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/Retryable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1300(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/Retryable;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    sget v2, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_send_error_toast:I

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback$1;-><init>(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;)V

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/Retryable;->onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 693
    :cond_2
    return-void

    .line 682
    :cond_3
    const/16 v0, 0xff

    goto :goto_0
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V
    .locals 3
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/CreateRequest;

    .prologue
    .line 653
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CreateRequest;->getId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 654
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CreateRequest;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/storage/RequestStorage;->setCommentCount(Ljava/lang/String;I)V

    .line 659
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1500(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionCompleted()V

    .line 663
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 664
    iget-object v0, p0, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->this$0:Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$1600(Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;)V

    .line 666
    :cond_1
    return-void

    .line 656
    :cond_2
    invoke-static {}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Attempted to store a null request in callback."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 649
    check-cast p1, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskFragment$RequestCallback;->onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V

    return-void
.end method
