.class public Lcom/zendesk/sdk/support/SupportActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "SupportActivity.java"

# interfaces
.implements Lcom/zendesk/sdk/support/SupportMvp$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/SupportActivity$Builder;
    }
.end annotation


# static fields
.field static final EXTRA_CATEGORIES_COLLAPSED:Ljava/lang/String; = "extra_categories_collapsed"

.field static final EXTRA_CATEGORY_IDS:Ljava/lang/String; = "extra_category_ids"

.field static final EXTRA_LABEL_NAMES:Ljava/lang/String; = "extra_label_names"

.field static final EXTRA_SECTION_IDS:Ljava/lang/String; = "extra_section_ids"

.field static final EXTRA_SHOW_CONTACT_US_BUTTON:Ljava/lang/String; = "extra_show_contact_us_button"

.field private static final LOG_TAG:Ljava/lang/String; = "SupportActivity"

.field static final SAVED_STATE_FAB_VISIBLE:Ljava/lang/String; = "fab_visibility"


# instance fields
.field private contactUsButton:Landroid/support/design/widget/FloatingActionButton;

.field private conversationsMenuItem:Landroid/view/MenuItem;

.field private errorSnackbar:Landroid/support/design/widget/Snackbar;

.field private feedbackConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

.field private loadingView:Landroid/view/View;

.field private presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

.field private searchViewMenuItem:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/SupportActivity;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/support/SupportActivity;)Lcom/zendesk/sdk/support/SupportMvp$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/SupportActivity;)Landroid/support/design/widget/Snackbar;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    return-object v0
.end method

.method private addFragment(Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 267
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/zendesk/sdk/R$id;->fragment_container:I

    .line 268
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 270
    return-void
.end method

.method private addOnBackStackChangedListener()V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/support/SupportActivity$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportActivity$2;-><init>(Lcom/zendesk/sdk/support/SupportActivity;)V

    .line 120
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 131
    return-void
.end method

.method private getCurrentFragment()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget v1, Lcom/zendesk/sdk/R$id;->fragment_container:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;
    .locals 3
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 161
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 162
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_is_nw_error"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 163
    const-string v1, "extra_error_reason"

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const-string v1, "extra_status_code"

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getStatus()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    return-object v0
.end method

.method private getFeedbackConfiguration()Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_contact_configuration"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_contact_configuration"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    instance-of v1, v1, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 172
    .local v0, "hasSuppliedContactConfiguration":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_contact_configuration"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .line 175
    :goto_1
    return-object v1

    .line 171
    .end local v0    # "hasSuppliedContactConfiguration":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 175
    .restart local v0    # "hasSuppliedContactConfiguration":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getSearchFragment()Lcom/zendesk/sdk/support/help/HelpSearchFragment;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    instance-of v2, v2, Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    if-eqz v2, :cond_0

    .line 307
    const-string v2, "SupportActivity"

    const-string v3, "showSearchResults: current fragment is a HelpSearchFragment"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    .line 320
    :goto_0
    return-object v1

    .line 311
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    .line 312
    invoke-virtual {v2}, Landroid/support/design/widget/FloatingActionButton;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->newInstance(Z)Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    move-result-object v0

    .line 314
    .local v0, "searchFragment":Lcom/zendesk/sdk/support/help/HelpSearchFragment;
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 315
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$id;->fragment_container:I

    .line 316
    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const/4 v2, 0x0

    .line 317
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 318
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    move-object v1, v0

    .line 320
    goto :goto_0
.end method

.method private noFragmentAdded()Z
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearSearchResults()V
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    if-eqz v0, :cond_0

    .line 326
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    .line 327
    invoke-virtual {v0}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->clearResults()V

    .line 329
    :cond_0
    return-void
.end method

.method public dismissError()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->dismiss()V

    .line 394
    :cond_0
    return-void
.end method

.method public exitActivity()V
    .locals 0

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->finish()V

    .line 411
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public hideLoadingState()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->loadingView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 287
    return-void
.end method

.method public isShowingHelp()Z
    .locals 1

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 75
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    sget v2, Lcom/zendesk/sdk/R$layout;->activity_support:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/support/SupportActivity;->setContentView(I)V

    .line 78
    invoke-static {p0}, Lcom/zendesk/sdk/ui/ToolbarSherlock;->installToolBar(Landroid/support/v7/app/AppCompatActivity;)V

    .line 80
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 81
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-nez v0, :cond_0

    .line 82
    const-string v1, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    .line 83
    .local v1, "errorMsg":Ljava/lang/String;
    const-string v2, "SupportActivity"

    const-string v3, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    new-instance v2, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v3, "This activity requires an AppCompat theme with an action bar, finishing activity..."

    invoke-direct {v2, v3}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/support/SupportActivity;->getErrorIntent(Lcom/zendesk/service/ErrorResponse;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v5, v2}, Lcom/zendesk/sdk/support/SupportActivity;->setResult(ILandroid/content/Intent;)V

    .line 85
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->finish()V

    .line 108
    .end local v1    # "errorMsg":Ljava/lang/String;
    :goto_0
    return-void

    .line 89
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 91
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getFeedbackConfiguration()Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    move-result-object v2

    iput-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->feedbackConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .line 92
    sget v2, Lcom/zendesk/sdk/R$id;->loading_view:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->loadingView:Landroid/view/View;

    .line 94
    sget v2, Lcom/zendesk/sdk/R$id;->contact_us_button:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/support/SupportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/FloatingActionButton;

    iput-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    .line 95
    iget-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    new-instance v3, Lcom/zendesk/sdk/support/SupportActivity$1;

    invoke-direct {v3, p0}, Lcom/zendesk/sdk/support/SupportActivity$1;-><init>(Lcom/zendesk/sdk/support/SupportActivity;)V

    invoke-virtual {v2, v3}, Landroid/support/design/widget/FloatingActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    new-instance v2, Lcom/zendesk/sdk/support/SupportPresenter;

    new-instance v3, Lcom/zendesk/sdk/support/SupportModel;

    invoke-direct {v3}, Lcom/zendesk/sdk/support/SupportModel;-><init>()V

    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 103
    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/network/impl/ProviderStore;->networkInfoProvider()Lcom/zendesk/sdk/network/NetworkInfoProvider;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Lcom/zendesk/sdk/support/SupportPresenter;-><init>(Lcom/zendesk/sdk/support/SupportMvp$View;Lcom/zendesk/sdk/support/SupportMvp$Model;Lcom/zendesk/sdk/network/NetworkInfoProvider;)V

    iput-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .line 105
    iget-object v2, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-interface {v2, p1}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->determineFirstScreen(Landroid/os/Bundle;)V

    .line 107
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->addOnBackStackChangedListener()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$menu;->fragment_help_menu_conversations:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 184
    sget v1, Lcom/zendesk/sdk/R$id;->fragment_help_menu_contact:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->conversationsMenuItem:Landroid/view/MenuItem;

    .line 185
    sget v1, Lcom/zendesk/sdk/R$id;->fragment_help_menu_search:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->searchViewMenuItem:Landroid/view/MenuItem;

    .line 187
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->searchViewMenuItem:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->searchViewMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    .line 191
    .local v0, "searchView":Landroid/support/v7/widget/SearchView;
    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->getImeOptions()I

    move-result v1

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setImeOptions(I)V

    .line 193
    new-instance v1, Lcom/zendesk/sdk/support/SupportActivity$3;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportActivity$3;-><init>(Lcom/zendesk/sdk/support/SupportActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 212
    .end local v0    # "searchView":Landroid/support/v7/widget/SearchView;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 234
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 237
    .local v0, "id":I
    const v2, 0x102002c

    if-ne v0, v2, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->onBackPressed()V

    .line 247
    :goto_0
    return v1

    .line 242
    :cond_0
    sget v2, Lcom/zendesk/sdk/R$id;->fragment_help_menu_contact:I

    if-ne v0, v2, :cond_1

    .line 243
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->showRequestList()V

    goto :goto_0

    .line 247
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 157
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onPause()V

    .line 158
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 218
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->searchViewMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->searchViewMenuItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-interface {v1}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->shouldShowSearchMenuItem()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->conversationsMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->conversationsMenuItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-interface {v1}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->shouldShowConversationsMenuItem()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 226
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 151
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-interface {v0, p0}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->onResume(Lcom/zendesk/sdk/support/SupportMvp$View;)V

    .line 152
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 403
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 405
    const-string v1, "fab_visibility"

    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    invoke-virtual {v0}, Landroid/support/design/widget/FloatingActionButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 406
    return-void

    .line 405
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showContactUsButton()V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/FloatingActionButton;->setVisibility(I)V

    .line 253
    return-void
.end method

.method public showContactZendesk()V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->feedbackConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-static {p0, v0}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->startActivity(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    .line 344
    return-void
.end method

.method public showError(I)V
    .locals 2
    .param p1, "errorMessageId"    # I

    .prologue
    .line 385
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    const/4 v1, -0x2

    invoke-static {v0, p1, v1}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    .line 386
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 387
    return-void
.end method

.method public showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 4
    .param p1, "errorType"    # Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    .param p2, "action"    # Lcom/zendesk/sdk/network/RetryAction;

    .prologue
    const/4 v3, 0x0

    .line 351
    if-nez p1, :cond_0

    .line 352
    const-string v1, "SupportActivity"

    const-string v2, "ErrorType was null, falling back to \'retry\' as label"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    sget v1, Lcom/zendesk/sdk/R$string;->retry_view_button_label:I

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/support/SupportActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "errorMessage":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->contactUsButton:Landroid/support/design/widget/FloatingActionButton;

    const/4 v2, -0x2

    invoke-static {v1, v0, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    .line 373
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    sget v2, Lcom/zendesk/sdk/R$string;->retry_view_button_label:I

    new-instance v3, Lcom/zendesk/sdk/support/SupportActivity$4;

    invoke-direct {v3, p0, p2}, Lcom/zendesk/sdk/support/SupportActivity$4;-><init>(Lcom/zendesk/sdk/support/SupportActivity;Lcom/zendesk/sdk/network/RetryAction;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    .line 380
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->errorSnackbar:Landroid/support/design/widget/Snackbar;

    invoke-virtual {v1}, Landroid/support/design/widget/Snackbar;->show()V

    .line 381
    return-void

    .line 355
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/zendesk/sdk/support/SupportActivity$5;->$SwitchMap$com$zendesk$sdk$support$SupportMvp$ErrorType:[I

    invoke-virtual {p1}, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 366
    const-string v1, "SupportActivity"

    const-string v2, "Unknown or unhandled error type, falling back to error type name as label"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/zendesk/sdk/R$string;->help_search_no_results_label:I

    invoke-virtual {p0, v2}, Lcom/zendesk/sdk/support/SupportActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/support/SupportMvp$ErrorType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 357
    .end local v0    # "errorMessage":Ljava/lang/String;
    :pswitch_0
    sget v1, Lcom/zendesk/sdk/R$string;->categories_list_fragment_error_message:I

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/support/SupportActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 358
    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 360
    .end local v0    # "errorMessage":Ljava/lang/String;
    :pswitch_1
    sget v1, Lcom/zendesk/sdk/R$string;->sections_list_fragment_error_message:I

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/support/SupportActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 361
    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 363
    .end local v0    # "errorMessage":Ljava/lang/String;
    :pswitch_2
    sget v1, Lcom/zendesk/sdk/R$string;->articles_list_fragment_error_message:I

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/support/SupportActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 364
    .restart local v0    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 355
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public showHelp(Lcom/zendesk/sdk/support/SupportUiConfig;)V
    .locals 2
    .param p1, "supportUiConfig"    # Lcom/zendesk/sdk/support/SupportUiConfig;

    .prologue
    .line 257
    invoke-static {p1}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->newInstance(Lcom/zendesk/sdk/support/SupportUiConfig;)Lcom/zendesk/sdk/support/help/SupportHelpFragment;

    move-result-object v0

    .line 259
    .local v0, "fragment":Lcom/zendesk/sdk/support/help/SupportHelpFragment;
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->setPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V

    .line 260
    invoke-direct {p0, v0}, Lcom/zendesk/sdk/support/SupportActivity;->addFragment(Landroid/support/v4/app/Fragment;)V

    .line 262
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->invalidateOptionsMenu()V

    .line 263
    return-void
.end method

.method public showLoadingState()V
    .locals 3

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 275
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 277
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 278
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->loadingView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 282
    return-void
.end method

.method public showRequestList()V
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->feedbackConfiguration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-static {p0, v0}, Lcom/zendesk/sdk/requests/RequestActivity;->startActivity(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    .line 339
    return-void
.end method

.method public showSearchResults(Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "searchArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;"
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getSearchFragment()Lcom/zendesk/sdk/support/help/HelpSearchFragment;

    move-result-object v0

    .line 292
    .local v0, "searchFragment":Lcom/zendesk/sdk/support/help/HelpSearchFragment;
    invoke-virtual {v0, p1, p2}, Lcom/zendesk/sdk/support/help/HelpSearchFragment;->updateResults(Ljava/util/List;Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method public startWithHelp()V
    .locals 2

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->noFragmentAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/support/SupportMvp$Presenter;->initWithBundle(Landroid/os/Bundle;)V

    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/SupportActivity;->invalidateOptionsMenu()V

    .line 142
    return-void

    .line 137
    :cond_1
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;

    if-eqz v0, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportActivity;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->setPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V

    goto :goto_0
.end method
