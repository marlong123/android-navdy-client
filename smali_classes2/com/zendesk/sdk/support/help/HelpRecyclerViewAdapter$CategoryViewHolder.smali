.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;
.super Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
.source "HelpRecyclerViewAdapter.java"


# annotations
.annotation build Landroid/support/annotation/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CategoryViewHolder"
.end annotation


# static fields
.field private static final ROTATION_END_LEVEL:I = 0x2710

.field private static final ROTATION_PROPERTY_NAME:Ljava/lang/String; = "level"

.field private static final ROTATION_START_LEVEL:I


# instance fields
.field private expanded:Z

.field private expanderDrawable:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V
    .locals 6
    .param p1, "this$0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 183
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .line 184
    invoke-direct {p0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;-><init>(Landroid/view/View;)V

    move-object v1, p2

    .line 186
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->textView:Landroid/widget/TextView;

    .line 189
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/zendesk/sdk/R$drawable;->ic_expand_more:I

    .line 188
    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/graphics/drawable/DrawableCompat;->wrap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    .line 190
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    const v2, 0x1010038

    .line 191
    invoke-static {p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/zendesk/sdk/R$color;->fallback_text_color:I

    .line 190
    invoke-static {v2, v3, v4}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToColor(ILandroid/content/Context;I)I

    move-result v2

    invoke-static {v1, v2}, Landroid/support/v4/graphics/drawable/DrawableCompat;->setTint(Landroid/graphics/drawable/Drawable;I)V

    .line 192
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-static {v1, v2}, Landroid/support/v4/graphics/drawable/DrawableCompat;->setTintMode(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V

    move-object v0, p2

    .line 194
    check-cast v0, Landroid/widget/TextView;

    .line 196
    .local v0, "textView":Landroid/widget/TextView;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 197
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanded:Z

    return v0
.end method

.method static synthetic access$102(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 173
    iput-boolean p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanded:Z

    return p1
.end method

.method static synthetic access$300(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->setHighlightColor(Z)V

    return-void
.end method

.method private setHighlightColor(Z)V
    .locals 3
    .param p1, "expanded"    # Z

    .prologue
    .line 236
    if-eqz p1, :cond_0

    .line 237
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$500(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 238
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$500(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 243
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$600(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 241
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$600(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0
.end method


# virtual methods
.method public bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
    .locals 4
    .param p1, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;
    .param p2, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 206
    if-nez p1, :cond_0

    .line 207
    const-string v2, "HelpRecyclerViewAdapter"

    const-string v3, "Category item was null, cannot bind"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    :goto_0
    return-void

    .line 211
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->textView:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 213
    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    .line 214
    .local v0, "categoryItem":Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->isExpanded()Z

    move-result v2

    iput-boolean v2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanded:Z

    .line 215
    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanderDrawable:Landroid/graphics/drawable/Drawable;

    iget-boolean v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanded:Z

    if-eqz v3, :cond_1

    const/16 v1, 0x2710

    :cond_1
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 216
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->isExpanded()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->setHighlightColor(Z)V

    .line 218
    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->textView:Landroid/widget/TextView;

    new-instance v2, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->expanded:Z

    return v0
.end method
