.class Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "HelpSearchRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HelpSearchViewHolder"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private subtitleTextView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

.field private titleTextView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;Landroid/view/View;Landroid/content/Context;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    .line 141
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 143
    sget v0, Lcom/zendesk/sdk/R$id;->title:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 144
    sget v0, Lcom/zendesk/sdk/R$id;->subtitle:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->subtitleTextView:Landroid/widget/TextView;

    .line 145
    iput-object p3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->context:Landroid/content/Context;

    .line 146
    return-void
.end method


# virtual methods
.method bindTo(Lcom/zendesk/sdk/model/helpcenter/SearchArticle;)V
    .locals 10
    .param p1, "searchArticle"    # Lcom/zendesk/sdk/model/helpcenter/SearchArticle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, -0x1

    .line 150
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v4

    if-nez v4, :cond_1

    .line 151
    :cond_0
    const-string v3, "HelpSearchRecyclerViewAdapter"

    const-string v4, "The article was null, cannot bind the view."

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    :goto_0
    return-void

    .line 155
    :cond_1
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/helpcenter/Article;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 156
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v4

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/helpcenter/Article;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 159
    .local v2, "title":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    invoke-static {v4}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    move v1, v3

    .line 163
    .local v1, "startIndex":I
    :goto_2
    if-eq v1, v3, :cond_4

    .line 164
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 166
    .local v0, "spannableString":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    .line 168
    invoke-static {v4}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    const/16 v5, 0x12

    .line 166
    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 171
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    .end local v0    # "spannableString":Landroid/text/SpannableString;
    :goto_3
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->subtitleTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->context:Landroid/content/Context;

    sget v5, Lcom/zendesk/sdk/R$string;->guide_search_subtitle_format:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    .line 178
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getCategory()Lcom/zendesk/sdk/model/helpcenter/Category;

    move-result-object v7

    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/Category;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getSection()Lcom/zendesk/sdk/model/helpcenter/Section;

    move-result-object v7

    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/Section;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    .line 176
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->itemView:Landroid/view/View;

    new-instance v4, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;

    invoke-direct {v4, p0, p1}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;-><init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;Lcom/zendesk/sdk/model/helpcenter/SearchArticle;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 156
    .end local v1    # "startIndex":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    const-string v2, ""

    goto :goto_1

    .line 161
    .restart local v2    # "title":Ljava/lang/String;
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;

    invoke-static {v5}, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_2

    .line 173
    .restart local v1    # "startIndex":I
    :cond_4
    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
