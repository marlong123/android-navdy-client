.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;
.super Ljava/lang/Object;
.source "HelpRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

.field final synthetic val$categoryItem:Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->val$categoryItem:Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    iput p3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x2710

    const/4 v1, 0x0

    .line 221
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    iget-object v3, v3, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v3}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$200(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->val$categoryItem:Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    iget v5, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->val$position:I

    invoke-interface {v3, v4, v5}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->onCategoryClick(Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;I)Z

    move-result v3

    invoke-static {v0, v3}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$102(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Z)Z

    .line 223
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .line 224
    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$300(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const-string v4, "level"

    const/4 v0, 0x2

    new-array v5, v0, [I

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .line 226
    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$100(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    aput v0, v5, v1

    const/4 v0, 0x1

    iget-object v6, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    .line 227
    invoke-static {v6}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$100(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_1
    aput v2, v5, v0

    .line 223
    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 228
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 230
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    invoke-static {v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$100(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;->access$400(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;Z)V

    .line 231
    return-void

    :cond_0
    move v0, v2

    .line 226
    goto :goto_0

    :cond_1
    move v2, v1

    .line 227
    goto :goto_1
.end method
