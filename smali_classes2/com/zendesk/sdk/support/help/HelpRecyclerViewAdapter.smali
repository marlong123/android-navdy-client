.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HelpRecyclerViewAdapter.java"

# interfaces
.implements Lcom/zendesk/sdk/support/help/HelpMvp$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ExtraPaddingViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$NoResultsViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$LoadingViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;,
        Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;",
        ">;",
        "Lcom/zendesk/sdk/support/help/HelpMvp$View;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HelpRecyclerViewAdapter"


# instance fields
.field private context:Landroid/content/Context;

.field private defaultCategoryTitleColour:I

.field private highlightCategoryTitleColour:I

.field private presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/support/SupportUiConfig;)V
    .locals 3
    .param p1, "supportUiConfig"    # Lcom/zendesk/sdk/support/SupportUiConfig;

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 55
    new-instance v0, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;

    new-instance v1, Lcom/zendesk/sdk/support/help/HelpModel;

    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 57
    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/zendesk/sdk/support/help/HelpModel;-><init>(Lcom/zendesk/sdk/network/HelpCenterProvider;)V

    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 58
    invoke-virtual {v2}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v2

    invoke-interface {v2}, Lcom/zendesk/sdk/network/impl/ProviderStore;->networkInfoProvider()Lcom/zendesk/sdk/network/NetworkInfoProvider;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/zendesk/sdk/support/help/HelpAdapterPresenter;-><init>(Lcom/zendesk/sdk/support/help/HelpMvp$View;Lcom/zendesk/sdk/support/help/HelpMvp$Model;Lcom/zendesk/sdk/network/NetworkInfoProvider;Lcom/zendesk/sdk/support/SupportUiConfig;)V

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .prologue
    .line 36
    iget v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->highlightCategoryTitleColour:I

    return v0
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .prologue
    .line 36
    iget v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->defaultCategoryTitleColour:I

    return v0
.end method

.method private inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "layoutId"    # I

    .prologue
    .line 148
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addItem(ILcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->notifyItemInserted(I)V

    .line 81
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->getItemCount()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 91
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->context:Landroid/content/Context;

    .line 93
    sget v0, Lcom/zendesk/sdk/R$attr;->colorPrimary:I

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->context:Landroid/content/Context;

    sget v2, Lcom/zendesk/sdk/R$color;->fallback_text_color:I

    invoke-static {v0, v1, v2}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToColor(ILandroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->highlightCategoryTitleColour:I

    .line 95
    const v0, 0x1010036

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->context:Landroid/content/Context;

    sget v2, Lcom/zendesk/sdk/R$color;->fallback_text_color:I

    invoke-static {v0, v1, v2}, Lcom/zendesk/sdk/util/UiUtils;->themeAttributeToColor(ILandroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->defaultCategoryTitleColour:I

    .line 98
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->onAttached()V

    .line 99
    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->onBindViewHolder(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 159
    if-nez p1, :cond_0

    .line 160
    const-string v0, "HelpRecyclerViewAdapter"

    const-string v1, "Holder was null, possible unexpected item type"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0, p2}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->getItemForBinding(I)Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$HelpViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 112
    packed-switch p2, :pswitch_data_0

    .line 142
    :pswitch_0
    const-string v1, "HelpRecyclerViewAdapter"

    const-string v2, "Unknown item type, returning null for holder"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 114
    :pswitch_1
    sget v1, Lcom/zendesk/sdk/R$layout;->row_category:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 115
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$CategoryViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 118
    .end local v0    # "view":Landroid/view/View;
    :pswitch_2
    sget v1, Lcom/zendesk/sdk/R$layout;->row_section:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 119
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SectionViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 122
    .end local v0    # "view":Landroid/view/View;
    :pswitch_3
    sget v1, Lcom/zendesk/sdk/R$layout;->row_article:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 123
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 126
    .end local v0    # "view":Landroid/view/View;
    :pswitch_4
    sget v1, Lcom/zendesk/sdk/R$layout;->row_action:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 127
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 130
    .end local v0    # "view":Landroid/view/View;
    :pswitch_5
    sget v1, Lcom/zendesk/sdk/R$layout;->row_loading_progress:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 131
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$LoadingViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$LoadingViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 134
    .end local v0    # "view":Landroid/view/View;
    :pswitch_6
    sget v1, Lcom/zendesk/sdk/R$layout;->row_no_articles_found:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 135
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$NoResultsViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$NoResultsViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 138
    .end local v0    # "view":Landroid/view/View;
    :pswitch_7
    sget v1, Lcom/zendesk/sdk/R$layout;->row_padding:I

    invoke-direct {p0, p1, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->inflateView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    .line 139
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ExtraPaddingViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ExtraPaddingViewHolder;-><init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 104
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->onDetached()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->context:Landroid/content/Context;

    .line 106
    return-void
.end method

.method public removeItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->notifyItemRemoved(I)V

    .line 86
    return-void
.end method

.method setContentUpdateListener(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V
    .locals 1
    .param p1, "listener"    # Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->presenter:Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->setContentPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V

    .line 71
    :cond_0
    return-void
.end method

.method public showItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 76
    return-void
.end method
