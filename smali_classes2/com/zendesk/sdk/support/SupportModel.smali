.class Lcom/zendesk/sdk/support/SupportModel;
.super Ljava/lang/Object;
.source "SupportModel.java"

# interfaces
.implements Lcom/zendesk/sdk/support/SupportMvp$Model;


# instance fields
.field private provider:Lcom/zendesk/sdk/network/HelpCenterProvider;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->helpCenterProvider()Lcom/zendesk/sdk/network/HelpCenterProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportModel;->provider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    .line 24
    return-void
.end method


# virtual methods
.method public getSettings(Lcom/zendesk/service/ZendeskCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/settings/SafeMobileSettings;>;"
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->hasStoredSdkSettings()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    .line 44
    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->sdkSettingsStorage()Lcom/zendesk/sdk/storage/SdkSettingsStorage;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/SdkSettingsStorage;->getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    .line 43
    invoke-virtual {p1, v0}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 48
    :goto_0
    return-void

    .line 46
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/impl/ProviderStore;->sdkSettingsProvider()Lcom/zendesk/sdk/network/SdkSettingsProvider;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/zendesk/sdk/network/SdkSettingsProvider;->getSettings(Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public search(Ljava/util/List;Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "labelNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/SearchArticle;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "categoryIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p2, "sectionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .local p5, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/SearchArticle;>;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportModel;->provider:Lcom/zendesk/sdk/network/HelpCenterProvider;

    new-instance v1, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;-><init>()V

    .line 31
    invoke-virtual {v1, p3}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->withQuery(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;

    move-result-object v1

    .line 32
    invoke-virtual {v1, p1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;

    move-result-object v1

    .line 33
    invoke-virtual {v1, p2}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;

    move-result-object v1

    .line 34
    invoke-virtual {v1, p4}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;

    move-result-object v1

    .line 35
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch$Builder;->build()Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;

    move-result-object v1

    .line 29
    invoke-interface {v0, v1, p5}, Lcom/zendesk/sdk/network/HelpCenterProvider;->searchArticles(Lcom/zendesk/sdk/model/helpcenter/HelpCenterSearch;Lcom/zendesk/service/ZendeskCallback;)V

    .line 37
    return-void
.end method
