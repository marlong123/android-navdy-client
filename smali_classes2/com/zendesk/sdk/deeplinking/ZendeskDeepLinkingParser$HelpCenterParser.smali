.class Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HelpCenterParser;
.super Ljava/lang/Object;
.source "ZendeskDeepLinkingParser.java"

# interfaces
.implements Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HelpCenterParser"
.end annotation


# static fields
.field private static final HC_PATH_ELEMENT_ARTICLE:Ljava/lang/String; = "articles"

.field private static final HC_PATH_ELEMENT_HC:Ljava/lang/String; = "hc"

.field private static final HC_PATH_ELEMENT_NAME_SEPARATOR:Ljava/lang/String; = "-"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private extractArticleId(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    .locals 10
    .param p1, "pathSegment"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 220
    const-string v8, "-"

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 222
    .local v6, "split":[Ljava/lang/String;
    invoke-static {v6}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty([Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 226
    const/4 v8, 0x0

    :try_start_0
    aget-object v8, v6, v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 231
    .local v0, "articleId":Ljava/lang/Long;
    const-string v1, ""

    .line 233
    .local v1, "articleName":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 234
    .local v2, "articleNameBuilder":Ljava/lang/StringBuilder;
    array-length v7, v6

    const/4 v8, 0x1

    if-le v7, v8, :cond_2

    .line 235
    const/4 v4, 0x1

    .local v4, "i":I
    array-length v5, v6

    .local v5, "len":I
    :goto_0
    if-ge v4, v5, :cond_1

    .line 236
    aget-object v7, v6, v4

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const/16 v7, 0x20

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 235
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "articleId":Ljava/lang/Long;
    .end local v1    # "articleName":Ljava/lang/String;
    .end local v2    # "articleNameBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "i":I
    .end local v5    # "len":I
    :catch_0
    move-exception v3

    .line 244
    :cond_0
    :goto_1
    return-object v7

    .line 239
    .restart local v0    # "articleId":Ljava/lang/Long;
    .restart local v1    # "articleName":Ljava/lang/String;
    .restart local v2    # "articleNameBuilder":Ljava/lang/StringBuilder;
    .restart local v4    # "i":I
    .restart local v5    # "len":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 241
    .end local v4    # "i":I
    .end local v5    # "len":I
    :cond_2
    new-instance v7, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    invoke-direct {v7, v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public parse(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .locals 10
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 171
    sget-object v7, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v7}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getZendeskUrl()Ljava/lang/String;

    move-result-object v6

    .line 173
    .local v6, "zendeskUrl":Ljava/lang/String;
    invoke-static {v6}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 174
    const-string v7, "ZendeskDeepLinkingParser"

    const-string v8, "Zendesk not initialised."

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    :cond_0
    :goto_0
    return-object v2

    .line 178
    :cond_1
    invoke-static {v6}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v5

    .line 179
    .local v5, "zendeskHttpUrl":Lokhttp3/HttpUrl;
    invoke-static {p1}, Lokhttp3/HttpUrl;->parse(Ljava/lang/String;)Lokhttp3/HttpUrl;

    move-result-object v3

    .line 181
    .local v3, "linkHttpUrl":Lokhttp3/HttpUrl;
    if-eqz v3, :cond_0

    if-eqz v5, :cond_0

    .line 185
    invoke-virtual {v5}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 189
    invoke-virtual {v3}, Lokhttp3/HttpUrl;->pathSegments()Ljava/util/List;

    move-result-object v4

    .line 192
    .local v4, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x4

    if-ge v7, v8, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x5

    if-gt v7, v8, :cond_0

    .line 198
    :cond_2
    const-string v7, "articles"

    invoke-interface {v4, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 199
    .local v1, "articleKeywordPathIndex":I
    const-string v7, "hc"

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    if-eq v1, v7, :cond_3

    const/4 v7, 0x2

    if-ne v1, v7, :cond_0

    .line 204
    :cond_3
    add-int/lit8 v7, v1, 0x2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-ne v7, v8, :cond_0

    .line 208
    add-int/lit8 v7, v1, 0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$HelpCenterParser;->extractArticleId(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    move-result-object v0

    .line 210
    .local v0, "articleInformation":Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    if-eqz v0, :cond_0

    .line 211
    new-instance v2, Landroid/content/Intent;

    const-class v7, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {v2, p2, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    .local v2, "intent":Landroid/content/Intent;
    const-string v7, "article_simple"

    invoke-virtual {v2, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method
