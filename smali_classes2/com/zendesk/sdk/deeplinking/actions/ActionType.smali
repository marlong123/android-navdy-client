.class public final enum Lcom/zendesk/sdk/deeplinking/actions/ActionType;
.super Ljava/lang/Enum;
.source "ActionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/deeplinking/actions/ActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/deeplinking/actions/ActionType;

.field public static final enum RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 11
    new-instance v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    const-string v1, "RELOAD_COMMENT_STREAM"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/deeplinking/actions/ActionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    .line 6
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    sget-object v1, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->RELOAD_COMMENT_STREAM:Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->$VALUES:[Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/deeplinking/actions/ActionType;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/zendesk/sdk/deeplinking/actions/ActionType;->$VALUES:[Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/deeplinking/actions/ActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/deeplinking/actions/ActionType;

    return-object v0
.end method
