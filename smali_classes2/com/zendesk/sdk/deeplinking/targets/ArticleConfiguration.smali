.class public Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
.super Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
.source "ArticleConfiguration.java"


# instance fields
.field private mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

.field private final mLocale:Ljava/lang/String;

.field private mSimpleArticle:Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V
    .locals 1
    .param p1, "article"    # Lcom/zendesk/sdk/model/helpcenter/Article;
    .param p2, "locale"    # Ljava/lang/String;
    .param p4, "fallBackActivity"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p3, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    sget-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Article:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    invoke-direct {p0, v0, p3, p4}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;-><init>(Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;Ljava/util/List;Landroid/content/Intent;)V

    .line 21
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 22
    iput-object p2, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mLocale:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V
    .locals 1
    .param p1, "article"    # Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    .param p2, "locale"    # Ljava/lang/String;
    .param p4, "fallBackActivity"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 26
    .local p3, "backStackActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/Intent;>;"
    sget-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;->Article:Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    invoke-direct {p0, v0, p3, p4}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;-><init>(Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;Ljava/util/List;Landroid/content/Intent;)V

    .line 27
    iput-object p1, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mSimpleArticle:Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    .line 28
    iput-object p2, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mLocale:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mArticle:Lcom/zendesk/sdk/model/helpcenter/Article;

    return-object v0
.end method

.method public bridge synthetic getBackStackActivities()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getDeepLinkType()Lcom/zendesk/sdk/deeplinking/targets/DeepLinkType;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getFallbackActivity()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getSimpleArticle()Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->mSimpleArticle:Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    return-object v0
.end method
