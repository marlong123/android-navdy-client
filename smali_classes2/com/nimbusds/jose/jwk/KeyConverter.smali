.class public Lcom/nimbusds/jose/jwk/KeyConverter;
.super Ljava/lang/Object;
.source "KeyConverter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toJavaKeys(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/nimbusds/jose/jwk/JWK;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/security/Key;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "jwkList":Ljava/util/List;, "Ljava/util/List<Lcom/nimbusds/jose/jwk/JWK;>;"
    if-nez p0, :cond_1

    .line 35
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 54
    :cond_0
    return-object v2

    .line 38
    :cond_1
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 39
    .local v2, "out":Ljava/util/List;, "Ljava/util/List<Ljava/security/Key;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nimbusds/jose/jwk/JWK;

    .line 41
    .local v0, "jwk":Lcom/nimbusds/jose/jwk/JWK;
    :try_start_0
    instance-of v4, v0, Lcom/nimbusds/jose/jwk/AssymetricJWK;

    if-eqz v4, :cond_3

    .line 42
    check-cast v0, Lcom/nimbusds/jose/jwk/AssymetricJWK;

    .end local v0    # "jwk":Lcom/nimbusds/jose/jwk/JWK;
    invoke-interface {v0}, Lcom/nimbusds/jose/jwk/AssymetricJWK;->toKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    .line 43
    .local v1, "keyPair":Ljava/security/KeyPair;
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 45
    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .end local v1    # "keyPair":Ljava/security/KeyPair;
    :catch_0
    move-exception v4

    goto :goto_0

    .line 47
    .restart local v0    # "jwk":Lcom/nimbusds/jose/jwk/JWK;
    :cond_3
    instance-of v4, v0, Lcom/nimbusds/jose/jwk/SecretJWK;

    if-eqz v4, :cond_2

    .line 48
    check-cast v0, Lcom/nimbusds/jose/jwk/SecretJWK;

    .end local v0    # "jwk":Lcom/nimbusds/jose/jwk/JWK;
    invoke-interface {v0}, Lcom/nimbusds/jose/jwk/SecretJWK;->toSecretKey()Ljavax/crypto/SecretKey;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/nimbusds/jose/JOSEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
