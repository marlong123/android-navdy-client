.class public Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
.super Ljava/lang/Object;
.source "JOSEMatcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/nimbusds/jose/proc/JOSEMatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private algs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/Algorithm;",
            ">;"
        }
    .end annotation
.end field

.field private classes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;>;"
        }
    .end annotation
.end field

.field private encs:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;"
        }
    .end annotation
.end field

.field private jkus:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field private kids:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public algorithm(Lcom/nimbusds/jose/Algorithm;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "alg"    # Lcom/nimbusds/jose/Algorithm;

    .prologue
    .line 165
    if-nez p1, :cond_0

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->algs:Ljava/util/Set;

    .line 170
    :goto_0
    return-object p0

    .line 168
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->algs:Ljava/util/Set;

    goto :goto_0
.end method

.method public algorithms(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/Algorithm;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "algs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/Algorithm;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->algs:Ljava/util/Set;

    .line 199
    return-object p0
.end method

.method public varargs algorithms([Lcom/nimbusds/jose/Algorithm;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "algs"    # [Lcom/nimbusds/jose/Algorithm;

    .prologue
    .line 183
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->algorithms(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;

    .line 184
    return-object p0
.end method

.method public build()Lcom/nimbusds/jose/proc/JOSEMatcher;
    .locals 6

    .prologue
    .line 350
    new-instance v0, Lcom/nimbusds/jose/proc/JOSEMatcher;

    iget-object v1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->classes:Ljava/util/Set;

    iget-object v2, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->algs:Ljava/util/Set;

    iget-object v3, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->encs:Ljava/util/Set;

    iget-object v4, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->jkus:Ljava/util/Set;

    iget-object v5, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->kids:Ljava/util/Set;

    invoke-direct/range {v0 .. v5}, Lcom/nimbusds/jose/proc/JOSEMatcher;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    return-object v0
.end method

.method public encryptionMethod(Lcom/nimbusds/jose/EncryptionMethod;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "enc"    # Lcom/nimbusds/jose/EncryptionMethod;

    .prologue
    .line 213
    if-nez p1, :cond_0

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->encs:Ljava/util/Set;

    .line 218
    :goto_0
    return-object p0

    .line 216
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->encs:Ljava/util/Set;

    goto :goto_0
.end method

.method public encryptionMethods(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/nimbusds/jose/EncryptionMethod;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 246
    .local p1, "encs":Ljava/util/Set;, "Ljava/util/Set<Lcom/nimbusds/jose/EncryptionMethod;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->encs:Ljava/util/Set;

    .line 247
    return-object p0
.end method

.method public varargs encryptionMethods([Lcom/nimbusds/jose/EncryptionMethod;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "encs"    # [Lcom/nimbusds/jose/EncryptionMethod;

    .prologue
    .line 231
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->encryptionMethods(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;

    .line 232
    return-object p0
.end method

.method public joseClass(Ljava/lang/Class;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/nimbusds/jose/JOSEObject;>;"
    if-nez p1, :cond_0

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->classes:Ljava/util/Set;

    .line 122
    :goto_0
    return-object p0

    .line 120
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->classes:Ljava/util/Set;

    goto :goto_0
.end method

.method public joseClasses(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;>;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "classes":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lcom/nimbusds/jose/JOSEObject;>;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->classes:Ljava/util/Set;

    .line 151
    return-object p0
.end method

.method public varargs joseClasses([Ljava/lang/Class;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "classes"    # [Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<+",
            "Lcom/nimbusds/jose/JOSEObject;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->joseClasses(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;

    .line 136
    return-object p0
.end method

.method public jwkURL(Ljava/net/URI;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "jku"    # Ljava/net/URI;

    .prologue
    .line 260
    if-nez p1, :cond_0

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->jkus:Ljava/util/Set;

    .line 265
    :goto_0
    return-object p0

    .line 263
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->jkus:Ljava/util/Set;

    goto :goto_0
.end method

.method public jwkURLs(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/net/URI;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 292
    .local p1, "jkus":Ljava/util/Set;, "Ljava/util/Set<Ljava/net/URI;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->jkus:Ljava/util/Set;

    .line 293
    return-object p0
.end method

.method public varargs jwkURLs([Ljava/net/URI;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "jkus"    # [Ljava/net/URI;

    .prologue
    .line 278
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->jwkURLs(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;

    .line 279
    return-object p0
.end method

.method public keyID(Ljava/lang/String;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "kid"    # Ljava/lang/String;

    .prologue
    .line 306
    if-nez p1, :cond_0

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->kids:Ljava/util/Set;

    .line 311
    :goto_0
    return-object p0

    .line 309
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->kids:Ljava/util/Set;

    goto :goto_0
.end method

.method public keyIDs(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "kids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->kids:Ljava/util/Set;

    .line 339
    return-object p0
.end method

.method public varargs keyIDs([Ljava/lang/String;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;
    .locals 2
    .param p1, "ids"    # [Ljava/lang/String;

    .prologue
    .line 324
    new-instance v0, Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;->keyIDs(Ljava/util/Set;)Lcom/nimbusds/jose/proc/JOSEMatcher$Builder;

    .line 325
    return-object p0
.end method
