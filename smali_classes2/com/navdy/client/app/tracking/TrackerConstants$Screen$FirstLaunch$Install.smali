.class public Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch$Install;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Install"
.end annotation


# static fields
.field public static final BOX_PICKER:Ljava/lang/String; = "Installation_Box_Picker"

.field public static final BOX_VIEWER_NEW_BOX:Ljava/lang/String; = "Installation_Box_Viewer_New_Box"

.field public static final BOX_VIEWER_NEW_BOX_PLUS_MOUNTS:Ljava/lang/String; = "Installation_Box_Viewer_New_Box_Plus_Mounts"

.field public static final BOX_VIEWER_OLD_BOX:Ljava/lang/String; = "Installation_Box_Viewer_Old_Box"

.field public static final CABLE_PICKER:Ljava/lang/String; = "Installation_Plugging_In"

.field public static final DIAL:Ljava/lang/String; = "Installation_Dial"

.field public static final FINISHED:Ljava/lang/String; = "Installation_Finished"

.field public static final LENS_POSITION:Ljava/lang/String; = "Installation_Lens_Position"

.field public static final MEDIUM_MOUNT_RECOMMENDED:Ljava/lang/String; = "Installation_Mount_Picker_Medium_Mount_Recommended"

.field public static final MEDIUM_TALL_IS_TOO_HIGH:Ljava/lang/String; = "Installation_Mount_Picker_Medium_Tall_Is_Too_High"

.field public static final MEDIUM_TALL_MOUNT:Ljava/lang/String; = "Installation_Medium_Tall_Mount"

.field public static final MEDIUM_TALL_MOUNT_NOT_RECOMMENDED:Ljava/lang/String; = "Installation_Medium_Tall_Mount_Not_Recommended"

.field public static final MOUNT_KIT_NEEDED:Ljava/lang/String; = "Installation_Mount_Kit_Needed"

.field public static final MOUNT_KIT_PURCHASE:Ljava/lang/String; = "Installation_Mount_Kit_Purchase"

.field public static final NO_MOUNT_SUPPORTED:Ljava/lang/String; = "Installation_Mount_Picker_No_Mount_Supported"

.field public static final OVERVIEW:Ljava/lang/String; = "Installation_Overview"

.field public static final PLUG_CLA:Ljava/lang/String; = "Installation_Plug_CLA"

.field public static final PLUG_OBD:Ljava/lang/String; = "Installation_Plug_OBD"

.field public static final POWER_ON:Ljava/lang/String; = "Installation_Power_On"

.field public static final READY_CHECK:Ljava/lang/String; = "Installation_Ready_Check"

.field public static final SECURE:Ljava/lang/String; = "Installation_Secure"

.field public static final SHORT_IS_TOO_LOW:Ljava/lang/String; = "Installation_Mount_Picker_Short_Is_Too_Low"

.field public static final SHORT_MOUNT:Ljava/lang/String; = "Installation_Short_Mount"

.field public static final SHORT_MOUNT_NOT_RECOMMENDED:Ljava/lang/String; = "Installation_Short_Mount_Not_Recommended"

.field public static final TIDY_UP:Ljava/lang/String; = "Installation_Tidy_Up"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
