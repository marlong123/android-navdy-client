.class public Lcom/navdy/client/app/tracking/TrackerConstants$Screen;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Screen"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Web;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Settings;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Home;,
        Lcom/navdy/client/app/tracking/TrackerConstants$Screen$FirstLaunch;
    }
.end annotation


# static fields
.field public static final ACTIVE_TRIP:Ljava/lang/String; = "Active_Trip"

.field public static final DETAILS:Ljava/lang/String; = "Destination_Details"

.field public static final EDIT_FAVORITES:Ljava/lang/String; = "Edit_Favorites"

.field public static final NOTIFICATION_GLANCES:Ljava/lang/String; = "Notification_Glances"

.field public static final ROUTING:Ljava/lang/String; = "Routing"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SPLASH:Ljava/lang/String; = "Splash"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
