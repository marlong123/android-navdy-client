.class public Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Settings;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# static fields
.field public static final AUDIO:Ljava/lang/String; = "Settings_Audio"

.field public static final BLUETOOTH:Ljava/lang/String; = "Settings_Bluetooth_Pairing"

.field public static final CONTACT:Ljava/lang/String; = "Settings_Contact_Us"

.field public static final DEBUG:Ljava/lang/String; = "Settings_Debug"

.field public static final LEGAL:Ljava/lang/String; = "Settings_Legal"

.field public static final MESSAGING:Ljava/lang/String; = "Settings_Messaging"

.field public static final NAVIGATION:Ljava/lang/String; = "Settings_Navigation"

.field public static final PROFILE:Ljava/lang/String; = "Settings_Profile"

.field public static final SUPPORT:Ljava/lang/String; = "Settings_Support"

.field public static final UPDATE:Ljava/lang/String; = "Settings_Update"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
