.class public Lcom/navdy/client/app/tracking/TrackerConstants$Screen$Home;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Home"
.end annotation


# static fields
.field public static final FAVORITES:Ljava/lang/String; = "Home_Favorites"

.field public static final GLANCES:Ljava/lang/String; = "Home_Glances"

.field public static final HOME:Ljava/lang/String; = "Home_Start"

.field public static final TRIPS:Ljava/lang/String; = "Home_Trips"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static tag(I)Ljava/lang/String;
    .locals 3
    .param p0, "index"    # I

    .prologue
    .line 214
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Home_Start"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Home_Favorites"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Home_Glances"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Home_Trips"

    aput-object v2, v0, v1

    .line 215
    .local v0, "fragments":[Ljava/lang/String;
    aget-object v1, v0, p0

    return-object v1
.end method
