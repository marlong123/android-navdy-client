.class public Lcom/navdy/client/app/ui/trips/TripsFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "TripsFragment.java"


# instance fields
.field private cursorAdaptor:Landroid/widget/CursorAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getDate(J)Ljava/lang/String;
    .locals 3
    .param p1, "milliseconds"    # J

    .prologue
    .line 80
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 81
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "never"

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    const v4, 0x7f0300af

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 35
    .local v2, "rootView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/trips/TripsFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 36
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getTripsCursor()Landroid/database/Cursor;

    move-result-object v3

    .line 37
    .local v3, "tripsCursor":Landroid/database/Cursor;
    new-instance v4, Lcom/navdy/client/app/ui/trips/TripsFragment$1;

    const/4 v5, 0x1

    invoke-direct {v4, p0, v0, v3, v5}, Lcom/navdy/client/app/ui/trips/TripsFragment$1;-><init>(Lcom/navdy/client/app/ui/trips/TripsFragment;Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput-object v4, p0, Lcom/navdy/client/app/ui/trips/TripsFragment;->cursorAdaptor:Landroid/widget/CursorAdapter;

    .line 73
    const v4, 0x7f10027d

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 74
    .local v1, "list":Landroid/widget/ListView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/trips/TripsFragment;->cursorAdaptor:Landroid/widget/CursorAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    return-object v2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/client/app/ui/trips/TripsFragment;->cursorAdaptor:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 90
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onDestroy()V

    .line 91
    return-void
.end method
