.class public Lcom/navdy/client/app/ui/base/BaseDialogFragment;
.super Landroid/app/DialogFragment;
.source "BaseDialogFragment.java"


# instance fields
.field protected baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

.field protected final handler:Landroid/os/Handler;

.field protected final logger:Lcom/navdy/service/library/log/Logger;

.field private volatile mIsInForeground:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 15
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 16
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->handler:Landroid/os/Handler;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->mIsInForeground:Z

    return-void
.end method


# virtual methods
.method public isAlive()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInForeground()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->mIsInForeground:Z

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onAttach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 57
    instance-of v0, p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    if-eqz v0, :cond_0

    .line 58
    check-cast p1, Lcom/navdy/client/app/ui/base/BaseActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 60
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 73
    return-void
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onDetach"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->baseActivity:Lcom/navdy/client/app/ui/base/BaseActivity;

    .line 66
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 67
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onPause"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 30
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->mIsInForeground:Z

    .line 32
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "::onResume"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 38
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->mIsInForeground:Z

    .line 40
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 41
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseDialogFragment;->mIsInForeground:Z

    .line 46
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method
