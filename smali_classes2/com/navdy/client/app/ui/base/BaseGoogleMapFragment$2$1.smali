.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->onReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 2
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$300(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerOnLastKnownLocation(Z)V

    .line 148
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$400(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    .line 150
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->val$onShowMapListener:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->val$onShowMapListener:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;->onShow()V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "do not centerOnLastKnownLocation"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$400(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V

    .line 157
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->val$onShowMapListener:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$2;->val$onShowMapListener:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;

    invoke-interface {v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$OnShowMapListener;->onShow()V

    goto :goto_0
.end method
