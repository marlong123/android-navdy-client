.class final Lcom/navdy/client/app/ui/base/BaseActivity$11;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseActivity;->showToast(II[Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$args:[Ljava/lang/Object;

.field final synthetic val$length:I

.field final synthetic val$messageResId:I


# direct methods
.method constructor <init>(I[Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1228
    iput p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$messageResId:I

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$args:[Ljava/lang/Object;

    iput p3, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$length:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1231
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->access$200()Landroid/widget/Toast;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1232
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->access$200()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->cancel()V

    .line 1234
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 1235
    .local v0, "appContext":Landroid/content/Context;
    iget v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$messageResId:I

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$args:[Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1236
    .local v1, "message":Ljava/lang/String;
    iget v2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$11;->val$length:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/ui/base/BaseActivity;->access$202(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1237
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->access$200()Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1238
    return-void
.end method
