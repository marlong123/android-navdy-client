.class public abstract Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "RecyclerViewCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<TVH;>;"
    }
.end annotation


# static fields
.field private static final FOOTER_VIEW:I = -0x2

.field private static final HEADER_VIEW:I = -0x1

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected context:Landroid/content/Context;

.field protected cursor:Landroid/database/Cursor;

.field private cursorAdapter:Landroid/widget/CursorAdapter;

.field protected dataIsValid:Z

.field private dataSetObserver:Landroid/database/DataSetObserver;

.field protected hasFooter:Z

.field protected hasHeader:Z

.field private observerRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hasHeader"    # Z
    .param p3, "hasFooter"    # Z

    .prologue
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    .line 24
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataIsValid:Z

    .line 25
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasHeader:Z

    .line 26
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasFooter:Z

    .line 45
    iput-object p1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->context:Landroid/content/Context;

    .line 46
    iput-boolean p2, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasHeader:Z

    .line 47
    iput-boolean p3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasFooter:Z

    .line 51
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->setHasStableIds(Z)V

    .line 53
    new-instance v0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;-><init>(Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    .line 68
    return-void
.end method

.method private swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 5
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 143
    iget-object v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-ne p1, v3, :cond_0

    .line 144
    const/4 v0, 0x0

    .line 180
    :goto_0
    return-object v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    .line 147
    .local v0, "oldCursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 148
    iget-boolean v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    if-eqz v3, :cond_1

    .line 149
    iget-object v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 150
    sget-object v3, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "observer unregistered old"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 151
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    .line 155
    :cond_1
    iput-object p1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    .line 156
    iget-object v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v3, :cond_2

    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataIsValid:Z

    .line 157
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_3

    .line 158
    sget-object v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "observer registered"

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v3}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 160
    iput-boolean v2, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    .line 163
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->notifyDataSetChanged()V

    .line 166
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursorAdapter:Landroid/widget/CursorAdapter;

    if-nez v1, :cond_4

    .line 167
    new-instance v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$2;

    iget-object v3, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->context:Landroid/content/Context;

    iget-object v4, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-direct {v1, p0, v3, v4, v2}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$2;-><init>(Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;Landroid/content/Context;Landroid/database/Cursor;Z)V

    iput-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursorAdapter:Landroid/widget/CursorAdapter;

    goto :goto_0

    .line 178
    :cond_4
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursorAdapter:Landroid/widget/CursorAdapter;

    iget-object v2, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 133
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 134
    .local v0, "oldCursor":Landroid/database/Cursor;
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 135
    return-void
.end method

.method public close()V
    .locals 3

    .prologue
    .line 184
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    sget-object v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "close"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 187
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    if-eqz v1, :cond_0

    .line 188
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    .line 189
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 190
    sget-object v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "observer unregistered"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_1
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 96
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    const/4 v0, 0x0

    .line 97
    .local v0, "count":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 99
    if-lez v0, :cond_0

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasFooter:Z

    if-eqz v1, :cond_0

    .line 100
    add-int/lit8 v0, v0, 0x1

    .line 103
    :cond_0
    iget-boolean v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasHeader:Z

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    .end local v0    # "count":I
    :cond_1
    return v0
.end method

.method public abstract getItemId(I)J
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 119
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasHeader:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 120
    const/4 v0, -0x1

    .line 125
    :goto_0
    return v0

    .line 122
    :cond_0
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->hasFooter:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 123
    const/4 v0, -0x2

    goto :goto_0

    .line 125
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public abstract getNewCursor()Landroid/database/Cursor;
.end method

.method public notifyDbChanged()V
    .locals 1

    .prologue
    .line 71
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 72
    return-void
.end method

.method public onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 76
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onAttachedToRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 77
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->getNewCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 78
    return-void
.end method

.method public abstract onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVH;I)V"
        }
    .end annotation
.end method

.method public abstract onCreateFooterViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation
.end method

.method public abstract onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation
.end method

.method public abstract onCreateNormalViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")TVH;"
        }
    .end annotation
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)TVH;"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 109
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .line 114
    :goto_0
    return-object v0

    .line 111
    :cond_0
    const/4 v0, -0x2

    if-ne p2, v0, :cond_1

    .line 112
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->onCreateFooterViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->onCreateNormalViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 82
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter<TVH;>;"
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onDetachedFromRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 83
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 84
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->observerRegistered:Z

    .line 87
    sget-object v0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "observer unregistered detached"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->cursor:Landroid/database/Cursor;

    .line 92
    :cond_1
    return-void
.end method
