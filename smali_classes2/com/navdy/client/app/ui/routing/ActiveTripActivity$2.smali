.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$2;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->centerOnDriver(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$2;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 160
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 6
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 152
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v1

    .line 153
    .local v1, "location":Landroid/location/Location;
    if-eqz v1, :cond_0

    .line 154
    new-instance v0, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 155
    .local v0, "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    sget-object v2, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {p1, v0, v2}, Lcom/here/android/mpa/mapping/Map;->setCenter(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 157
    .end local v0    # "geoCoordinate":Lcom/here/android/mpa/common/GeoCoordinate;
    :cond_0
    return-void
.end method
