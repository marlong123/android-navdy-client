.class public Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SearchRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;,
        Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;,
        Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final CONTACT_PHOTO_RES_VALUE:I = 0x0

.field private static final INVALID_DISTANCE:D = -1.0

.field private static final NO_FLAGS:I

.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private card:Landroid/widget/RelativeLayout;

.field private contactPhotos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

.field private googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field private lastQuery:Ljava/lang/String;

.field private listener:Landroid/view/View$OnClickListener;

.field private markers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

.field private searchItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;",
            ">;"
        }
    .end annotation
.end field

.field private final searchType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method constructor <init>(ILcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Landroid/widget/RelativeLayout;)V
    .locals 2
    .param p1, "searchType"    # I
    .param p2, "googleMapFragment"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;
    .param p3, "card"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 143
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 77
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 86
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->EMPTY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->markers:Ljava/util/HashMap;

    .line 144
    iput p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchType:I

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    .line 146
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->contactPhotos:Ljava/util/HashMap;

    .line 147
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    .line 148
    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 149
    iput-object p3, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->card:Landroid/widget/RelativeLayout;

    .line 150
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "bus registered"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;
    .param p1, "x1"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setLastRoutedIfRecent(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->markers:Ljava/util/HashMap;

    return-object v0
.end method

.method private addContactWithMultipleAddress(Lcom/navdy/client/app/framework/models/ContactModel;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "contact"    # Lcom/navdy/client/app/framework/models/ContactModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 872
    .local p2, "preDeduplicatedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 876
    :cond_1
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 877
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 878
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0001

    .line 879
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 880
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 878
    invoke-virtual {v0, v2, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 881
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 883
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 884
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT_PHOTO:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 885
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->contactPhotos:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    :goto_1
    iput-object p2, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    .line 891
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 887
    .end local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    :cond_2
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .restart local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    goto :goto_1
.end method

.method private addContactWithOneAddress(Lcom/navdy/client/app/framework/models/ContactModel;Lcom/navdy/client/app/framework/models/Address;)V
    .locals 10
    .param p1, "contact"    # Lcom/navdy/client/app/framework/models/ContactModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "address"    # Lcom/navdy/client/app/framework/models/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 826
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 869
    :cond_0
    :goto_0
    return-void

    .line 830
    :cond_1
    invoke-static {p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getCleanAddressString(Lcom/navdy/client/app/framework/models/Address;)Ljava/lang/String;

    move-result-object v6

    .line 831
    .local v6, "fullAddress":Ljava/lang/String;
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 836
    iget v0, p2, Lcom/navdy/client/app/framework/models/Address;->type:I

    packed-switch v0, :pswitch_data_0

    .line 848
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    const v2, 0x7f080327

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 852
    .local v3, "addressType":Ljava/lang/String;
    :goto_1
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 853
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 854
    iput-object v6, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 855
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 856
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$Type;->CONTACT:Lcom/navdy/client/app/framework/models/Destination$Type;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->type:Lcom/navdy/client/app/framework/models/Destination$Type;

    .line 857
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/ContactModel;->lookupKey:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->contactLookupKey:Ljava/lang/String;

    .line 858
    iget-wide v8, p1, Lcom/navdy/client/app/framework/models/ContactModel;->id:J

    iput-wide v8, v1, Lcom/navdy/client/app/framework/models/Destination;->lastKnownContactId:J

    .line 859
    iget-wide v8, p1, Lcom/navdy/client/app/framework/models/ContactModel;->lookupTimestamp:J

    iput-wide v8, v1, Lcom/navdy/client/app/framework/models/Destination;->lastContactLookup:J

    .line 861
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 862
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT_PHOTO:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 863
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->contactPhotos:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/client/app/framework/models/ContactModel;->photo:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    :goto_2
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 838
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "addressType":Ljava/lang/String;
    .end local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    const v2, 0x7f080240

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 839
    .restart local v3    # "addressType":Ljava/lang/String;
    goto :goto_1

    .line 841
    .end local v3    # "addressType":Ljava/lang/String;
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    const v2, 0x7f080504

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 842
    .restart local v3    # "addressType":Ljava/lang/String;
    goto :goto_1

    .line 844
    .end local v3    # "addressType":Ljava/lang/String;
    :pswitch_2
    iget-object v3, p2, Lcom/navdy/client/app/framework/models/Address;->label:Ljava/lang/String;

    .line 845
    .restart local v3    # "addressType":Ljava/lang/String;
    goto :goto_1

    .line 865
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->CONTACT:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .restart local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    goto :goto_2

    .line 836
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private addMarker(I)V
    .locals 4
    .param p1, "destinationIndex"    # I

    .prologue
    .line 1015
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-nez v1, :cond_0

    .line 1056
    :goto_0
    return-void

    .line 1019
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;

    invoke-direct {v2, p0, p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$5;-><init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;I)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1053
    :catch_0
    move-exception v0

    .line 1054
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to add marker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private firstItemIsPoweredByGoogleHeader()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 916
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 923
    :cond_0
    :goto_0
    return v1

    .line 919
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 920
    .local v0, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget v2, v2, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v3, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 923
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static getCleanAddressString(Lcom/navdy/client/app/framework/models/Address;)Ljava/lang/String;
    .locals 3
    .param p0, "address"    # Lcom/navdy/client/app/framework/models/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 811
    if-nez p0, :cond_0

    .line 812
    const-string v0, ""

    .line 822
    :goto_0
    return-object v0

    .line 815
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Address;->getFullAddress()Ljava/lang/String;

    move-result-object v0

    .line 817
    .local v0, "fullAddress":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 818
    const-string v0, ""

    goto :goto_0

    .line 821
    :cond_1
    const-string v1, "\n"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 822
    goto :goto_0
.end method

.method private getDeduplicatedAddressesForContact(Lcom/navdy/client/app/framework/models/ContactModel;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "contact"    # Lcom/navdy/client/app/framework/models/ContactModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 790
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 791
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 807
    :cond_1
    return-object v3

    .line 793
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 794
    .local v0, "addedAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 795
    .local v3, "validAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    iget-object v4, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Address;

    .line 797
    .local v1, "address":Lcom/navdy/client/app/framework/models/Address;
    if-eqz v1, :cond_3

    .line 800
    invoke-static {v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getCleanAddressString(Lcom/navdy/client/app/framework/models/Address;)Ljava/lang/String;

    move-result-object v2

    .line 801
    .local v2, "fullAddress":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 804
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static getPriceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "level"    # Ljava/lang/String;

    .prologue
    .line 966
    if-nez p0, :cond_0

    .line 967
    const-string v4, ""

    .line 979
    :goto_0
    return-object v4

    .line 970
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 972
    .local v3, "stringPrice":Ljava/lang/StringBuilder;
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 973
    .local v2, "price":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 974
    const-string v4, "$"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 973
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 976
    .end local v1    # "i":I
    .end local v2    # "price":I
    :catch_0
    move-exception v0

    .line 977
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to parse a price string out of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 979
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private static highlightSearchedText(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 12
    .param p0, "autocompleteName"    # Ljava/lang/String;
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const v11, -0x777778

    const/4 v10, 0x0

    .line 474
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 475
    :cond_0
    const/4 v6, 0x0

    .line 520
    :cond_1
    :goto_0
    return-object v6

    .line 478
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "autocompleteNameLowercase":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 481
    .local v5, "queryLowercase":Ljava/lang/String;
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v8

    invoke-virtual {v8, p0}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v6

    .line 483
    .local v6, "queryWithHighlightedSelection":Landroid/text/Spannable;
    const/4 v7, 0x0

    .line 487
    .local v7, "startIndex":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 488
    .local v3, "length":I
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    .line 491
    .local v4, "queryLength":I
    :cond_3
    move v2, v7

    .line 492
    .local v2, "lastEndIndex":I
    invoke-virtual {v0, v5, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v7

    .line 494
    if-gez v7, :cond_4

    .line 495
    move v7, v2

    .line 515
    :goto_1
    if-ge v7, v3, :cond_1

    .line 517
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v6, v8, v7, v3, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 499
    :cond_4
    add-int v1, v7, v4

    .line 500
    .local v1, "endIndex":I
    if-le v1, v3, :cond_5

    .line 501
    move v1, v3

    .line 504
    :cond_5
    if-le v7, v2, :cond_6

    .line 506
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v8, v11}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v6, v8, v2, v7, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 510
    :cond_6
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    const/high16 v9, -0x1000000

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v6, v8, v7, v1, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 512
    move v7, v1

    .line 513
    if-lt v7, v3, :cond_3

    goto :goto_1
.end method

.method private insertAutoCompleteItem(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;)V
    .locals 1
    .param p1, "newAddress"    # Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .prologue
    .line 576
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->insertAutoCompleteItem(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;Z)V

    .line 577
    return-void
.end method

.method private insertAutoCompleteItem(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;Z)V
    .locals 8
    .param p1, "newAddress"    # Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .param p2, "checkForDuplicates"    # Z

    .prologue
    .line 580
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 581
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 582
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->address:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 583
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    .line 584
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 586
    if-eqz p2, :cond_1

    .line 588
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 589
    .local v6, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    iget-object v2, v6, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 600
    .end local v6    # "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    :goto_0
    return-void

    .line 594
    :cond_1
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 595
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    iget-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 596
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 598
    :cond_2
    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 599
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyItemInserted(I)V

    goto :goto_0
.end method

.method private insertPreviousSearch(Ljava/lang/String;)V
    .locals 7
    .param p1, "previousSearch"    # Ljava/lang/String;

    .prologue
    .line 603
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 605
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iput-object p1, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 606
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->AUTOCOMPLETE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 607
    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    .line 608
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyItemInserted(I)V

    .line 610
    return-void
.end method

.method private static isHeader(I)Z
    .locals 1
    .param p0, "itemViewType"    # I

    .prologue
    .line 466
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLastRoutedIfRecent(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;)V
    .locals 5
    .param p1, "searchItem"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .prologue
    .line 655
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 656
    iget-object v1, p1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    iput v2, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 657
    iget-object v1, p1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v1, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v0

    .line 658
    .local v0, "dateAndTime":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    const v2, 0x7f080276

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->price:Ljava/lang/String;

    .line 659
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->distance:Ljava/lang/Double;

    .line 661
    .end local v0    # "dateAndTime":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setRowHeightTo(Landroid/view/View;I)V
    .locals 3
    .param p1, "row"    # Landroid/view/View;
    .param p2, "dimenRes"    # I

    .prologue
    .line 459
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v1, v2

    .line 460
    .local v1, "pixels":F
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 461
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    float-to-int v2, v1

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 462
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 463
    return-void
.end method

.method private setTitleForHeader(Landroid/view/View;I)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "viewType"    # I

    .prologue
    .line 268
    const v2, 0x7f10035d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 269
    .local v0, "leftTitle":Landroid/widget/TextView;
    const v2, 0x7f10035e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 271
    .local v1, "rightSideImage":Landroid/widget/ImageView;
    if-nez v0, :cond_1

    .line 272
    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "The Search Header view doesn\'t exist"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    if-ne p2, v2, :cond_0

    .line 277
    const v2, 0x7f080568

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 278
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 279
    const v2, 0x7f0200d8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 280
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method addNoResultsItem()V
    .locals 7

    .prologue
    .line 960
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 961
    .local v1, "noResultItem":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 962
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 963
    return-void
.end method

.method varargs addNoResultsItemIfEmpty([Ljava/util/List;)V
    .locals 4
    .param p1, "lists"    # [Ljava/util/List;

    .prologue
    .line 944
    if-eqz p1, :cond_0

    array-length v1, p1

    if-gtz v1, :cond_2

    .line 945
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItem()V

    .line 957
    :cond_1
    :goto_0
    return-void

    .line 949
    :cond_2
    array-length v2, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_4

    aget-object v0, p1, v1

    .line 950
    .local v0, "list":Ljava/util/List;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 949
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 956
    .end local v0    # "list":Ljava/util/List;
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItem()V

    goto :goto_0
.end method

.method addSearchPoweredByGoogleHeader()V
    .locals 8

    .prologue
    .line 927
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 930
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->firstItemIsPoweredByGoogleHeader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 931
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 932
    .local v1, "header":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 933
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    const/4 v7, 0x0

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v7, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method centerMapOnMarkers(Z)V
    .locals 14
    .param p1, "animate"    # Z

    .prologue
    .line 1063
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v9, :cond_6

    .line 1064
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1065
    .local v7, "sanitizedLocations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v9}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v5

    .line 1067
    .local v5, "myLocation":Lcom/navdy/service/library/events/location/Coordinate;
    invoke-static {v5}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1068
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v9, v5, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    iget-object v9, v5, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-direct {v4, v10, v11, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1069
    .local v4, "myLatLong":Lcom/google/android/gms/maps/model/LatLng;
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1072
    .end local v4    # "myLatLong":Lcom/google/android/gms/maps/model/LatLng;
    :cond_0
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    if-eqz v9, :cond_4

    .line 1073
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 1074
    .local v8, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    iget-object v2, v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1076
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v2, :cond_1

    .line 1077
    const/4 v6, 0x0

    .line 1079
    .local v6, "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-wide v10, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v12, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v10, v11, v12, v13}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1080
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    .end local v6    # "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-wide v10, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v12, v2, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-direct {v6, v10, v11, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1085
    .restart local v6    # "position":Lcom/google/android/gms/maps/model/LatLng;
    :cond_2
    :goto_1
    if-eqz v6, :cond_1

    .line 1086
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1081
    :cond_3
    iget-wide v10, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v12, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v10, v11, v12, v13}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1082
    new-instance v6, Lcom/google/android/gms/maps/model/LatLng;

    .end local v6    # "position":Lcom/google/android/gms/maps/model/LatLng;
    iget-wide v10, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v12, v2, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v6, v10, v11, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .restart local v6    # "position":Lcom/google/android/gms/maps/model/LatLng;
    goto :goto_1

    .line 1092
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v6    # "position":Lcom/google/android/gms/maps/model/LatLng;
    .end local v8    # "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    :cond_4
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_7

    .line 1093
    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->builder()Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v1

    .line 1095
    .local v1, "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/model/LatLng;

    .line 1096
    .local v3, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    goto :goto_2

    .line 1099
    .end local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 1100
    .local v0, "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v9, v0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->zoomTo(Lcom/google/android/gms/maps/model/LatLngBounds;Z)V

    .line 1106
    .end local v0    # "bounds":Lcom/google/android/gms/maps/model/LatLngBounds;
    .end local v1    # "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    .end local v5    # "myLocation":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v7    # "sanitizedLocations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    :cond_6
    :goto_3
    return-void

    .line 1101
    .restart local v5    # "myLocation":Lcom/navdy/service/library/events/location/Coordinate;
    .restart local v7    # "sanitizedLocations":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/LatLng;>;"
    :cond_7
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_6

    .line 1102
    const/4 v9, 0x0

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/maps/model/LatLng;

    .line 1103
    .restart local v3    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    const/high16 v10, 0x41600000    # 14.0f

    invoke-virtual {v9, v3, v10, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->moveMap(Lcom/google/android/gms/maps/model/LatLng;FZ)V

    goto :goto_3
.end method

.method clear()V
    .locals 2

    .prologue
    .line 984
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "clear"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 985
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 986
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 987
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->contactPhotos:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 989
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    if-eqz v0, :cond_0

    .line 990
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->googleMapFragment:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$4;-><init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)V

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 1004
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->markers:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 1005
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->markers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1007
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->card:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 1008
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->card:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1010
    :cond_2
    return-void
.end method

.method createSearchForMoreFooter(Ljava/lang/String;)V
    .locals 7
    .param p1, "autocompleteQuery"    # Ljava/lang/String;

    .prologue
    .line 908
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 909
    .local v1, "moreDestination":Lcom/navdy/client/app/framework/models/Destination;
    iput-object p1, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 910
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 911
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 912
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 913
    return-void
.end method

.method getDestinationAt(I)Lcom/navdy/client/app/framework/models/Destination;
    .locals 2
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 219
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getItemAt(I)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    move-result-object v0

    .line 220
    .local v0, "item":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    if-eqz v0, :cond_0

    .line 221
    iget-object v1, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 223
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getDestinationIndexForMarker(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p1, "markerId"    # Ljava/lang/String;

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->markers:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method getItemAt(I)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    .locals 1
    .param p1, "position"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 210
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    .line 211
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 214
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 228
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt p1, v2, :cond_0

    .line 230
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    .line 241
    :goto_0
    return v2

    .line 233
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 234
    .local v1, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    const/4 v0, 0x0

    .line 235
    .local v0, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v1, :cond_1

    .line 236
    iget-object v0, v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 238
    :cond_1
    if-eqz v0, :cond_2

    .line 239
    iget v2, v0, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    goto :goto_0

    .line 241
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 68
    check-cast p1, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->onBindViewHolder(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V
    .locals 0
    .param p1, "searchViewHolder"    # Lcom/navdy/client/app/ui/search/SearchViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setCardData(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V

    .line 288
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/search/SearchViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/search/SearchViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v4, 0x0

    .line 248
    invoke-static {p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->isHeader(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    const v0, 0x7f0300e5

    .line 250
    .local v0, "headerLayout":I
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v0, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 251
    .local v1, "v":Landroid/view/View;
    invoke-direct {p0, v1, p2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setTitleForHeader(Landroid/view/View;I)V

    .line 264
    .end local v0    # "headerLayout":I
    :goto_0
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;-><init>(Landroid/view/View;)V

    :goto_1
    return-object v2

    .line 252
    .end local v1    # "v":Landroid/view/View;
    :cond_0
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    if-ne p2, v2, :cond_1

    .line 253
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300e8

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 254
    .restart local v1    # "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 255
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    if-ne p2, v2, :cond_2

    .line 256
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300e7

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 257
    .restart local v1    # "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 258
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/models/Destination$SearchType;->FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v2

    if-ne p2, v2, :cond_3

    .line 259
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300cc

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 260
    .restart local v1    # "v":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/search/SearchViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 262
    .end local v1    # "v":Landroid/view/View;
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300e6

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method removeSearchPoweredByGoogleHeader()V
    .locals 2

    .prologue
    .line 938
    invoke-direct {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->firstItemIsPoweredByGoogleHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 939
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 941
    :cond_0
    return-void
.end method

.method setCardData(Lcom/navdy/client/app/ui/search/SearchViewHolder;I)V
    .locals 16
    .param p1, "searchViewHolder"    # Lcom/navdy/client/app/ui/search/SearchViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 291
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getItemViewType(I)I

    move-result v6

    .line 293
    .local v6, "itemViewType":I
    sget-object v13, Lcom/navdy/client/app/framework/models/Destination$SearchType;->FOOTER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v13}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v13

    if-ne v6, v13, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 300
    .local v8, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    if-nez v8, :cond_2

    .line 301
    sget-object v13, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unable to get the searchItem at position: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :cond_2
    iget-object v3, v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 307
    .local v3, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-nez v3, :cond_3

    .line 308
    sget-object v13, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unable to get the destination at position: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 312
    :cond_3
    if-nez p1, :cond_4

    .line 313
    sget-object v13, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unable to get the searchViewHolder at position: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_4
    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->isHeader(I)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 318
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->itemView:Landroid/view/View;

    .line 319
    .local v12, "v":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v6}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setTitleForHeader(Landroid/view/View;I)V

    .line 320
    const v13, 0x7f10035e

    invoke-virtual {v12, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 321
    .local v7, "rightSideImage":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    invoke-static {v13}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_7

    .line 322
    if-eqz v7, :cond_5

    .line 323
    const/4 v13, 0x4

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 343
    :cond_5
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getItemCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    move/from16 v0, p2

    if-eq v0, v13, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_9

    .line 344
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->hideBottomDivider()V

    goto/16 :goto_0

    .line 329
    :cond_7
    sget-object v13, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_POWERED_BY_GOOGLE_HEADER:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v13}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v13

    if-ne v6, v13, :cond_5

    .line 330
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_8

    .line 331
    const v13, 0x7f0f00cc

    invoke-virtual {v12, v13}, Landroid/view/View;->setBackgroundResource(I)V

    .line 336
    :goto_2
    if-eqz v7, :cond_5

    .line 337
    const v13, 0x7f0200d8

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 338
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 333
    :cond_8
    const v13, 0x7f0f00c0

    invoke-virtual {v12, v13}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 346
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showLongDivider()V

    goto/16 :goto_0

    .line 351
    .end local v7    # "rightSideImage":Landroid/widget/ImageView;
    .end local v12    # "v":Landroid/view/View;
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getItemCount()I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    move/from16 v0, p2

    if-ne v0, v13, :cond_d

    .line 352
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->hideBottomDivider()V

    .line 359
    :goto_3
    sget-object v13, Lcom/navdy/client/app/framework/models/Destination$SearchType;->NO_RESULTS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v13}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v13

    if-eq v6, v13, :cond_0

    sget-object v13, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 360
    invoke-virtual {v13}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v13

    if-eq v6, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    .line 361
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    move/from16 v0, p2

    if-ge v0, v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    .line 362
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    .line 366
    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v11

    .line 368
    .local v11, "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v10, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    .line 370
    .local v10, "title":Ljava/lang/String;
    iget v13, v3, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v14, Lcom/navdy/client/app/framework/models/Destination$SearchType;->MORE:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v14}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v14

    if-ne v13, v14, :cond_f

    .line 371
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setTitle(Ljava/lang/String;)V

    .line 383
    :goto_4
    iget-object v9, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    .line 385
    .local v9, "subTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_13

    .line 386
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->lastQuery:Ljava/lang/String;

    invoke-static {v9, v13}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->highlightSearchedText(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v4

    .line 387
    .local v4, "highlightedText":Landroid/text/Spannable;
    if-eqz v4, :cond_12

    .line 388
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setDetails(Landroid/text/Spannable;)V

    .line 396
    .end local v4    # "highlightedText":Landroid/text/Spannable;
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_14

    .line 397
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    const v14, 0x7f0b0088

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setRowHeightTo(Landroid/view/View;I)V

    .line 402
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_15

    const/4 v5, 0x1

    .line 404
    .local v5, "isAutocomplete":Z
    :goto_7
    sget-object v13, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$6;->$SwitchMap$com$navdy$client$app$ui$search$SearchRecyclerAdapter$ImageEnum:[I

    iget-object v14, v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->imageEnum:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    invoke-virtual {v14}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 427
    :pswitch_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setImage(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;Z)V

    .line 428
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->hideNavButton()V

    .line 431
    :goto_8
    sget-object v13, Lcom/navdy/client/app/framework/models/Destination$SearchType;->CONTACT_ADDRESS_SELECTION:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v13}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v13

    if-eq v6, v13, :cond_b

    move-object/from16 v0, p0

    iget v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchType:I

    .line 432
    invoke-static {v13}, Lcom/navdy/client/app/ui/search/SearchActivity;->isFavoriteSearch(I)Z

    move-result v13

    if-eqz v13, :cond_c

    iget v13, v3, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    sget-object v14, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    .line 433
    invoke-virtual {v14}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v14

    if-eq v13, v14, :cond_c

    .line 434
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->hideNavButton()V

    .line 437
    :cond_c
    iget-object v13, v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->distance:Ljava/lang/Double;

    invoke-virtual {v13}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setDistance(D)V

    .line 438
    iget-object v13, v8, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->price:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setPrice(Ljava/lang/String;)V

    .line 440
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    new-instance v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v14, v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$1;-><init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;I)V

    invoke-virtual {v13, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->navButton:Landroid/widget/ImageButton;

    new-instance v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$2;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v14, v0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$2;-><init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;I)V

    invoke-virtual {v13, v14}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 353
    .end local v5    # "isAutocomplete":Z
    .end local v9    # "subTitle":Ljava/lang/String;
    .end local v10    # "title":Ljava/lang/String;
    .end local v11    # "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_e

    .line 354
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showLongDivider()V

    goto/16 :goto_3

    .line 356
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showShortDivider()V

    goto/16 :goto_3

    .line 372
    .restart local v10    # "title":Ljava/lang/String;
    .restart local v11    # "titleAndSubtitle":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v14, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->AUTO_COMPLETE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v13, v14, :cond_11

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->lastQuery:Ljava/lang/String;

    invoke-static {v10, v13}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->highlightSearchedText(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v4

    .line 374
    .restart local v4    # "highlightedText":Landroid/text/Spannable;
    if-eqz v4, :cond_10

    .line 375
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setTitle(Landroid/text/Spannable;)V

    goto/16 :goto_4

    .line 377
    :cond_10
    const v13, 0x7f0f0059

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v13}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setTitle(Ljava/lang/String;I)V

    goto/16 :goto_4

    .line 380
    .end local v4    # "highlightedText":Landroid/text/Spannable;
    :cond_11
    const v13, 0x7f0f0010

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v13}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setTitle(Ljava/lang/String;I)V

    goto/16 :goto_4

    .line 390
    .restart local v4    # "highlightedText":Landroid/text/Spannable;
    .restart local v9    # "subTitle":Ljava/lang/String;
    :cond_12
    const v13, 0x7f0f0059

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v13}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setDetails(Ljava/lang/String;I)V

    goto/16 :goto_5

    .line 393
    .end local v4    # "highlightedText":Landroid/text/Spannable;
    :cond_13
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setDetails(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 399
    :cond_14
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchViewHolder;->row:Landroid/view/View;

    const v14, 0x7f0b0089

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setRowHeightTo(Landroid/view/View;I)V

    goto/16 :goto_6

    .line 402
    :cond_15
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 407
    .restart local v5    # "isAutocomplete":Z
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->contactPhotos:Ljava/util/HashMap;

    invoke-virtual {v13, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 408
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setImage(Landroid/graphics/Bitmap;)V

    .line 409
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showNavButton()V

    goto/16 :goto_8

    .line 413
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    :pswitch_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setImage(Ljava/lang/String;)V

    .line 414
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showNavButton()V

    goto/16 :goto_8

    .line 417
    :pswitch_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setImage(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;Z)V

    .line 418
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showNavButton()V

    goto/16 :goto_8

    .line 421
    :pswitch_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->setImage(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;Z)V

    .line 422
    invoke-virtual/range {p1 .. p1}, Lcom/navdy/client/app/ui/search/SearchViewHolder;->showDeleteButton()V

    goto/16 :goto_8

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 199
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setClickListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 200
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->listener:Landroid/view/View$OnClickListener;

    .line 201
    return-void
.end method

.method setLastQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->lastQuery:Ljava/lang/String;

    .line 1115
    return-void
.end method

.method setUpServicesAndHistory(Z)V
    .locals 14
    .param p1, "showService"    # Z

    .prologue
    .line 156
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    .line 157
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addSearchPoweredByGoogleHeader()V

    .line 164
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->navdyLocationManager:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartLocation()Landroid/location/Location;

    move-result-object v11

    .line 166
    .local v11, "lastLocation":Landroid/location/Location;
    if-eqz v11, :cond_1

    .line 167
    invoke-virtual {v11}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_1

    .line 168
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 169
    .local v1, "servicesDestination":Lcom/navdy/client/app/framework/models/Destination;
    const-string v0, ""

    iput-object v0, v1, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 170
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SERVICES_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 171
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const-string v3, ""

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->NONE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    .end local v1    # "servicesDestination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v11    # "lastLocation":Landroid/location/Location;
    :cond_1
    const/4 v8, 0x0

    .line 179
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getSearchHistoryCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 180
    :goto_0
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 181
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 182
    .local v10, "idIndex":I
    const-string v0, "query"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 184
    .local v13, "queryIndex":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 185
    .local v9, "id":I
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 187
    .local v12, "query":Ljava/lang/String;
    new-instance v3, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v3}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 188
    .local v3, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iput v9, v3, Lcom/navdy/client/app/framework/models/Destination;->id:I

    .line 189
    iput-object v12, v3, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    .line 190
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->SEARCH_HISTORY:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v3, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 191
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const-string v5, ""

    sget-object v6, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->SEARCH_HISTORY:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 194
    .end local v3    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v9    # "id":I
    .end local v10    # "idIndex":I
    .end local v12    # "query":Ljava/lang/String;
    .end local v13    # "queryIndex":I
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v0

    :cond_2
    invoke-static {v8}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 196
    return-void
.end method

.method updateNearbySearchResultsWithKnownDestinationData()V
    .locals 3

    .prologue
    .line 713
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    .line 714
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;>;"
    new-instance v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;

    invoke-direct {v1, p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;-><init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;Ljava/util/List;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    .line 728
    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 729
    return-void
.end method

.method updateRecyclerViewWithAutocomplete(Ljava/util/List;Ljava/util/ArrayList;Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "constraint"    # Ljava/lang/CharSequence;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "newAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    .local p2, "previousSearches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 531
    if-nez p1, :cond_0

    .line 532
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "newAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-direct {p1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 535
    .restart local p1    # "newAddresses":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :cond_0
    if-nez p2, :cond_1

    .line 536
    new-instance p2, Ljava/util/ArrayList;

    .end local p2    # "previousSearches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 539
    .restart local p2    # "previousSearches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    sget-object v3, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "updateRecyclerViewWithAutocomplete"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 542
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 543
    .local v1, "newAddress":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    iget-object v4, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 544
    const/4 v4, 0x1

    invoke-direct {p0, v1, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->insertAutoCompleteItem(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;Z)V

    goto :goto_0

    .line 549
    .end local v1    # "newAddress":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_3
    if-eqz p3, :cond_5

    .line 550
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->createSearchForMoreFooter(Ljava/lang/String;)V

    .line 555
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 557
    .local v0, "googleSearchSuggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 558
    .restart local v1    # "newAddress":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    iget-object v4, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->place_id:Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    .line 559
    invoke-static {v4, p3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 560
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->insertAutoCompleteItem(Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;)V

    .line 561
    iget-object v4, v1, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->name:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 552
    .end local v0    # "googleSearchSuggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v1    # "newAddress":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_5
    const-string p3, ""

    goto :goto_1

    .line 566
    .restart local v0    # "googleSearchSuggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 567
    .local v2, "previousSearch":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 568
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 569
    invoke-direct {p0, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->insertPreviousSearch(Ljava/lang/String;)V

    .line 570
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 573
    .end local v2    # "previousSearch":Ljava/lang/String;
    :cond_8
    return-void
.end method

.method updateRecyclerViewWithContact(Lcom/navdy/client/app/framework/models/ContactModel;)V
    .locals 3
    .param p1, "contact"    # Lcom/navdy/client/app/framework/models/ContactModel;

    .prologue
    .line 895
    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    if-nez v1, :cond_2

    .line 896
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItem()V

    .line 905
    :cond_1
    return-void

    .line 900
    :cond_2
    sget-object v1, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateRecyclerViewWithContact"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 902
    iget-object v1, p1, Lcom/navdy/client/app/framework/models/ContactModel;->addresses:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    .line 903
    .local v0, "address":Lcom/navdy/client/app/framework/models/Address;
    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addContactWithOneAddress(Lcom/navdy/client/app/framework/models/ContactModel;Lcom/navdy/client/app/framework/models/Address;)V

    goto :goto_0
.end method

.method updateRecyclerViewWithContactsForAutoComplete(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 768
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 787
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "updateRecyclerViewWithContactsForAutoComplete"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 775
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 776
    .local v1, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDeduplicatedAddressesForContact(Lcom/navdy/client/app/framework/models/ContactModel;)Ljava/util/ArrayList;

    move-result-object v0

    .line 777
    .local v0, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 780
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_3

    .line 781
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Address;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addContactWithOneAddress(Lcom/navdy/client/app/framework/models/ContactModel;Lcom/navdy/client/app/framework/models/Address;)V

    goto :goto_1

    .line 783
    :cond_3
    invoke-direct {p0, v1, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addContactWithMultipleAddress(Lcom/navdy/client/app/framework/models/ContactModel;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 786
    .end local v0    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v1    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method updateRecyclerViewWithContactsForTextSearch(Ljava/util/List;)V
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 738
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-gtz v3, :cond_1

    .line 759
    :cond_0
    :goto_0
    return-void

    .line 742
    :cond_1
    sget-object v3, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "updateRecyclerViewWithContactsForTextSearch"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 745
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 746
    .local v2, "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    invoke-direct {p0, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getDeduplicatedAddressesForContact(Lcom/navdy/client/app/framework/models/ContactModel;)Ljava/util/ArrayList;

    move-result-object v1

    .line 747
    .local v1, "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 750
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    .line 752
    .local v0, "address":Lcom/navdy/client/app/framework/models/Address;
    if-eqz v0, :cond_3

    .line 755
    invoke-direct {p0, v2, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addContactWithOneAddress(Lcom/navdy/client/app/framework/models/ContactModel;Lcom/navdy/client/app/framework/models/Address;)V

    goto :goto_1

    .line 758
    .end local v0    # "address":Lcom/navdy/client/app/framework/models/Address;
    .end local v1    # "addresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Address;>;"
    .end local v2    # "contact":Lcom/navdy/client/app/framework/models/ContactModel;
    :cond_4
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method updateRecyclerViewWithDatabaseData(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 616
    .local p1, "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 620
    :cond_1
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateRecyclerViewWithDatabaseData"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 623
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 624
    .local v7, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 625
    .local v6, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/Destination;>;"
    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 626
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Destination;

    .line 627
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v2, v7, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/models/Destination;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 629
    iput-object v1, v7, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 630
    invoke-direct {p0, v7}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setLastRoutedIfRecent(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;)V

    .line 631
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 636
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v6    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/navdy/client/app/framework/models/Destination;>;"
    .end local v7    # "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    :cond_4
    invoke-static {p1, p2}, Lcom/navdy/client/app/framework/util/VoiceCommandUtils;->addHomeAndWorkIfMatch(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 639
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Destination;

    .line 640
    .restart local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getImageEnum()Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    move-result-object v4

    .line 641
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->RECENT_PLACES:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$SearchType;->getValue()I

    move-result v0

    iput v0, v1, Lcom/navdy/client/app/framework/models/Destination;->searchResultType:I

    .line 643
    const-string v3, ""

    .line 644
    .local v3, "lastRoutedOn":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->currentMode:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    sget-object v2, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;->FULL_SEARCH:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$Mode;

    if-ne v0, v2, :cond_5

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->isRecentDestination()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 645
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->context:Landroid/content/Context;

    const v2, 0x7f080276

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-wide v10, v1, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 646
    invoke-static {v10, v11}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->getDateAndTime(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v9

    .line 645
    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 648
    :cond_5
    iget-object v9, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-wide/high16 v10, -0x4010000000000000L    # -1.0

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 649
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addMarker(I)V

    goto :goto_2

    .line 651
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "lastRoutedOn":Ljava/lang/String;
    .end local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    :cond_6
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method updateRecyclerViewWithPlaceSearchResults(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 667
    .local p1, "placesSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "updateRecyclerViewWithPlaceSearchResults"

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 673
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addSearchPoweredByGoogleHeader()V

    .line 675
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/service/library/events/places/PlacesSearchResult;

    .line 676
    .local v6, "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 677
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    invoke-static {v6, v1}, Lcom/navdy/client/app/framework/models/Destination;->placesSearchResultToDestinationObject(Lcom/navdy/service/library/events/places/PlacesSearchResult;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 679
    iget-object v0, v6, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 680
    iget-object v2, v6, Lcom/navdy/service/library/events/places/PlacesSearchResult;->distance:Ljava/lang/Double;

    .line 684
    .local v2, "distance":Ljava/lang/Double;
    :goto_2
    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    const-string v3, ""

    sget-object v4, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;->PLACE:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 685
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addMarker(I)V

    goto :goto_1

    .line 682
    .end local v2    # "distance":Ljava/lang/Double;
    :cond_2
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .restart local v2    # "distance":Ljava/lang/Double;
    goto :goto_2

    .line 687
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v2    # "distance":Ljava/lang/Double;
    .end local v6    # "placesSearchResult":Lcom/navdy/service/library/events/places/PlacesSearchResult;
    :cond_3
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method updateRecyclerViewWithTextSearchResults(Ljava/util/List;)V
    .locals 12
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 691
    .local p1, "textSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 695
    :cond_1
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "updateRecyclerViewWithTextSearchResults"

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 698
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addSearchPoweredByGoogleHeader()V

    .line 700
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 701
    .local v6, "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    new-instance v1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/models/Destination;-><init>()V

    .line 702
    .local v1, "destination":Lcom/navdy/client/app/framework/models/Destination;
    sget-object v0, Lcom/navdy/client/app/framework/models/Destination$SearchType;->TEXT_SEARCH:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    invoke-virtual {v6, v0, v1}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 703
    iget-object v0, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->price_level:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->getPriceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 705
    .local v3, "price":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getImageEnum()Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;

    move-result-object v4

    .line 706
    .local v4, "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    iget-object v8, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    new-instance v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    iget-wide v10, v6, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->distance:D

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Double;Ljava/lang/String;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;Lcom/navdy/client/app/framework/models/ContactModel;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 707
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->searchItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addMarker(I)V

    goto :goto_1

    .line 709
    .end local v1    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v3    # "price":Ljava/lang/String;
    .end local v4    # "imageEnum":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$ImageEnum;
    .end local v6    # "result":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
