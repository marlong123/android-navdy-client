.class Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;
.super Landroid/os/AsyncTask;
.source "SearchRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateNearbySearchResultsWithKnownDestinationData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

.field final synthetic val$items:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->val$items:Ljava/util/List;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 714
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 717
    iget-object v1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->val$items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;

    .line 718
    .local v0, "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    iget-object v2, v0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabase()V

    .line 719
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-static {v2, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$200(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;)V

    goto :goto_0

    .line 721
    .end local v0    # "searchItem":Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$SearchItem;
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 714
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 726
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$3;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->notifyDataSetChanged()V

    .line 727
    return-void
.end method
