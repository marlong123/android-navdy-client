.class Lcom/navdy/client/app/ui/search/SearchActivity$23;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->putSearchResultExtraAndFinish(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 1289
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 3
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$4100(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Destination address: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 1293
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    if-nez v0, :cond_0

    .line 1294
    const/4 v0, 0x0

    .line 1301
    :goto_0
    return-object v0

    .line 1298
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->addToSearchHistory(Ljava/lang/String;)I

    .line 1300
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->reloadSelfFromDatabase()V

    .line 1301
    iget-object v0, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1289
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$23;->doInBackground([Ljava/lang/Void;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 9
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1306
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1307
    .local v1, "result":Landroid/content/Intent;
    const-string v3, "search_result"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1309
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$4200(Lcom/navdy/client/app/ui/search/SearchActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1310
    invoke-static {}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;

    move-result-object v3

    const-string v4, "Search_Map"

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/tracking/SetDestinationTracker;->setDestinationType(Ljava/lang/String;)V

    .line 1313
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$4300(Lcom/navdy/client/app/ui/search/SearchActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1314
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getTitleAndSubtitle()Landroid/util/Pair;

    move-result-object v0

    .line 1315
    .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v0, :cond_1

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1316
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    const v4, 0x7f080320

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Lcom/navdy/client/app/ui/search/SearchActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1317
    .local v2, "speech":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iget-object v3, v3, Lcom/navdy/client/app/ui/search/SearchActivity;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v7}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->processTTSRequest(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1319
    .end local v2    # "speech":Ljava/lang/String;
    :cond_1
    const-string v3, "extra_is_voice_search"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1322
    .end local v0    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    const/4 v4, -0x1

    invoke-virtual {v3, v4, v1}, Lcom/navdy/client/app/ui/search/SearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 1324
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    .line 1325
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$23;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->finish()V

    .line 1326
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1289
    check-cast p1, Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$23;->onPostExecute(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method
