.class synthetic Lcom/navdy/client/app/ui/search/SearchActivity$26;
.super Ljava/lang/Object;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 897
    invoke-static {}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->values()[Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchActivity$26;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchActivity$26;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    :try_start_1
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchActivity$26;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchActivity$26;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_0
.end method
