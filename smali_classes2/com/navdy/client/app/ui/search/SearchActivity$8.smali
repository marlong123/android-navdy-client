.class Lcom/navdy/client/app/ui/search/SearchActivity$8;
.super Landroid/os/AsyncTask;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->runOfflineAutocompleteSearch(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/support/v4/util/Pair",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/navdy/client/app/framework/models/ContactModel;",
        ">;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/navdy/client/app/framework/models/Destination;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

.field final synthetic val$constraint:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 622
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->val$constraint:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/support/v4/util/Pair;
    .locals 5
    .param p1, "unusedParams"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 625
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1700(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "runOfflineAutocompleteSearch doInBackground"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 627
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->val$constraint:Ljava/lang/String;

    sget-object v4, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->ADDRESS:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 628
    invoke-static {v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsAndLoadPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v0

    .line 629
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    iget-object v2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->val$constraint:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationsFromSearchQuery(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 630
    .local v1, "knownDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    new-instance v2, Landroid/support/v4/util/Pair;

    invoke-direct {v2, v0, v1}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 622
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$8;->doInBackground([Ljava/lang/Void;)Landroid/support/v4/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/support/v4/util/Pair;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/ContactModel;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Destination;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 635
    .local p1, "pair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;>;"
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1800(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    const-string v4, "runOfflineAutocompleteSearch onPostExecute"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 636
    iget-object v0, p1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 637
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    iget-object v1, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    .line 639
    .local v1, "knownDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 641
    .local v2, "query":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->val$constraint:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 642
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 643
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->setLastQuery(Ljava/lang/String;)V

    .line 644
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithContactsForTextSearch(Ljava/util/List;)V

    .line 645
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithDatabaseData(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 647
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/util/List;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->addNoResultsItemIfEmpty([Ljava/util/List;)V

    .line 649
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 650
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1300(Lcom/navdy/client/app/ui/search/SearchActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->createSearchForMoreFooter(Ljava/lang/String;)V

    .line 652
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$8;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1500(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 658
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 622
    check-cast p1, Landroid/support/v4/util/Pair;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/search/SearchActivity$8;->onPostExecute(Landroid/support/v4/util/Pair;)V

    return-void
.end method
