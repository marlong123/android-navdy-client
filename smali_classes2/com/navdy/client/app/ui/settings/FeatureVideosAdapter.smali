.class Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "FeatureVideosAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    }
.end annotation


# static fields
.field private static final CACHE_SIZE:I = 0x3200000

.field private static final IMAGE_MAX_SIZE:I = 0x400000


# instance fields
.field private context:Landroid/content/Context;

.field private final featureVideoArray:[Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

.field private featureVideoArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;",
            ">;"
        }
    .end annotation
.end field

.field private hasWatched:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field private logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "activityContext"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    .line 34
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    .line 36
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->hasWatched:Ljava/util/HashSet;

    .line 37
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    .line 39
    const/16 v0, 0x13

    new-array v6, v0, [Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const/4 v7, 0x0

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_features_overview"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080186

    .line 40
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020221

    const-string v4, "1:59"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/00-FeatureOverview-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x1

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_power"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080187

    .line 41
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020228

    const-string v4, "0:30"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/01-PowerAndYourNavdyDisplay-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x2

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_connectiing"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017c

    .line 42
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021c

    const-string v4, "0:53"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/02-ConnectingtoYourNavdyDisplay-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x3

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_mobile_app_overview"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080185

    .line 43
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020226

    const-string v4, "0:53"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/03-MobileAppOverview-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x4

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_dial"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017f

    .line 44
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021e

    const-string v4, "0:47"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/04-UsingYourNavdyDial-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x5

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_gestures"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080182

    .line 45
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0201e8

    const-string v4, "1:03"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/05-UsingGestures-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x6

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_brightness"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017b

    .line 46
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021b

    const-string v4, "0:31"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/06-AdjustingBrightness-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/4 v7, 0x7

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_dash_mode"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017e

    .line 47
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021d

    const-string v4, "1:32"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/07-DashMode-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0x8

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_map_mode"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080184

    .line 48
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020225

    const-string v4, "1:24"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/08-MapMode-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0x9

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_searching"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080189

    .line 49
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02022a

    const-string v4, "1:14"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/09-SearchingForANewDestination-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xa

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_adding_favorites"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080180

    .line 50
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020219

    const-string v4, "0:46"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/10-AddingFavoritePlaces-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xb

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_driving_favorite"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080181

    .line 51
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021f

    const-string v4, "0:27"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/11-DrivingToAFavPlace-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xc

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_driving_suggested"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08018a

    .line 52
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020220

    const-string v4, "0:30"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/12-DrivingToASuggestedPlace-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xd

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_answering_calls"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017a

    .line 53
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02021a

    const-string v4, "0:29"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/13-AnsweringCalls-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xe

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_making_calls"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080183

    .line 54
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020224

    const-string v4, "0:38"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/14-MakingCalls-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0xf

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_reading_glances"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080188

    .line 55
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020229

    const-string v4, "1:43"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/15-ReadingGlancesFromYourPhone-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0x10

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_music"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08017d

    .line 56
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020227

    const-string v4, "0:43"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/16-ControllingYourMusic-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0x11

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_google_now"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f08018b

    .line 57
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020223

    const-string v4, "0:55"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/17-UsingGoogleNow-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    const/16 v7, 0x12

    new-instance v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    const-string v1, "image_video_up_to_date"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    const v3, 0x7f080178

    .line 58
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f02022b

    const-string v4, "0:38"

    const-string v5, "https://s3-us-west-1.amazonaws.com/navdy-public/video/features/android/18-KeepingNavdyUpToDate-Android-App.mp4"

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    aput-object v0, v6, v7

    iput-object v6, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArray:[Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArray:[Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    .line 64
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    const/high16 v1, 0x3200000

    const/high16 v2, 0x400000

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>(II)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 65
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->initImageCache()V

    .line 66
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->initHasWatched()V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->saveVideoAsWatched(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)Lcom/navdy/client/app/framework/util/ImageCache;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    return-object v0
.end method

.method private initHasWatched()V
    .locals 5

    .prologue
    .line 142
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 144
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    .line 145
    .local v0, "featureVideo":Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    iget-object v3, v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->sharedPrefKey:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->hasWatched:Ljava/util/HashSet;

    iget-object v4, v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->sharedPrefKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    .end local v0    # "featureVideo":Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    :cond_1
    return-void
.end method

.method private initImageCache()V
    .locals 3

    .prologue
    .line 124
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$2;-><init>(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 138
    return-void
.end method

.method private saveVideoAsWatched(Ljava/lang/String;)V
    .locals 4
    .param p1, "featureVideoKey"    # Ljava/lang/String;

    .prologue
    .line 152
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "featureVideoKey was empty"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 160
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->hasWatched:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hasWatched: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 158
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 159
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method


# virtual methods
.method public getItem(I)Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 5
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 88
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->featureVideoArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;

    .line 90
    .local v1, "featureVideo":Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;
    instance-of v3, p1, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;

    if-eqz v3, :cond_1

    move-object v2, p1

    .line 91
    check-cast v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;

    .line 96
    .local v2, "featureVideoViewHolder":Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;
    if-eqz v1, :cond_0

    .line 97
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->title:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/util/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 99
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 100
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 104
    :goto_0
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->duration:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->duration:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->hasWatched:Ljava/util/HashSet;

    iget-object v4, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->sharedPrefKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 106
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->watchedLayout:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 110
    :goto_1
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->row:Landroid/view/View;

    new-instance v4, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;

    invoke-direct {v4, p0, v1}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$1;-><init>(Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "featureVideoViewHolder":Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;
    :cond_0
    :goto_2
    return-void

    .line 93
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "The ViewHolder was not a FeatureVideoViewHolder."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 102
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v2    # "featureVideoViewHolder":Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;
    :cond_2
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->image:Landroid/widget/ImageView;

    iget v4, v1, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter$FeatureVideo;->imageResource:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 108
    :cond_3
    iget-object v3, v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;->watchedLayout:Landroid/widget/RelativeLayout;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 81
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/FeatureVideosAdapter;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 82
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030058

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 83
    .local v1, "view":Landroid/view/View;
    new-instance v2, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;

    invoke-direct {v2, v1}, Lcom/navdy/client/app/ui/settings/FeatureVideoViewHolder;-><init>(Landroid/view/View;)V

    return-object v2
.end method
