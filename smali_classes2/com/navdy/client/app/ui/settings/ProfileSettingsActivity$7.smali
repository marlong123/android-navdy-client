.class Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;
.super Ljava/lang/Object;
.source "ProfileSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onChangePhoto(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 291
    packed-switch p2, :pswitch_data_0

    .line 311
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->eraseUserProfilePhoto()V

    .line 312
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->profileFragment:Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    if-eqz v1, :cond_0

    .line 313
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->profileFragment:Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->getNameText()Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "nameText":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v1, v1, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    const v2, 0x7f0201ce

    invoke-virtual {v1, v2, v0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;)V

    .line 315
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$702(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Z)Z

    .line 319
    .end local v0    # "nameText":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 293
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    new-instance v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7$1;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;)V

    new-instance v3, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7$2;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7$2;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;)V

    invoke-virtual {v1, v2, v3}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 307
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;->this$0:Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->access$600(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "temp_photo.jpg"

    invoke-static {v1, v2, v3}, Lcom/navdy/client/app/ui/PhotoUtils;->takePhoto(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V

    goto :goto_0

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
