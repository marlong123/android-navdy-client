.class public Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "VideoPlayerActivity.java"


# static fields
.field public static final EXTRA_VIDEO_URL:Ljava/lang/String; = "video_url"


# instance fields
.field private currentPosition:I

.field private myVideo:Landroid/widget/VideoView;

.field private subtitlesTextView:Landroid/widget/TextView;

.field private videoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;[Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;
    .param p1, "x1"    # [Landroid/media/MediaPlayer$TrackInfo;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->findTrackIndexFor([Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->subtitlesTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private findTrackIndexFor([Landroid/media/MediaPlayer$TrackInfo;ILjava/lang/String;)I
    .locals 3
    .param p1, "trackInfo"    # [Landroid/media/MediaPlayer$TrackInfo;
    .param p2, "mediaTrackType"    # I
    .param p3, "language"    # Ljava/lang/String;

    .prologue
    .line 159
    const/4 v1, -0x1

    .line 160
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 161
    aget-object v2, p1, v0

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    if-ne v2, p2, :cond_0

    aget-object v2, p1, v0

    .line 162
    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 160
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 166
    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v1, 0x7f030079

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->setContentView(I)V

    .line 40
    const v1, 0x7f1001e8

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->subtitlesTextView:Landroid/widget/TextView;

    .line 42
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->showProgressDialog()V

    .line 44
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$1;

    invoke-direct {v0, p0, p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;Landroid/content/Context;)V

    .line 56
    .local v0, "mediaController":Landroid/widget/MediaController;
    const v1, 0x7f1001e7

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    .line 58
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->videoUrl:Ljava/lang/String;

    .line 59
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->videoUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid video URL! ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->videoUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    if-eqz v1, :cond_1

    .line 64
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v1, v0}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 65
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->videoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 68
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    .line 70
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$2;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 77
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    new-instance v2, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity$3;-><init>(Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 152
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    .line 154
    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    .line 197
    invoke-virtual {v0}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    .line 198
    invoke-virtual {v0}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 201
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onDestroy()V

    .line 202
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->currentPosition:I

    .line 184
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 185
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onPause()V

    .line 186
    return-void
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 172
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->currentPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 174
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->myVideo:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->resume()V

    .line 178
    :cond_0
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onRestart()V

    .line 179
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "First_Launch_Install_Video: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;->videoUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 192
    return-void
.end method
