.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;
.super Ljava/lang/Object;
.source "EditCarInfoUsingWebApiActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->goToNextStep()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

.field final synthetic val$nextStep:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->val$nextStep:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 511
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->hideProgressDialog()V

    .line 512
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->val$nextStep:Ljava/lang/String;

    const-string v2, "installation_flow"

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 514
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/CheckMountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 515
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->startActivity(Landroid/content/Intent;)V

    .line 520
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/client/app/ui/firstlaunch/CarInfoActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->startActivity(Landroid/content/Intent;)V

    .line 518
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack$3;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;

    iget-object v1, v1, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$ObdInfoCallBack;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->finish()V

    goto :goto_0
.end method
