.class public Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "ScreenLightActivity.java"


# static fields
.field private static final WAKE_LOCK_TAG:Ljava/lang/String; = "TORCH_WAKE_LOCK"


# instance fields
.field private logger:Lcom/navdy/service/library/log/Logger;

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    .line 20
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method private startWakeLock()V
    .locals 3

    .prologue
    .line 49
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Getting a new WakeLock"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 51
    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 52
    .local v0, "pm":Landroid/os/PowerManager;
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "PowerManager acquired"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 53
    const/4 v1, 0x1

    const-string v2, "TORCH_WAKE_LOCK"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    .line 54
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "WakeLock set"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 56
    .end local v0    # "pm":Landroid/os/PowerManager;
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 57
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "WakeLock acquired"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method private stopWakeLock()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "WakeLock released"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f0300e3

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->setContentView(I)V

    .line 30
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/navdy/client/app/framework/FlashlightManager;->getInstance()Lcom/navdy/client/app/framework/FlashlightManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/FlashlightManager;->setScreenBackToNormalBrightness(Landroid/view/Window;)V

    .line 44
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->stopWakeLock()V

    .line 45
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onPause()V

    .line 46
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 35
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->startWakeLock()V

    .line 36
    invoke-static {}, Lcom/navdy/client/app/framework/FlashlightManager;->getInstance()Lcom/navdy/client/app/framework/FlashlightManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/FlashlightManager;->setScreenToFullBrightness(Landroid/view/Window;)V

    .line 38
    const-string v0, "First_Launch_Screen_Light"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public onScreenTap(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/ScreenLightActivity;->finish()V

    .line 69
    return-void
.end method
