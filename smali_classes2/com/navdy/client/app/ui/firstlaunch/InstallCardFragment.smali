.class public Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
.super Lcom/navdy/client/app/ui/base/BaseSupportFragment;
.source "InstallCardFragment.java"


# static fields
.field public static final LAYOUT_ID:Ljava/lang/String; = "layoutId"


# instance fields
.field private layoutId:I

.field public mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field protected rootView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getScreen()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    packed-switch v0, :pswitch_data_0

    .line 140
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 123
    :pswitch_1
    const-string v0, "Installation_Overview"

    goto :goto_0

    .line 126
    :pswitch_2
    const-string v0, "Installation_Short_Mount"

    goto :goto_0

    .line 128
    :pswitch_3
    const-string v0, "Installation_Lens_Position"

    goto :goto_0

    .line 130
    :pswitch_4
    const-string v0, "Installation_Plug_OBD"

    goto :goto_0

    .line 132
    :pswitch_5
    const-string v0, "Installation_Secure"

    goto :goto_0

    .line 134
    :pswitch_6
    const-string v0, "Installation_Tidy_Up"

    goto :goto_0

    .line 136
    :pswitch_7
    const-string v0, "Installation_Power_On"

    goto :goto_0

    .line 138
    :pswitch_8
    const-string v0, "Installation_Dial"

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x7f03006e
        :pswitch_8
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 53
    if-eqz p3, :cond_0

    .line 54
    const-string v8, "layoutId"

    invoke-virtual {p3, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    .line 57
    :cond_0
    const/4 v3, 0x0

    .line 58
    .local v3, "imageCache":Lcom/navdy/client/app/framework/util/ImageCache;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    .line 59
    .local v0, "activity":Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;
    if-eqz v0, :cond_1

    .line 60
    iget-object v3, v0, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 63
    :cond_1
    iget v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    invoke-virtual {p1, v8, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->rootView:Landroid/view/View;

    .line 64
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->rootView:Landroid/view/View;

    const v9, 0x7f1000b1

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 65
    .local v4, "imageView":Landroid/widget/ImageView;
    if-eqz v4, :cond_3

    .line 67
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v9, "box"

    const-string v10, "Old_Box"

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "box":Ljava/lang/String;
    const-string v8, "Old_Box"

    invoke-static {v1, v8}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    const/4 v7, 0x1

    .line 70
    .local v7, "showNewShortMountAssets":Z
    :cond_2
    iget v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    packed-switch v8, :pswitch_data_0

    .line 110
    .end local v1    # "box":Ljava/lang/String;
    .end local v7    # "showNewShortMountAssets":Z
    :cond_3
    :goto_0
    :pswitch_0
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->rootView:Landroid/view/View;

    return-object v8

    .line 72
    .restart local v1    # "box":Ljava/lang/String;
    .restart local v7    # "showNewShortMountAssets":Z
    :pswitch_1
    const v5, 0x7f020054

    .line 73
    .local v5, "overview_asset":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    sget-object v9, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne v8, v9, :cond_5

    .line 74
    if-eqz v7, :cond_4

    .line 75
    const v5, 0x7f020056

    .line 80
    :cond_4
    :goto_1
    invoke-static {v4, v5, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 82
    const-string v8, "New_Box_Plus_Mounts"

    invoke-static {v1, v8}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 83
    iget-object v8, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->rootView:Landroid/view/View;

    const v9, 0x7f1001c0

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 84
    .local v2, "checkText2":Landroid/widget/TextView;
    if-eqz v2, :cond_3

    .line 85
    const v8, 0x7f0804ff

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 78
    .end local v2    # "checkText2":Landroid/widget/TextView;
    :cond_5
    const v5, 0x7f020058

    goto :goto_1

    .line 91
    .end local v5    # "overview_asset":I
    :pswitch_2
    const v6, 0x7f020248

    .line 92
    .local v6, "secureAsset":I
    if-eqz v7, :cond_6

    .line 93
    const v6, 0x7f020249

    .line 95
    :cond_6
    invoke-static {v4, v6, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    goto :goto_0

    .line 98
    .end local v6    # "secureAsset":I
    :pswitch_3
    const v8, 0x7f0201e6

    invoke-static {v4, v8, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    goto :goto_0

    .line 101
    :pswitch_4
    const v8, 0x7f0201e5

    invoke-static {v4, v8, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    goto :goto_0

    .line 104
    :pswitch_5
    const v8, 0x7f0201e3

    invoke-static {v4, v8, v3}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x7f03006e
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    const-string v0, "layoutId"

    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 116
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseSupportFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 117
    return-void
.end method

.method public setLayoutId(I)V
    .locals 0
    .param p1, "layoutId"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->layoutId:I

    .line 45
    return-void
.end method

.method public setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 41
    return-void
.end method
