.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;
.super Ljava/lang/Object;
.source "EditCarInfoActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private handleSelection(I)V
    .locals 3
    .param p1, "selectedYearPosition"    # I

    .prologue
    .line 231
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$300(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$302(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Z)Z

    .line 242
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$400(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onItemSelected selectedYearPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p1, v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    goto :goto_0

    .line 239
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$500(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$702(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 240
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;->access$800(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "selectedYearPosition"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0, p3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->handleSelection(I)V

    .line 223
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 227
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoActivity$2;->handleSelection(I)V

    .line 228
    return-void
.end method
