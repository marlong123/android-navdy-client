.class Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;
.super Ljava/lang/Object;
.source "GlancesAppAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

.field final synthetic val$appName:Landroid/widget/Switch;

.field final synthetic val$data:Landroid/content/pm/ApplicationInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;Landroid/content/pm/ApplicationInfo;Landroid/widget/Switch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->this$0:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$data:Landroid/content/pm/ApplicationInfo;

    iput-object p3, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$appName:Landroid/widget/Switch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$data:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$appName:Landroid/widget/Switch;

    .line 101
    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    .line 100
    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 102
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->this$0:Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;

    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$appName:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter$1;->val$appName:Landroid/widget/Switch;

    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/ui/glances/GlancesAppAdapter;->setSwitchColor(Landroid/widget/Switch;Z)V

    .line 104
    return-void
.end method
