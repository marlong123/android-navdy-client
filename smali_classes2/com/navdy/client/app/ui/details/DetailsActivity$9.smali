.class Lcom/navdy/client/app/ui/details/DetailsActivity$9;
.super Ljava/lang/Object;
.source "DetailsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/details/DetailsActivity;->updateFavoriteButtonIfAlreadyFavorited(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

.field final synthetic val$placeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/details/DetailsActivity;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/details/DetailsActivity;

    .prologue
    .line 852
    iput-object p1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$9;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$9;->val$placeId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 855
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$9;->val$placeId:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getThisDestinationWithPlaceId(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 857
    .local v0, "destinationFromDb":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    iget-object v1, p0, Lcom/navdy/client/app/ui/details/DetailsActivity$9;->this$0:Lcom/navdy/client/app/ui/details/DetailsActivity;

    new-instance v2, Lcom/navdy/client/app/ui/details/DetailsActivity$9$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/client/app/ui/details/DetailsActivity$9$1;-><init>(Lcom/navdy/client/app/ui/details/DetailsActivity$9;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/details/DetailsActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 867
    :cond_0
    return-void
.end method
