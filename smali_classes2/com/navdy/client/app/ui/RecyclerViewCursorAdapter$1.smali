.class Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;
.super Landroid/database/DataSetObserver;
.source "RecyclerViewCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;-><init>(Landroid/content/Context;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    .prologue
    .line 53
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;"
    iput-object p1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 56
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;"
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataIsValid:Z

    .line 58
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->getNewCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 59
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 63
    .local p0, "this":Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;, "Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;"
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->dataIsValid:Z

    .line 65
    iget-object v0, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    iget-object v1, p0, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter$1;->this$0:Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->getNewCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/RecyclerViewCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 66
    return-void
.end method
