.class public Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;
.super Landroid/support/v4/view/PagerAdapter;
.source "ImageResourcePagerAdaper.java"


# instance fields
.field private imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field protected imageResourceIds:[I

.field protected mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "imageResourceIds"    # [I
    .param p3, "imageCache"    # Lcom/navdy/client/app/framework/util/ImageCache;

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageResourceIds:[I

    .line 23
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 26
    invoke-virtual {p2}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageResourceIds:[I

    .line 27
    iput-object p3, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 28
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 29
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p3, Landroid/widget/ImageView;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 57
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageResourceIds:[I

    array-length v0, v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 43
    iget-object v1, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03009e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 44
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 51
    .end local v0    # "imageView":Landroid/widget/ImageView;
    :goto_0
    return-object v0

    .line 48
    .restart local v0    # "imageView":Landroid/widget/ImageView;
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageResourceIds:[I

    aget v1, v1, p2

    iget-object v2, p0, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-static {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ImageUtils;->loadImage(Landroid/widget/ImageView;ILcom/navdy/client/app/framework/util/ImageCache;)V

    .line 50
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
