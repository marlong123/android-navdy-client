.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->setupBurgerMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

.field final synthetic val$applicationContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x1

    .line 421
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 459
    :goto_0
    return v5

    .line 426
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 458
    :goto_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$300(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V

    goto :goto_0

    .line 429
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 432
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/CalendarSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 435
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/NavigationSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 438
    :pswitch_4
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 441
    :pswitch_5
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 444
    :pswitch_6
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 447
    :pswitch_7
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/SupportActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 450
    :pswitch_8
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->val$applicationContext:Landroid/content/Context;

    const-class v4, Lcom/navdy/client/app/ui/settings/LegalSettingsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 453
    :pswitch_9
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    const-class v2, Lcom/navdy/client/debug/MainDebugActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 454
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$3;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x7f10040a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
