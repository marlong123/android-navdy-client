.class public Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SuggestionsViewHolder.java"


# instance fields
.field public background:Landroid/view/View;

.field public firstLine:Landroid/widget/TextView;

.field public icon:Landroid/widget/ImageView;

.field public infoButton:Landroid/widget/ImageButton;

.field public rightChevron:Landroid/widget/ImageView;

.field public row:Landroid/view/View;

.field public secondLine:Landroid/widget/TextView;

.field public separator:Landroid/view/View;

.field public subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

.field public thirdLine:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceType"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 33
    const v0, 0x7f100150

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->background:Landroid/view/View;

    .line 34
    const v0, 0x7f100151

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->row:Landroid/view/View;

    .line 35
    const v0, 0x7f1002f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->separator:Landroid/view/View;

    .line 36
    const v0, 0x7f1000b1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->icon:Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f100153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->firstLine:Landroid/widget/TextView;

    .line 38
    const v0, 0x7f100154

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f1002f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    .line 40
    const v0, 0x7f1002f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    .line 41
    const v0, 0x7f1002f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->rightChevron:Landroid/widget/ImageView;

    .line 42
    const v0, 0x7f1002f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->infoButton:Landroid/widget/ImageButton;

    .line 44
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;->thirdLine:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    :cond_0
    return-void
.end method
