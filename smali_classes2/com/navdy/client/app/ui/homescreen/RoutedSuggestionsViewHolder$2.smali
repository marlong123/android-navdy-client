.class Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;
.super Ljava/lang/Object;
.source "RoutedSuggestionsViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 124
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$100(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)V

    .line 126
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 128
    .local v0, "now":J
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$200(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$300(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v4}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$400(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v4

    iget-object v4, v4, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-wide v4, v4, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 130
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SuggestionManager;->onCalendarChanged(Z)V

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$500(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)J

    move-result-wide v2

    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$600()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 132
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v3}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$400(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Lcom/navdy/client/app/framework/models/Suggestion;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$700(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    goto :goto_0

    .line 133
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$200(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    sget-object v2, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-static {v3}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$800(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$900()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
