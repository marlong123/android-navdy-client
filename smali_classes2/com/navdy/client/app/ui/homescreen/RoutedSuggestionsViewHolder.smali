.class Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
.super Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;
.source "RoutedSuggestionsViewHolder.java"


# static fields
.field private static final ETA_RECOMPUTE_RATE:J

.field private static final ETA_REFRESH_RATE:J

.field private static final UNKNOWN_DURATION:I = -0x1

.field static handler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private etaRefresher:Ljava/lang/Runnable;

.field private etaString:Ljava/lang/String;

.field private lastRouteCalculation:J

.field private routeDurationWithTraffic:I

.field private routeDurationWithoutTraffic:I

.field private routeLength:I

.field private suggestion:Lcom/navdy/client/app/framework/models/Suggestion;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->ETA_REFRESH_RATE:J

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->ETA_RECOMPUTE_RATE:J

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handler:Landroid/os/Handler;

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v0, -0x1

    .line 49
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsViewHolder;-><init>(Landroid/view/View;)V

    .line 40
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithTraffic:I

    .line 41
    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithoutTraffic:I

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->lastRouteCalculation:J

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 121
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$2;-><init>(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaRefresher:Ljava/lang/Runnable;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
    .param p1, "x1"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handleRouteCalculated(Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/Suggestion;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->updateCalendarEta()V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->isCalendarEvent()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->getTimeWhenWeGetThere()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Lcom/navdy/client/app/framework/models/Suggestion;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    iget-wide v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->lastRouteCalculation:J

    return-wide v0
.end method

.method static synthetic access$600()J
    .locals 2

    .prologue
    .line 30
    sget-wide v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->ETA_RECOMPUTE_RATE:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->calculateEtas(Lcom/navdy/client/app/framework/models/Suggestion;)V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaRefresher:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900()J
    .locals 2

    .prologue
    .line 30
    sget-wide v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->ETA_REFRESH_RATE:J

    return-wide v0
.end method

.method private calculateEtas(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 7
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 64
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->lastRouteCalculation:J

    .line 66
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 75
    .local v2, "latitude":D
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 86
    .local v4, "longitude":D
    :goto_1
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->getInstance()Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    move-result-object v1

    new-instance v6, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;

    invoke-direct {v6, p0, p1}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;-><init>(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V

    invoke-virtual/range {v1 .. v6}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    goto :goto_0

    .line 76
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :cond_2
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 79
    .restart local v2    # "latitude":D
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-wide v4, v0, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .restart local v4    # "longitude":D
    goto :goto_1

    .line 82
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :cond_3
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "No location for calculateEtaForFirstDestination"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getTimeWhenWeGetThere()J
    .locals 4

    .prologue
    .line 193
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 194
    .local v0, "now":J
    iget v2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithTraffic:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, v0

    return-wide v2
.end method

.method private handleRouteCalculated(Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    const v1, 0xfffffff

    .line 99
    if-nez p1, :cond_0

    .line 100
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Attempting to handleRouteCalculated on a null route !"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 104
    :cond_0
    if-nez p2, :cond_1

    .line 105
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Attempting to handleRouteCalculated on a null suggestion !"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_1
    sget-object v0, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {p1, v0, v1}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithTraffic:I

    .line 110
    sget-object v0, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->DISABLED:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {p1, v0, v1}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithoutTraffic:I

    .line 111
    invoke-virtual {p1}, Lcom/here/android/mpa/routing/Route;->getLength()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeLength:I

    .line 113
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->updateCalendarEta()V

    .line 115
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeLength:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 118
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->restartRefresher()V

    goto :goto_0
.end method

.method private isCalendarEvent()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private restartRefresher()V
    .locals 4

    .prologue
    .line 145
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaRefresher:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 146
    sget-object v0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaRefresher:Ljava/lang/Runnable;

    sget-wide v2, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->ETA_REFRESH_RATE:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 147
    return-void
.end method

.method private updateCalendarEta()V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 150
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    if-nez v8, :cond_1

    .line 151
    sget-object v8, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "Can\'t update calendar ETA on a null suggestion."

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->isCalendarEvent()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 159
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 161
    .local v0, "context":Landroid/content/Context;
    iget v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithTraffic:I

    const/4 v9, -0x1

    if-eq v8, v9, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->getTimeWhenWeGetThere()J

    move-result-wide v6

    .line 166
    .local v6, "timeWhenWeGetThere":J
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v8, v8, Lcom/navdy/client/app/framework/models/Suggestion;->event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-wide v8, v8, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    sub-long v4, v8, v6

    .line 169
    .local v4, "timeLeft":J
    cmp-long v8, v4, v10

    if-lez v8, :cond_2

    .line 170
    const v3, 0x7f080124

    .line 171
    .local v3, "etaResString":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    const v9, 0x7f0f0068

    invoke-static {v0, v9}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 176
    :goto_1
    const-wide/16 v8, 0x3e8

    div-long v8, v4, v8

    long-to-int v8, v8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 177
    .local v1, "deltaInSeconds":I
    invoke-static {v1}, Lcom/navdy/client/debug/util/FormatUtils;->formatDurationFromSecondsToSecondsMinutesHours(I)Ljava/lang/String;

    move-result-object v2

    .line 179
    .local v2, "deltaText":Ljava/lang/String;
    cmp-long v8, v4, v10

    if-nez v8, :cond_3

    .line 180
    const v8, 0x7f080509

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    .line 185
    :goto_2
    iget v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithTraffic:I

    int-to-double v8, v8

    iget v10, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeDurationWithoutTraffic:I

    int-to-double v10, v10

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x3ff4000000000000L    # 1.25

    cmpl-double v8, v8, v10

    if-ltz v8, :cond_4

    .line 186
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    const v9, 0x7f0801f0

    new-array v10, v13, [Ljava/lang/Object;

    iget-object v11, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    aput-object v11, v10, v12

    invoke-virtual {v0, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 173
    .end local v1    # "deltaInSeconds":I
    .end local v2    # "deltaText":Ljava/lang/String;
    .end local v3    # "etaResString":I
    :cond_2
    const v3, 0x7f080508

    .line 174
    .restart local v3    # "etaResString":I
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    const v9, 0x7f0f00af

    invoke-static {v0, v9}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 182
    .restart local v1    # "deltaInSeconds":I
    .restart local v2    # "deltaText":Ljava/lang/String;
    :cond_3
    new-array v8, v13, [Ljava/lang/Object;

    aput-object v2, v8, v12

    invoke-virtual {v0, v3, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    goto :goto_2

    .line 188
    :cond_4
    iget-object v8, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method onBindViewHolder(Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 4
    .param p1, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeLength:I

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->calculateEtas(Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 61
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->secondLine:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->etaString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->subIllustrationText:Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->routeLength:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/navdy/client/app/ui/customviews/UnitSystemTextView;->setDistance(D)V

    .line 59
    invoke-direct {p0}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->restartRefresher()V

    goto :goto_0
.end method
