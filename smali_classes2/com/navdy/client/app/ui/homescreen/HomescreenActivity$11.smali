.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$11;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->onSuccessBannerClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .prologue
    .line 966
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$11;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 969
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 970
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 971
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 972
    .local v1, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    .line 975
    new-instance v2, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/navdy/service/library/events/connection/DisconnectRequest;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 978
    .end local v0    # "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    .end local v1    # "device":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    return-void
.end method
