.class Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;
.super Ljava/lang/Object;
.source "HomescreenActivity.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;

    .prologue
    .line 1432
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 3
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 1440
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1900(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1441
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1435
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17$1;->this$1:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity$17;->this$0:Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-static {v0, p1}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->access$1800(Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1436
    return-void
.end method
