.class Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;
.super Ljava/lang/Object;
.source "SuggestedDestinationsAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->onBindPendingTrip(Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

.field final synthetic val$pendingTripViewHolder:Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

.field final synthetic val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    .prologue
    .line 653
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->val$pendingTripViewHolder:Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    iput-object p3, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 656
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;->access$300(Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter;)Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->val$pendingTripViewHolder:Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;

    .line 657
    invoke-virtual {v1}, Lcom/navdy/client/app/ui/homescreen/PendingTripViewHolder;->getAdapterPosition()I

    move-result v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestedDestinationsAdapter$6;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    .line 656
    invoke-interface {v0, p1, v1, v2}, Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;->onItemLongClick(Landroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z

    .line 659
    const/4 v0, 0x1

    return v0
.end method
