.class public Lcom/navdy/client/app/ui/customviews/Gauge;
.super Landroid/view/View;
.source "Gauge.java"

# interfaces
.implements Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;


# static fields
.field private static final DEFAULT_SHADOW_THICKNESS_DP:I = 0x0

.field private static final DEFAULT_SUB_TEXT_SIZE:F = 40.0f

.field private static final DEFAULT_TEXT_SIZE:F = 90.0f

.field private static final DEFAULT_THICKNESS_DP:I = 0x28

.field private static final DEFAULT_TIC_LENGTH_DP:I = 0x4

.field private static final SHADOW_START_ANGLE:I = 0x2

.field private static final TIC_STYLE_CIRCLE:I = 0x2

.field private static final TIC_STYLE_LINE:I = 0x1

.field private static final TIC_STYLE_NONE:I

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private AntialiasSetting:Z

.field private forwardAnimationDuration:I

.field private mBackgroundColor:I

.field private mBackgroundPaint:Landroid/graphics/Paint;

.field private mCenterSubtext:Ljava/lang/String;

.field private mCenterText:Ljava/lang/String;

.field private mEndColor:I

.field private mMaxValue:I

.field private mMinValue:I

.field private mSerialValueAnimator:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

.field private mShadowColor:I

.field private mShadowThickness:I

.field private mShadowThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

.field private mStartAngle:I

.field private mStartColor:I

.field private mSubTextSize:F

.field private mSubTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

.field private mSweepAngle:I

.field private mTextColor:I

.field private mTextSize:F

.field private mTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

.field private mThickness:I

.field private mThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

.field private mTicColor:I

.field private mTicInterval:I

.field private mTicLength:I

.field private mTicPadding:I

.field private mTicPaint:Landroid/graphics/Paint;

.field private mTicStyle:I

.field private mValue:I

.field private mWarningColor:I

.field private mWarningValue:I

.field private reverseAnimationDuration:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/Gauge;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/Gauge;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const/16 v0, 0x12c

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->forwardAnimationDuration:I

    .line 26
    const/16 v0, 0x64

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->reverseAnimationDuration:I

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicColor:I

    .line 80
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->AntialiasSetting:Z

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/customviews/Gauge;->initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->initDrawingTools()V

    .line 100
    new-instance v0, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->forwardAnimationDuration:I

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->reverseAnimationDuration:I

    invoke-direct {v0, p0, v1, v2}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;-><init>(Lcom/navdy/client/app/ui/customviews/SerialValueAnimator$SerialValueAnimatorAdapter;II)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSerialValueAnimator:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    .line 101
    return-void
.end method

.method private chooseDimension(II)I
    .locals 1
    .param p1, "mode"    # I
    .param p2, "size"    # I

    .prologue
    .line 182
    const/high16 v0, -0x80000000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    if-ne p1, v0, :cond_1

    .line 185
    .end local p2    # "size":I
    :cond_0
    :goto_0
    return p2

    .restart local p2    # "size":I
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getPreferredSize()I

    move-result p2

    goto :goto_0
.end method

.method private deltaToAngle(I)F
    .locals 3
    .param p1, "deltaValue"    # I

    .prologue
    .line 297
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    int-to-float v0, v0

    int-to-float v1, p1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private drawBackground(Landroid/graphics/Canvas;)V
    .locals 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v0

    int-to-float v14, v0

    .line 263
    .local v14, "width":F
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v0

    int-to-float v12, v0

    .line 264
    .local v12, "height":F
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getRadius()F

    move-result v13

    .line 266
    .local v13, "radius":F
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 268
    .local v5, "paint":Landroid/graphics/Paint;
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 269
    .local v1, "rect":Landroid/graphics/RectF;
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, v14, v0

    sub-float/2addr v0, v13

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v12, v2

    sub-float/2addr v2, v13

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v14, v4

    add-float/2addr v4, v13

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v12, v6

    add-float/2addr v6, v13

    invoke-virtual {v1, v0, v2, v4, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 270
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundColor:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 271
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    int-to-float v9, v0

    .line 273
    .local v9, "backgroundSweep":F
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningValue:I

    if-eqz v0, :cond_0

    .line 274
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningValue:I

    sub-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;->deltaToAngle(I)F

    move-result v3

    .line 275
    .local v3, "warningSweep":F
    sub-float/2addr v9, v3

    .line 276
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningColor:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 277
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningValue:I

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;->valueToAngle(I)F

    move-result v2

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 280
    .end local v3    # "warningSweep":F
    :cond_0
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundColor:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 281
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v8, v0

    const/4 v10, 0x0

    move-object/from16 v6, p1

    move-object v7, v1

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 282
    return-void
.end method

.method private drawIndicator(Landroid/graphics/Canvas;)V
    .locals 28
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 368
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v4

    int-to-float v0, v4

    move/from16 v27, v0

    .line 369
    .local v27, "width":F
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v4

    int-to-float v0, v4

    move/from16 v18, v0

    .line 370
    .local v18, "height":F
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getRadius()F

    move-result v21

    .line 372
    .local v21, "radius":F
    new-instance v15, Landroid/graphics/Paint;

    invoke-direct {v15}, Landroid/graphics/Paint;-><init>()V

    .line 374
    .local v15, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartColor:I

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 375
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    int-to-float v4, v4

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 376
    const/4 v4, 0x1

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 377
    sget-object v4, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 378
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 381
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mValue:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/navdy/client/app/ui/customviews/Gauge;->deltaToAngle(I)F

    move-result v7

    .line 383
    .local v7, "sweep":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartColor:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mEndColor:I

    if-eq v4, v5, :cond_0

    .line 384
    const/4 v4, 0x4

    new-array v0, v4, [I

    move-object/from16 v17, v0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartColor:I

    aput v5, v17, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartColor:I

    aput v5, v17, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mEndColor:I

    aput v5, v17, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mEndColor:I

    aput v5, v17, v4

    .line 385
    .local v17, "colors":[I
    const/4 v4, 0x4

    new-array v0, v4, [F

    move-object/from16 v20, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v20, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v5, v5

    const/high16 v6, 0x43b40000    # 360.0f

    div-float/2addr v5, v6

    aput v5, v20, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v5, v5

    add-float/2addr v5, v7

    const/high16 v6, 0x43b40000    # 360.0f

    div-float/2addr v5, v6

    aput v5, v20, v4

    const/4 v4, 0x3

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v20, v4

    .line 386
    .local v20, "positions":[F
    new-instance v4, Landroid/graphics/SweepGradient;

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v27, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v6, v18, v6

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v4, v5, v6, v0, v1}, Landroid/graphics/SweepGradient;-><init>(FF[I[F)V

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 389
    .end local v17    # "colors":[I
    .end local v20    # "positions":[F
    :cond_0
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v27, v4

    sub-float v24, v4, v21

    .line 390
    .local v24, "rectLeft":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v18, v4

    sub-float v26, v4, v21

    .line 391
    .local v26, "rectTop":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v27, v4

    add-float v25, v4, v21

    .line 392
    .local v25, "rectRight":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v18, v4

    add-float v23, v4, v21

    .line 393
    .local v23, "rectBottom":F
    const/16 v22, 0x0

    .line 394
    .local v22, "radiusDeltaForShadow":F
    const/16 v16, 0x0

    .line 396
    .local v16, "angleDelta":F
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThickness:I

    if-lez v4, :cond_1

    .line 397
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9, v15}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 398
    .local v9, "shadowPaint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowColor:I

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 399
    new-instance v5, Landroid/graphics/RectF;

    move/from16 v0, v24

    move/from16 v1, v26

    move/from16 v2, v25

    move/from16 v3, v23

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v6, v4

    const/4 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 402
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThickness:I

    sub-int v19, v4, v5

    .line 403
    .local v19, "newThickness":I
    move/from16 v0, v19

    int-to-float v4, v0

    invoke-virtual {v15, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 404
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/customviews/Gauge;->getRadius(I)F

    move-result v4

    sub-float v22, v21, v4

    .line 405
    const/high16 v16, 0x40000000    # 2.0f

    .line 409
    .end local v9    # "shadowPaint":Landroid/graphics/Paint;
    .end local v19    # "newThickness":I
    :cond_1
    cmpl-float v4, v7, v16

    if-lez v4, :cond_2

    .line 410
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 411
    .local v11, "rect":Landroid/graphics/RectF;
    add-float v4, v24, v22

    add-float v5, v26, v22

    sub-float v6, v25, v22

    sub-float v8, v23, v22

    invoke-virtual {v11, v4, v5, v6, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 413
    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v4, v4

    add-float v12, v4, v16

    sub-float v13, v7, v16

    const/4 v14, 0x0

    move-object/from16 v10, p1

    invoke-virtual/range {v10 .. v15}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 415
    .end local v11    # "rect":Landroid/graphics/RectF;
    :cond_2
    return-void
.end method

.method private drawText(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 341
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterText:Ljava/lang/String;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterSubtext:Ljava/lang/String;

    if-nez v5, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 345
    .local v3, "paint":Landroid/graphics/Paint;
    iget v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextColor:I

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 346
    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextSize:F

    .line 347
    .local v2, "mainTextSize":F
    iget v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSubTextSize:F

    .line 349
    .local v4, "subTextSize":F
    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 350
    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 352
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v0, v5, v6

    .line 353
    .local v0, "centerX":F
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v1, v5, v6

    .line 356
    .local v1, "centerY":F
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterText:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 357
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterText:Ljava/lang/String;

    invoke-virtual {p1, v5, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 360
    :cond_2
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterSubtext:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 361
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 362
    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterSubtext:Ljava/lang/String;

    add-float v6, v1, v4

    const/high16 v7, 0x41200000    # 10.0f

    add-float/2addr v6, v7

    invoke-virtual {p1, v5, v0, v6, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawTics(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 301
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicStyle:I

    if-nez v1, :cond_0

    .line 338
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v10, v1

    .line 305
    .local v10, "centerX":F
    invoke-virtual/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v3, v1

    .line 306
    .local v3, "centerY":F
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getRadius()F

    move-result v13

    .line 308
    .local v13, "radius":F
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 309
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v1, v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v10, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 310
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v14, v1

    .line 311
    .local v14, "tickAngle":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicInterval:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/customviews/Gauge;->deltaToAngle(I)F

    move-result v15

    .line 313
    .local v15, "tickSweep":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    add-int/2addr v1, v2

    int-to-float v12, v1

    .line 316
    .local v12, "maxAngle":F
    const/high16 v1, 0x40000000    # 2.0f

    div-float v9, v15, v1

    .line 317
    .local v9, "alphaSweep":F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mValue:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/navdy/client/app/ui/customviews/Gauge;->valueToAngle(I)F

    move-result v16

    .line 318
    .local v16, "valueAngle":F
    const/16 v11, 0x80

    .line 319
    .local v11, "inactive":I
    const/16 v7, 0xff

    .line 321
    .local v7, "active":I
    :goto_1
    cmpg-float v1, v14, v12

    if-gtz v1, :cond_5

    .line 322
    move v8, v11

    .line 323
    .local v8, "alpha":I
    cmpl-float v1, v16, v14

    if-lez v1, :cond_3

    .line 324
    move v8, v7

    .line 328
    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 329
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicStyle:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 330
    add-float v1, v10, v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicLength:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPadding:I

    int-to-float v2, v2

    sub-float v2, v1, v2

    add-float v1, v10, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    sub-float/2addr v1, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPadding:I

    int-to-float v4, v4

    sub-float v4, v1, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    move v5, v3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 334
    :cond_2
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v10, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 335
    add-float/2addr v14, v15

    .line 336
    goto :goto_1

    .line 325
    :cond_3
    sub-float v1, v14, v9

    cmpl-float v1, v16, v1

    if-lez v1, :cond_1

    .line 326
    const/16 v1, 0x7f

    int-to-float v1, v1

    sub-float v2, v14, v9

    sub-float v2, v16, v2

    mul-float/2addr v1, v2

    div-float/2addr v1, v9

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int v8, v11, v1

    goto :goto_2

    .line 331
    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicStyle:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 332
    add-float v1, v10, v13

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicLength:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPadding:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicLength:I

    int-to-float v2, v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    .line 337
    .end local v8    # "alpha":I
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private evaluateDimensions()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 226
    .local v0, "smallSide":I
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    int-to-float v2, v0

    invoke-virtual {v1, p0, v2, v3}, Lcom/navdy/client/app/framework/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    .line 227
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    int-to-float v2, v0

    invoke-virtual {v1, p0, v2, v3}, Lcom/navdy/client/app/framework/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThickness:I

    .line 228
    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThickness:I

    if-gt v1, v2, :cond_0

    .line 229
    sget-object v1, Lcom/navdy/client/app/ui/customviews/Gauge;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Shadow is set to be bigger than gauge\'s thickness - removing shadow"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 230
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThickness:I

    .line 233
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    int-to-float v2, v0

    invoke-virtual {v1, p0, v2, v3}, Lcom/navdy/client/app/framework/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextSize:F

    .line 234
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSubTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    int-to-float v2, v0

    invoke-virtual {v1, p0, v2, v3}, Lcom/navdy/client/app/framework/util/CustomDimension;->getSize(Landroid/view/View;FF)F

    move-result v1

    iput v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSubTextSize:F

    .line 235
    return-void
.end method

.method private getPreferredSize()I
    .locals 1

    .prologue
    .line 191
    const/16 v0, 0x12c

    return v0
.end method

.method private getRadius()F
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    invoke-direct {p0, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getRadius(I)F

    move-result v0

    return v0
.end method

.method private getRadius(I)F
    .locals 2
    .param p1, "thickness"    # I

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int/2addr v0, p1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method private initDrawingTools()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 238
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->evaluateDimensions()V

    .line 240
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    .line 241
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThickness:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 242
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 243
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 244
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 246
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    .line 247
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 248
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicStyle:I

    if-ne v0, v2, :cond_0

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 249
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 250
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPaint:Landroid/graphics/Paint;

    iget-boolean v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->AntialiasSetting:Z

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 251
    return-void

    .line 248
    :cond_0
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    goto :goto_0
.end method

.method private initFromAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v8, 0x42200000    # 40.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget-object v5, Lcom/navdy/client/R$styleable;->Gauge:[I

    invoke-virtual {v4, p2, v5, v6, v6}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 115
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-static {v8}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v2

    .line 116
    .local v2, "defaultThickness":I
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v1

    .line 117
    .local v1, "defaultShadowThickness":I
    const/high16 v4, 0x40800000    # 4.0f

    invoke-static {v4}, Lcom/navdy/client/app/ui/UiUtils;->convertDpToPx(F)I

    move-result v3

    .line 120
    .local v3, "defaultTicLength":I
    const/4 v4, 0x4

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    .line 121
    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mValue:I

    .line 122
    const/4 v4, 0x5

    const/16 v5, 0x78

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    .line 123
    const/4 v4, 0x0

    const/16 v5, 0x96

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    .line 124
    const/4 v4, 0x1

    const/16 v5, 0xf0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    .line 125
    const/16 v4, 0x13

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicPadding:I

    .line 126
    const/4 v4, 0x2

    int-to-float v5, v2

    invoke-static {p0, v0, v4, v5}, Lcom/navdy/client/app/framework/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/client/app/framework/util/CustomDimension;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    .line 127
    const/4 v4, 0x3

    int-to-float v5, v1

    invoke-static {p0, v0, v4, v5}, Lcom/navdy/client/app/framework/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/client/app/framework/util/CustomDimension;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowThicknessAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    .line 129
    const/16 v4, 0xd

    const/high16 v5, 0x42b40000    # 90.0f

    invoke-static {p0, v0, v4, v5}, Lcom/navdy/client/app/framework/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/client/app/framework/util/CustomDimension;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    .line 130
    const/16 v4, 0xe

    const/high16 v5, 0x42200000    # 40.0f

    invoke-static {p0, v0, v4, v5}, Lcom/navdy/client/app/framework/util/CustomDimension;->getDimension(Landroid/view/View;Landroid/content/res/TypedArray;IF)Lcom/navdy/client/app/framework/util/CustomDimension;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSubTextSizeAttribute:Lcom/navdy/client/app/framework/util/CustomDimension;

    .line 131
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicStyle:I

    .line 132
    const/16 v4, 0x14

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicLength:I

    .line 133
    const/16 v4, 0x15

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicInterval:I

    .line 134
    const/4 v4, 0x7

    const v5, -0x777778

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundColor:I

    .line 135
    const/16 v4, 0x9

    const/high16 v5, -0x10000

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningColor:I

    .line 136
    const/16 v4, 0xa

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTextColor:I

    .line 137
    const/16 v4, 0x8

    const/16 v5, 0x4b

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mWarningValue:I

    .line 138
    const/16 v4, 0xf

    const/high16 v5, -0x1000000

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartColor:I

    .line 139
    const/16 v4, 0x10

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mEndColor:I

    .line 140
    const/16 v4, 0x11

    const v5, -0x777778

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mShadowColor:I

    .line 141
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterText:Ljava/lang/String;

    .line 142
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterSubtext:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 149
    iget v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    iget v5, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    if-ge v4, v5, :cond_0

    .line 150
    iget v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    .line 153
    :cond_0
    iget v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    if-gtz v4, :cond_1

    .line 154
    iput v7, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    .line 157
    :cond_1
    iget v4, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicInterval:I

    if-gtz v4, :cond_2

    .line 158
    iput v7, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mTicInterval:I

    .line 160
    :cond_2
    return-void

    .line 144
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v4
.end method

.method private valueToAngle(I)F
    .locals 4
    .param p1, "value"    # I

    .prologue
    .line 289
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mStartAngle:I

    int-to-float v0, v0

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSweepAngle:I

    int-to-float v1, v1

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    sub-int v2, p1, v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    iget v3, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public clearAnimationQueue()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSerialValueAnimator:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->release()V

    .line 468
    return-void
.end method

.method public getValue()F
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mValue:I

    int-to-float v0, v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 420
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/Gauge;->drawBackground(Landroid/graphics/Canvas;)V

    .line 421
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/Gauge;->drawTics(Landroid/graphics/Canvas;)V

    .line 423
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/Gauge;->drawText(Landroid/graphics/Canvas;)V

    .line 424
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/customviews/Gauge;->drawIndicator(Landroid/graphics/Canvas;)V

    .line 426
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 164
    sget-object v7, Lcom/navdy/client/app/ui/customviews/Gauge;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Width spec: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 165
    sget-object v7, Lcom/navdy/client/app/ui/customviews/Gauge;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Height spec: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 167
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 168
    .local v5, "widthMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 170
    .local v6, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 171
    .local v3, "heightMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 173
    .local v4, "heightSize":I
    invoke-direct {p0, v5, v6}, Lcom/navdy/client/app/ui/customviews/Gauge;->chooseDimension(II)I

    move-result v2

    .line 174
    .local v2, "chosenWidth":I
    invoke-direct {p0, v3, v4}, Lcom/navdy/client/app/ui/customviews/Gauge;->chooseDimension(II)I

    move-result v1

    .line 176
    .local v1, "chosenHeight":I
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 178
    .local v0, "chosenDimension":I
    invoke-virtual {p0, v0, v0}, Lcom/navdy/client/app/ui/customviews/Gauge;->setMeasuredDimension(II)V

    .line 179
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 458
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->initDrawingTools()V

    .line 459
    return-void
.end method

.method public setAnimationSpeeds(II)V
    .locals 1
    .param p1, "forwardAnimationDuration"    # I
    .param p2, "reverseAnimationDuration"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->forwardAnimationDuration:I

    .line 105
    iput p2, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->reverseAnimationDuration:I

    .line 106
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSerialValueAnimator:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    invoke-virtual {v0, p1, p2}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->setAnimationSpeeds(II)V

    .line 107
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 462
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mBackgroundColor:I

    .line 463
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->invalidate()V

    .line 464
    return-void
.end method

.method public setCenterSubtext(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterSubtext:Ljava/lang/String;

    .line 221
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->invalidate()V

    .line 222
    return-void
.end method

.method public setCenterText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mCenterText:Ljava/lang/String;

    .line 216
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->invalidate()V

    .line 217
    return-void
.end method

.method public setGaugeValue(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 195
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    if-ge p1, v0, :cond_0

    .line 196
    iget p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMinValue:I

    .line 198
    :cond_0
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    if-le p1, v0, :cond_1

    .line 199
    iget p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mSerialValueAnimator:Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/SerialValueAnimator;->setValue(F)V

    .line 202
    return-void
.end method

.method public setMaxValue(I)V
    .locals 0
    .param p1, "mMaxValue"    # I

    .prologue
    .line 211
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mMaxValue:I

    .line 212
    return-void
.end method

.method public setValue(F)V
    .locals 1
    .param p1, "newValue"    # F

    .prologue
    .line 206
    float-to-int v0, p1

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/Gauge;->mValue:I

    .line 207
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/Gauge;->invalidate()V

    .line 208
    return-void
.end method
