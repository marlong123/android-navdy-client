.class public Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;
.super Lcom/navdy/client/app/ui/customviews/TripCardView;
.source "RoutingTripCardView.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/customviews/TripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method protected appendPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    return-object p1
.end method

.method public setNameAddressAndIcon(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 4
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 37
    if-nez p1, :cond_1

    .line 38
    sget-object v2, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Destination object is null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->icon:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 44
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getBadgeAssetForActiveTrip()I

    move-result v0

    .line 45
    .local v0, "asset":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 48
    .end local v0    # "asset":I
    :cond_2
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getMultilineAddress()Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;

    move-result-object v1

    .line 49
    .local v1, "mla":Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripName:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 50
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripName:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->title:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->appendPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripAddressFirstLine:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 53
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripAddressFirstLine:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressFirstLine:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripAddressSecondLine:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 56
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/RoutingTripCardView;->tripAddressSecondLine:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressSecondLine:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
