.class public Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;
.super Lcom/navdy/client/app/ui/customviews/TripCardView;
.source "ActiveTripCardView.java"


# static fields
.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/customviews/TripCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->context:Landroid/content/Context;

    .line 34
    new-instance v0, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView$1;-><init>(Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;)V

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method protected appendPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800aa

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleOnClick()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ActiveTripCardView;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->startActiveTripActivity(Landroid/content/Context;)V

    .line 45
    return-void
.end method
