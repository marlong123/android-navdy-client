.class public Lcom/navdy/client/app/providers/NavdyContentProviderConstants;
.super Ljava/lang/Object;
.source "NavdyContentProviderConstants.java"


# static fields
.field public static final DB_NAME:Ljava/lang/String; = "navdy"

.field static final DB_VERSION:I = 0x13

.field public static final DESTINATIONS_ADDRESS:Ljava/lang/String; = "address"

.field public static final DESTINATIONS_CITY:Ljava/lang/String; = "city"

.field public static final DESTINATIONS_CONTACT_LOOKUP_KEY:Ljava/lang/String; = "contact_lookup_key"

.field public static final DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

.field public static final DESTINATIONS_COUNTRY:Ljava/lang/String; = "country"

.field public static final DESTINATIONS_COUNTRY_CODE:Ljava/lang/String; = "countryCode"

.field public static final DESTINATIONS_DISPLAY_LAT:Ljava/lang/String; = "displat"

.field public static final DESTINATIONS_DISPLAY_LONG:Ljava/lang/String; = "displong"

.field public static final DESTINATIONS_DO_NOT_SUGGEST:Ljava/lang/String; = "do_not_suggest"

.field public static final DESTINATIONS_FAVORITE_TYPE:Ljava/lang/String; = "is_special"

.field public static final DESTINATIONS_ID:Ljava/lang/String; = "_id"

.field public static final DESTINATIONS_LABEL:Ljava/lang/String; = "favorite_label"

.field public static final DESTINATIONS_LAST_CONTACT_LOOKUP:Ljava/lang/String; = "last_contact_lookup"

.field public static final DESTINATIONS_LAST_KNOWN_CONTACT_ID:Ljava/lang/String; = "last_known_contact_id"

.field public static final DESTINATIONS_LAST_PLACE_ID_REFRESH:Ljava/lang/String; = "last_place_id_refresh"

.field public static final DESTINATIONS_LAST_ROUTED_DATE:Ljava/lang/String; = "last_routed_date"

.field public static final DESTINATIONS_NAME:Ljava/lang/String; = "place_name"

.field public static final DESTINATIONS_NAVIGATION_LAT:Ljava/lang/String; = "navlat"

.field public static final DESTINATIONS_NAVIGATION_LONG:Ljava/lang/String; = "navlong"

.field public static final DESTINATIONS_ORDER:Ljava/lang/String; = "favorite_listing_order"

.field public static final DESTINATIONS_PLACE_DETAIL_JSON:Ljava/lang/String; = "place_detail_json"

.field public static final DESTINATIONS_PLACE_ID:Ljava/lang/String; = "place_id"

.field public static final DESTINATIONS_PRECISION_LEVEL:Ljava/lang/String; = "precision_level"

.field static final DESTINATIONS_PROJECTION:[Ljava/lang/String;

.field public static final DESTINATIONS_STATE:Ljava/lang/String; = "state"

.field public static final DESTINATIONS_STREET_NAME:Ljava/lang/String; = "street_name"

.field public static final DESTINATIONS_STREET_NUMBER:Ljava/lang/String; = "street_number"

.field static final DESTINATIONS_TABLE_NAME:Ljava/lang/String; = "destinations"

.field public static final DESTINATIONS_TYPE:Ljava/lang/String; = "type"

.field static final DESTINATIONS_URI_CODE:I = 0x1

.field static final DESTINATIONS_URI_PATH:Ljava/lang/String; = "destinations"

.field private static final DESTINATIONS_URL:Ljava/lang/String;

.field public static final DESTINATIONS_ZIP_CODE:Ljava/lang/String; = "zip_code"

.field static final DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

.field static final DESTINATION_CACHE_DESTIANTION_ID:Ljava/lang/String; = "destination_id"

.field static final DESTINATION_CACHE_ID:Ljava/lang/String; = "_id"

.field static final DESTINATION_CACHE_LAST_RESPONSE_DATE:Ljava/lang/String; = "last_response_date"

.field static final DESTINATION_CACHE_LOCATION_STRING:Ljava/lang/String; = "location"

.field static final DESTINATION_CACHE_PROJECTION:[Ljava/lang/String;

.field static final DESTINATION_CACHE_TABLE_NAME:Ljava/lang/String; = "destination_cache"

.field static final DESTINATION_CACHE_URI_CODE:I = 0x4

.field static final DESTINATION_CACHE_URI_PATH:Ljava/lang/String; = "destination_cache"

.field private static final DESTINATION_CACHE_URL:Ljava/lang/String;

.field public static final FAVORITE_CONTACT:I = -0x4

.field public static final FAVORITE_CUSTOM:I = -0x1

.field public static final FAVORITE_HOME:I = -0x3

.field public static final FAVORITE_WORK:I = -0x2

.field static final MAX_DESTINATION_CACHE_AGE:J

.field static final MAX_DESTINATION_CACHE_SIZE:J = 0x3e8L

.field public static final NOT_A_FAVORITE:I = 0x0

.field public static final PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

.field public static final PLAYLISTS_ID:Ljava/lang/String; = "playlist_id"

.field public static final PLAYLISTS_NAME:Ljava/lang/String; = "playlist_name"

.field static final PLAYLISTS_PROJECTION:[Ljava/lang/String;

.field public static final PLAYLISTS_TABLE_NAME:Ljava/lang/String; = "playlists"

.field public static final PLAYLISTS_URI_CODE:I = 0x5

.field public static final PLAYLISTS_URI_PATH:Ljava/lang/String; = "playlists"

.field private static final PLAYLISTS_URL:Ljava/lang/String;

.field public static final PLAYLIST_MEMBERS_ALBUM:Ljava/lang/String; = "album"

.field public static final PLAYLIST_MEMBERS_ARTIST:Ljava/lang/String; = "artist"

.field public static final PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

.field public static final PLAYLIST_MEMBERS_PLAYLIST_ID:Ljava/lang/String; = "playlist_id"

.field static final PLAYLIST_MEMBERS_PROJECTION:[Ljava/lang/String;

.field public static final PLAYLIST_MEMBERS_SOURCE_ID:Ljava/lang/String; = "SourceId"

.field public static final PLAYLIST_MEMBERS_TABLE_NAME:Ljava/lang/String; = "playlist_members"

.field public static final PLAYLIST_MEMBERS_TITLE:Ljava/lang/String; = "title"

.field public static final PLAYLIST_MEMBERS_URI_CODE:I = 0x6

.field public static final PLAYLIST_MEMBERS_URI_PATH:Ljava/lang/String; = "playlist_members"

.field private static final PLAYLIST_MEMBERS_URL:Ljava/lang/String;

.field static final PROVIDER_NAME:Ljava/lang/String;

.field static final SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

.field static final SEARCH_HISTORY_DATE:Ljava/lang/String; = "last_searched_on"

.field public static final SEARCH_HISTORY_ID:Ljava/lang/String; = "_id"

.field static final SEARCH_HISTORY_PROJECTION:[Ljava/lang/String;

.field public static final SEARCH_HISTORY_QUERY:Ljava/lang/String; = "query"

.field static final SEARCH_HISTORY_TABLE_NAME:Ljava/lang/String; = "search_history"

.field static final SEARCH_HISTORY_URI_CODE:I = 0x3

.field static final SEARCH_HISTORY_URI_PATH:Ljava/lang/String; = "search_history"

.field private static final SEARCH_HISTORY_URL:Ljava/lang/String;

.field public static final TRIPS_ARRIVED_AT_DESTINATION:Ljava/lang/String; = "arrived_at_destination"

.field public static final TRIPS_CONTENT_URI:Landroid/net/Uri;

.field public static final TRIPS_DESTINATION_ID:Ljava/lang/String; = "destination_id"

.field public static final TRIPS_END_LAT:Ljava/lang/String; = "end_lat"

.field public static final TRIPS_END_LONG:Ljava/lang/String; = "end_long"

.field public static final TRIPS_END_ODOMETER:Ljava/lang/String; = "end_odometer"

.field public static final TRIPS_END_TIME:Ljava/lang/String; = "end_time"

.field public static final TRIPS_ID:Ljava/lang/String; = "_id"

.field static final TRIPS_PROJECTION:[Ljava/lang/String;

.field public static final TRIPS_START_LAT:Ljava/lang/String; = "start_lat"

.field public static final TRIPS_START_LONG:Ljava/lang/String; = "start_long"

.field public static final TRIPS_START_ODOMETER:Ljava/lang/String; = "start_odometer"

.field public static final TRIPS_START_TIME:Ljava/lang/String; = "start_time"

.field public static final TRIPS_START_TIME_ZONE_N_DST:Ljava/lang/String; = "start_time_zone_n_dst"

.field static final TRIPS_TABLE_NAME:Ljava/lang/String; = "trips"

.field public static final TRIPS_TRIP_NUMBER:Ljava/lang/String; = "trip_number"

.field static final TRIPS_URI_CODE:I = 0x2

.field static final TRIPS_URI_PATH:Ljava/lang/String; = "trips"

.field private static final TRIPS_URL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    const-class v0, Lcom/navdy/client/app/providers/NavdyContentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "destinations"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_URL:Ljava/lang/String;

    .line 23
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_CONTENT_URI:Landroid/net/Uri;

    .line 60
    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "place_id"

    aput-object v1, v0, v5

    const-string v1, "last_place_id_refresh"

    aput-object v1, v0, v6

    const-string v1, "displat"

    aput-object v1, v0, v7

    const-string v1, "displong"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "navlat"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "navlong"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "place_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "address"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "street_number"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "street_name"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "city"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "state"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "zip_code"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "country"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "countryCode"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "do_not_suggest"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "last_routed_date"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "favorite_label"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "favorite_listing_order"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "is_special"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "place_detail_json"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "precision_level"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "contact_lookup_key"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "last_known_contact_id"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "last_contact_lookup"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "trips"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_URL:Ljava/lang/String;

    .line 98
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_CONTENT_URI:Landroid/net/Uri;

    .line 115
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "trip_number"

    aput-object v1, v0, v5

    const-string v1, "start_time"

    aput-object v1, v0, v6

    const-string v1, "start_time_zone_n_dst"

    aput-object v1, v0, v7

    const-string v1, "start_odometer"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "start_lat"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "start_long"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "end_time"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "end_odometer"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "end_lat"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "end_long"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "arrived_at_destination"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "destination_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->TRIPS_PROJECTION:[Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "search_history"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_URL:Ljava/lang/String;

    .line 138
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_CONTENT_URI:Landroid/net/Uri;

    .line 145
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "last_searched_on"

    aput-object v1, v0, v5

    const-string v1, "query"

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->SEARCH_HISTORY_PROJECTION:[Ljava/lang/String;

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "destination_cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_URL:Ljava/lang/String;

    .line 158
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_CONTENT_URI:Landroid/net/Uri;

    .line 160
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->MAX_DESTINATION_CACHE_AGE:J

    .line 168
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "last_response_date"

    aput-object v1, v0, v5

    const-string v1, "location"

    aput-object v1, v0, v6

    const-string v1, "destination_id"

    aput-object v1, v0, v7

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->DESTINATION_CACHE_PROJECTION:[Ljava/lang/String;

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "playlists"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_URL:Ljava/lang/String;

    .line 181
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    .line 187
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "playlist_id"

    aput-object v1, v0, v4

    const-string v1, "playlist_name"

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_PROJECTION:[Ljava/lang/String;

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PROVIDER_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "playlist_members"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_URL:Ljava/lang/String;

    .line 198
    sget-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_URL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_CONTENT_URI:Landroid/net/Uri;

    .line 207
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "playlist_id"

    aput-object v1, v0, v4

    const-string v1, "SourceId"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album"

    aput-object v1, v0, v7

    const-string v1, "title"

    aput-object v1, v0, v8

    sput-object v0, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLIST_MEMBERS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
