.class Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat;
.super Ljava/lang/Object;
.source "DataCollectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/service/DataCollectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataJsonFormat"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat$SensorData;,
        Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat$Location;,
        Lcom/navdy/client/app/service/DataCollectionService$DataJsonFormat$Activity;
    }
.end annotation


# static fields
.field static final ACTIVITIES:Ljava/lang/String; = "android_drivings"

.field static final ANDROID_SENSORS:Ljava/lang/String; = "android_sensors"

.field static final APP_VERSION:Ljava/lang/String; = "app_version"

.field static final DEVICE:Ljava/lang/String; = "device"

.field static final ENDTIMESTAMP:Ljava/lang/String; = "endTimestamp"

.field static final GRAVITY:Ljava/lang/String; = "gravities"

.field static final GYROSCOPE:Ljava/lang/String; = "gyroscopes"

.field static final IS_SPLIT_DATA:Ljava/lang/String; = "is_split_data"

.field static final LINEAR_ACCELERATION:Ljava/lang/String; = "linear_accelerations"

.field static final LOCATIONS:Ljava/lang/String; = "locations"

.field static final MAGNETIC_FIELD:Ljava/lang/String; = "magnetic_fields"

.field static final ORIENTATION:Ljava/lang/String; = "orientations"

.field static final OS_VERSION:Ljava/lang/String; = "os_version"

.field static final PLATFORM:Ljava/lang/String; = "platform"

.field static final ROTATION_VECTOR:Ljava/lang/String; = "rotation_vectors"

.field static final STARTTIMESTAMP:Ljava/lang/String; = "startTimestamp"

.field static final TIME_ZONE_OFFSET:Ljava/lang/String; = "time_zone_offset"

.field static final TRIP_UUID:Ljava/lang/String; = "trip_uuid"

.field static final VEHICLE_MAKE:Ljava/lang/String; = "vehicle-make"

.field static final VEHICLE_MODEL:Ljava/lang/String; = "vehicle-model"

.field static final VEHICLE_YEAR:Ljava/lang/String; = "vehicle-year"

.field static final VERSION:Ljava/lang/String; = "version"

.field static final VERSION_NUMBER:I = 0x1


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
