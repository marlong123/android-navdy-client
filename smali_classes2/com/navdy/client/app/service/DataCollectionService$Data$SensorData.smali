.class public Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;
.super Ljava/lang/Object;
.source "DataCollectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/service/DataCollectionService$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SensorData"
.end annotation


# instance fields
.field accuracy:I

.field hasW:Z

.field timestamp:J

.field values:[F


# direct methods
.method constructor <init>(Landroid/hardware/SensorEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->hasW:Z

    .line 374
    invoke-direct {p0, p1}, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->getEpochTimestamp(Landroid/hardware/SensorEvent;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->timestamp:J

    .line 375
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    .line 376
    return-void
.end method

.method private getEpochTimestamp(Landroid/hardware/SensorEvent;)J
    .locals 18
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 402
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->access$000()Ljava/lang/Boolean;

    move-result-object v14

    if-nez v14, :cond_2

    .line 403
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 404
    .local v4, "nanoTime":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v8

    .line 406
    .local v8, "realtimeNanos":J
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long/2addr v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 407
    .local v6, "nanoTimeDiff":J
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long/2addr v14, v8

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    .line 409
    .local v10, "realtimeNanosDiff":J
    cmp-long v14, v6, v10

    if-lez v14, :cond_1

    .line 410
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long v12, v14, v8

    .line 411
    .local v12, "theRightDiff":J
    const-wide/16 v14, 0x0

    cmp-long v14, v10, v14

    if-lez v14, :cond_0

    const-wide/16 v14, 0x2

    mul-long/2addr v14, v10

    cmp-long v14, v6, v14

    if-lez v14, :cond_0

    .line 412
    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-static {v14}, Lcom/navdy/client/app/service/DataCollectionService;->access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 426
    .end local v4    # "nanoTime":J
    .end local v6    # "nanoTimeDiff":J
    .end local v8    # "realtimeNanos":J
    .end local v10    # "realtimeNanosDiff":J
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 427
    .local v2, "currentTimeInMilliseconds":J
    const-wide/32 v14, 0xf4240

    div-long v14, v12, v14

    add-long/2addr v14, v2

    return-wide v14

    .line 415
    .end local v2    # "currentTimeInMilliseconds":J
    .end local v12    # "theRightDiff":J
    .restart local v4    # "nanoTime":J
    .restart local v6    # "nanoTimeDiff":J
    .restart local v8    # "realtimeNanos":J
    .restart local v10    # "realtimeNanosDiff":J
    :cond_1
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    sub-long v12, v14, v4

    .line 416
    .restart local v12    # "theRightDiff":J
    const-wide/16 v14, 0x0

    cmp-long v14, v6, v14

    if-lez v14, :cond_0

    const-wide/16 v14, 0x2

    mul-long/2addr v14, v6

    cmp-long v14, v10, v14

    if-lez v14, :cond_0

    .line 417
    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-static {v14}, Lcom/navdy/client/app/service/DataCollectionService;->access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0

    .line 420
    .end local v4    # "nanoTime":J
    .end local v6    # "nanoTimeDiff":J
    .end local v8    # "realtimeNanos":J
    .end local v10    # "realtimeNanosDiff":J
    .end local v12    # "theRightDiff":J
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->access$000()Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 421
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v16

    sub-long v12, v14, v16

    .restart local v12    # "theRightDiff":J
    goto :goto_0

    .line 423
    .end local v12    # "theRightDiff":J
    :cond_3
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v16

    sub-long v12, v14, v16

    .restart local v12    # "theRightDiff":J
    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SensorData{timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->timestamp:J

    .line 433
    invoke-static {v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->getTimestampString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", \tvalues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    .line 434
    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", \taccuracy="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public writeToJson(Landroid/util/JsonWriter;)V
    .locals 5
    .param p1, "writer"    # Landroid/util/JsonWriter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    .line 440
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 441
    iget v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 442
    const-string v0, "accuracy"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->accuracy:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(J)Landroid/util/JsonWriter;

    .line 444
    :cond_0
    const-string v0, "timestamp"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-wide v2, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->timestamp:J

    invoke-static {v2, v3}, Lcom/navdy/client/app/service/DataCollectionService;->getTimestampString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 445
    const-string v0, "x"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(D)Landroid/util/JsonWriter;

    .line 446
    const-string v0, "y"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(D)Landroid/util/JsonWriter;

    .line 447
    const-string v0, "z"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(D)Landroid/util/JsonWriter;

    .line 448
    iget-boolean v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->hasW:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    array-length v0, v0

    if-le v0, v4, :cond_1

    .line 449
    const-string v0, "w"

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$Data$SensorData;->values:[F

    aget v1, v1, v4

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/util/JsonWriter;->value(D)Landroid/util/JsonWriter;

    .line 451
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 452
    return-void
.end method
