.class Lcom/navdy/client/app/service/DataCollectionService$7;
.super Ljava/lang/Object;
.source "DataCollectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/service/DataCollectionService;->addLastLocationToData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/service/DataCollectionService;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/service/DataCollectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 990
    iput-object p1, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 993
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$100(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 994
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$1100(Lcom/navdy/client/app/service/DataCollectionService;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 995
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LOCATION           : nbRecords="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v3, v3, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v3, v3, Lcom/navdy/client/app/service/DataCollectionService$Data;->locations:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " \tdata="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v3}, Lcom/navdy/client/app/service/DataCollectionService;->access$1100(Lcom/navdy/client/app/service/DataCollectionService;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 996
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v0, v0, Lcom/navdy/client/app/service/DataCollectionService$Data;->locations:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v2}, Lcom/navdy/client/app/service/DataCollectionService;->access$1100(Lcom/navdy/client/app/service/DataCollectionService;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 997
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/navdy/client/app/service/DataCollectionService;->access$1102(Lcom/navdy/client/app/service/DataCollectionService;Lcom/navdy/client/app/service/DataCollectionService$Data$Location;)Lcom/navdy/client/app/service/DataCollectionService$Data$Location;

    .line 999
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v0}, Lcom/navdy/client/app/service/DataCollectionService;->access$1300(Lcom/navdy/client/app/service/DataCollectionService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v2, p0, Lcom/navdy/client/app/service/DataCollectionService$7;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v2}, Lcom/navdy/client/app/service/DataCollectionService;->access$1200(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1000
    monitor-exit v1

    .line 1001
    return-void

    .line 1000
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
