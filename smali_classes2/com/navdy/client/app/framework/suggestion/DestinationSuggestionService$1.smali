.class Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;
.super Ljava/lang/Object;
.source "DestinationSuggestionService.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->handleSuggest(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "suggestions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const/4 v2, 0x0

    .line 170
    iget-object v1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-static {v1, p1}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$000(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Ljava/util/ArrayList;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    new-instance v0, Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;)V

    .line 173
    .local v0, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    iget-object v1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    sget-object v2, Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;->SUGGESTION_CALENDAR:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-static {v1, v0, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$100(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 179
    .end local v0    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$1;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$200(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Z)V

    goto :goto_0
.end method
