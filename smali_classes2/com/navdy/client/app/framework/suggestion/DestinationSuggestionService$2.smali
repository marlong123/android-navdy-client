.class Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;
.super Ljava/lang/Object;
.source "DestinationSuggestionService.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->sendSuggestionToHud(Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

.field final synthetic val$destinationMessage:Lcom/navdy/service/library/events/destination/Destination;

.field final synthetic val$suggestionType:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/service/library/events/destination/Destination;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    iput-object p2, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->val$destinationMessage:Lcom/navdy/service/library/events/destination/Destination;

    iput-object p3, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->val$suggestionType:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V
    .locals 0
    .param p1, "routeHandle"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 289
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V
    .locals 6
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    const v4, 0xfffffff

    .line 294
    const/4 v0, -0x1

    .line 295
    .local v0, "routeDurationWithTraffic":I
    sget-object v3, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    if-ne p1, v3, :cond_0

    .line 296
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {p2, v3, v4}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v1

    .line 299
    .local v1, "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    if-eqz v1, :cond_0

    .line 300
    sget-object v3, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    invoke-virtual {p2, v3, v4}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v3

    .line 302
    invoke-virtual {v3}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v0

    .line 305
    .end local v1    # "routeTta":Lcom/here/android/mpa/routing/RouteTta;
    :cond_0
    new-instance v2, Lcom/navdy/service/library/events/places/SuggestedDestination;

    iget-object v3, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->val$destinationMessage:Lcom/navdy/service/library/events/destination/Destination;

    .line 307
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->val$suggestionType:Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;

    invoke-direct {v2, v3, v4, v5}, Lcom/navdy/service/library/events/places/SuggestedDestination;-><init>(Lcom/navdy/service/library/events/destination/Destination;Ljava/lang/Integer;Lcom/navdy/service/library/events/places/SuggestedDestination$SuggestionType;)V

    .line 309
    .local v2, "suggestedDestinationEvent":Lcom/navdy/service/library/events/places/SuggestedDestination;
    iget-object v3, p0, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService$2;->this$0:Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;

    invoke-static {v3, v2}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->access$300(Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;Lcom/navdy/service/library/events/places/SuggestedDestination;)V

    .line 310
    return-void
.end method
