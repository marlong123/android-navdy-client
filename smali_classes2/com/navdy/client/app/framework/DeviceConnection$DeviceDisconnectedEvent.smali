.class public Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
.super Lcom/navdy/client/app/framework/DeviceConnection$DeviceRelatedEvent;
.source "DeviceConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/DeviceConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceDisconnectedEvent"
.end annotation


# instance fields
.field cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/DeviceConnection$DeviceRelatedEvent;-><init>(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 154
    iput-object p2, p0, Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;->cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .line 155
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceConnectionFailedEvent: cause=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;->cause:Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] device=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;->device:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
