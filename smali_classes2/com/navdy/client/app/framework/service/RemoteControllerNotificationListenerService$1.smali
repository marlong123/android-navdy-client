.class Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;
.super Ljava/lang/Object;
.source "RemoteControllerNotificationListenerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->initializeRemoteController()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/16 v6, 0xc8

    .line 69
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    new-instance v3, Landroid/media/RemoteController;

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    iget-object v5, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-direct {v3, v4, v5}, Landroid/media/RemoteController;-><init>(Landroid/content/Context;Landroid/media/RemoteController$OnClientUpdateListener;)V

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$002(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;Landroid/media/RemoteController;)Landroid/media/RemoteController;

    .line 73
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-static {v2}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$000(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/RemoteController;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Landroid/media/RemoteController;->setArtworkConfiguration(II)Z

    move-result v0

    .line 76
    .local v0, "artworkConfigurationSuccessful":Z
    if-nez v0, :cond_0

    .line 77
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Couldn\'t set artwork configuration"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    invoke-static {v3, v2}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$102(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;Landroid/media/AudioManager;)Landroid/media/AudioManager;

    .line 83
    :try_start_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-static {v2}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$100(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/AudioManager;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-static {v3}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$000(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/RemoteController;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->registerRemoteController(Landroid/media/RemoteController;)Z

    .line 84
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-static {v4}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$000(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;)Landroid/media/RemoteController;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/navdy/client/app/framework/music/RemoteControllerSeekHelper;-><init>(Landroid/media/RemoteController;)V

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setMusicSeekHelper(Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler$MusicSeekHelper;)V

    .line 85
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Remote controller and audio manager initialized"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService$1;->this$0:Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->access$002(Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;Landroid/media/RemoteController;)Landroid/media/RemoteController;

    .line 89
    sget-object v2, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote controller could not be initialized: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
