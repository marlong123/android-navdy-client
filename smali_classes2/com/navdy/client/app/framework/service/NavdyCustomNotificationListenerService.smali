.class public Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
.super Landroid/service/notification/NotificationListenerService;
.source "NavdyCustomNotificationListenerService.java"


# static fields
.field private static final INDENT_INCREMENT:I = 0x4

.field private static final NOTIFICATION_WAIT_FOR_MORE_DELAY:J = 0x3e8L

.field private static final SPACES:Ljava/lang/String; = " "

.field public static final notifListenerServiceHandler:Landroid/os/Handler;

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;

.field public static final splitTickerTextOnColon:Ljava/util/regex/Pattern;


# instance fields
.field private activeMusicApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private incomingNotifications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private knownGroups:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private knownNotifications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lastOngoing:Ljava/lang/String;

.field private pendingGroupNotifications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-string v1, "NavdyNotifHandler"

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 151
    const-string v0, "(.*): (.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->splitTickerTextOnColon:Ljava/util/regex/Pattern;

    .line 152
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->notifListenerServiceHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 171
    invoke-direct {p0}, Landroid/service/notification/NotificationListenerService;-><init>()V

    .line 162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->activeMusicApps:Ljava/util/List;

    .line 164
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownNotifications:Ljava/util/HashMap;

    .line 165
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownGroups:Ljava/util/HashMap;

    .line 166
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->pendingGroupNotifications:Ljava/util/HashMap;

    .line 167
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->incomingNotifications:Ljava/util/HashMap;

    .line 169
    const-string v1, ""

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->lastOngoing:Ljava/lang/String;

    .line 172
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 173
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 174
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getDialerPackage(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->addPackageToGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)V

    .line 176
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
    .param p1, "x1"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->handleStatusBarNotification(Landroid/service/notification/StatusBarNotification;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownGroups:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
    .param p1, "x1"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->categorizeAndHandleNotification(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->pendingGroupNotifications:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
    .param p1, "x1"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->isMusicNotification(Landroid/service/notification/StatusBarNotification;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;
    .param p1, "x1"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getHash(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownNotifications:Ljava/util/HashMap;

    return-object v0
.end method

.method private categorizeAndHandleNotification(Landroid/service/notification/StatusBarNotification;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "ticker"    # Ljava/lang/String;

    .prologue
    .line 493
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "categorizeAndHandleNotification: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] ticker["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 494
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MESSAGING_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p2, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    invoke-static {p1}, Lcom/navdy/client/app/framework/glances/MessagingNotificationHandler;->handleMessageNotification(Landroid/service/notification/StatusBarNotification;)V

    .line 512
    :goto_0
    return-void

    .line 496
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->EMAIL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p2, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 497
    invoke-static {p1}, Lcom/navdy/client/app/framework/glances/EmailNotificationHandler;->handleEmailNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 498
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->CALENDAR_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p2, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.android.calendar"

    .line 499
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 501
    :cond_2
    const-string v0, "com.google.android.calendar"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p3, :cond_3

    .line 502
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "null ticker"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :cond_3
    invoke-static {p1}, Lcom/navdy/client/app/framework/glances/CalendarNotificationHandler;->handleCalendarNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 506
    :cond_4
    sget-object v0, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->SOCIAL_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {p2, v0}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 507
    invoke-static {p1}, Lcom/navdy/client/app/framework/glances/SocialNotificationHandler;->handleSocialNotification(Landroid/service/notification/StatusBarNotification;)V

    goto :goto_0

    .line 509
    :cond_5
    invoke-static {p1}, Lcom/navdy/client/app/framework/glances/GenericNotificationHandler;->handleGenericNotification(Landroid/service/notification/StatusBarNotification;)V

    .line 510
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NotifListenerService: generic notification ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFirstParticipantInCarConversation(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 4
    .param p0, "extras"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 314
    const-string v3, "android.car.EXTENSIONS"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 315
    .local v0, "carExt":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 316
    const-string v3, "car_conversation"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 317
    .local v1, "convo":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 318
    const-string v3, "participants"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 319
    .local v2, "pariticipants":[Ljava/lang/String;
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    .line 320
    const/4 v3, 0x0

    aget-object v3, v2, v3

    .line 324
    .end local v1    # "convo":Landroid/os/Bundle;
    .end local v2    # "pariticipants":[Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getHash(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 2
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLastMessageInCarConversation(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 7
    .param p0, "extras"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 277
    const/4 v3, 0x0

    .line 278
    .local v3, "message":Ljava/lang/String;
    const-string v6, "android.car.EXTENSIONS"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 279
    .local v0, "carExt":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 280
    const-string v6, "car_conversation"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 281
    .local v1, "convo":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 282
    const-string v6, "messages"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    .line 283
    .local v4, "messages":[Landroid/os/Parcelable;
    if-eqz v4, :cond_0

    array-length v6, v4

    if-lez v6, :cond_0

    .line 284
    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v2, v4, v6

    .line 285
    .local v2, "lastMessage":Landroid/os/Parcelable;
    if-eqz v2, :cond_0

    instance-of v6, v2, Landroid/os/Bundle;

    if-eqz v6, :cond_0

    .line 288
    check-cast v2, Landroid/os/Bundle;

    .end local v2    # "lastMessage":Landroid/os/Parcelable;
    const-string v6, "text"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 289
    .local v5, "text":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 293
    move-object v3, v5

    .line 299
    .end local v1    # "convo":Landroid/os/Bundle;
    .end local v4    # "messages":[Landroid/os/Parcelable;
    .end local v5    # "text":Ljava/lang/String;
    :cond_0
    return-object v3
.end method

.method public static getLastTextLine(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 4
    .param p0, "extras"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 329
    const-string v3, "android.textLines"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 330
    .local v2, "textLines":Ljava/lang/Object;
    if-eqz v2, :cond_0

    instance-of v3, v2, [Ljava/lang/CharSequence;

    if-eqz v3, :cond_0

    .line 331
    check-cast v2, [Ljava/lang/CharSequence;

    .end local v2    # "textLines":Ljava/lang/Object;
    move-object v1, v2

    check-cast v1, [Ljava/lang/CharSequence;

    .line 332
    .local v1, "lines":[Ljava/lang/CharSequence;
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v1, v3

    .line 333
    .local v0, "lastLine":Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 334
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 337
    .end local v0    # "lastLine":Ljava/lang/CharSequence;
    .end local v1    # "lines":[Ljava/lang/CharSequence;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "extras"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 342
    const/4 v1, 0x0

    .line 343
    .local v1, "string":Ljava/lang/String;
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 344
    .local v0, "o":Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 345
    check-cast v1, Ljava/lang/String;

    .line 349
    :cond_0
    :goto_0
    return-object v1

    .line 346
    :cond_1
    instance-of v2, v0, Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    .line 347
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getText(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;
    .locals 5
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 224
    if-nez p1, :cond_1

    move-object v2, v3

    .line 262
    :cond_0
    :goto_0
    return-object v2

    .line 228
    :cond_1
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v1

    .line 229
    .local v1, "notif":Landroid/app/Notification;
    if-nez v1, :cond_2

    move-object v2, v3

    .line 230
    goto :goto_0

    .line 233
    :cond_2
    invoke-static {v1}, Landroid/support/v4/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v0

    .line 234
    .local v0, "extras":Landroid/os/Bundle;
    if-nez v0, :cond_3

    move-object v2, v3

    .line 235
    goto :goto_0

    .line 238
    :cond_3
    invoke-static {v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getLastMessageInCarConversation(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    .line 239
    .local v2, "text":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 243
    const-string v4, "android.text"

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 244
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 248
    const-string v4, "android.bigText"

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 249
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 253
    const-string v4, "android.title"

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getStringSafely(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 254
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 258
    iget-object v4, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    if-eqz v4, :cond_4

    .line 259
    iget-object v3, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_4
    move-object v2, v3

    .line 262
    goto :goto_0
.end method

.method private handleStatusBarNotification(Landroid/service/notification/StatusBarNotification;)V
    .locals 12
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 354
    :try_start_0
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 355
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v2

    .line 356
    .local v2, "notif":Landroid/app/Notification;
    invoke-static {v2}, Landroid/support/v4/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v6

    .line 358
    .local v6, "extras":Landroid/os/Bundle;
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->isMusicNotification(Landroid/service/notification/StatusBarNotification;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 359
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Music notification posted: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->addActiveMusicApp(Ljava/lang/String;)V

    .line 488
    .end local v2    # "notif":Landroid/app/Notification;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v6    # "extras":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 367
    .restart local v2    # "notif":Landroid/app/Notification;
    .restart local v3    # "packageName":Ljava/lang/String;
    .restart local v6    # "extras":Landroid/os/Bundle;
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v4, "glances"

    const/4 v10, 0x0

    invoke-interface {v1, v4, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isOngoing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 373
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->lastOngoing:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 374
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleStatusBarNotification: ongoing notif skip:"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 375
    iput-object v3, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->lastOngoing:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 485
    .end local v2    # "notif":Landroid/app/Notification;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v6    # "extras":Landroid/os/Bundle;
    :catch_0
    move-exception v8

    .line 486
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v1, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 380
    .end local v8    # "t":Ljava/lang/Throwable;
    .restart local v2    # "notif":Landroid/app/Notification;
    .restart local v3    # "packageName":Ljava/lang/String;
    .restart local v6    # "extras":Landroid/os/Bundle;
    :cond_2
    const/4 v9, 0x0

    .line 381
    .local v9, "ticker":Ljava/lang/String;
    :try_start_1
    iget-object v1, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    .line 382
    iget-object v1, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 385
    :cond_3
    sget-object v1, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->IGNORE_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {v3, v1}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 387
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleStatusBarNotification: ignored ["

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "]"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_4
    invoke-static {}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->isConnectedToHud()Z

    move-result v1

    if-nez v1, :cond_5

    .line 393
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleStatusBarNotification: hud not connected ["

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "]"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 397
    :cond_5
    if-nez v6, :cond_6

    .line 398
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleStatusBarNotification: no extras ["

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "]"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 407
    :cond_6
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getHash(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v7

    .line 408
    .local v7, "notificationHashKey":Ljava/lang/String;
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownNotifications:Ljava/util/HashMap;

    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 409
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "handleStatusBarNotification: already seen this notification: ["

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "] so won\'t send to HUD to avoid duplicates"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 413
    :cond_7
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->knownNotifications:Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v7, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    move-object v5, v9

    .line 416
    .local v5, "finalTicker":Ljava/lang/String;
    new-instance v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$2;-><init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/app/Notification;Ljava/lang/String;Landroid/service/notification/StatusBarNotification;Ljava/lang/String;)V

    .line 480
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->cancelPendingHandlerIfAnyFor(Landroid/service/notification/StatusBarNotification;)V

    .line 482
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onNotificationPosted:   Adding a pending runnable for "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 483
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->incomingNotifications:Ljava/util/HashMap;

    invoke-virtual {v1, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->notifListenerServiceHandler:Landroid/os/Handler;

    const-wide/16 v10, 0x3e8

    invoke-virtual {v1, v0, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private static isConnectedToHud()Z
    .locals 1

    .prologue
    .line 609
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v0

    return v0
.end method

.method private isMusicNotification(Landroid/service/notification/StatusBarNotification;)Z
    .locals 6
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 568
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 571
    .local v1, "packageName":Ljava/lang/String;
    const-string v4, "com.spotify.music"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v3

    .line 582
    :cond_0
    :goto_0
    return v2

    .line 575
    :cond_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_2

    .line 576
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v4

    invoke-static {v4}, Landroid/support/v4/app/NotificationCompat;->getCategory(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v0

    .line 577
    .local v0, "category":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v4, "transport"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 582
    .end local v0    # "category":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isOngoing()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;->MUSIC_GROUP:Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;

    invoke-static {v1, v4}, Lcom/navdy/client/app/framework/glances/GlanceConstants;->isPackageInGroup(Ljava/lang/String;Lcom/navdy/client/app/framework/glances/GlanceConstants$Group;)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method private logNotification(Landroid/app/Notification;I)V
    .locals 13
    .param p1, "notif"    # Landroid/app/Notification;
    .param p2, "indentation"    # I

    .prologue
    .line 631
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "s################ NOTIFICATION"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 632
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sPriority: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, p1, Landroid/app/Notification;->priority:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 633
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sFlags: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, p1, Landroid/app/Notification;->flags:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 634
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sTicker text: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 635
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x15

    if-lt v8, v9, :cond_0

    .line 636
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sPublic Version: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget-object v12, p1, Landroid/app/Notification;->publicVersion:Landroid/app/Notification;

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 639
    :cond_0
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->getCategory(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v2

    .line 640
    .local v2, "category":Ljava/lang/String;
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->getGroup(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v4

    .line 641
    .local v4, "group":Ljava/lang/String;
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->getSortKey(Landroid/app/Notification;)Ljava/lang/String;

    move-result-object v7

    .line 642
    .local v7, "sortKey":Ljava/lang/String;
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->isGroupSummary(Landroid/app/Notification;)Z

    move-result v6

    .line 643
    .local v6, "isGroupSummary":Z
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sCategory: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 644
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sGroup: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 645
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sSort Key: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v7, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 646
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sIs Group Summary: %s"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 648
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->getExtras(Landroid/app/Notification;)Landroid/os/Bundle;

    move-result-object v3

    .line 649
    .local v3, "extras":Landroid/os/Bundle;
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "s####### BUNDLE"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 650
    add-int/lit8 v8, p2, 0x4

    invoke-virtual {p0, v8, v3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logBundle(ILandroid/os/Bundle;)V

    .line 652
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "s####### ACTIONS"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 653
    invoke-static {p1}, Landroid/support/v4/app/NotificationCompat;->getActionCount(Landroid/app/Notification;)I

    move-result v1

    .line 654
    .local v1, "actionCount":I
    sget-object v8, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "sAction Count: %d"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, " "

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 655
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_1

    .line 656
    invoke-static {p1, v5}, Landroid/support/v4/app/NotificationCompat;->getAction(Landroid/app/Notification;I)Landroid/support/v4/app/NotificationCompat$Action;

    move-result-object v0

    .line 657
    .local v0, "action":Landroid/support/v4/app/NotificationCompat$Action;
    invoke-virtual {p0, p2, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logAction(ILandroid/support/v4/app/NotificationCompat$Action;)V

    .line 655
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 659
    .end local v0    # "action":Landroid/support/v4/app/NotificationCompat$Action;
    :cond_1
    return-void
.end method

.method private print(Landroid/service/notification/StatusBarNotification;)V
    .locals 6
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 613
    if-nez p1, :cond_0

    .line 628
    :goto_0
    return-void

    .line 617
    :cond_0
    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getNotification()Landroid/app/Notification;

    move-result-object v0

    .line 619
    .local v0, "notif":Landroid/app/Notification;
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "################ STATUS BAR NOTIFICATION ###############"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 620
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPostTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 621
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 622
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tag: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getTag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 623
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 624
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ongoing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isOngoing()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 625
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Clearable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/service/notification/StatusBarNotification;->isClearable()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 627
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logNotification(Landroid/app/Notification;I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public addActiveMusicApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 586
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->activeMusicApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->activeMusicApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->setLastMusicApp(Ljava/lang/String;)V

    .line 595
    :cond_0
    return-void
.end method

.method public cancelPendingHandlerIfAnyFor(Landroid/service/notification/StatusBarNotification;)V
    .locals 5
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 552
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->getHash(Landroid/service/notification/StatusBarNotification;)Ljava/lang/String;

    move-result-object v0

    .line 553
    .local v0, "notificationHashKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->incomingNotifications:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 554
    .local v1, "previousRunnable":Ljava/lang/Runnable;
    if-eqz v1, :cond_0

    .line 555
    sget-object v2, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNotificationPosted: Removing a pending runnable for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 556
    sget-object v2, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->notifListenerServiceHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 558
    :cond_0
    return-void
.end method

.method public logAction(ILandroid/app/Notification$Action;)V
    .locals 4
    .param p1, "indentation"    # I
    .param p2, "action"    # Landroid/app/Notification$Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 734
    new-instance v0, Landroid/support/v4/app/NotificationCompat$Action$Builder;

    iget v1, p2, Landroid/app/Notification$Action;->icon:I

    iget-object v2, p2, Landroid/app/Notification$Action;->title:Ljava/lang/CharSequence;

    iget-object v3, p2, Landroid/app/Notification$Action;->actionIntent:Landroid/app/PendingIntent;

    invoke-direct {v0, v1, v2, v3}, Landroid/support/v4/app/NotificationCompat$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 736
    .local v0, "ncab":Landroid/support/v4/app/NotificationCompat$Action$Builder;
    invoke-virtual {v0}, Landroid/support/v4/app/NotificationCompat$Action$Builder;->build()Landroid/support/v4/app/NotificationCompat$Action;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logAction(ILandroid/support/v4/app/NotificationCompat$Action;)V

    .line 737
    return-void
.end method

.method public logAction(ILandroid/support/v4/app/NotificationCompat$Action;)V
    .locals 9
    .param p1, "indentation"    # I
    .param p2, "action"    # Landroid/support/v4/app/NotificationCompat$Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 740
    sget-object v3, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "s-- %s:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, " "

    aput-object v6, v5, v2

    iget-object v6, p2, Landroid/support/v4/app/NotificationCompat$Action;->title:Ljava/lang/CharSequence;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 741
    sget-object v3, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, p1, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sAction Intent: %s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, " "

    aput-object v6, v5, v2

    iget-object v6, p2, Landroid/support/v4/app/NotificationCompat$Action;->actionIntent:Landroid/app/PendingIntent;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 743
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_0

    .line 744
    sget-object v3, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, p1, 0x4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sAllow generated replies: %b"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, " "

    aput-object v6, v5, v2

    .line 745
    invoke-virtual {p2}, Landroid/support/v4/app/NotificationCompat$Action;->getAllowGeneratedReplies()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v7

    .line 744
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 748
    :cond_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x14

    if-lt v3, v4, :cond_1

    .line 749
    add-int/lit8 v3, p1, 0x4

    invoke-virtual {p2}, Landroid/support/v4/app/NotificationCompat$Action;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logBundle(ILandroid/os/Bundle;)V

    .line 750
    invoke-virtual {p2}, Landroid/support/v4/app/NotificationCompat$Action;->getRemoteInputs()[Landroid/support/v4/app/RemoteInput;

    move-result-object v1

    .line 751
    .local v1, "remoteInputs":[Landroid/support/v4/app/RemoteInput;
    if-eqz v1, :cond_1

    .line 752
    array-length v3, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 753
    .local v0, "remoteInput":Landroid/support/v4/app/RemoteInput;
    add-int/lit8 v4, p1, 0x4

    invoke-virtual {p0, v4, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logRemoteInput(ILandroid/support/v4/app/RemoteInput;)V

    .line 752
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 757
    .end local v0    # "remoteInput":Landroid/support/v4/app/RemoteInput;
    .end local v1    # "remoteInputs":[Landroid/support/v4/app/RemoteInput;
    :cond_1
    return-void
.end method

.method public logBundle(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "indentation"    # I
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 662
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 668
    :cond_0
    return-void

    .line 664
    :cond_1
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 665
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 666
    .local v1, "value":Ljava/lang/Object;
    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logUnknownObject(ILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public logRemoteInput(ILandroid/app/RemoteInput;)V
    .locals 4
    .param p1, "indentation"    # I
    .param p2, "remoteInput"    # Landroid/app/RemoteInput;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 710
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x14

    if-lt v2, v3, :cond_0

    .line 711
    new-instance v0, Landroid/support/v4/app/RemoteInput$Builder;

    invoke-virtual {p2}, Landroid/app/RemoteInput;->getResultKey()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v4/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    .line 712
    .local v0, "rib":Landroid/support/v4/app/RemoteInput$Builder;
    invoke-virtual {p2}, Landroid/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/support/v4/app/RemoteInput$Builder;

    .line 713
    invoke-virtual {p2}, Landroid/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/support/v4/app/RemoteInput$Builder;

    .line 714
    invoke-virtual {p2}, Landroid/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/support/v4/app/RemoteInput$Builder;

    .line 715
    invoke-virtual {p2}, Landroid/app/RemoteInput;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/support/v4/app/RemoteInput$Builder;

    .line 716
    invoke-virtual {v0}, Landroid/support/v4/app/RemoteInput$Builder;->build()Landroid/support/v4/app/RemoteInput;

    move-result-object v1

    .line 717
    .local v1, "supportRemoteInput":Landroid/support/v4/app/RemoteInput;
    invoke-virtual {p0, p1, v1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logRemoteInput(ILandroid/support/v4/app/RemoteInput;)V

    .line 719
    .end local v0    # "rib":Landroid/support/v4/app/RemoteInput$Builder;
    .end local v1    # "supportRemoteInput":Landroid/support/v4/app/RemoteInput;
    :cond_0
    return-void
.end method

.method public logRemoteInput(ILandroid/support/v4/app/RemoteInput;)V
    .locals 11
    .param p1, "indentation"    # I
    .param p2, "input"    # Landroid/support/v4/app/RemoteInput;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 722
    sget-object v2, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sInput: %s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, " "

    aput-object v6, v5, v3

    invoke-virtual {p2}, Landroid/support/v4/app/RemoteInput;->getLabel()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 723
    sget-object v2, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sAllowFreeFormInput: %s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v10, [Ljava/lang/Object;

    const-string v6, " "

    aput-object v6, v5, v3

    .line 724
    invoke-virtual {p2}, Landroid/support/v4/app/RemoteInput;->getAllowFreeFormInput()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v9

    .line 723
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 725
    invoke-virtual {p2}, Landroid/support/v4/app/RemoteInput;->getChoices()[Ljava/lang/CharSequence;

    move-result-object v1

    .line 726
    .local v1, "choices":[Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    .line 727
    array-length v4, v1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v1, v2

    .line 728
    .local v0, "choice":Ljava/lang/CharSequence;
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s  + Choice: %s"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v10, [Ljava/lang/Object;

    const-string v8, " "

    aput-object v8, v7, v3

    aput-object v0, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 727
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 731
    .end local v0    # "choice":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method

.method public logUnknownObject(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 10
    .param p1, "indentation"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/Object;

    .prologue
    .line 672
    if-eqz p3, :cond_7

    .line 673
    instance-of v5, p3, Landroid/os/Bundle;

    if-eqz v5, :cond_1

    .line 674
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s####### %s: \"%s\" (%s)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, " "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    .line 675
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 674
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 676
    add-int/lit8 v5, p1, 0x4

    check-cast p3, Landroid/os/Bundle;

    .end local p3    # "value":Ljava/lang/Object;
    invoke-virtual {p0, v5, p3}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logBundle(ILandroid/os/Bundle;)V

    .line 707
    :cond_0
    :goto_0
    return-void

    .line 677
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_1
    instance-of v5, p3, Landroid/support/v4/app/RemoteInput;

    if-eqz v5, :cond_2

    move-object v4, p3

    .line 678
    check-cast v4, Landroid/support/v4/app/RemoteInput;

    .line 679
    .local v4, "remoteInput":Landroid/support/v4/app/RemoteInput;
    invoke-virtual {p0, p1, v4}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logRemoteInput(ILandroid/support/v4/app/RemoteInput;)V

    goto :goto_0

    .line 680
    .end local v4    # "remoteInput":Landroid/support/v4/app/RemoteInput;
    :cond_2
    instance-of v5, p3, Landroid/app/Notification$Action;

    if-eqz v5, :cond_3

    move-object v0, p3

    .line 681
    check-cast v0, Landroid/app/Notification$Action;

    .line 682
    .local v0, "action":Landroid/app/Notification$Action;
    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logAction(ILandroid/app/Notification$Action;)V

    goto :goto_0

    .line 683
    .end local v0    # "action":Landroid/app/Notification$Action;
    :cond_3
    instance-of v5, p3, Landroid/app/Notification;

    if-eqz v5, :cond_4

    move-object v3, p3

    .line 684
    check-cast v3, Landroid/app/Notification;

    .line 685
    .local v3, "notificaiton":Landroid/app/Notification;
    invoke-direct {p0, v3, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logNotification(Landroid/app/Notification;I)V

    goto :goto_0

    .line 686
    .end local v3    # "notificaiton":Landroid/app/Notification;
    :cond_4
    instance-of v5, p3, [Ljava/lang/Object;

    if-eqz v5, :cond_5

    .line 687
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s- %s: \"%s\" (%s)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, " "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    .line 688
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 687
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 689
    check-cast p3, [Ljava/lang/Object;

    .end local p3    # "value":Ljava/lang/Object;
    move-object v1, p3

    check-cast v1, [Ljava/lang/Object;

    .line 690
    .local v1, "array":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v1

    if-ge v2, v5, :cond_0

    .line 691
    add-int/lit8 v5, p1, 0x4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aget-object v7, v1, v2

    invoke-virtual {p0, v5, v6, v7}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logUnknownObject(ILjava/lang/String;Ljava/lang/Object;)V

    .line 690
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 693
    .end local v1    # "array":[Ljava/lang/Object;
    .end local v2    # "i":I
    .restart local p3    # "value":Ljava/lang/Object;
    :cond_5
    instance-of v5, p3, Ljava/util/List;

    if-eqz v5, :cond_6

    .line 694
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s- %s: \"%s\" (%s)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, " "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    .line 695
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 694
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    move-object v1, p3

    .line 696
    check-cast v1, Ljava/util/List;

    .line 697
    .local v1, "array":Ljava/util/List;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 698
    add-int/lit8 v5, p1, 0x4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {p0, v5, v6, v7}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->logUnknownObject(ILjava/lang/String;Ljava/lang/Object;)V

    .line 697
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 701
    .end local v1    # "array":Ljava/util/List;
    .end local v2    # "i":I
    :cond_6
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s- %s: \"%s\" (%s)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, " "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    .line 702
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 701
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 705
    :cond_7
    sget-object v5, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "s- %s: null"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, " "

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 180
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 181
    invoke-super {p0}, Landroid/service/notification/NotificationListenerService;->onCreate()V

    .line 182
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 186
    sget-object v0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDestroy"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 187
    invoke-super {p0}, Landroid/service/notification/NotificationListenerService;->onDestroy()V

    .line 188
    return-void
.end method

.method public onNotificationPosted(Landroid/service/notification/StatusBarNotification;)V
    .locals 3
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 202
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$1;-><init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)V

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public onNotificationRemoved(Landroid/service/notification/StatusBarNotification;)V
    .locals 3
    .param p1, "sbn"    # Landroid/service/notification/StatusBarNotification;

    .prologue
    .line 516
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService$3;-><init>(Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;Landroid/service/notification/StatusBarNotification;)V

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 548
    return-void
.end method

.method public removeActiveMusicApp(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 598
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->activeMusicApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->activeMusicApps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 606
    :cond_0
    return-void
.end method

.method public removePendingHandlingForGroup(Ljava/lang/String;)V
    .locals 2
    .param p1, "group"    # Ljava/lang/String;

    .prologue
    .line 561
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->pendingGroupNotifications:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 562
    .local v0, "pendingRunnable":Ljava/lang/Runnable;
    if-eqz v0, :cond_0

    .line 563
    sget-object v1, Lcom/navdy/client/app/framework/service/NavdyCustomNotificationListenerService;->notifListenerServiceHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 565
    :cond_0
    return-void
.end method
