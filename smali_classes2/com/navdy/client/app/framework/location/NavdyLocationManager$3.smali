.class Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/location/NavdyLocationManager;->initLocationServices()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/location/NavdyLocationManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/location/Location;Landroid/location/Location;)I
    .locals 4
    .param p1, "lhs"    # Landroid/location/Location;
    .param p2, "rhs"    # Landroid/location/Location;

    .prologue
    .line 290
    iget-object v0, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v0, p1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1000(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)D

    move-result-wide v0

    iget-object v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;->this$0:Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    invoke-static {v2, p2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1000(Lcom/navdy/client/app/framework/location/NavdyLocationManager;Landroid/location/Location;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 287
    check-cast p1, Landroid/location/Location;

    check-cast p2, Landroid/location/Location;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/location/NavdyLocationManager$3;->compare(Landroid/location/Location;Landroid/location/Location;)I

    move-result v0

    return v0
.end method
