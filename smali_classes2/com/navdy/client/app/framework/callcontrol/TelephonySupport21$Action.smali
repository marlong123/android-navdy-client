.class final enum Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;
.super Ljava/lang/Enum;
.source "TelephonySupport21.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

.field public static final enum ACCEPT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

.field public static final enum END_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

.field public static final enum REJECT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    const-string v1, "ACCEPT_CALL"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->ACCEPT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    .line 32
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    const-string v1, "REJECT_CALL"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->REJECT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    .line 35
    new-instance v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    const-string v1, "END_CALL"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->END_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    sget-object v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->ACCEPT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->REJECT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->END_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->$VALUES:[Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->$VALUES:[Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    return-object v0
.end method
