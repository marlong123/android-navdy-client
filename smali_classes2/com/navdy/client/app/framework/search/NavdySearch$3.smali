.class Lcom/navdy/client/app/framework/search/NavdySearch$3;
.super Landroid/os/AsyncTask;
.source "NavdySearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/search/NavdySearch;->handleHudSearchResponse(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

.field final synthetic val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/search/NavdySearch;Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/search/NavdySearch;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    iput-object p2, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 310
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 313
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    iget-object v1, v1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$100(Lcom/navdy/client/app/framework/search/NavdySearch;Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 310
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/search/NavdySearch$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 320
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    iget-object v1, v1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    if-ne v1, v2, :cond_0

    .line 321
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    iget-object v0, v1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    .line 326
    .local v0, "hudSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$300(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/client/app/framework/search/SearchResults;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/framework/search/SearchResults;->setHudSearchResults(Ljava/util/List;)V

    .line 327
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$400(Lcom/navdy/client/app/framework/search/NavdySearch;)V

    .line 328
    return-void

    .line 323
    .end local v0    # "hudSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    :cond_0
    const/4 v0, 0x0

    .line 324
    .restart local v0    # "hudSearchResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    iget-object v1, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->this$0:Lcom/navdy/client/app/framework/search/NavdySearch;

    invoke-static {v1}, Lcom/navdy/client/app/framework/search/NavdySearch;->access$200(Lcom/navdy/client/app/framework/search/NavdySearch;)Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Request failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/search/NavdySearch$3;->val$placesSearchResponse:Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    iget-object v3, v3, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0
.end method
