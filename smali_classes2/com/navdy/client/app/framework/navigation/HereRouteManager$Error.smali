.class public final enum Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
.super Ljava/lang/Enum;
.source "HereRouteManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

.field public static final enum HERE_INTERNAL_ERROR:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

.field public static final enum NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

.field public static final enum NO_LOCATION:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

.field public static final enum NO_ROUTES:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 184
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    .line 185
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const-string v1, "NO_ROUTES"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NO_ROUTES:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    .line 186
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const-string v1, "NO_LOCATION"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NO_LOCATION:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    .line 187
    new-instance v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const-string v1, "HERE_INTERNAL_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->HERE_INTERNAL_ERROR:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    .line 183
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NO_ROUTES:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NO_LOCATION:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->HERE_INTERNAL_ERROR:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->$VALUES:[Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 183
    const-class v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
    .locals 1

    .prologue
    .line 183
    sget-object v0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->$VALUES:[Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    return-object v0
.end method
