.class Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;
.super Ljava/lang/Object;
.source "HereMapsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereMapsManager$1;->onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

.field final synthetic val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereMapsManager$1;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$002(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/here/android/mpa/common/OnEngineInitListener$Error;)Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .line 62
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    sget-object v1, Lcom/here/android/mpa/common/OnEngineInitListener$Error;->NONE:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    if-ne v0, v1, :cond_0

    .line 63
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "MapEngine has initialized correctly"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 64
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HERE SDK version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/here/android/mpa/common/Version;->getSdkVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsConfigurator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/map/HereMapsConfigurator;->updateMapsConfig()V

    .line 67
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$202(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/navdy/client/app/framework/map/HereMapsManager$State;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$500(Lcom/navdy/client/app/framework/map/HereMapsManager;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1$1;-><init>(Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 86
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->FAILED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$202(Lcom/navdy/client/app/framework/map/HereMapsManager;Lcom/navdy/client/app/framework/map/HereMapsManager$State;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .line 81
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Map failed to initialize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->val$error:Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 82
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "ERROR ON MAP ENGINE INIT, RESTARTING"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereMapsManager$1$1;->this$1:Lcom/navdy/client/app/framework/map/HereMapsManager$1;

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/HereMapsManager$1;->this$0:Lcom/navdy/client/app/framework/map/HereMapsManager;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/map/HereMapsManager;->killSelfAndMapEngine()V

    goto :goto_0
.end method
