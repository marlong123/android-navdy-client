.class public Lcom/navdy/client/app/framework/PathManager;
.super Ljava/lang/Object;
.source "PathManager.java"


# static fields
.field private static final DISKCACHE_DIR:Ljava/lang/String; = "diskcache-v4"

.field private static final HERE_MAPS_CONFIG_BASE_PATH:Ljava/lang/String; = ".here-maps"

.field private static final HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

.field private static final HERE_MAPS_CONFIG_DIRS_REGEX:Ljava/lang/String; = "[0-9]{10}"

.field private static final LOGS_FOLDER:Ljava/lang/String; = "logs"

.field private static final OTA_INCREMENTAL_UPDATE_FILE_FORMAT:Ljava/lang/String; = "navdy_ota_%d_to_%d.zip"

.field private static final OTA_UPDATE_FILE_FORMAT:Ljava/lang/String; = "navdy_ota_%d.zip"

.field private static final OTA_UPDATE_FILE_NAME:Ljava/lang/String; = "hudupdate.zip"

.field private static final OTA_UPDATE_FOLDER:Ljava/lang/String; = "update"

.field private static final S3_PROGRESS_FILE_EXTENSION:Ljava/lang/String; = ".s3"

.field private static final S3_PROGRESS_FILE_PREFIX:Ljava/lang/String; = "navdy_ota_progress_"

.field public static final TICKETS_FOLDER_NAME:Ljava/lang/String; = "navdy/support_tickets/"

.field private static final TIMESTAMP_MWCONFIG_LATEST:Ljava/lang/String; = "1482069896"

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final sSingleton:Lcom/navdy/client/app/framework/PathManager;


# instance fields
.field private hereMapsConfigDirs:Ljava/io/File;

.field private mLogsFolder:Ljava/lang/String;

.field private mOtaUpdatePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/PathManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/PathManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 31
    const-string v0, "[0-9]{10}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/framework/PathManager;->HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

    .line 35
    new-instance v0, Lcom/navdy/client/app/framework/PathManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/PathManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/PathManager;->sSingleton:Lcom/navdy/client/app/framework/PathManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "internalAppPath":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "update"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mOtaUpdatePath:Ljava/lang/String;

    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "logs"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mLogsFolder:Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mOtaUpdatePath:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/lang/String;)Z

    .line 50
    iget-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mLogsFolder:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->createDirectory(Ljava/lang/String;)Z

    .line 52
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".here-maps"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    .line 53
    return-void
.end method

.method static synthetic access$000()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/client/app/framework/PathManager;->HERE_MAPS_CONFIG_DIRS_PATTERN:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/PathManager;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/navdy/client/app/framework/PathManager;->sSingleton:Lcom/navdy/client/app/framework/PathManager;

    return-object v0
.end method


# virtual methods
.method public getHereMapsConfigDirs()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 86
    .local v2, "pathList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/navdy/client/app/framework/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    new-instance v4, Lcom/navdy/client/app/framework/PathManager$1;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/framework/PathManager$1;-><init>(Lcom/navdy/client/app/framework/PathManager;)V

    invoke-virtual {v3, v4}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    .line 94
    .local v1, "hereDirs":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 95
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    .line 96
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    return-object v2
.end method

.method public getLatestHereMapsConfigPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "1482069896"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogsFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/navdy/client/app/framework/PathManager;->mLogsFolder:Ljava/lang/String;

    return-object v0
.end method

.method public getOTAUpdateFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mOtaUpdatePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hudupdate.zip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOtaUpdateFileNameOnHUD(I)Ljava/lang/String;
    .locals 4
    .param p1, "version"    # I

    .prologue
    .line 68
    const-string v0, "navdy_ota_%d.zip"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOtaUpdateFileNameOnHUD(II)Ljava/lang/String;
    .locals 4
    .param p1, "fromVersion"    # I
    .param p2, "targetVersion"    # I

    .prologue
    .line 72
    const-string v0, "navdy_ota_%d_to_%d.zip"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOtaUpdateFolderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/navdy/client/app/framework/PathManager;->mOtaUpdatePath:Ljava/lang/String;

    return-object v0
.end method

.method public getS3DownloadProgressFile(I)Ljava/lang/String;
    .locals 2
    .param p1, "incrementalVersion"    # I

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/navdy/client/app/framework/PathManager;->mOtaUpdatePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "navdy_ota_progress_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".s3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTicketFolderPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "navdy/support_tickets/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasDiskCacheDir()Z
    .locals 3

    .prologue
    .line 104
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/app/framework/PathManager;->hereMapsConfigDirs:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "diskcache-v4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v0, "diskCacheDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method
