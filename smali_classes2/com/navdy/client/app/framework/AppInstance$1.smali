.class Lcom/navdy/client/app/framework/AppInstance$1;
.super Ljava/lang/Object;
.source "AppInstance.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/AppInstance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/AppInstance;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/AppInstance;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/AppInstance;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 205
    instance-of v1, p2, Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;

    if-nez v1, :cond_0

    .line 206
    sget-object v1, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "service is not a LocalBinder, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 210
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Connected to ClientConnectionService"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    move-object v0, p2

    .line 211
    check-cast v0, Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;

    .line 212
    .local v0, "localBinder":Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;->getService()Lcom/navdy/client/app/framework/service/ClientConnectionService;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/AppInstance;->access$002(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/client/app/framework/service/ClientConnectionService;)Lcom/navdy/client/app/framework/service/ClientConnectionService;

    .line 214
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-static {v1}, Lcom/navdy/client/app/framework/AppInstance;->access$000(Lcom/navdy/client/app/framework/AppInstance;)Lcom/navdy/client/app/framework/service/ClientConnectionService;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/service/ClientConnectionService;->addListener(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;)V

    .line 216
    iget-object v1, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->initializeDevice()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 221
    sget-object v0, Lcom/navdy/client/app/framework/AppInstance;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Disconnected from ClientConnectionService service"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/navdy/client/app/framework/AppInstance$1;->this$0:Lcom/navdy/client/app/framework/AppInstance;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/AppInstance;->access$002(Lcom/navdy/client/app/framework/AppInstance;Lcom/navdy/client/app/framework/service/ClientConnectionService;)Lcom/navdy/client/app/framework/service/ClientConnectionService;

    .line 223
    return-void
.end method
