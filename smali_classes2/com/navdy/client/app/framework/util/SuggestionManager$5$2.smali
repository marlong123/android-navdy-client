.class Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;
.super Ljava/lang/Object;
.source "SuggestionManager.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager$5;->processNextCalEvent(Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

.field final synthetic val$addedCount:I

.field final synthetic val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

.field final synthetic val$event:Lcom/navdy/client/app/framework/models/CalendarEvent;

.field final synthetic val$events:Ljava/util/List;

.field final synthetic val$finalDestination:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$i:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SuggestionManager$5;Lcom/navdy/client/app/framework/models/CalendarEvent;Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    .prologue
    .line 808
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iput-object p3, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$events:Ljava/util/List;

    iput p4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$i:I

    iput p5, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$addedCount:I

    iput-object p6, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    iput-object p7, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$finalDestination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private willArriveOnTime(Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/CalendarEvent;)Z
    .locals 10
    .param p1, "route"    # Lcom/here/android/mpa/routing/Route;
    .param p2, "event"    # Lcom/navdy/client/app/framework/models/CalendarEvent;

    .prologue
    const/4 v1, 0x0

    .line 833
    if-nez p1, :cond_1

    .line 851
    :cond_0
    :goto_0
    return v1

    .line 837
    :cond_1
    sget-object v4, Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;->OPTIMAL:Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;

    const v5, 0xfffffff

    .line 838
    invoke-virtual {p1, v4, v5}, Lcom/here/android/mpa/routing/Route;->getTta(Lcom/here/android/mpa/routing/Route$TrafficPenaltyMode;I)Lcom/here/android/mpa/routing/RouteTta;

    move-result-object v4

    .line 841
    invoke-virtual {v4}, Lcom/here/android/mpa/routing/RouteTta;->getDuration()I

    move-result v4

    mul-int/lit16 v0, v4, 0x3e8

    .line 842
    .local v0, "durationWithTraffic":I
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 844
    .local v2, "now":J
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "It will take "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    div-int/lit16 v6, v0, 0x3e8

    invoke-static {v6}, Lcom/navdy/client/debug/util/FormatUtils;->formatDurationFromSecondsToSecondsMinutesHours(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to get to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 846
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "You would arrive on "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    int-to-long v8, v0

    add-long/2addr v8, v2

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 847
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " starts on "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v8, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and ends on "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/Date;

    iget-wide v8, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 850
    int-to-long v4, v0

    add-long/2addr v4, v2

    iget-wide v6, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->endTimestamp:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    int-to-long v4, v0

    add-long/2addr v4, v2

    iget-wide v6, p2, Lcom/navdy/client/app/framework/models/CalendarEvent;->startTimestamp:J

    .line 851
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$700()J

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V
    .locals 0
    .param p1, "routeHandle"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 810
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V
    .locals 8
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 816
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-direct {p0, p2, v4}, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->willArriveOnTime(Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/CalendarEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 817
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "We would not arrive on time for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-object v5, v5, Lcom/navdy/client/app/framework/models/CalendarEvent;->displayName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " so will not recommend"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 818
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$events:Ljava/util/List;

    iget v5, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$i:I

    add-int/lit8 v5, v5, 0x1

    iget v6, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$addedCount:I

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    invoke-static {v3, v4, v5, v6, v7}, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->access$500(Lcom/navdy/client/app/framework/util/SuggestionManager$5;Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    .line 830
    :goto_0
    return-void

    .line 822
    :cond_0
    new-instance v2, Lcom/navdy/client/app/framework/models/Suggestion;

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$finalDestination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v5, Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;->CALENDAR:Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;

    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$event:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-direct {v2, v4, v5, v6}, Lcom/navdy/client/app/framework/models/Suggestion;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/models/Suggestion$SuggestionType;Lcom/navdy/client/app/framework/models/CalendarEvent;)V

    .line 823
    .local v2, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    iput-boolean v3, v2, Lcom/navdy/client/app/framework/models/Suggestion;->calculateSuggestedRoute:Z

    .line 825
    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    iget-object v4, v4, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->val$calendarSuggestions:Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$600(Lcom/navdy/client/app/framework/models/Suggestion;Ljava/util/ArrayList;)Z

    move-result v1

    .line 826
    .local v1, "success":Z
    iget v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$addedCount:I

    if-eqz v1, :cond_1

    :goto_1
    add-int v0, v4, v3

    .line 827
    .local v0, "newCount":I
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v1, :cond_2

    const-string v3, "Was"

    :goto_2
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " able to add it as a calendarSuggestions. addedCount = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 829
    iget-object v3, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->this$0:Lcom/navdy/client/app/framework/util/SuggestionManager$5;

    iget-object v4, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$events:Ljava/util/List;

    iget v5, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$i:I

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SuggestionManager$5$2;->val$callback:Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;

    invoke-static {v3, v4, v5, v0, v6}, Lcom/navdy/client/app/framework/util/SuggestionManager$5;->access$500(Lcom/navdy/client/app/framework/util/SuggestionManager$5;Ljava/util/List;IILcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;)V

    goto :goto_0

    .line 826
    .end local v0    # "newCount":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 827
    .restart local v0    # "newCount":I
    :cond_2
    const-string v3, "Was not"

    goto :goto_2
.end method
