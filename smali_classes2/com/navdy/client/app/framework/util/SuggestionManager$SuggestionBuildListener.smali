.class public interface abstract Lcom/navdy/client/app/framework/util/SuggestionManager$SuggestionBuildListener;
.super Ljava/lang/Object;
.source "SuggestionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/SuggestionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SuggestionBuildListener"
.end annotation


# virtual methods
.method public abstract onSuggestionBuildComplete(Ljava/util/ArrayList;)V
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation
.end method
