.class public Lcom/navdy/client/app/framework/util/ZendeskJWT;
.super Ljava/lang/Object;
.source "ZendeskJWT.java"


# static fields
.field public static final EMAIL:Ljava/lang/String; = "zendesk-sso@navdy.com"

.field public static final NAME:Ljava/lang/String; = "Navdy"

.field public static final SSO_URL:Ljava/lang/String; = "https://navdy.zendesk.com/access/jwt?jwt="


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getZendeskUri()Landroid/net/Uri;
    .locals 9

    .prologue
    .line 30
    new-instance v6, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;

    invoke-direct {v6}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;-><init>()V

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    .line 31
    invoke-virtual {v6, v7}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->issueTime(Ljava/util/Date;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;

    move-result-object v6

    .line 32
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->jwtID(Ljava/lang/String;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;

    move-result-object v6

    const-string v7, "name"

    const-string v8, "Navdy"

    .line 33
    invoke-virtual {v6, v7, v8}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claim(Ljava/lang/String;Ljava/lang/Object;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;

    move-result-object v6

    const-string v7, "email"

    const-string v8, "zendesk-sso@navdy.com"

    .line 34
    invoke-virtual {v6, v7, v8}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->claim(Ljava/lang/String;Ljava/lang/Object;)Lcom/nimbusds/jwt/JWTClaimsSet$Builder;

    move-result-object v6

    .line 35
    invoke-virtual {v6}, Lcom/nimbusds/jwt/JWTClaimsSet$Builder;->build()Lcom/nimbusds/jwt/JWTClaimsSet;

    move-result-object v3

    .line 37
    .local v3, "jwtClaims":Lcom/nimbusds/jwt/JWTClaimsSet;
    new-instance v6, Lcom/nimbusds/jose/JWSHeader$Builder;

    sget-object v7, Lcom/nimbusds/jose/JWSAlgorithm;->HS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-direct {v6, v7}, Lcom/nimbusds/jose/JWSHeader$Builder;-><init>(Lcom/nimbusds/jose/JWSAlgorithm;)V

    const-string v7, "text/plain"

    .line 38
    invoke-virtual {v6, v7}, Lcom/nimbusds/jose/JWSHeader$Builder;->contentType(Ljava/lang/String;)Lcom/nimbusds/jose/JWSHeader$Builder;

    move-result-object v6

    .line 39
    invoke-virtual {v6}, Lcom/nimbusds/jose/JWSHeader$Builder;->build()Lcom/nimbusds/jose/JWSHeader;

    move-result-object v1

    .line 41
    .local v1, "header":Lcom/nimbusds/jose/JWSHeader;
    new-instance v2, Lcom/nimbusds/jose/JWSObject;

    new-instance v6, Lcom/nimbusds/jose/Payload;

    invoke-virtual {v3}, Lcom/nimbusds/jwt/JWTClaimsSet;->toJSONObject()Lnet/minidev/json/JSONObject;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/nimbusds/jose/Payload;-><init>(Lnet/minidev/json/JSONObject;)V

    invoke-direct {v2, v1, v6}, Lcom/nimbusds/jose/JWSObject;-><init>(Lcom/nimbusds/jose/JWSHeader;Lcom/nimbusds/jose/Payload;)V

    .line 45
    .local v2, "jwsObject":Lcom/nimbusds/jose/JWSObject;
    :try_start_0
    new-instance v5, Lcom/nimbusds/jose/crypto/MACSigner;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f080572

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/crypto/MACSigner;-><init>([B)V

    .line 46
    .local v5, "signer":Lcom/nimbusds/jose/JWSSigner;
    invoke-virtual {v2, v5}, Lcom/nimbusds/jose/JWSObject;->sign(Lcom/nimbusds/jose/JWSSigner;)V
    :try_end_0
    .catch Lcom/nimbusds/jose/JOSEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .end local v5    # "signer":Lcom/nimbusds/jose/JWSSigner;
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "https://navdy.zendesk.com/access/jwt?jwt="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/nimbusds/jose/JWSObject;->serialize()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 52
    .local v4, "redirectUrl":Ljava/lang/String;
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    return-object v6

    .line 47
    .end local v4    # "redirectUrl":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Lcom/nimbusds/jose/JOSEException;
    invoke-virtual {v0}, Lcom/nimbusds/jose/JOSEException;->printStackTrace()V

    goto :goto_0
.end method
