.class Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;
.super Ljava/lang/Object;
.source "SubmitTicketWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->processNextTicketFolder(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

.field final synthetic val$ticketFolder:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 99
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 100
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    .line 147
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Now processing: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 107
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$200()J

    move-result-wide v8

    sub-long/2addr v6, v8

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_1

    .line 109
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleting old folder: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-static {v6, v7}, Lcom/navdy/service/library/util/IOUtils;->deleteDirectory(Landroid/content/Context;Ljava/io/File;)V

    .line 112
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$300(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 143
    :catch_0
    move-exception v2

    .line 144
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception was found: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 145
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    goto/16 :goto_0

    .line 114
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "attachments.zip"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v0, "attachments":Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "attachment filepath: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "attachments.zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 118
    new-instance v1, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ticket_contents.txt"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v1, "content":Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "content filepath: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "ticket_contents.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 122
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->getJSONObjectFromFile(Ljava/io/File;)Lorg/json/JSONObject;

    move-result-object v4

    .line 124
    .local v4, "jsonObject":Lorg/json/JSONObject;
    if-eqz v4, :cond_4

    .line 125
    const-string v6, "ticket_action"

    const-string v7, ""

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 127
    .local v5, "ticketAction":Ljava/lang/String;
    const-string v6, "fetch_hud_log_upload_delete"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 128
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$400(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)Ljava/io/File;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$400(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 129
    const/4 v6, 0x1

    new-array v3, v6, [Ljava/io/File;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v7}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$400(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)Ljava/io/File;

    move-result-object v7

    aput-object v7, v3, v6

    .line 130
    .local v3, "hudLogArray":[Ljava/io/File;
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6, v4, v3, v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$500(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;[Ljava/io/File;Ljava/io/File;)V

    .line 131
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-static {v6, v4, v0, v7}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$600(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V

    goto/16 :goto_0

    .line 133
    .end local v3    # "hudLogArray":[Ljava/io/File;
    :cond_2
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$300(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V

    goto/16 :goto_0

    .line 136
    :cond_3
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iget-object v7, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-static {v6, v4, v0, v7}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$600(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Lorg/json/JSONObject;Ljava/io/File;Ljava/io/File;)V

    goto/16 :goto_0

    .line 139
    .end local v5    # "ticketAction":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No JSON Object found in the folder "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->val$ticketFolder:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 140
    iget-object v6, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$1;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$000(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
