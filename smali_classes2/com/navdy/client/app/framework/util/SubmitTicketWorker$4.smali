.class Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;
.super Ljava/lang/Object;
.source "SubmitTicketWorker.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/SupportTicketService$OnLogCollectedInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->fetchHudLogAndProcessTickets([Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

.field final synthetic val$folders:[Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;[Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->val$folders:[Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLogCollectionFailed()V
    .locals 2

    .prologue
    .line 406
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "fetching hud log failed."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 407
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->val$folders:[Ljava/io/File;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$900(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;[Ljava/io/File;)V

    .line 408
    return-void
.end method

.method public onLogCollectionSucceeded(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 392
    .local p1, "logAttachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-static {}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "fetching hud log succeeded"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 393
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 394
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 395
    .local v0, "log":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "display_log"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 396
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$402(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;Ljava/io/File;)Ljava/io/File;

    .line 401
    .end local v0    # "log":Ljava/io/File;
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->this$0:Lcom/navdy/client/app/framework/util/SubmitTicketWorker;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SubmitTicketWorker$4;->val$folders:[Ljava/io/File;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/SubmitTicketWorker;->access$900(Lcom/navdy/client/app/framework/util/SubmitTicketWorker;[Ljava/io/File;)V

    .line 402
    return-void
.end method
