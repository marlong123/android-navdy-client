.class Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;
.super Ljava/lang/Object;
.source "MusicUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

.field final synthetic val$bitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/MusicUtils$4$1;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 262
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->val$bitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->val$bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 263
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Received photo has null or empty byte array"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 265
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->this$0:Lcom/navdy/client/app/framework/util/MusicUtils$4;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->access$100(Lcom/navdy/client/app/framework/util/MusicUtils$4;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 266
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->this$0:Lcom/navdy/client/app/framework/util/MusicUtils$4;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->access$200(Lcom/navdy/client/app/framework/util/MusicUtils$4;)V

    .line 280
    :goto_0
    return-void

    .line 271
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->this$0:Lcom/navdy/client/app/framework/util/MusicUtils$4;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->val$bitmap:Landroid/graphics/Bitmap;

    const/16 v3, 0xc8

    const/16 v4, 0xc8

    sget-object v5, Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;->FIT:Lcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;

    .line 272
    invoke-static {v2, v3, v4, v5}, Lcom/navdy/service/library/util/ScalingUtilities;->createScaledBitmapAndRecycleOriginalIfScaled(Landroid/graphics/Bitmap;IILcom/navdy/service/library/util/ScalingUtilities$ScalingLogic;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 271
    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->access$302(Lcom/navdy/client/app/framework/util/MusicUtils$4;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 275
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->this$0:Lcom/navdy/client/app/framework/util/MusicUtils$4;

    iget-object v2, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->val$trackInfo:Lcom/navdy/service/library/events/audio/MusicTrackInfo;

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->access$100(Lcom/navdy/client/app/framework/util/MusicUtils$4;Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V

    .line 276
    iget-object v1, p0, Lcom/navdy/client/app/framework/util/MusicUtils$4$1$1;->this$1:Lcom/navdy/client/app/framework/util/MusicUtils$4$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/util/MusicUtils$4$1;->this$0:Lcom/navdy/client/app/framework/util/MusicUtils$4;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/MusicUtils$4;->access$200(Lcom/navdy/client/app/framework/util/MusicUtils$4;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Error updating the artwork"

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
