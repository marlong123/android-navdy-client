.class public final Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;
.super Ldagger/internal/ProvidesBinding;
.source "ProdModule$$ModuleAdapter.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvideNetworkStatusManagerProvidesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ProvidesBinding",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;",
        ">;",
        "Ljavax/inject/Provider",
        "<",
        "Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/navdy/client/app/framework/ProdModule;


# direct methods
.method public constructor <init>(Lcom/navdy/client/app/framework/ProdModule;)V
    .locals 4
    .param p1, "module"    # Lcom/navdy/client/app/framework/ProdModule;

    .prologue
    const/4 v3, 0x1

    .line 127
    const-string v0, "com.navdy.client.app.framework.servicehandler.NetworkStatusManager"

    const-string v1, "com.navdy.client.app.framework.ProdModule"

    const-string v2, "provideNetworkStatusManager"

    invoke-direct {p0, v0, v3, v1, v2}, Ldagger/internal/ProvidesBinding;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 128
    iput-object p1, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    .line 129
    invoke-virtual {p0, v3}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;->setLibrary(Z)V

    .line 130
    return-void
.end method


# virtual methods
.method public get()Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;->module:Lcom/navdy/client/app/framework/ProdModule;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/ProdModule;->provideNetworkStatusManager()Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;->get()Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    move-result-object v0

    return-object v0
.end method
