.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DisplayInitiatedRequestEvent"
.end annotation


# instance fields
.field public final destination:Lcom/navdy/client/app/framework/models/Destination;

.field public final requestId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;
    .param p2, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->requestId:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 51
    return-void
.end method
