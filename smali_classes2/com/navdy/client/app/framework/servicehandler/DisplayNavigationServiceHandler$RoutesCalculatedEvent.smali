.class public Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;
.super Ljava/lang/Object;
.source "DisplayNavigationServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RoutesCalculatedEvent"
.end annotation


# instance fields
.field public final requestId:Ljava/lang/String;

.field public final routes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .param p2, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "routes":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;->routes:Ljava/util/List;

    .line 92
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;->requestId:Ljava/lang/String;

    .line 93
    return-void
.end method
