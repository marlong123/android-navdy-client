.class Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;
.super Ljava/lang/Object;
.source "ContactServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->onContactRequest(Lcom/navdy/service/library/events/contacts/ContactRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

.field final synthetic val$request:Lcom/navdy/service/library/events/contacts/ContactRequest;

.field final synthetic val$responseBuilder:Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Lcom/navdy/service/library/events/contacts/ContactRequest;Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$request:Lcom/navdy/service/library/events/contacts/ContactRequest;

    iput-object p3, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 151
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->getInstance()Lcom/navdy/client/app/framework/util/ContactsManager;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$request:Lcom/navdy/service/library/events/contacts/ContactRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/contacts/ContactRequest;->identifier:Ljava/lang/String;

    sget-object v7, Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;->PHONE:Lcom/navdy/client/app/framework/util/ContactsManager$Attributes;

    .line 153
    invoke-static {v7}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v7

    .line 152
    invoke-virtual {v5, v6, v7}, Lcom/navdy/client/app/framework/util/ContactsManager;->getContactsWithoutLoadingPhotos(Ljava/lang/String;Ljava/util/EnumSet;)Ljava/util/List;

    move-result-object v2

    .line 156
    .local v2, "contactList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    new-instance v3, Ljava/util/ArrayList;

    const/4 v5, 0x4

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 158
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 160
    const/4 v5, 0x0

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/ContactModel;

    .line 162
    .local v0, "c":Lcom/navdy/client/app/framework/models/ContactModel;
    iget-object v5, v0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    if-eqz v5, :cond_1

    .line 163
    iget-object v5, v0, Lcom/navdy/client/app/framework/models/ContactModel;->phoneNumbers:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/navdy/client/app/framework/models/PhoneNumberModel;

    .line 164
    .local v4, "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    iget-object v6, v4, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 165
    new-instance v1, Lcom/navdy/service/library/events/contacts/Contact;

    iget-object v6, v0, Lcom/navdy/client/app/framework/models/ContactModel;->name:Ljava/lang/String;

    iget-object v7, v4, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->number:Ljava/lang/String;

    iget-object v8, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->this$0:Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    iget v9, v4, Lcom/navdy/client/app/framework/models/PhoneNumberModel;->type:I

    .line 167
    invoke-static {v8, v9}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->access$100(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;I)Lcom/navdy/service/library/events/contacts/PhoneNumberType;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/navdy/service/library/events/contacts/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/contacts/PhoneNumberType;Ljava/lang/String;)V

    .line 169
    .local v1, "contact":Lcom/navdy/service/library/events/contacts/Contact;
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    .end local v0    # "c":Lcom/navdy/client/app/framework/models/ContactModel;
    .end local v1    # "contact":Lcom/navdy/service/library/events/contacts/Contact;
    .end local v4    # "phoneNumber":Lcom/navdy/client/app/framework/models/PhoneNumberModel;
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$request:Lcom/navdy/service/library/events/contacts/ContactRequest;

    iget-object v6, v6, Lcom/navdy/service/library/events/contacts/ContactRequest;->identifier:Ljava/lang/String;

    .line 176
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    move-result-object v5

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    .line 177
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    move-result-object v5

    .line 178
    invoke-virtual {v5, v3}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->contacts(Ljava/util/List;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    .line 179
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler$2;->val$responseBuilder:Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 180
    return-void
.end method
