.class Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;
.super Ljava/lang/Object;
.source "OTAUpdateManagerImpl.java"

# interfaces
.implements Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->uploadToHUD(Ljava/io/File;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

.field final synthetic val$total:J


# direct methods
.method constructor <init>(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iput-wide p2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
    .locals 8
    .param p1, "errorCode"    # Lcom/navdy/service/library/events/file/FileTransferError;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 393
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Transfer Error :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 395
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFileTransferError: Upload is already aborted , so ignoring the error"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 403
    :goto_0
    return-void

    .line 399
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 400
    :catch_0
    move-exception v7

    .line 401
    .local v7, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Bad listener :"

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 8
    .param p1, "response"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 354
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFileTransferResponse: Upload is already aborted , so ignoring the response"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    iget-object v6, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 362
    :catch_0
    move-exception v7

    .line 363
    .local v7, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Bad listener "

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
    .locals 8
    .param p1, "status"    # Lcom/navdy/service/library/events/file/FileTransferStatus;

    .prologue
    .line 370
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    iget-object v0, v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->mUploadAborted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onFileTransferStatus: Upload is already aborted , so ignoring the status"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 389
    :goto_0
    return-void

    .line 376
    :cond_0
    :try_start_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->transferComplete:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    iget-wide v2, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    iget-wide v4, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 386
    :catch_0
    move-exception v7

    .line 387
    .local v7, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Bad listener "

    invoke-virtual {v0, v1, v7}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 380
    .end local v7    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->totalBytesTransferred:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V

    goto :goto_0

    .line 383
    :cond_2
    iget-object v0, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->this$0:Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-static {v0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;->access$000(Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;)Lcom/navdy/client/ota/OTAUpdateListener;

    move-result-object v0

    sget-object v1, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl$2;->val$total:J

    iget-object v6, p1, Lcom/navdy/service/library/events/file/FileTransferStatus;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    invoke-interface/range {v0 .. v6}, Lcom/navdy/client/ota/OTAUpdateListener;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
