.class public Lcom/navdy/client/debug/gesture/GestureControlFragment;
.super Landroid/app/Fragment;
.source "GestureControlFragment.java"


# static fields
.field private static final DIAL_BUTTONS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private keyListener:Landroid/view/View$OnKeyListener;

.field private lastGesture:Lcom/navdy/service/library/events/input/Gesture;

.field protected mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

.field private seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field slider:Landroid/widget/SeekBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100240
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 43
    new-instance v0, Lcom/navdy/client/debug/gesture/GestureControlFragment$1;

    invoke-direct {v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment$1;-><init>()V

    sput-object v0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->DIAL_BUTTONS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 86
    new-instance v0, Lcom/navdy/client/debug/gesture/GestureControlFragment$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/gesture/GestureControlFragment$2;-><init>(Lcom/navdy/client/debug/gesture/GestureControlFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->keyListener:Landroid/view/View$OnKeyListener;

    .line 138
    sget-object v0, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    iput-object v0, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->lastGesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 140
    new-instance v0, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/gesture/GestureControlFragment$3;-><init>(Lcom/navdy/client/debug/gesture/GestureControlFragment;)V

    iput-object v0, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 56
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/squareup/wire/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/gesture/GestureControlFragment;
    .param p1, "x1"    # Lcom/squareup/wire/Message;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/gesture/GestureControlFragment;)Lcom/navdy/service/library/events/input/Gesture;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/gesture/GestureControlFragment;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->lastGesture:Lcom/navdy/service/library/events/input/Gesture;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/debug/gesture/GestureControlFragment;Lcom/navdy/service/library/events/input/Gesture;)Lcom/navdy/service/library/events/input/Gesture;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/gesture/GestureControlFragment;
    .param p1, "x1"    # Lcom/navdy/service/library/events/input/Gesture;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->lastGesture:Lcom/navdy/service/library/events/input/Gesture;

    return-object p1
.end method

.method private sendEvent(Lcom/squareup/wire/Message;)V
    .locals 2
    .param p1, "event"    # Lcom/squareup/wire/Message;

    .prologue
    .line 208
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 210
    .local v0, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {v0, p1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 214
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 62
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    const v1, 0x7f03008b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 74
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 76
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->slider:Landroid/widget/SeekBar;

    const/16 v2, 0x258

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 77
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->slider:Landroid/widget/SeekBar;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 79
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->slider:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 81
    iget-object v1, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->keyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 83
    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 68
    return-void
.end method

.method public onDialSimulationClicked(Landroid/view/View;)V
    .locals 3
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100248,
            0x7f100247,
            0x7f100246,
            0x7f100249
        }
    .end annotation

    .prologue
    .line 204
    new-instance v1, Lcom/navdy/service/library/events/input/DialSimulationEvent;

    sget-object v0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->DIAL_BUTTONS:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-direct {v1, v0}, Lcom/navdy/service/library/events/input/DialSimulationEvent;-><init>(Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 205
    return-void
.end method

.method public onFistClicked(Landroid/view/View;)V
    .locals 8
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100243,
            0x7f100242,
            0x7f100244
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 194
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown button id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 180
    :pswitch_0
    const/4 v5, 0x3

    new-array v3, v5, [Lcom/navdy/service/library/events/input/Gesture;

    sget-object v5, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v5, v3, v4

    const/4 v5, 0x1

    sget-object v6, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v6, v3, v5

    const/4 v5, 0x2

    sget-object v6, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_PINCH_TO_FINGER:Lcom/navdy/service/library/events/input/Gesture;

    aput-object v6, v3, v5

    .line 181
    .local v3, "gestures":[Lcom/navdy/service/library/events/input/Gesture;
    array-length v5, v3

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v3, v4

    .line 182
    .local v1, "g":Lcom/navdy/service/library/events/input/Gesture;
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    invoke-direct {v0, v1, v7, v7}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 183
    .local v0, "event":Lcom/navdy/service/library/events/input/GestureEvent;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 181
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 185
    .end local v0    # "event":Lcom/navdy/service/library/events/input/GestureEvent;
    .end local v1    # "g":Lcom/navdy/service/library/events/input/Gesture;
    :cond_0
    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_TO_PINCH:Lcom/navdy/service/library/events/input/Gesture;

    .line 196
    .end local v3    # "gestures":[Lcom/navdy/service/library/events/input/Gesture;
    .local v2, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :goto_1
    iput-object v2, p0, Lcom/navdy/client/debug/gesture/GestureControlFragment;->lastGesture:Lcom/navdy/service/library/events/input/Gesture;

    .line 198
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    invoke-direct {v0, v2, v7, v7}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 199
    .restart local v0    # "event":Lcom/navdy/service/library/events/input/GestureEvent;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 200
    return-void

    .line 188
    .end local v0    # "event":Lcom/navdy/service/library/events/input/GestureEvent;
    .end local v2    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :pswitch_1
    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_DOWN:Lcom/navdy/service/library/events/input/Gesture;

    .line 189
    .restart local v2    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    goto :goto_1

    .line 191
    .end local v2    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :pswitch_2
    sget-object v2, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_FINGER_UP:Lcom/navdy/service/library/events/input/Gesture;

    .line 192
    .restart local v2    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    goto :goto_1

    .line 177
    :pswitch_data_0
    .packed-switch 0x7f100242
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSwipeClicked(Landroid/view/View;)V
    .locals 5
    .param p1, "button"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10023f,
            0x7f100241
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 167
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f10023f

    if-ne v2, v3, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_LEFT:Lcom/navdy/service/library/events/input/Gesture;

    .line 169
    .local v1, "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :goto_0
    new-instance v0, Lcom/navdy/service/library/events/input/GestureEvent;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/GestureEvent;-><init>(Lcom/navdy/service/library/events/input/Gesture;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 170
    .local v0, "event":Lcom/navdy/service/library/events/input/GestureEvent;
    invoke-direct {p0, v0}, Lcom/navdy/client/debug/gesture/GestureControlFragment;->sendEvent(Lcom/squareup/wire/Message;)V

    .line 171
    return-void

    .line 167
    .end local v0    # "event":Lcom/navdy/service/library/events/input/GestureEvent;
    .end local v1    # "gesture":Lcom/navdy/service/library/events/input/Gesture;
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/input/Gesture;->GESTURE_SWIPE_RIGHT:Lcom/navdy/service/library/events/input/Gesture;

    goto :goto_0
.end method
