.class final enum Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;
.super Ljava/lang/Enum;
.source "CustomNotificationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/CustomNotificationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum APPLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum BRIGHTNESS:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_ALL_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_ALL_TOAST_AND_CURRENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_CURRENT_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum CLEAR_TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_BATTERY_VERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_FORGOTTEN_MULTIPLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum DIAL_FORGOTTEN_SINGLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum END_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum FIND_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum FIND_ROUTE_TO_CLOSEST_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum FUEL_ADDED_TEST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum GENERIC_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum GENERIC_2:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum GOOGLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum INCOMING_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum LOW_FUEL_LEVEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum LOW_FUEL_LEVEL_CLEAR:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum LOW_FUEL_LEVEL_NOCHECK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum MUSIC:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_APP_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum PHONE_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TEXT_NOTIFICAION_WITH_NO_REPLY_510:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TEXT_NOTIFICAION_WITH_REPLY_408:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TEXT_NOTIFICAION_WITH_REPLY_999:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

.field public static final enum VOICE_ASSIST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;


# instance fields
.field private final mText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "INCOMING_PHONE_CALL_323"

    const-string v2, "Incoming phone 323-222-1111"

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->INCOMING_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 57
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "END_PHONE_CALL_323"

    const-string v2, "End phone 323-222-1111"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->END_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 58
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_BATTERY_LOW"

    const-string v2, "Phone battery Low"

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 59
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_BATTERY_EXTREMELY_LOW"

    const-string v2, "Phone battery Extremely Low"

    invoke-direct {v0, v1, v7, v2}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 60
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_BATTERY_OK"

    const-string v2, "Phone battery ok"

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 61
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TEXT_NOTIFICAION_WITH_REPLY_408"

    const/4 v2, 0x5

    const-string v3, "Text message 408-111-1111"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_REPLY_408:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 62
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TEXT_NOTIFICAION_WITH_REPLY_999"

    const/4 v2, 0x6

    const-string v3, "Text message 999-999-0999"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_REPLY_999:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 63
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TEXT_NOTIFICAION_WITH_NO_REPLY_510"

    const/4 v2, 0x7

    const-string v3, "Text message 510-454-4444"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_NO_REPLY_510:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 64
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_CONNECTED"

    const/16 v2, 0x8

    const-string v3, "Dial Connected"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 65
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_DISCONNECTED"

    const/16 v2, 0x9

    const-string v3, "Dial Disconnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 66
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_FORGOTTEN_SINGLE"

    const/16 v2, 0xa

    const-string v3, "Dial Forgotten Single"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_FORGOTTEN_SINGLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 67
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_FORGOTTEN_MULTIPLE"

    const/16 v2, 0xb

    const-string v3, "Dial Forgotten Multiple"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_FORGOTTEN_MULTIPLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 68
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_BATTERY_LOW"

    const/16 v2, 0xc

    const-string v3, "Dial battery Low"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 69
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_BATTERY_EXTREMELY_LOW"

    const/16 v2, 0xd

    const-string v3, "Dial battery Extremely Low"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 70
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_BATTERY_VERY_LOW"

    const/16 v2, 0xe

    const-string v3, "Dial battery Very Low"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_VERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 71
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "DIAL_BATTERY_OK"

    const/16 v2, 0xf

    const-string v3, "Dial battery OK"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 72
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "MUSIC"

    const/16 v2, 0x10

    const-string v3, "Music notification"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->MUSIC:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 73
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "VOICE_ASSIST"

    const/16 v2, 0x11

    const-string v3, "Voice assistance"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->VOICE_ASSIST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 74
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "BRIGHTNESS"

    const/16 v2, 0x12

    const-string v3, "Brightness"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->BRIGHTNESS:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 75
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TRAFFIC_REROUTE"

    const/16 v2, 0x13

    const-string v3, "Traffic reroute"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 76
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_TRAFFIC_REROUTE"

    const/16 v2, 0x14

    const-string v3, "Clear Traffic reroute"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 77
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TRAFFIC_JAM"

    const/16 v2, 0x15

    const-string v3, "Traffic Jam"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 78
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_TRAFFIC_JAM"

    const/16 v2, 0x16

    const-string v3, "Clear Traffic Jam"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 79
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TRAFFIC_INCIDENT"

    const/16 v2, 0x17

    const-string v3, "Traffic Incident"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 80
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_TRAFFIC_INCIDENT"

    const/16 v2, 0x18

    const-string v3, "Clear Traffic Incident"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 81
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TRAFFIC_DELAY"

    const/16 v2, 0x19

    const-string v3, "Traffic Delay"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 82
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_TRAFFIC_DELAY"

    const/16 v2, 0x1a

    const-string v3, "Clear Traffic Delay"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 83
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_DISCONNECTED"

    const/16 v2, 0x1b

    const-string v3, "Phone disconnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 84
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_CONNECTED"

    const/16 v2, 0x1c

    const-string v3, "Phone connected"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 85
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "PHONE_APP_DISCONNECTED"

    const/16 v2, 0x1d

    const-string v3, "App disconnected"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_APP_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 86
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_CURRENT_TOAST"

    const/16 v2, 0x1e

    const-string v3, "Clear current toast"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_CURRENT_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 87
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_ALL_TOAST"

    const/16 v2, 0x1f

    const-string v3, "Clear all toast"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_ALL_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 88
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_ALL_TOAST_AND_CURRENT"

    const/16 v2, 0x20

    const-string v3, "Clear all toast + current"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_ALL_TOAST_AND_CURRENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 89
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "GOOGLE_CALENDAR_1"

    const/16 v2, 0x21

    const-string v3, "Google Calendar"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GOOGLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 90
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "APPLE_CALENDAR_1"

    const/16 v2, 0x22

    const-string v3, "Apple Calendar"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->APPLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 91
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "LOW_FUEL_LEVEL"

    const/16 v2, 0x23

    const-string v3, "Low fuel Level Glance"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 92
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "LOW_FUEL_LEVEL_NOCHECK"

    const/16 v2, 0x24

    const-string v3, "Low fuel Level Glance - no preference check"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL_NOCHECK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 93
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "LOW_FUEL_LEVEL_CLEAR"

    const/16 v2, 0x25

    const-string v3, "Clear Low Fuel Level Glance"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL_CLEAR:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 94
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "TEST_OBD_LOW_FUEL"

    const/16 v2, 0x26

    const-string v3, "Test OBD Low Fuel Level"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 95
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "FIND_GAS_STATION"

    const/16 v2, 0x27

    const-string v3, "Find Gas Stations in vicinity"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FIND_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 96
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "FIND_ROUTE_TO_CLOSEST_GAS_STATION"

    const/16 v2, 0x28

    const-string v3, "Find Route to nearest Gas Station"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FIND_ROUTE_TO_CLOSEST_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 97
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "CLEAR_TEST_OBD_LOW_FUEL"

    const/16 v2, 0x29

    const-string v3, "Clear Test OBD Low Fuel Level"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 98
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "FUEL_ADDED_TEST"

    const/16 v2, 0x2a

    const-string v3, "Test for fuel added after going to a gas station"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FUEL_ADDED_TEST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 99
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "GENERIC_1"

    const/16 v2, 0x2b

    const-string v3, "Generic 1"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GENERIC_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 100
    new-instance v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    const-string v1, "GENERIC_2"

    const/16 v2, 0x2c

    const-string v3, "Generic 2"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GENERIC_2:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    .line 55
    const/16 v0, 0x2d

    new-array v0, v0, [Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->INCOMING_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->END_PHONE_CALL_323:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_REPLY_408:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_REPLY_999:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEXT_NOTIFICAION_WITH_NO_REPLY_510:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_FORGOTTEN_SINGLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_FORGOTTEN_MULTIPLE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_EXTREMELY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_VERY_LOW:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->DIAL_BATTERY_OK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->MUSIC:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->VOICE_ASSIST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->BRIGHTNESS:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_REROUTE:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_JAM:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_INCIDENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TRAFFIC_DELAY:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_CONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->PHONE_APP_DISCONNECTED:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_CURRENT_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_ALL_TOAST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_ALL_TOAST_AND_CURRENT:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GOOGLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->APPLE_CALENDAR_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL_NOCHECK:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->LOW_FUEL_LEVEL_CLEAR:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FIND_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FIND_ROUTE_TO_CLOSEST_GAS_STATION:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->CLEAR_TEST_OBD_LOW_FUEL:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->FUEL_ADDED_TEST:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GENERIC_1:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->GENERIC_2:Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "item"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 105
    iput-object p3, p0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->mText:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 55
    const-class v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/navdy/client/debug/CustomNotificationFragment$ListItem;->mText:Ljava/lang/String;

    return-object v0
.end method
