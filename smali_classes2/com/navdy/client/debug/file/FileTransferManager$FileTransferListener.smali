.class public interface abstract Lcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;
.super Ljava/lang/Object;
.source "FileTransferManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/file/FileTransferManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileTransferListener"
.end annotation


# virtual methods
.method public abstract onError(Lcom/navdy/service/library/events/file/FileTransferError;Ljava/lang/String;)V
.end method

.method public abstract onFileTransferResponse(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
.end method

.method public abstract onFileTransferStatus(Lcom/navdy/service/library/events/file/FileTransferStatus;)V
.end method
