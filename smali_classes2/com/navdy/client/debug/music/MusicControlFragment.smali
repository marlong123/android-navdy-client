.class public Lcom/navdy/client/debug/music/MusicControlFragment;
.super Landroid/app/Fragment;
.source "MusicControlFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/music/MusicControlFragment;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/music/MusicControlFragment;
    .param p1, "x1"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/music/MusicControlFragment;->performAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    return-void
.end method

.method private performAction(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V
    .locals 4
    .param p1, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 48
    new-instance v1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;-><init>()V

    .line 49
    invoke-virtual {v1, p1}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v1

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 50
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v0

    .line 53
    .local v0, "event":Lcom/navdy/service/library/events/audio/MusicEvent;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/debug/music/MusicControlFragment$2;

    invoke-direct {v2, p0, v0}, Lcom/navdy/client/debug/music/MusicControlFragment$2;-><init>(Lcom/navdy/client/debug/music/MusicControlFragment;Lcom/navdy/service/library/events/audio/MusicEvent;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 59
    return-void
.end method

.method private setUpButton(Landroid/widget/Button;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V
    .locals 1
    .param p1, "pauseButton"    # Landroid/widget/Button;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 37
    if-eqz p1, :cond_0

    .line 38
    new-instance v0, Lcom/navdy/client/debug/music/MusicControlFragment$1;

    invoke-direct {v0, p0, p2}, Lcom/navdy/client/debug/music/MusicControlFragment$1;-><init>(Lcom/navdy/client/debug/music/MusicControlFragment;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 26
    const v1, 0x7f030090

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 28
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f100264

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/music/MusicControlFragment;->setUpButton(Landroid/widget/Button;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    .line 29
    const v1, 0x7f100265

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/music/MusicControlFragment;->setUpButton(Landroid/widget/Button;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    .line 30
    const v1, 0x7f100266

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PAUSE:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/music/MusicControlFragment;->setUpButton(Landroid/widget/Button;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    .line 31
    const v1, 0x7f100183

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_NEXT:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-direct {p0, v1, v2}, Lcom/navdy/client/debug/music/MusicControlFragment;->setUpButton(Landroid/widget/Button;Lcom/navdy/service/library/events/audio/MusicEvent$Action;)V

    .line 33
    return-object v0
.end method
