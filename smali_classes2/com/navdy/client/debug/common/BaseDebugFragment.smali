.class public Lcom/navdy/client/debug/common/BaseDebugFragment;
.super Landroid/app/Fragment;
.source "BaseDebugFragment.java"


# instance fields
.field protected baseActivity:Lcom/navdy/client/debug/common/BaseDebugActivity;

.field protected logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 12
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/debug/common/BaseDebugFragment;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method


# virtual methods
.method public isAlive()Z
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/navdy/client/debug/common/BaseDebugFragment;->baseActivity:Lcom/navdy/client/debug/common/BaseDebugActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/debug/common/BaseDebugFragment;->baseActivity:Lcom/navdy/client/debug/common/BaseDebugActivity;

    invoke-virtual {v0}, Lcom/navdy/client/debug/common/BaseDebugActivity;->isActivityDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 18
    move-object v0, p1

    check-cast v0, Lcom/navdy/client/debug/common/BaseDebugActivity;

    iput-object v0, p0, Lcom/navdy/client/debug/common/BaseDebugFragment;->baseActivity:Lcom/navdy/client/debug/common/BaseDebugActivity;

    .line 19
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 20
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/common/BaseDebugFragment;->baseActivity:Lcom/navdy/client/debug/common/BaseDebugActivity;

    .line 25
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 26
    return-void
.end method
