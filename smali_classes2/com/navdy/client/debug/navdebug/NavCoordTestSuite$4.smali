.class Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->doApiCalls(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 469
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    invoke-virtual {p2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onFailure(Ljava/lang/String;)V

    .line 482
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 5
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 472
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$4;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v1

    .line 473
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getNavigationCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    .line 474
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getHereAddress()Lcom/here/android/mpa/search/Address;

    move-result-object v3

    iget-object v4, p1, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 472
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onSuccess(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    .line 476
    return-void
.end method
