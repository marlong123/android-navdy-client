.class Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->doApiCalls(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 16
    .param p1, "displayCoords"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "navigationCoords"    # Lcom/here/android/mpa/common/GeoCoordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "address"    # Lcom/here/android/mpa/search/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "precision"    # Lcom/navdy/client/app/framework/models/Destination$Precision;

    .prologue
    .line 424
    new-instance v2, Lcom/navdy/service/library/events/location/Coordinate;

    .line 425
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    .line 426
    invoke-virtual/range {p1 .. p1}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    const/4 v5, 0x0

    .line 427
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    const-string v10, "HERE"

    invoke-direct/range {v2 .. v10}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 428
    .local v2, "dc":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz p2, :cond_0

    .line 430
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLatitude()D

    move-result-wide v12

    .line 431
    .local v12, "nla":D
    :goto_0
    if-eqz p2, :cond_1

    .line 433
    invoke-virtual/range {p2 .. p2}, Lcom/here/android/mpa/common/GeoCoordinate;->getLongitude()D

    move-result-wide v14

    .line 434
    .local v14, "nlo":D
    :goto_1
    new-instance v3, Lcom/navdy/service/library/events/location/Coordinate;

    .line 435
    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    const-string v11, "HERE"

    invoke-direct/range {v3 .. v11}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 436
    .local v3, "nc":Lcom/navdy/service/library/events/location/Coordinate;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-interface {v4, v2, v3, v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onSuccess(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    .line 437
    return-void

    .line 430
    .end local v3    # "nc":Lcom/navdy/service/library/events/location/Coordinate;
    .end local v12    # "nla":D
    .end local v14    # "nlo":D
    :cond_0
    const-wide/16 v12, 0x0

    goto :goto_0

    .line 433
    .restart local v12    # "nla":D
    :cond_1
    const-wide/16 v14, 0x0

    goto :goto_1
.end method

.method public onError(Lcom/here/android/mpa/search/ErrorCode;)V
    .locals 2
    .param p1, "error"    # Lcom/here/android/mpa/search/ErrorCode;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 441
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$3;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    invoke-virtual {p1}, Lcom/here/android/mpa/search/ErrorCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onFailure(Ljava/lang/String;)V

    .line 442
    return-void
.end method
