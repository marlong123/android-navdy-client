.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;
.super Ljava/lang/Object;
.source "NavigationDemoActivity.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/VoiceCatalog$OnDownloadDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->downloadTargetVoiceSkin()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 384
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadDone(Lcom/here/android/mpa/guidance/VoiceCatalog$Error;)V
    .locals 6
    .param p1, "error"    # Lcom/here/android/mpa/guidance/VoiceCatalog$Error;

    .prologue
    .line 388
    sget-object v2, Lcom/here/android/mpa/guidance/VoiceCatalog$Error;->NONE:Lcom/here/android/mpa/guidance/VoiceCatalog$Error;

    if-ne p1, v2, :cond_2

    .line 390
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$400(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/VoiceCatalog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getCatalogList()Ljava/util/List;

    move-result-object v1

    .line 393
    .local v1, "voicePackages":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoicePackage;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/guidance/VoicePackage;

    .line 395
    .local v0, "voicePackage":Lcom/here/android/mpa/guidance/VoicePackage;
    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/VoicePackage;->getMarcCode()Ljava/lang/String;

    move-result-object v3

    const-string v4, "eng"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 396
    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/VoicePackage;->isTts()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 399
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$400(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/VoiceCatalog;

    move-result-object v2

    .line 400
    invoke-virtual {v0}, Lcom/here/android/mpa/guidance/VoicePackage;->getId()J

    move-result-wide v4

    new-instance v3, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;-><init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;Lcom/here/android/mpa/guidance/VoicePackage;)V

    .line 399
    invoke-virtual {v2, v4, v5, v3}, Lcom/here/android/mpa/guidance/VoiceCatalog;->downloadVoice(JLcom/here/android/mpa/guidance/VoiceCatalog$OnDownloadDoneListener;)Z

    .line 420
    .end local v0    # "voicePackage":Lcom/here/android/mpa/guidance/VoicePackage;
    .end local v1    # "voicePackages":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/guidance/VoicePackage;>;"
    :cond_1
    :goto_0
    return-void

    .line 418
    :cond_2
    sget-object v2, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Download voice catalog failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/here/android/mpa/guidance/VoiceCatalog$Error;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
