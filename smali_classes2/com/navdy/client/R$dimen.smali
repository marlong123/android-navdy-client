.class public final Lcom/navdy/client/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final Body1_font_size:I = 0x7f0b000e

.field public static final Body2_font_size:I = 0x7f0b000f

.field public static final Body3_font_size:I = 0x7f0b0010

.field public static final Body_extra_line_spacing:I = 0x7f0b0011

.field public static final Button_font_size:I = 0x7f0b0012

.field public static final Button_image_padding:I = 0x7f0b0013

.field public static final Button_sides_padding:I = 0x7f0b0014

.field public static final Button_sides_padding_small:I = 0x7f0b0015

.field public static final Button_top_n_bottom_padding:I = 0x7f0b0016

.field public static final Button_top_n_bottom_padding_small:I = 0x7f0b0017

.field public static final H1_5_font_size:I = 0x7f0b0018

.field public static final H1_font_size:I = 0x7f0b0019

.field public static final H2_font_size:I = 0x7f0b001a

.field public static final H3_font_size:I = 0x7f0b003e

.field public static final H4_font_size:I = 0x7f0b001b

.field public static final abc_action_bar_content_inset_material:I = 0x7f0b002b

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f0b002c

.field public static final abc_action_bar_default_height_material:I = 0x7f0b0002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f0b002d

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f0b002e

.field public static final abc_action_bar_elevation_material:I = 0x7f0b003f

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0b0040

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0b0041

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0b0042

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0b0003

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0b0043

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0b0044

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0b0045

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0b0046

.field public static final abc_action_button_min_height_material:I = 0x7f0b0047

.field public static final abc_action_button_min_width_material:I = 0x7f0b0048

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0b0049

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0b0001

.field public static final abc_button_inset_horizontal_material:I = 0x7f0b004a

.field public static final abc_button_inset_vertical_material:I = 0x7f0b004b

.field public static final abc_button_padding_horizontal_material:I = 0x7f0b004c

.field public static final abc_button_padding_vertical_material:I = 0x7f0b004d

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f0b004e

.field public static final abc_config_prefDialogWidth:I = 0x7f0b0007

.field public static final abc_control_corner_material:I = 0x7f0b004f

.field public static final abc_control_inset_material:I = 0x7f0b0050

.field public static final abc_control_padding_material:I = 0x7f0b0051

.field public static final abc_dialog_fixed_height_major:I = 0x7f0b0008

.field public static final abc_dialog_fixed_height_minor:I = 0x7f0b0009

.field public static final abc_dialog_fixed_width_major:I = 0x7f0b000a

.field public static final abc_dialog_fixed_width_minor:I = 0x7f0b000b

.field public static final abc_dialog_list_padding_bottom_no_buttons:I = 0x7f0b0052

.field public static final abc_dialog_list_padding_top_no_title:I = 0x7f0b0053

.field public static final abc_dialog_min_width_major:I = 0x7f0b000c

.field public static final abc_dialog_min_width_minor:I = 0x7f0b000d

.field public static final abc_dialog_padding_material:I = 0x7f0b0054

.field public static final abc_dialog_padding_top_material:I = 0x7f0b0055

.field public static final abc_dialog_title_divider_material:I = 0x7f0b0056

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0b0057

.field public static final abc_disabled_alpha_material_light:I = 0x7f0b0058

.field public static final abc_dropdownitem_icon_width:I = 0x7f0b0059

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0b005a

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0b005b

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0b005c

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0b005d

.field public static final abc_edit_text_inset_top_material:I = 0x7f0b005e

.field public static final abc_floating_window_z:I = 0x7f0b005f

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0b0060

.field public static final abc_panel_menu_list_width:I = 0x7f0b0061

.field public static final abc_progress_bar_height_material:I = 0x7f0b0062

.field public static final abc_search_view_preferred_height:I = 0x7f0b0063

.field public static final abc_search_view_preferred_width:I = 0x7f0b0064

.field public static final abc_seekbar_track_background_height_material:I = 0x7f0b0065

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f0b0066

.field public static final abc_select_dialog_padding_start_material:I = 0x7f0b0067

.field public static final abc_switch_padding:I = 0x7f0b0038

.field public static final abc_text_size_body_1_material:I = 0x7f0b0068

.field public static final abc_text_size_body_2_material:I = 0x7f0b0069

.field public static final abc_text_size_button_material:I = 0x7f0b006a

.field public static final abc_text_size_caption_material:I = 0x7f0b006b

.field public static final abc_text_size_display_1_material:I = 0x7f0b006c

.field public static final abc_text_size_display_2_material:I = 0x7f0b006d

.field public static final abc_text_size_display_3_material:I = 0x7f0b006e

.field public static final abc_text_size_display_4_material:I = 0x7f0b006f

.field public static final abc_text_size_headline_material:I = 0x7f0b0070

.field public static final abc_text_size_large_material:I = 0x7f0b0071

.field public static final abc_text_size_medium_material:I = 0x7f0b0072

.field public static final abc_text_size_menu_header_material:I = 0x7f0b0073

.field public static final abc_text_size_menu_material:I = 0x7f0b0074

.field public static final abc_text_size_small_material:I = 0x7f0b0075

.field public static final abc_text_size_subhead_material:I = 0x7f0b0076

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0b0004

.field public static final abc_text_size_title_material:I = 0x7f0b0077

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0b0005

.field public static final action_bar_size:I = 0x7f0b0078

.field public static final activity_horizontal_margin:I = 0x7f0b003d

.field public static final activity_vertical_margin:I = 0x7f0b0079

.field public static final article_attachment_list_divider_height:I = 0x7f0b007a

.field public static final article_attachment_row_filename_margin_left:I = 0x7f0b007b

.field public static final article_attachment_row_filename_margin_right:I = 0x7f0b007c

.field public static final article_attachment_row_filesize_margin_right:I = 0x7f0b007d

.field public static final article_attachment_row_minimum_height:I = 0x7f0b007e

.field public static final attachment_container_host_horizontal_height:I = 0x7f0b007f

.field public static final attachment_container_host_horizontal_margin:I = 0x7f0b0080

.field public static final attachment_container_host_vertical_margin:I = 0x7f0b0081

.field public static final attachment_container_image_margin:I = 0x7f0b0082

.field public static final attachments_horizontal_margin:I = 0x7f0b0083

.field public static final attachments_vertical_margin:I = 0x7f0b0084

.field public static final automatic_dp_spacer:I = 0x7f0b0000

.field public static final big_icon_size:I = 0x7f0b001c

.field public static final border_size:I = 0x7f0b0085

.field public static final border_size_focused:I = 0x7f0b0086

.field public static final box_thumbnail_size:I = 0x7f0b001d

.field public static final burger_header_height:I = 0x7f0b0087

.field public static final card_height:I = 0x7f0b0088

.field public static final card_height_small:I = 0x7f0b0089

.field public static final cardview_compat_inset_shadow:I = 0x7f0b008a

.field public static final cardview_default_elevation:I = 0x7f0b008b

.field public static final cardview_default_radius:I = 0x7f0b008c

.field public static final connection_banner_height:I = 0x7f0b008d

.field public static final contact_fragment_separator_height:I = 0x7f0b008e

.field public static final contact_image_height:I = 0x7f0b008f

.field public static final contact_image_large_text:I = 0x7f0b0090

.field public static final contact_image_small_text:I = 0x7f0b0091

.field public static final contact_image_width:I = 0x7f0b0092

.field public static final crop__bar_height:I = 0x7f0b0006

.field public static final default_gap:I = 0x7f0b0093

.field public static final design_appbar_elevation:I = 0x7f0b0094

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f0b0095

.field public static final design_bottom_navigation_active_text_size:I = 0x7f0b0096

.field public static final design_bottom_navigation_elevation:I = 0x7f0b0097

.field public static final design_bottom_navigation_height:I = 0x7f0b0098

.field public static final design_bottom_navigation_item_max_width:I = 0x7f0b0099

.field public static final design_bottom_navigation_item_min_width:I = 0x7f0b009a

.field public static final design_bottom_navigation_margin:I = 0x7f0b009b

.field public static final design_bottom_navigation_shadow_height:I = 0x7f0b009c

.field public static final design_bottom_navigation_text_size:I = 0x7f0b009d

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f0b009e

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f0b009f

.field public static final design_fab_border_width:I = 0x7f0b00a0

.field public static final design_fab_elevation:I = 0x7f0b00a1

.field public static final design_fab_image_size:I = 0x7f0b00a2

.field public static final design_fab_size_mini:I = 0x7f0b00a3

.field public static final design_fab_size_normal:I = 0x7f0b00a4

.field public static final design_fab_translation_z_pressed:I = 0x7f0b00a5

.field public static final design_navigation_elevation:I = 0x7f0b00a6

.field public static final design_navigation_icon_padding:I = 0x7f0b00a7

.field public static final design_navigation_icon_size:I = 0x7f0b00a8

.field public static final design_navigation_max_width:I = 0x7f0b002f

.field public static final design_navigation_padding_bottom:I = 0x7f0b00a9

.field public static final design_navigation_separator_vertical_padding:I = 0x7f0b00aa

.field public static final design_snackbar_action_inline_max_width:I = 0x7f0b0030

.field public static final design_snackbar_background_corner_radius:I = 0x7f0b0031

.field public static final design_snackbar_elevation:I = 0x7f0b00ab

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f0b0032

.field public static final design_snackbar_max_width:I = 0x7f0b0033

.field public static final design_snackbar_min_width:I = 0x7f0b0034

.field public static final design_snackbar_padding_horizontal:I = 0x7f0b00ac

.field public static final design_snackbar_padding_vertical:I = 0x7f0b00ad

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f0b0035

.field public static final design_snackbar_text_size:I = 0x7f0b00ae

.field public static final design_tab_max_width:I = 0x7f0b00af

.field public static final design_tab_scrollable_min_width:I = 0x7f0b0036

.field public static final design_tab_text_size:I = 0x7f0b00b0

.field public static final design_tab_text_size_2line:I = 0x7f0b00b1

.field public static final disabled_alpha_material_dark:I = 0x7f0b00b2

.field public static final disabled_alpha_material_light:I = 0x7f0b00b3

.field public static final drop_pin_map_top_half_padding:I = 0x7f0b00b4

.field public static final drop_pin_map_top_padding:I = 0x7f0b00b5

.field public static final elevation:I = 0x7f0b00b6

.field public static final elevation_plus_one:I = 0x7f0b00b7

.field public static final fab_margin:I = 0x7f0b00b8

.field public static final feature_video_height:I = 0x7f0b00b9

.field public static final feature_video_width:I = 0x7f0b00ba

.field public static final google_map_bottom_overlap:I = 0x7f0b00bb

.field public static final help_recycler_view_bottom_padding:I = 0x7f0b00bc

.field public static final help_row_action_height:I = 0x7f0b00bd

.field public static final help_row_action_padding_bottom:I = 0x7f0b00be

.field public static final help_row_action_padding_end:I = 0x7f0b00bf

.field public static final help_row_action_padding_start:I = 0x7f0b00c0

.field public static final help_row_action_padding_top:I = 0x7f0b00c1

.field public static final help_row_article_min_height:I = 0x7f0b00c2

.field public static final help_row_article_padding_bottom:I = 0x7f0b00c3

.field public static final help_row_article_padding_end:I = 0x7f0b00c4

.field public static final help_row_article_padding_start:I = 0x7f0b00c5

.field public static final help_row_article_padding_top:I = 0x7f0b00c6

.field public static final help_row_category_min_height:I = 0x7f0b00c7

.field public static final help_row_category_padding_bottom:I = 0x7f0b00c8

.field public static final help_row_category_padding_end:I = 0x7f0b00c9

.field public static final help_row_category_padding_start:I = 0x7f0b00ca

.field public static final help_row_category_padding_top:I = 0x7f0b00cb

.field public static final help_row_section_min_height:I = 0x7f0b00cc

.field public static final help_row_section_padding_bottom:I = 0x7f0b00cd

.field public static final help_row_section_padding_end:I = 0x7f0b00ce

.field public static final help_row_section_padding_start:I = 0x7f0b00cf

.field public static final help_row_section_padding_top:I = 0x7f0b00d0

.field public static final help_search_row_min_height:I = 0x7f0b00d1

.field public static final help_search_row_padding_bottom:I = 0x7f0b00d2

.field public static final help_search_row_padding_end:I = 0x7f0b00d3

.field public static final help_search_row_padding_start:I = 0x7f0b00d4

.field public static final help_search_row_padding_top:I = 0x7f0b00d5

.field public static final here_map_bottom_overlap:I = 0x7f0b00d6

.field public static final here_map_padding:I = 0x7f0b00d7

.field public static final highlight_alpha_material_colored:I = 0x7f0b00d8

.field public static final highlight_alpha_material_dark:I = 0x7f0b00d9

.field public static final highlight_alpha_material_light:I = 0x7f0b00da

.field public static final hint_alpha_material_dark:I = 0x7f0b00db

.field public static final hint_alpha_material_light:I = 0x7f0b00dc

.field public static final hint_pressed_alpha_material_dark:I = 0x7f0b00dd

.field public static final hint_pressed_alpha_material_light:I = 0x7f0b00de

.field public static final horizontal_page_margin:I = 0x7f0b00df

.field public static final in_between_fab_margin:I = 0x7f0b00e0

.field public static final inner_big_margin:I = 0x7f0b001e

.field public static final inner_huge_margin:I = 0x7f0b001f

.field public static final inner_margin:I = 0x7f0b0020

.field public static final install_bottom_button_padding:I = 0x7f0b00e1

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f0b00e2

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f0b00e3

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f0b00e4

.field public static final list_header_height:I = 0x7f0b00e5

.field public static final maps_btn_map_toolbar_bottom_shadow:I = 0x7f0b00e6

.field public static final maps_btn_map_toolbar_divider:I = 0x7f0b00e7

.field public static final maps_btn_map_toolbar_margin:I = 0x7f0b00e8

.field public static final maps_btn_margin:I = 0x7f0b00e9

.field public static final maps_btn_width:I = 0x7f0b00ea

.field public static final maps_btn_zoom_y_margin:I = 0x7f0b00eb

.field public static final maps_dav_compass_size:I = 0x7f0b00ec

.field public static final maps_dav_hud_copyright_fontsize:I = 0x7f0b00ed

.field public static final maps_dav_my_location_bubble_y_offset:I = 0x7f0b00ee

.field public static final maps_lite_mode_grid_spacing:I = 0x7f0b00ef

.field public static final maps_sv_arrow_height:I = 0x7f0b00f0

.field public static final maps_sv_arrow_width:I = 0x7f0b00f1

.field public static final maps_sv_circle_height:I = 0x7f0b00f2

.field public static final maps_sv_circle_width:I = 0x7f0b00f3

.field public static final maps_vm_mylocation_dot_size:I = 0x7f0b00f4

.field public static final maps_watermark_margin:I = 0x7f0b00f5

.field public static final margin_huge:I = 0x7f0b0021

.field public static final margin_large:I = 0x7f0b0022

.field public static final margin_medium:I = 0x7f0b0023

.field public static final margin_right_for_custom_spinner:I = 0x7f0b003c

.field public static final margin_small:I = 0x7f0b0024

.field public static final margin_tiny:I = 0x7f0b0025

.field public static final marketing_hud_ui_page_margin:I = 0x7f0b00f6

.field public static final mount_thumbnail_size:I = 0x7f0b0026

.field public static final no_network_view_bottom_padding:I = 0x7f0b00f7

.field public static final no_network_view_separator_height:I = 0x7f0b00f8

.field public static final no_network_view_top_padding:I = 0x7f0b00f9

.field public static final notification_action_icon_size:I = 0x7f0b00fa

.field public static final notification_action_text_size:I = 0x7f0b00fb

.field public static final notification_big_circle_margin:I = 0x7f0b00fc

.field public static final notification_content_margin_start:I = 0x7f0b0039

.field public static final notification_large_icon_height:I = 0x7f0b00fd

.field public static final notification_large_icon_width:I = 0x7f0b00fe

.field public static final notification_main_column_padding_top:I = 0x7f0b003a

.field public static final notification_media_narrow_margin:I = 0x7f0b003b

.field public static final notification_right_icon_size:I = 0x7f0b00ff

.field public static final notification_right_side_padding_top:I = 0x7f0b0037

.field public static final notification_small_icon_background_padding:I = 0x7f0b0100

.field public static final notification_small_icon_size_as_large:I = 0x7f0b0101

.field public static final notification_subtext_size:I = 0x7f0b0102

.field public static final notification_top_pad:I = 0x7f0b0103

.field public static final notification_top_pad_large_text:I = 0x7f0b0104

.field public static final outer_big_margin:I = 0x7f0b0027

.field public static final outer_margin:I = 0x7f0b0028

.field public static final place_autocomplete_button_padding:I = 0x7f0b0105

.field public static final place_autocomplete_powered_by_google_height:I = 0x7f0b0106

.field public static final place_autocomplete_powered_by_google_start:I = 0x7f0b0107

.field public static final place_autocomplete_prediction_height:I = 0x7f0b0108

.field public static final place_autocomplete_prediction_horizontal_margin:I = 0x7f0b0109

.field public static final place_autocomplete_prediction_primary_text:I = 0x7f0b010a

.field public static final place_autocomplete_prediction_secondary_text:I = 0x7f0b010b

.field public static final place_autocomplete_progress_horizontal_margin:I = 0x7f0b010c

.field public static final place_autocomplete_progress_size:I = 0x7f0b010d

.field public static final place_autocomplete_separator_start:I = 0x7f0b010e

.field public static final request_list_divider_height:I = 0x7f0b010f

.field public static final request_list_error_container_horizontal_padding:I = 0x7f0b0110

.field public static final request_list_error_container_vertical_padding:I = 0x7f0b0111

.field public static final request_list_row_date_padding_top:I = 0x7f0b0112

.field public static final request_list_row_date_text_size:I = 0x7f0b0113

.field public static final request_list_row_description_text_size:I = 0x7f0b0114

.field public static final request_list_row_horizontal_padding:I = 0x7f0b0115

.field public static final request_list_row_minimum_height:I = 0x7f0b0116

.field public static final request_list_row_vertical_padding:I = 0x7f0b0117

.field public static final request_list_unread_indicator_size:I = 0x7f0b0118

.field public static final retry_view_bottom_padding:I = 0x7f0b0119

.field public static final retry_view_button_layout_height:I = 0x7f0b011a

.field public static final retry_view_button_layout_width:I = 0x7f0b011b

.field public static final retry_view_button_margin_bottom:I = 0x7f0b011c

.field public static final retry_view_button_margin_right:I = 0x7f0b011d

.field public static final retry_view_button_margin_top:I = 0x7f0b011e

.field public static final retry_view_button_textsize:I = 0x7f0b011f

.field public static final retry_view_left_padding:I = 0x7f0b0120

.field public static final retry_view_right_padding:I = 0x7f0b0121

.field public static final retry_view_separator_height:I = 0x7f0b0122

.field public static final retry_view_textview_textsize:I = 0x7f0b0123

.field public static final retry_view_top_padding:I = 0x7f0b0124

.field public static final rma_button_text:I = 0x7f0b0125

.field public static final rma_divider_height:I = 0x7f0b0126

.field public static final rma_edit_text:I = 0x7f0b0127

.field public static final rma_progress_height:I = 0x7f0b0128

.field public static final rma_progress_width:I = 0x7f0b0129

.field public static final rma_subtitle_text:I = 0x7f0b012a

.field public static final rma_title_text:I = 0x7f0b012b

.field public static final search_bar_height:I = 0x7f0b012c

.field public static final search_bar_margin:I = 0x7f0b012d

.field public static final search_toolbar_height:I = 0x7f0b012e

.field public static final section_header_height:I = 0x7f0b012f

.field public static final small_icon_size:I = 0x7f0b0029

.field public static final status_bar_height:I = 0x7f0b0130

.field public static final suggestion_list_top_height:I = 0x7f0b0131

.field public static final tab_bar_height:I = 0x7f0b0132

.field public static final tab_font_size:I = 0x7f0b002a

.field public static final toolbar_inner_height:I = 0x7f0b0133

.field public static final trip_card_height:I = 0x7f0b0134

.field public static final trip_card_margin:I = 0x7f0b0135

.field public static final util_button_margin:I = 0x7f0b0136

.field public static final vertical_page_margin:I = 0x7f0b0137

.field public static final view_horizontal_margin:I = 0x7f0b0138

.field public static final view_request_comment_avatar_size:I = 0x7f0b0139

.field public static final view_request_comment_list_separator_height:I = 0x7f0b013a

.field public static final view_request_comment_send_button_size:I = 0x7f0b013b

.field public static final view_request_content_margin:I = 0x7f0b013c

.field public static final view_request_horizontal_margin:I = 0x7f0b013d

.field public static final view_request_separator_height:I = 0x7f0b013e

.field public static final view_request_vertical_margin:I = 0x7f0b013f

.field public static final view_vertical_margin:I = 0x7f0b0140

.field public static final zd_dark_icon_active_opacity:I = 0x7f0b0141

.field public static final zd_dark_icon_inactive_opacity:I = 0x7f0b0142

.field public static final zd_light_icon_active_opacity:I = 0x7f0b0143

.field public static final zd_light_icon_inactive_opacity:I = 0x7f0b0144

.field public static final zd_progress_bar_normal:I = 0x7f0b0145

.field public static final zd_progress_bar_small:I = 0x7f0b0146

.field public static final zd_toolbar_elevation:I = 0x7f0b0147


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4628
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
