.class public Lcom/navdy/service/library/util/NetworkActivityTracker;
.super Ljava/lang/Thread;
.source "NetworkActivityTracker.java"


# static fields
.field private static final LOGGING_INTERVAL:I = 0xea60

.field private static final sInstance:Lcom/navdy/service/library/util/NetworkActivityTracker;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bytesReceived:Ljava/util/concurrent/atomic/AtomicLong;

.field private bytesSent:Ljava/util/concurrent/atomic/AtomicLong;

.field private highestReceivedBandwidth:D

.field private highestSentBandwidth:D

.field private starttime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/util/NetworkActivityTracker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 12
    new-instance v0, Lcom/navdy/service/library/util/NetworkActivityTracker;

    invoke-direct {v0}, Lcom/navdy/service/library/util/NetworkActivityTracker;-><init>()V

    sput-object v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->sInstance:Lcom/navdy/service/library/util/NetworkActivityTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesSent:Ljava/util/concurrent/atomic/AtomicLong;

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesReceived:Ljava/util/concurrent/atomic/AtomicLong;

    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->starttime:J

    .line 29
    iput-wide v2, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestSentBandwidth:D

    .line 30
    iput-wide v2, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestReceivedBandwidth:D

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/navdy/service/library/util/NetworkActivityTracker;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->sInstance:Lcom/navdy/service/library/util/NetworkActivityTracker;

    return-object v0
.end method


# virtual methods
.method public addBytesReceived(I)V
    .locals 4
    .param p1, "bytes"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesReceived:Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 39
    return-void
.end method

.method public addBytesSent(I)V
    .locals 4
    .param p1, "bytes"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesSent:Ljava/util/concurrent/atomic/AtomicLong;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    .line 35
    return-void
.end method

.method public run()V
    .locals 36

    .prologue
    .line 42
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesSent:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v24

    .line 43
    .local v24, "lastBytesSent":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesReceived:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v22

    .line 44
    .local v22, "lastBytesReceived":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->starttime:J

    move-wide/from16 v26, v0

    .line 45
    .local v26, "lastTime":J
    const/4 v11, 0x0

    .line 48
    .local v11, "endLoop":Z
    :cond_0
    const-wide/32 v32, 0xea60

    :try_start_0
    invoke-static/range {v32 .. v33}, Lcom/navdy/service/library/util/NetworkActivityTracker;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesSent:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    .line 54
    .local v4, "currentByesSent":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->bytesReceived:Ljava/util/concurrent/atomic/AtomicLong;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v6

    .line 55
    .local v6, "currentBytesReceived":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 57
    .local v8, "currentTime":J
    sub-long v16, v4, v24

    .line 58
    .local v16, "intervalBytesSent":J
    sub-long v14, v6, v22

    .line 59
    .local v14, "intervalBytesReceived":J
    sub-long v12, v8, v26

    .line 60
    .local v12, "interval":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->starttime:J

    move-wide/from16 v32, v0

    sub-long v30, v8, v32

    .line 62
    .local v30, "sinceStart":J
    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v32, v0

    const-wide v34, 0x408f400000000000L    # 1000.0

    mul-double v32, v32, v34

    long-to-double v0, v12

    move-wide/from16 v34, v0

    div-double v20, v32, v34

    .line 63
    .local v20, "intervalSentBandwidth":D
    long-to-double v0, v14

    move-wide/from16 v32, v0

    const-wide v34, 0x408f400000000000L    # 1000.0

    mul-double v32, v32, v34

    long-to-double v0, v12

    move-wide/from16 v34, v0

    div-double v18, v32, v34

    .line 65
    .local v18, "intervalReceivedBandwidth":D
    move-wide/from16 v24, v4

    .line 66
    move-wide/from16 v22, v6

    .line 67
    move-wide/from16 v26, v8

    .line 68
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestSentBandwidth:D

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-wide/from16 v2, v20

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v32

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestSentBandwidth:D

    .line 69
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestReceivedBandwidth:D

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v32

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestReceivedBandwidth:D

    .line 71
    const-wide/16 v32, 0x0

    cmp-long v29, v16, v32

    if-gtz v29, :cond_1

    const-wide/16 v32, 0x0

    cmp-long v29, v14, v32

    if-lez v29, :cond_2

    .line 72
    :cond_1
    const-string v29, "Total time: %dms interval: %dms sent: %d bytes received: %d bytes totalSent: %d bytes totalReceived: %d bytes sentBandwidth: %.2f bytes/sec receivedBandwidth: %.2f bytes/sec maxSentBandwidth: %.2f bytes/sec maxReceivedBandwidth: %.2f bytes/sec"

    const/16 v32, 0xa

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    .line 76
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x1

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x2

    .line 77
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x3

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x5

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x6

    .line 78
    invoke-static/range {v20 .. v21}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x7

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x8

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestSentBandwidth:D

    move-wide/from16 v34, v0

    .line 79
    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v32, v33

    const/16 v33, 0x9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/navdy/service/library/util/NetworkActivityTracker;->highestReceivedBandwidth:D

    move-wide/from16 v34, v0

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v34

    aput-object v34, v32, v33

    .line 72
    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    .line 81
    .local v28, "message":Ljava/lang/String;
    sget-object v29, Lcom/navdy/service/library/util/NetworkActivityTracker;->sLogger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 84
    .end local v28    # "message":Ljava/lang/String;
    :cond_2
    if-eqz v11, :cond_0

    .line 86
    return-void

    .line 49
    .end local v4    # "currentByesSent":J
    .end local v6    # "currentBytesReceived":J
    .end local v8    # "currentTime":J
    .end local v12    # "interval":J
    .end local v14    # "intervalBytesReceived":J
    .end local v16    # "intervalBytesSent":J
    .end local v18    # "intervalReceivedBandwidth":D
    .end local v20    # "intervalSentBandwidth":D
    .end local v30    # "sinceStart":J
    :catch_0
    move-exception v10

    .line 50
    .local v10, "e":Ljava/lang/InterruptedException;
    const/4 v11, 0x1

    goto/16 :goto_0
.end method
