.class Lcom/navdy/service/library/device/connection/ConnectionListener$4;
.super Ljava/lang/Object;
.source "ConnectionListener.java"

# interfaces
.implements Lcom/navdy/service/library/device/connection/ConnectionListener$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/connection/ConnectionListener;->dispatchConnected(Lcom/navdy/service/library/device/connection/Connection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionListener;

.field final synthetic val$connection:Lcom/navdy/service/library/device/connection/Connection;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/Connection;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/ConnectionListener;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionListener$4;->this$0:Lcom/navdy/service/library/device/connection/ConnectionListener;

    iput-object p2, p0, Lcom/navdy/service/library/device/connection/ConnectionListener$4;->val$connection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/service/library/device/connection/ConnectionListener;
    .param p2, "listener"    # Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/navdy/service/library/device/connection/ConnectionListener$4;->val$connection:Lcom/navdy/service/library/device/connection/Connection;

    invoke-interface {p2, p1, v0}, Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;->onConnected(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/Connection;)V

    .line 110
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 106
    check-cast p1, Lcom/navdy/service/library/device/connection/ConnectionListener;

    check-cast p2, Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/device/connection/ConnectionListener$4;->dispatchEvent(Lcom/navdy/service/library/device/connection/ConnectionListener;Lcom/navdy/service/library/device/connection/ConnectionListener$Listener;)V

    return-void
.end method
