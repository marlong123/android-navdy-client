.class Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;
.super Ljava/lang/Object;
.source "BTRemoteDeviceScanner.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 54
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$000(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$002(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;Z)Z

    .line 57
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 58
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 59
    const-string v1, "android.bluetooth.device.action.UUID"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 60
    const-string v1, "android.bluetooth.device.action.NAME_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 61
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    iget-object v1, v1, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v2}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$100(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 64
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1, v2}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$202(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;Ljava/util/Set;)Ljava/util/Set;

    .line 66
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$300(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$300(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$300(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$300(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 74
    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->dispatchOnScanStarted()V

    .line 77
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_1
    return-void
.end method
