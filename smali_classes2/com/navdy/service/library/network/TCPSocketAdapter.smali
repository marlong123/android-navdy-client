.class public Lcom/navdy/service/library/network/TCPSocketAdapter;
.super Ljava/lang/Object;
.source "TCPSocketAdapter.java"

# interfaces
.implements Lcom/navdy/service/library/network/SocketAdapter;


# instance fields
.field private final host:Ljava/lang/String;

.field private final port:I

.field private socket:Ljava/net/Socket;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    if-nez p1, :cond_0

    .line 21
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Host must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->host:Ljava/lang/String;

    .line 24
    iput p2, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->port:I

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->host:Ljava/lang/String;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->port:I

    .line 31
    iput-object p1, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    .line 32
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    .line 48
    return-void
.end method

.method public connect()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->host:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t connect with accepted socket"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_0
    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->host:Ljava/lang/String;

    iget v2, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->port:I

    invoke-direct {v0, v1, v2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    .line 40
    return-void
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/navdy/service/library/network/TCPSocketAdapter;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/navdy/service/library/network/TCPSocketAdapter;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    sget-object v0, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/navdy/service/library/network/TCPSocketAdapter;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
