.class public final Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoUpdateQueryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public identifier:Ljava/lang/String;

.field public updateRequired:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 64
    if-nez p1, :cond_0

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->identifier:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;->updateRequired:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->updateRequired:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->checkRequiredFields()V

    .line 82
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse;

    move-result-object v0

    return-object v0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->identifier:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public updateRequired(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;
    .locals 0
    .param p1, "updateRequired"    # Ljava/lang/Boolean;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdateQueryResponse$Builder;->updateRequired:Ljava/lang/Boolean;

    .line 76
    return-object p0
.end method
