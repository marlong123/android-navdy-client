.class public final Lcom/navdy/service/library/events/TripUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TripUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/TripUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/TripUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public arrived_at_destination_id:Ljava/lang/String;

.field public bearing:Ljava/lang/Float;

.field public chosen_destination_id:Ljava/lang/String;

.field public current_position:Lcom/navdy/service/library/events/location/LatLong;

.field public distance_to_destination:Ljava/lang/Integer;

.field public distance_traveled:Ljava/lang/Integer;

.field public elevation:Ljava/lang/Double;

.field public elevation_accuracy:Ljava/lang/Float;

.field public estimated_time_remaining:Ljava/lang/Integer;

.field public excessive_speeding_ratio:Ljava/lang/Double;

.field public gps_speed:Ljava/lang/Float;

.field public hard_acceleration_count:Ljava/lang/Integer;

.field public hard_breaking_count:Ljava/lang/Integer;

.field public high_g_count:Ljava/lang/Integer;

.field public horizontal_accuracy:Ljava/lang/Float;

.field public last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

.field public meters_traveled_since_boot:Ljava/lang/Integer;

.field public obd_speed:Ljava/lang/Integer;

.field public road_element:Ljava/lang/String;

.field public sequence_number:Ljava/lang/Integer;

.field public speeding_ratio:Ljava/lang/Double;

.field public timestamp:Ljava/lang/Long;

.field public trip_number:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 316
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/TripUpdate;

    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 320
    if-nez p1, :cond_0

    .line 344
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->trip_number:Ljava/lang/Long;

    .line 322
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->sequence_number:Ljava/lang/Integer;

    .line 323
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->timestamp:Ljava/lang/Long;

    .line 324
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_traveled:Ljava/lang/Integer;

    .line 325
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 326
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation:Ljava/lang/Double;

    .line 327
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->bearing:Ljava/lang/Float;

    .line 328
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->gps_speed:Ljava/lang/Float;

    .line 329
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->obd_speed:Ljava/lang/Integer;

    .line 330
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->road_element:Ljava/lang/String;

    .line 331
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id:Ljava/lang/String;

    .line 332
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->arrived_at_destination_id:Ljava/lang/String;

    .line 333
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->estimated_time_remaining:Ljava/lang/Integer;

    .line 334
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_to_destination:Ljava/lang/Integer;

    .line 335
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->high_g_count:Ljava/lang/Integer;

    .line 336
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_breaking_count:Ljava/lang/Integer;

    .line 337
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_acceleration_count:Ljava/lang/Integer;

    .line 338
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->speeding_ratio:Ljava/lang/Double;

    .line 339
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->excessive_speeding_ratio:Ljava/lang/Double;

    .line 340
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->meters_traveled_since_boot:Ljava/lang/Integer;

    .line 341
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->horizontal_accuracy:Ljava/lang/Float;

    .line 342
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation_accuracy:Ljava/lang/Float;

    .line 343
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    goto :goto_0
.end method


# virtual methods
.method public arrived_at_destination_id(Ljava/lang/String;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "arrived_at_destination_id"    # Ljava/lang/String;

    .prologue
    .line 445
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->arrived_at_destination_id:Ljava/lang/String;

    .line 446
    return-object p0
.end method

.method public bearing(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "bearing"    # Ljava/lang/Float;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->bearing:Ljava/lang/Float;

    .line 402
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/TripUpdate;
    .locals 2

    .prologue
    .line 538
    invoke-virtual {p0}, Lcom/navdy/service/library/events/TripUpdate$Builder;->checkRequiredFields()V

    .line 539
    new-instance v0, Lcom/navdy/service/library/events/TripUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/TripUpdate;-><init>(Lcom/navdy/service/library/events/TripUpdate$Builder;Lcom/navdy/service/library/events/TripUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/navdy/service/library/events/TripUpdate$Builder;->build()Lcom/navdy/service/library/events/TripUpdate;

    move-result-object v0

    return-object v0
.end method

.method public chosen_destination_id(Ljava/lang/String;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "chosen_destination_id"    # Ljava/lang/String;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id:Ljava/lang/String;

    .line 437
    return-object p0
.end method

.method public current_position(Lcom/navdy/service/library/events/location/LatLong;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "current_position"    # Lcom/navdy/service/library/events/location/LatLong;

    .prologue
    .line 385
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 386
    return-object p0
.end method

.method public distance_to_destination(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "distance_to_destination"    # Ljava/lang/Integer;

    .prologue
    .line 463
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_to_destination:Ljava/lang/Integer;

    .line 464
    return-object p0
.end method

.method public distance_traveled(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "distance_traveled"    # Ljava/lang/Integer;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_traveled:Ljava/lang/Integer;

    .line 378
    return-object p0
.end method

.method public elevation(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "elevation"    # Ljava/lang/Double;

    .prologue
    .line 393
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation:Ljava/lang/Double;

    .line 394
    return-object p0
.end method

.method public elevation_accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "elevation_accuracy"    # Ljava/lang/Float;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation_accuracy:Ljava/lang/Float;

    .line 525
    return-object p0
.end method

.method public estimated_time_remaining(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "estimated_time_remaining"    # Ljava/lang/Integer;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->estimated_time_remaining:Ljava/lang/Integer;

    .line 455
    return-object p0
.end method

.method public excessive_speeding_ratio(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "excessive_speeding_ratio"    # Ljava/lang/Double;

    .prologue
    .line 503
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->excessive_speeding_ratio:Ljava/lang/Double;

    .line 504
    return-object p0
.end method

.method public gps_speed(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "gps_speed"    # Ljava/lang/Float;

    .prologue
    .line 409
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->gps_speed:Ljava/lang/Float;

    .line 410
    return-object p0
.end method

.method public hard_acceleration_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "hard_acceleration_count"    # Ljava/lang/Integer;

    .prologue
    .line 487
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_acceleration_count:Ljava/lang/Integer;

    .line 488
    return-object p0
.end method

.method public hard_breaking_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "hard_breaking_count"    # Ljava/lang/Integer;

    .prologue
    .line 479
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_breaking_count:Ljava/lang/Integer;

    .line 480
    return-object p0
.end method

.method public high_g_count(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "high_g_count"    # Ljava/lang/Integer;

    .prologue
    .line 471
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->high_g_count:Ljava/lang/Integer;

    .line 472
    return-object p0
.end method

.method public horizontal_accuracy(Ljava/lang/Float;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "horizontal_accuracy"    # Ljava/lang/Float;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->horizontal_accuracy:Ljava/lang/Float;

    .line 517
    return-object p0
.end method

.method public last_raw_coordinate(Lcom/navdy/service/library/events/location/Coordinate;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "last_raw_coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 532
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 533
    return-object p0
.end method

.method public meters_traveled_since_boot(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "meters_traveled_since_boot"    # Ljava/lang/Integer;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->meters_traveled_since_boot:Ljava/lang/Integer;

    .line 509
    return-object p0
.end method

.method public obd_speed(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "obd_speed"    # Ljava/lang/Integer;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->obd_speed:Ljava/lang/Integer;

    .line 420
    return-object p0
.end method

.method public road_element(Ljava/lang/String;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "road_element"    # Ljava/lang/String;

    .prologue
    .line 427
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->road_element:Ljava/lang/String;

    .line 428
    return-object p0
.end method

.method public sequence_number(Ljava/lang/Integer;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "sequence_number"    # Ljava/lang/Integer;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->sequence_number:Ljava/lang/Integer;

    .line 359
    return-object p0
.end method

.method public speeding_ratio(Ljava/lang/Double;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "speeding_ratio"    # Ljava/lang/Double;

    .prologue
    .line 495
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->speeding_ratio:Ljava/lang/Double;

    .line 496
    return-object p0
.end method

.method public timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/Long;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->timestamp:Ljava/lang/Long;

    .line 367
    return-object p0
.end method

.method public trip_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/TripUpdate$Builder;
    .locals 0
    .param p1, "trip_number"    # Ljava/lang/Long;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate$Builder;->trip_number:Ljava/lang/Long;

    .line 351
    return-object p0
.end method
