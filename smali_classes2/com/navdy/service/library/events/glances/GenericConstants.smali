.class public final enum Lcom/navdy/service/library/events/glances/GenericConstants;
.super Ljava/lang/Enum;
.source "GenericConstants.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/glances/GenericConstants;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/glances/GenericConstants;

.field public static final enum GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

.field public static final enum GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

.field public static final enum GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

.field public static final enum GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    const-string v1, "GENERIC_TITLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/glances/GenericConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    const-string v1, "GENERIC_MESSAGE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/glances/GenericConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    const-string v1, "GENERIC_MAIN_ICON"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/glances/GenericConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    const-string v1, "GENERIC_SIDE_ICON"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/glances/GenericConstants;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/service/library/events/glances/GenericConstants;

    sget-object v1, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_TITLE:Lcom/navdy/service/library/events/glances/GenericConstants;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MESSAGE:Lcom/navdy/service/library/events/glances/GenericConstants;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_MAIN_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/glances/GenericConstants;->GENERIC_SIDE_ICON:Lcom/navdy/service/library/events/glances/GenericConstants;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/GenericConstants;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 17
    iput p3, p0, Lcom/navdy/service/library/events/glances/GenericConstants;->value:I

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/glances/GenericConstants;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/glances/GenericConstants;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/glances/GenericConstants;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/glances/GenericConstants;->$VALUES:[Lcom/navdy/service/library/events/glances/GenericConstants;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/glances/GenericConstants;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/glances/GenericConstants;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/service/library/events/glances/GenericConstants;->value:I

    return v0
.end method
