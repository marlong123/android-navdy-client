.class public final Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhoneEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/PhoneEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public callUUID:Ljava/lang/String;

.field public contact_name:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public number:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 111
    if-nez p1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 113
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->label:Ljava/lang/String;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->callUUID:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->checkRequiredFields()V

    .line 163
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v0

    return-object v0
.end method

.method public callUUID(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .locals 0
    .param p1, "callUUID"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->callUUID:Ljava/lang/String;

    .line 157
    return-object p0
.end method

.method public contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .locals 0
    .param p1, "contact_name"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name:Ljava/lang/String;

    .line 141
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->label:Ljava/lang/String;

    .line 149
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number:Ljava/lang/String;

    .line 133
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 124
    return-object p0
.end method
