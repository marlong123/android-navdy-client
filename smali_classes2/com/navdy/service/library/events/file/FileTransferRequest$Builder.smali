.class public final Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileTransferRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/FileTransferRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/FileTransferRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public destinationFileName:Ljava/lang/String;

.field public fileDataChecksum:Ljava/lang/String;

.field public fileSize:Ljava/lang/Long;

.field public fileType:Lcom/navdy/service/library/events/file/FileType;

.field public offset:Ljava/lang/Long;

.field public override:Ljava/lang/Boolean;

.field public supportsAcks:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 135
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileTransferRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/FileTransferRequest;

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 139
    if-nez p1, :cond_0

    .line 147
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 141
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->destinationFileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->destinationFileName:Ljava/lang/String;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileSize:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileSize:Ljava/lang/Long;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->offset:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->offset:Ljava/lang/Long;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->fileDataChecksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileDataChecksum:Ljava/lang/String;

    .line 145
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->override:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->override:Ljava/lang/Boolean;

    .line 146
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferRequest;->supportsAcks:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->supportsAcks:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/FileTransferRequest;
    .locals 2

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->checkRequiredFields()V

    .line 217
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/FileTransferRequest;-><init>(Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;Lcom/navdy/service/library/events/file/FileTransferRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferRequest;

    move-result-object v0

    return-object v0
.end method

.method public destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "destinationFileName"    # Ljava/lang/String;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->destinationFileName:Ljava/lang/String;

    .line 163
    return-object p0
.end method

.method public fileDataChecksum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "fileDataChecksum"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileDataChecksum:Ljava/lang/String;

    .line 190
    return-object p0
.end method

.method public fileSize(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "fileSize"    # Ljava/lang/Long;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileSize:Ljava/lang/Long;

    .line 171
    return-object p0
.end method

.method public fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 155
    return-object p0
.end method

.method public offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "offset"    # Ljava/lang/Long;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->offset:Ljava/lang/Long;

    .line 182
    return-object p0
.end method

.method public override(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "override"    # Ljava/lang/Boolean;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->override:Ljava/lang/Boolean;

    .line 200
    return-object p0
.end method

.method public supportsAcks(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;
    .locals 0
    .param p1, "supportsAcks"    # Ljava/lang/Boolean;

    .prologue
    .line 210
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferRequest$Builder;->supportsAcks:Ljava/lang/Boolean;

    .line 211
    return-object p0
.end method
