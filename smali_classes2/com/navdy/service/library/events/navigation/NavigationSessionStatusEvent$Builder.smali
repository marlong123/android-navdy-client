.class public final Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationSessionStatusEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public destination_identifier:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field public routeId:Ljava/lang/String;

.field public sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 106
    if-nez p1, :cond_0

    .line 112
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 108
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->label:Ljava/lang/String;

    .line 109
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->routeId:Ljava/lang/String;

    .line 110
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 111
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->destination_identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->destination_identifier:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->checkRequiredFields()V

    .line 157
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;

    move-result-object v0

    return-object v0
.end method

.method public destination_identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .locals 0
    .param p1, "destination_identifier"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->destination_identifier:Ljava/lang/String;

    .line 151
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->label:Ljava/lang/String;

    .line 127
    return-object p0
.end method

.method public route(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .locals 0
    .param p1, "route"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 143
    return-object p0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->routeId:Ljava/lang/String;

    .line 135
    return-object p0
.end method

.method public sessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;
    .locals 0
    .param p1, "sessionState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent$Builder;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 119
    return-object p0
.end method
