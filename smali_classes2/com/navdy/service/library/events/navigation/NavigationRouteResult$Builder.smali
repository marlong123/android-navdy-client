.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NavigationRouteResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
        ">;"
    }
.end annotation


# instance fields
.field public address:Ljava/lang/String;

.field public duration:Ljava/lang/Integer;

.field public duration_traffic:Ljava/lang/Integer;

.field public geoPoints_OBSOLETE:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;"
        }
    .end annotation
.end field

.field public label:Ljava/lang/String;

.field public length:Ljava/lang/Integer;

.field public routeId:Ljava/lang/String;

.field public routeLatLongs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public via:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 151
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 155
    if-nez p1, :cond_0

    .line 165
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeId:Ljava/lang/String;

    .line 157
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->label:Ljava/lang/String;

    .line 158
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->geoPoints_OBSOLETE:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->geoPoints_OBSOLETE:Ljava/util/List;

    .line 159
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->length:Ljava/lang/Integer;

    .line 160
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration:Ljava/lang/Integer;

    .line 161
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration_traffic:Ljava/lang/Integer;

    .line 162
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->access$100(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeLatLongs:Ljava/util/List;

    .line 163
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->via:Ljava/lang/String;

    .line 164
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->address:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->address:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public address(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->address:Ljava/lang/String;

    .line 238
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->checkRequiredFields()V

    .line 244
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteResult$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->build()Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v0

    return-object v0
.end method

.method public duration(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "duration"    # Ljava/lang/Integer;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration:Ljava/lang/Integer;

    .line 204
    return-object p0
.end method

.method public duration_traffic(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "duration_traffic"    # Ljava/lang/Integer;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->duration_traffic:Ljava/lang/Integer;

    .line 212
    return-object p0
.end method

.method public geoPoints_OBSOLETE(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            ">;)",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "geoPoints_OBSOLETE":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/location/Coordinate;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->geoPoints_OBSOLETE:Ljava/util/List;

    .line 188
    return-object p0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->label:Ljava/lang/String;

    .line 180
    return-object p0
.end method

.method public length(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "length"    # Ljava/lang/Integer;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->length:Ljava/lang/Integer;

    .line 196
    return-object p0
.end method

.method public routeId(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "routeId"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeId:Ljava/lang/String;

    .line 172
    return-object p0
.end method

.method public routeLatLongs(Ljava/util/List;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "routeLatLongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->routeLatLongs:Ljava/util/List;

    .line 222
    return-object p0
.end method

.method public via(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;
    .locals 0
    .param p1, "via"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult$Builder;->via:Ljava/lang/String;

    .line 230
    return-object p0
.end method
