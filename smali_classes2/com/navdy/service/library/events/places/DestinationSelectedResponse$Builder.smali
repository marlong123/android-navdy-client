.class public final Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DestinationSelectedResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/DestinationSelectedResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public destination:Lcom/navdy/service/library/events/destination/Destination;

.field public request_id:Ljava/lang/String;

.field public request_status:Lcom/navdy/service/library/events/RequestStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/DestinationSelectedResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 85
    if-nez p1, :cond_0

    .line 89
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_id:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_id:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 88
    iget-object v0, p1, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/DestinationSelectedResponse;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse;-><init>(Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;Lcom/navdy/service/library/events/places/DestinationSelectedResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->build()Lcom/navdy/service/library/events/places/DestinationSelectedResponse;

    move-result-object v0

    return-object v0
.end method

.method public destination(Lcom/navdy/service/library/events/destination/Destination;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
    .locals 0
    .param p1, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 112
    return-object p0
.end method

.method public request_id(Ljava/lang/String;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
    .locals 0
    .param p1, "request_id"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_id:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public request_status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;
    .locals 0
    .param p1, "request_status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/navdy/service/library/events/places/DestinationSelectedResponse$Builder;->request_status:Lcom/navdy/service/library/events/RequestStatus;

    .line 104
    return-object p0
.end method
