.class public final Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PlacesSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/PlacesSearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/PlacesSearchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public requestId:Ljava/lang/String;

.field public results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field public searchQuery:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 105
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/PlacesSearchResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 109
    if-nez p1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->searchQuery:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 112
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 113
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->results:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results:Ljava/util/List;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/places/PlacesSearchResponse;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->requestId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/PlacesSearchResponse;
    .locals 2

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->checkRequiredFields()V

    .line 162
    new-instance v0, Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse;-><init>(Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;Lcom/navdy/service/library/events/places/PlacesSearchResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->build()Lcom/navdy/service/library/events/places/PlacesSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public requestId(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .locals 0
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->requestId:Ljava/lang/String;

    .line 156
    return-object p0
.end method

.method public results(Ljava/util/List;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/places/PlacesSearchResult;",
            ">;)",
            "Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->results:Ljava/util/List;

    .line 146
    return-object p0
.end method

.method public searchQuery(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .locals 0
    .param p1, "searchQuery"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->searchQuery:Ljava/lang/String;

    .line 122
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 130
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/navdy/service/library/events/places/PlacesSearchResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 138
    return-object p0
.end method
