.class public final enum Lcom/navdy/service/library/events/audio/MusicPlaybackState;
.super Ljava/lang/Enum;
.source "MusicPlaybackState.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicPlaybackState;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_BUFFERING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_CONNECTING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_ERROR:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_FAST_FORWARDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_REWINDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_SKIPPING_TO_NEXT:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_SKIPPING_TO_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

.field public static final enum PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_BUFFERING"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_BUFFERING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_CONNECTING"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_CONNECTING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_ERROR"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_ERROR:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_PLAYING"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_PAUSED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_STOPPED"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_FAST_FORWARDING"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_FAST_FORWARDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 17
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_REWINDING"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_REWINDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_SKIPPING_TO_NEXT"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_NEXT:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 19
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const-string v1, "PLAYBACK_SKIPPING_TO_PREVIOUS"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/MusicPlaybackState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    .line 7
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_NONE:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_BUFFERING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_CONNECTING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_ERROR:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PLAYING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_PAUSED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_STOPPED:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_FAST_FORWARDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_REWINDING:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_NEXT:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->PLAYBACK_SKIPPING_TO_PREVIOUS:Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->value:I

    .line 25
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicPlaybackState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/MusicPlaybackState;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->$VALUES:[Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/MusicPlaybackState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/MusicPlaybackState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicPlaybackState;->value:I

    return v0
.end method
