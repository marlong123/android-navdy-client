.class public Lcom/navdy/service/library/log/FilteringAppender;
.super Ljava/lang/Object;
.source "FilteringAppender.java"

# interfaces
.implements Lcom/navdy/service/library/log/LogAppender;


# instance fields
.field private baseAppender:Lcom/navdy/service/library/log/LogAppender;

.field private filter:Lcom/navdy/service/library/log/Filter;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/log/LogAppender;Lcom/navdy/service/library/log/Filter;)V
    .locals 0
    .param p1, "baseAppender"    # Lcom/navdy/service/library/log/LogAppender;
    .param p2, "filter"    # Lcom/navdy/service/library/log/Filter;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    .line 14
    iput-object p2, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    .line 15
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0}, Lcom/navdy/service/library/log/LogAppender;->close()V

    .line 95
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x3

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x3

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/service/library/log/LogAppender;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x6

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/service/library/log/LogAppender;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    :cond_0
    return-void
.end method

.method public flush()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0}, Lcom/navdy/service/library/log/LogAppender;->flush()V

    .line 90
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_0
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/service/library/log/LogAppender;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    :cond_0
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 20
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    :cond_0
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x2

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/service/library/log/LogAppender;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    :cond_0
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x5

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2}, Lcom/navdy/service/library/log/LogAppender;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_0
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->filter:Lcom/navdy/service/library/log/Filter;

    const/4 v1, 0x5

    invoke-interface {v0, v1, p1, p2}, Lcom/navdy/service/library/log/Filter;->matches(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/navdy/service/library/log/FilteringAppender;->baseAppender:Lcom/navdy/service/library/log/LogAppender;

    invoke-interface {v0, p1, p2, p3}, Lcom/navdy/service/library/log/LogAppender;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    :cond_0
    return-void
.end method
