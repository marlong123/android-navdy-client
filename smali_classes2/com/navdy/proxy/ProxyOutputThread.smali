.class public Lcom/navdy/proxy/ProxyOutputThread;
.super Ljava/lang/Thread;
.source "ProxyOutputThread.java"


# static fields
.field private static final VERBOSE_DBG:Z

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public mHandler:Landroid/os/Handler;

.field mOutputStream:Ljava/io/OutputStream;

.field mProxyThread:Lcom/navdy/proxy/ProxyThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/proxy/ProxyOutputThread;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/proxy/ProxyOutputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/proxy/ProxyThread;Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "proxyThread"    # Lcom/navdy/proxy/ProxyThread;
    .param p2, "outputStream"    # Ljava/io/OutputStream;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 25
    const-class v0, Lcom/navdy/proxy/ProxyOutputThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/proxy/ProxyOutputThread;->setName(Ljava/lang/String;)V

    .line 26
    iput-object p2, p0, Lcom/navdy/proxy/ProxyOutputThread;->mOutputStream:Ljava/io/OutputStream;

    .line 27
    iput-object p1, p0, Lcom/navdy/proxy/ProxyOutputThread;->mProxyThread:Lcom/navdy/proxy/ProxyThread;

    .line 28
    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/proxy/ProxyOutputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 31
    monitor-enter p0

    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/navdy/proxy/ProxyOutputThread;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 33
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    iget-object v1, p0, Lcom/navdy/proxy/ProxyOutputThread;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 38
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v1, p0, Lcom/navdy/proxy/ProxyOutputThread;->mHandler:Landroid/os/Handler;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v1

    .line 31
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public run()V
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 45
    monitor-enter p0

    .line 46
    :try_start_0
    new-instance v0, Lcom/navdy/proxy/ProxyOutputThread$1;

    invoke-direct {v0, p0}, Lcom/navdy/proxy/ProxyOutputThread$1;-><init>(Lcom/navdy/proxy/ProxyOutputThread;)V

    iput-object v0, p0, Lcom/navdy/proxy/ProxyOutputThread;->mHandler:Landroid/os/Handler;

    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 63
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    sget-object v0, Lcom/navdy/proxy/ProxyOutputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "entering loop"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 65
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 66
    sget-object v0, Lcom/navdy/proxy/ProxyOutputThread;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "loop exited"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/navdy/proxy/ProxyOutputThread;->mProxyThread:Lcom/navdy/proxy/ProxyThread;

    invoke-virtual {v0}, Lcom/navdy/proxy/ProxyThread;->outputThreadWillFinish()V

    .line 69
    return-void

    .line 63
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
