.class final Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;
.super Lcom/localytics/android/MarketingWebView$MarketingWebViewClient;
.source "InAppDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/localytics/android/InAppDialogFragment$InAppDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "InAppWebViewClient"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;


# direct methods
.method constructor <init>(Lcom/localytics/android/InAppDialogFragment$InAppDialog;Lcom/localytics/android/MarketingWebViewManager;)V
    .locals 0
    .param p2, "manager"    # Lcom/localytics/android/MarketingWebViewManager;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    .line 437
    invoke-direct {p0, p2}, Lcom/localytics/android/MarketingWebView$MarketingWebViewClient;-><init>(Lcom/localytics/android/MarketingWebViewManager;)V

    .line 438
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 11
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    .line 444
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment;->access$100(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/InAppCampaign;

    move-result-object v8

    invoke-virtual {v8}, Lcom/localytics/android/InAppCampaign;->getDisplayLocation()Ljava/lang/String;

    move-result-object v1

    .line 445
    .local v1, "location":Ljava/lang/String;
    const-string v8, "center"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/high16 v8, 0x41200000    # 10.0f

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v10

    float-to-int v8, v8

    shl-int/lit8 v2, v8, 0x1

    .line 446
    .local v2, "margin":I
    :goto_0
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    sub-int v4, v8, v2

    .line 447
    .local v4, "maxWidth":I
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v8, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    sub-int v3, v8, v2

    .line 448
    .local v3, "maxHeight":I
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$300(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)F

    move-result v8

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v10

    float-to-int v8, v8

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    div-float v6, v8, v9

    .line 449
    .local v6, "viewportWidth":F
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$400(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)F

    move-result v8

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v8, v9

    add-float/2addr v8, v10

    float-to-int v8, v8

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$200(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->density:F

    div-float v5, v8, v9

    .line 452
    .local v5, "viewportHeight":F
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/CharSequence;

    iget-object v9, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    iget-object v9, v9, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-static {v9}, Lcom/localytics/android/InAppDialogFragment;->access$500(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/JavaScriptClient;

    move-result-object v9

    invoke-virtual {v9, v6, v5}, Lcom/localytics/android/JavaScriptClient;->getViewportAdjuster(FF)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v7

    const/4 v9, 0x1

    const-string v10, ";"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-object v10, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    iget-object v10, v10, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-static {v10}, Lcom/localytics/android/InAppDialogFragment;->access$500(Lcom/localytics/android/InAppDialogFragment;)Lcom/localytics/android/JavaScriptClient;

    move-result-object v10

    invoke-virtual {v10}, Lcom/localytics/android/JavaScriptClient;->getJavaScriptBridge()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v8}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "javascript":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 460
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->access$600(Lcom/localytics/android/InAppDialogFragment$InAppDialog;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 461
    iget-object v8, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    iget-object v8, v8, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->this$0:Lcom/localytics/android/InAppDialogFragment;

    invoke-static {v8}, Lcom/localytics/android/InAppDialogFragment;->access$700(Lcom/localytics/android/InAppDialogFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 463
    iget-object v7, p0, Lcom/localytics/android/InAppDialogFragment$InAppDialog$InAppWebViewClient;->this$1:Lcom/localytics/android/InAppDialogFragment$InAppDialog;

    invoke-virtual {v7}, Lcom/localytics/android/InAppDialogFragment$InAppDialog;->enterWithAnimation()V

    .line 465
    :cond_0
    return-void

    .end local v0    # "javascript":Ljava/lang/String;
    .end local v2    # "margin":I
    .end local v3    # "maxHeight":I
    .end local v4    # "maxWidth":I
    .end local v5    # "viewportHeight":F
    .end local v6    # "viewportWidth":F
    :cond_1
    move v2, v7

    .line 445
    goto/16 :goto_0
.end method
