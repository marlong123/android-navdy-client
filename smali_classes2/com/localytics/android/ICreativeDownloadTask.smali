.class interface abstract Lcom/localytics/android/ICreativeDownloadTask;
.super Ljava/lang/Object;
.source "ICreativeDownloadTask.java"

# interfaces
.implements Ljava/lang/Runnable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/localytics/android/ICreativeDownloadTask$Priority;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/localytics/android/ICreativeDownloadTask;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract getCampaign()Lcom/localytics/android/MarketingMessage;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getPriority()Lcom/localytics/android/ICreativeDownloadTask$Priority;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract getSequence()I
.end method

.method public abstract setPriority(Lcom/localytics/android/ICreativeDownloadTask$Priority;)V
    .param p1    # Lcom/localytics/android/ICreativeDownloadTask$Priority;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
