.class Lcom/localytics/android/PushManager$2;
.super Ljava/lang/Object;
.source "PushManager.java"

# interfaces
.implements Lcom/localytics/android/PushManager$POSTBodyBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/localytics/android/PushManager;->handleDeviceInfo(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/localytics/android/PushManager;

.field final synthetic val$appContext:Landroid/content/Context;

.field final synthetic val$customerIdFuture:Ljava/util/concurrent/Future;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/localytics/android/PushManager;Landroid/net/Uri;Ljava/util/concurrent/Future;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    iput-object p2, p0, Lcom/localytics/android/PushManager$2;->val$uri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/localytics/android/PushManager$2;->val$customerIdFuture:Ljava/util/concurrent/Future;

    iput-object p4, p0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/String;
    .locals 22

    .prologue
    .line 341
    :try_start_0
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    .line 343
    .local v12, "postBody":Lorg/json/JSONObject;
    new-instance v17, Lorg/json/JSONObject;

    invoke-direct/range {v17 .. v17}, Lorg/json/JSONObject;-><init>()V

    .line 344
    .local v17, "requestInfo":Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$uri:Landroid/net/Uri;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v14

    .line 345
    .local v14, "query":Ljava/lang/String;
    if-eqz v14, :cond_1

    .line 347
    const-string v18, "&"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 348
    .local v15, "queryParameters":[Ljava/lang/String;
    move-object v2, v15

    .local v2, "arr$":[Ljava/lang/String;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v9, :cond_1

    aget-object v13, v2, v6

    .line 350
    .local v13, "qp":Ljava/lang/String;
    const-string v18, "="

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 351
    .local v8, "keyValue":[Ljava/lang/String;
    array-length v0, v8

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 353
    const/16 v18, 0x0

    aget-object v18, v8, v18

    const/16 v19, 0x1

    aget-object v19, v8, v19

    invoke-virtual/range {v17 .. v19}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 348
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 357
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "keyValue":[Ljava/lang/String;
    .end local v9    # "len$":I
    .end local v13    # "qp":Ljava/lang/String;
    .end local v15    # "queryParameters":[Ljava/lang/String;
    :cond_1
    const-string v18, "request_info"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/localytics/android/LocalyticsDao;->getPushRegistrationId()Ljava/lang/String;

    move-result-object v16

    .line 360
    .local v16, "registrationId":Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 361
    .local v4, "deviceInfo":Lorg/json/JSONObject;
    const-string v18, "app_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/localytics/android/LocalyticsDao;->getAppKey()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    const-string v18, "customer_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$customerIdFuture:Ljava/util/concurrent/Future;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 363
    const-string v18, "install_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/localytics/android/LocalyticsDao;->getInstallationId()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 364
    const-string v18, "manufacturer"

    invoke-static {}, Lcom/localytics/android/DatapointHelper;->getManufacturer()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 365
    const-string v18, "os_version"

    sget-object v19, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 366
    const-string v18, "model"

    sget-object v19, Landroid/os/Build;->MODEL:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 367
    const-string v18, "library_version"

    sget-object v19, Lcom/localytics/android/Constants;->LOCALYTICS_CLIENT_LIBRARY_VERSION:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 368
    const-string v18, "app_version"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/localytics/android/DatapointHelper;->getAppVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 369
    const-string v18, "push_token"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 370
    const-string v19, "notifications_enabled"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/localytics/android/LocalyticsDao;->areNotificationsDisabled()Z

    move-result v18

    if-nez v18, :cond_2

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_2

    const/16 v18, 0x1

    :goto_1
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 371
    const-string v18, "device_platform"

    const-string v19, "Android"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 372
    const-string v18, "package_name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 373
    const-string v18, "is_opted_out"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/localytics/android/LocalyticsDao;->isOptedOut()Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 374
    const-string v18, "sender_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->this$0:Lcom/localytics/android/PushManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/localytics/android/PushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/localytics/android/LocalyticsDao;->getSenderId()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 375
    const-string v18, "device_info"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 377
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 379
    .local v7, "integrationInfo":Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 380
    .local v10, "manifestInfo":Lorg/json/JSONObject;
    const-string v18, "has_gcm_receiver"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "com.google.android.gms.gcm.GcmReceiver"

    invoke-static/range {v19 .. v20}, Lcom/localytics/android/ManifestUtil;->isReceiverInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 381
    const-string v18, "has_gcm_listener_service"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-class v20, Lcom/localytics/android/GcmListenerService;

    invoke-static/range {v19 .. v20}, Lcom/localytics/android/ManifestUtil;->isServiceInManifest(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 382
    const-string v18, "has_instance_id_listener_service"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-class v20, Lcom/localytics/android/InstanceIDListenerService;

    invoke-static/range {v19 .. v20}, Lcom/localytics/android/ManifestUtil;->isServiceInManifest(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 383
    const-string v18, "has_push_tracking_activity"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-class v20, Lcom/localytics/android/PushTrackingActivity;

    invoke-static/range {v19 .. v20}, Lcom/localytics/android/ManifestUtil;->isActivityInManifest(Landroid/content/Context;Ljava/lang/Class;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 384
    const-string v18, "%s.permission.C2D_MESSAGE"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 385
    .local v11, "permissionC2dMessage":Ljava/lang/String;
    const-string v18, "has_permission_c2d_message"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v11}, Lcom/localytics/android/ManifestUtil;->isPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 386
    const-string v18, "has_uses_permission_c2d_message"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v11}, Lcom/localytics/android/ManifestUtil;->isRequestedPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 387
    const-string v18, "has_uses_permission_receive"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/PushManager$2;->val$appContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const-string v20, "com.google.android.c2dm.permission.RECEIVE"

    invoke-static/range {v19 .. v20}, Lcom/localytics/android/ManifestUtil;->isRequestedPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 388
    const-string v18, "manifest_info"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 390
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 391
    .local v3, "dependencyInfo":Lorg/json/JSONObject;
    const-string v18, "has_gcm"

    const-string v19, "com.google.android.gms.gcm.GcmListenerService"

    invoke-static/range {v19 .. v19}, Lcom/localytics/android/Utils;->classExists(Ljava/lang/String;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 392
    const-string v18, "dependency_info"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 394
    const-string v18, "integration_info"

    move-object/from16 v0, v18

    invoke-virtual {v12, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 396
    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    .line 403
    .end local v3    # "dependencyInfo":Lorg/json/JSONObject;
    .end local v4    # "deviceInfo":Lorg/json/JSONObject;
    .end local v7    # "integrationInfo":Lorg/json/JSONObject;
    .end local v10    # "manifestInfo":Lorg/json/JSONObject;
    .end local v11    # "permissionC2dMessage":Ljava/lang/String;
    .end local v12    # "postBody":Lorg/json/JSONObject;
    .end local v14    # "query":Ljava/lang/String;
    .end local v16    # "registrationId":Ljava/lang/String;
    .end local v17    # "requestInfo":Lorg/json/JSONObject;
    :goto_2
    return-object v18

    .line 370
    .restart local v4    # "deviceInfo":Lorg/json/JSONObject;
    .restart local v12    # "postBody":Lorg/json/JSONObject;
    .restart local v14    # "query":Ljava/lang/String;
    .restart local v16    # "registrationId":Ljava/lang/String;
    .restart local v17    # "requestInfo":Lorg/json/JSONObject;
    :cond_2
    const/16 v18, 0x0

    goto/16 :goto_1

    .line 398
    .end local v4    # "deviceInfo":Lorg/json/JSONObject;
    .end local v12    # "postBody":Lorg/json/JSONObject;
    .end local v14    # "query":Ljava/lang/String;
    .end local v16    # "registrationId":Ljava/lang/String;
    .end local v17    # "requestInfo":Lorg/json/JSONObject;
    :catch_0
    move-exception v5

    .line 400
    .local v5, "e":Ljava/lang/Exception;
    const-string v18, "Failed to create device info POST body"

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 403
    const/16 v18, 0x0

    goto :goto_2
.end method
