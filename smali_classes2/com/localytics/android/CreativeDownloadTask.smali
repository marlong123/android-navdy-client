.class Lcom/localytics/android/CreativeDownloadTask;
.super Ljava/lang/Object;
.source "CreativeDownloadTask.java"

# interfaces
.implements Lcom/localytics/android/ICreativeDownloadTask;


# instance fields
.field private mCallback:Lcom/localytics/android/ICreativeDownloadTaskCallback;

.field private mCampaign:Lcom/localytics/android/MarketingMessage;

.field private mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

.field private mPriority:Lcom/localytics/android/ICreativeDownloadTask$Priority;

.field private mRemoteFileLocation:Ljava/lang/String;

.field private mSequence:I


# direct methods
.method public constructor <init>(Lcom/localytics/android/MarketingMessage;Lcom/localytics/android/ICreativeDownloadTask$Priority;ILcom/localytics/android/LocalyticsDao;Lcom/localytics/android/ICreativeDownloadTaskCallback;)V
    .locals 1
    .param p1, "campaign"    # Lcom/localytics/android/MarketingMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "priority"    # Lcom/localytics/android/ICreativeDownloadTask$Priority;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "sequence"    # I
    .param p4, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "callback"    # Lcom/localytics/android/ICreativeDownloadTaskCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/localytics/android/CreativeDownloadTask;->mCampaign:Lcom/localytics/android/MarketingMessage;

    .line 32
    const-string v0, "download_url"

    invoke-static {p1, v0}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/localytics/android/CreativeDownloadTask;->mRemoteFileLocation:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/localytics/android/CreativeDownloadTask;->mPriority:Lcom/localytics/android/ICreativeDownloadTask$Priority;

    .line 34
    iput p3, p0, Lcom/localytics/android/CreativeDownloadTask;->mSequence:I

    .line 35
    iput-object p4, p0, Lcom/localytics/android/CreativeDownloadTask;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    .line 36
    iput-object p5, p0, Lcom/localytics/android/CreativeDownloadTask;->mCallback:Lcom/localytics/android/ICreativeDownloadTaskCallback;

    .line 37
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/localytics/android/ICreativeDownloadTask;)I
    .locals 4
    .param p1, "another"    # Lcom/localytics/android/ICreativeDownloadTask;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 146
    iget-object v0, p0, Lcom/localytics/android/CreativeDownloadTask;->mPriority:Lcom/localytics/android/ICreativeDownloadTask$Priority;

    .line 147
    .local v0, "left":Lcom/localytics/android/ICreativeDownloadTask$Priority;
    invoke-interface {p1}, Lcom/localytics/android/ICreativeDownloadTask;->getPriority()Lcom/localytics/android/ICreativeDownloadTask$Priority;

    move-result-object v1

    .line 151
    .local v1, "right":Lcom/localytics/android/ICreativeDownloadTask$Priority;
    if-ne v0, v1, :cond_0

    iget v2, p0, Lcom/localytics/android/CreativeDownloadTask;->mSequence:I

    invoke-interface {p1}, Lcom/localytics/android/ICreativeDownloadTask;->getSequence()I

    move-result v3

    sub-int/2addr v2, v3

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Lcom/localytics/android/ICreativeDownloadTask$Priority;->ordinal()I

    move-result v2

    invoke-virtual {v0}, Lcom/localytics/android/ICreativeDownloadTask$Priority;->ordinal()I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/localytics/android/ICreativeDownloadTask;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/localytics/android/CreativeDownloadTask;->compareTo(Lcom/localytics/android/ICreativeDownloadTask;)I

    move-result v0

    return v0
.end method

.method downloadFile(Ljava/lang/String;Ljava/lang/String;ZLjava/net/Proxy;)Z
    .locals 20
    .param p1, "remoteFilePath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "localFilePath"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "isOverwrite"    # Z
    .param p4, "proxy"    # Ljava/net/Proxy;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 84
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 85
    .local v8, "file":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_1

    if-nez p3, :cond_1

    .line 87
    const-string v14, "The file %s does exist and overwrite is turned off."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 88
    const/4 v14, 0x1

    .line 140
    :cond_0
    :goto_0
    return v14

    .line 91
    :cond_1
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 92
    .local v6, "dir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_2

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-nez v14, :cond_2

    .line 94
    const-string v14, "Could not create the directory %s for saving file."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    .line 95
    const/4 v14, 0x0

    goto :goto_0

    .line 98
    :cond_2
    new-instance v12, Ljava/io/File;

    const-string v14, "%s_%s_temp"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p2, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/localytics/android/CreativeDownloadTask;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Lcom/localytics/android/LocalyticsDao;->getCurrentTimeMillis()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v12, "tempFile":Ljava/io/File;
    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 103
    new-instance v13, Ljava/net/URL;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 104
    .local v13, "url":Ljava/net/URL;
    move-object/from16 v0, p4

    invoke-static {v13, v0}, Lcom/localytics/android/BaseUploadThread;->createURLConnection(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v5

    .line 106
    .local v5, "connection":Ljava/net/URLConnection;
    const/16 v2, 0x2000

    .line 107
    .local v2, "BUF_SIZE":I
    invoke-virtual {v5}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    .line 108
    .local v10, "is":Ljava/io/InputStream;
    new-instance v3, Ljava/io/BufferedInputStream;

    const/16 v14, 0x4000

    invoke-direct {v3, v10, v14}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 109
    .local v3, "bis":Ljava/io/BufferedInputStream;
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-virtual {v12}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 112
    .local v9, "fos":Ljava/io/FileOutputStream;
    const/16 v14, 0x2000

    new-array v4, v14, [B

    .line 113
    .local v4, "buffer":[B
    :goto_1
    invoke-virtual {v3, v4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v11

    .local v11, "read":I
    const/4 v14, -0x1

    if-eq v11, v14, :cond_4

    .line 115
    const/4 v14, 0x0

    invoke-virtual {v9, v4, v14, v11}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 128
    .end local v2    # "BUF_SIZE":I
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "buffer":[B
    .end local v5    # "connection":Ljava/net/URLConnection;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v11    # "read":I
    .end local v13    # "url":Ljava/net/URL;
    :catch_0
    move-exception v7

    .line 130
    .local v7, "e":Ljava/io/IOException;
    :try_start_1
    const-string v14, "Failed to download campaign creative"

    invoke-static {v14, v7}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v14

    if-nez v14, :cond_3

    .line 136
    const-string v14, "Failed to delete temporary file for campaign"

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    .line 140
    .end local v7    # "e":Ljava/io/IOException;
    :cond_3
    :goto_2
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 117
    .restart local v2    # "BUF_SIZE":I
    .restart local v3    # "bis":Ljava/io/BufferedInputStream;
    .restart local v4    # "buffer":[B
    .restart local v5    # "connection":Ljava/net/URLConnection;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "is":Ljava/io/InputStream;
    .restart local v11    # "read":I
    .restart local v13    # "url":Ljava/net/URL;
    :cond_4
    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    .line 119
    invoke-virtual {v12, v8}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 121
    const-string v14, "Failed to create permanent file for campaign"

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 122
    const/4 v14, 0x0

    .line 134
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v15

    if-nez v15, :cond_0

    .line 136
    const-string v15, "Failed to delete temporary file for campaign"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 125
    :cond_5
    const/4 v14, 0x1

    .line 134
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v15

    if-nez v15, :cond_0

    .line 136
    const-string v15, "Failed to delete temporary file for campaign"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 134
    .end local v2    # "BUF_SIZE":I
    .end local v3    # "bis":Ljava/io/BufferedInputStream;
    .end local v4    # "buffer":[B
    .end local v5    # "connection":Ljava/net/URLConnection;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .end local v10    # "is":Ljava/io/InputStream;
    .end local v11    # "read":I
    .end local v13    # "url":Ljava/net/URL;
    :cond_6
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v14

    if-nez v14, :cond_3

    .line 136
    const-string v14, "Failed to delete temporary file for campaign"

    invoke-static {v14}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    goto :goto_2

    .line 134
    :catchall_0
    move-exception v14

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v15

    if-nez v15, :cond_7

    .line 136
    const-string v15, "Failed to delete temporary file for campaign"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->e(Ljava/lang/String;)I

    :cond_7
    throw v14
.end method

.method public getCampaign()Lcom/localytics/android/MarketingMessage;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/localytics/android/CreativeDownloadTask;->mCampaign:Lcom/localytics/android/MarketingMessage;

    return-object v0
.end method

.method public getPriority()Lcom/localytics/android/ICreativeDownloadTask$Priority;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/localytics/android/CreativeDownloadTask;->mPriority:Lcom/localytics/android/ICreativeDownloadTask$Priority;

    return-object v0
.end method

.method public getSequence()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/localytics/android/CreativeDownloadTask;->mSequence:I

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 64
    const/4 v1, 0x0

    .line 65
    .local v1, "success":Z
    iget-object v2, p0, Lcom/localytics/android/CreativeDownloadTask;->mCampaign:Lcom/localytics/android/MarketingMessage;

    const-string v3, "local_file_location"

    invoke-static {v2, v3}, Lcom/localytics/android/JsonHelper;->getSafeStringFromMap(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "localFileLocation":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 68
    iget-object v2, p0, Lcom/localytics/android/CreativeDownloadTask;->mRemoteFileLocation:Ljava/lang/String;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/localytics/android/CreativeDownloadTask;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v4}, Lcom/localytics/android/LocalyticsDao;->getProxy()Ljava/net/Proxy;

    move-result-object v4

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/localytics/android/CreativeDownloadTask;->downloadFile(Ljava/lang/String;Ljava/lang/String;ZLjava/net/Proxy;)Z

    move-result v1

    .line 71
    :cond_0
    if-eqz v1, :cond_1

    .line 73
    iget-object v2, p0, Lcom/localytics/android/CreativeDownloadTask;->mCallback:Lcom/localytics/android/ICreativeDownloadTaskCallback;

    invoke-interface {v2, p0}, Lcom/localytics/android/ICreativeDownloadTaskCallback;->onComplete(Lcom/localytics/android/ICreativeDownloadTask;)V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/localytics/android/CreativeDownloadTask;->mCallback:Lcom/localytics/android/ICreativeDownloadTaskCallback;

    invoke-interface {v2, p0}, Lcom/localytics/android/ICreativeDownloadTaskCallback;->onError(Lcom/localytics/android/ICreativeDownloadTask;)V

    goto :goto_0
.end method

.method public setPriority(Lcom/localytics/android/ICreativeDownloadTask$Priority;)V
    .locals 0
    .param p1, "priority"    # Lcom/localytics/android/ICreativeDownloadTask$Priority;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 47
    iput-object p1, p0, Lcom/localytics/android/CreativeDownloadTask;->mPriority:Lcom/localytics/android/ICreativeDownloadTask$Priority;

    .line 48
    return-void
.end method
