.class Lcom/localytics/android/BasePushManager;
.super Lcom/localytics/android/BaseMarketingManager;
.source "BasePushManager.java"


# static fields
.field protected static final ACTION_ATTRIBUTE:Ljava/lang/String; = "Action"

.field protected static final APP_CONTEXT_ATTRIBUTE:Ljava/lang/String; = "App Context"

.field protected static final CAMPAIGN_ID_ATTRIBUTE:Ljava/lang/String; = "Campaign ID"

.field protected static final CREATIVE_DISPLAYED_ATTRIBUTE:Ljava/lang/String; = "Creative Displayed"

.field protected static final CREATIVE_ID_ATTRIBUTE:Ljava/lang/String; = "Creative ID"

.field protected static final CREATIVE_TYPE_ATTRIBUTE:Ljava/lang/String; = "Creative Type"

.field protected static final PUSH_NOTIFICATIONS_ENABLED_ATTRIBUTE:Ljava/lang/String; = "Push Notifications Enabled"


# instance fields
.field protected final mMarketingHandler:Lcom/localytics/android/MarketingHandler;


# direct methods
.method public constructor <init>(Lcom/localytics/android/LocalyticsDao;Lcom/localytics/android/MarketingHandler;)V
    .locals 0
    .param p1, "localyticsDao"    # Lcom/localytics/android/LocalyticsDao;
    .param p2, "marketingHandler"    # Lcom/localytics/android/MarketingHandler;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/localytics/android/BaseMarketingManager;-><init>(Lcom/localytics/android/LocalyticsDao;)V

    .line 35
    iput-object p2, p0, Lcom/localytics/android/BasePushManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    .line 36
    return-void
.end method

.method private _getSoundUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "soundFilename"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 131
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 141
    :goto_0
    return-object v0

    .line 137
    :cond_0
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    const/4 v0, 0x0

    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 141
    :cond_1
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "android.resource"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v1}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "raw"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private logNotification(Landroid/app/Notification;Landroid/app/PendingIntent;)V
    .locals 4
    .param p1, "notification"    # Landroid/app/Notification;
    .param p2, "contentIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 228
    const-string v1, "The notification returned by the user contains %s content intent"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    invoke-virtual {p2, v0}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "the same"

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 230
    invoke-virtual {p1}, Landroid/app/Notification;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/localytics/android/Localytics$Log;->v(Ljava/lang/String;)I

    .line 231
    return-void

    .line 228
    :cond_0
    const-string v0, "a different"

    goto :goto_0
.end method


# virtual methods
.method _creativeTypeForMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "creativeType"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    const-string p1, "Alert"

    .line 52
    .end local p1    # "creativeType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 48
    .restart local p1    # "creativeType":Ljava/lang/String;
    :cond_1
    const-string p1, "Silent"

    goto :goto_0
.end method

.method _handlePushIntegrationReceivedVerification(Ljava/lang/String;)V
    .locals 0
    .param p1, "pip"    # Ljava/lang/String;

    .prologue
    .line 126
    return-void
.end method

.method _hasMessage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method _showPushNotification(Ljava/lang/String;Ljava/lang/String;JLcom/localytics/android/Campaign;Landroid/os/Bundle;)V
    .locals 19
    .param p1, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "soundFilename"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "campaignId"    # J
    .param p5, "campaign"    # Lcom/localytics/android/Campaign;
    .param p6, "extras"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 155
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v15}, Lcom/localytics/android/LocalyticsDao;->getAppContext()Landroid/content/Context;

    move-result-object v2

    .line 156
    .local v2, "appContext":Landroid/content/Context;
    const-string v4, ""

    .line 157
    .local v4, "appName":Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/localytics/android/DatapointHelper;->getLocalyticsNotificationIcon(Landroid/content/Context;)I

    move-result v3

    .line 160
    .local v3, "appIcon":I
    :try_start_0
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    .line 161
    .local v5, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    invoke-virtual {v15, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 169
    .end local v5    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    new-instance v14, Landroid/content/Intent;

    const-class v15, Lcom/localytics/android/PushTrackingActivity;

    invoke-direct {v14, v2, v15}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 170
    .local v14, "trackingIntent":Landroid/content/Intent;
    move-object/from16 v0, p6

    invoke-virtual {v14, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 171
    move-wide/from16 v0, p3

    long-to-int v15, v0

    const/high16 v16, 0x8000000

    move/from16 v0, v16

    invoke-static {v2, v15, v14, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 173
    .local v7, "contentIntent":Landroid/app/PendingIntent;
    new-instance v15, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v15, v2}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v15, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    invoke-virtual {v15, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    invoke-virtual {v15, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 179
    .local v6, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/localytics/android/BasePushManager;->_getSoundUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 180
    .local v13, "soundUri":Landroid/net/Uri;
    if-eqz v13, :cond_3

    .line 182
    invoke-virtual {v6, v13}, Landroid/support/v4/app/NotificationCompat$Builder;->setSound(Landroid/net/Uri;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 183
    const/4 v15, 0x6

    invoke-virtual {v6, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 189
    :goto_1
    const-string v15, "ll_public_message"

    move-object/from16 v0, p6

    invoke-virtual {v0, v15}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 190
    .local v12, "publicMessage":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 192
    invoke-virtual {v6, v12}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    new-instance v16, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct/range {v16 .. v16}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 197
    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v15

    new-instance v16, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct/range {v16 .. v16}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 201
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/localytics/android/BasePushManager;->mMarketingHandler:Lcom/localytics/android/MarketingHandler;

    iget-object v15, v15, Lcom/localytics/android/MarketingHandler;->mListeners:Lcom/localytics/android/ListenersSet;

    invoke-virtual {v15}, Lcom/localytics/android/ListenersSet;->getDevListener()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/localytics/android/MessagingListener;

    .line 202
    .local v8, "devListener":Lcom/localytics/android/MessagingListener;
    if-eqz v8, :cond_1

    .line 204
    move-object/from16 v0, p5

    instance-of v15, v0, Lcom/localytics/android/PlacesCampaign;

    if-eqz v15, :cond_4

    .line 206
    check-cast p5, Lcom/localytics/android/PlacesCampaign;

    .end local p5    # "campaign":Lcom/localytics/android/Campaign;
    move-object/from16 v0, p5

    invoke-interface {v8, v6, v0}, Lcom/localytics/android/MessagingListener;->localyticsWillShowPlacesPushNotification(Landroid/support/v4/app/NotificationCompat$Builder;Lcom/localytics/android/PlacesCampaign;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 215
    :cond_1
    :goto_2
    const-string v15, "notification"

    invoke-virtual {v2, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    .line 216
    .local v11, "notificationManager":Landroid/app/NotificationManager;
    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v10

    .line 219
    .local v10, "notification":Landroid/app/Notification;
    if-eqz v8, :cond_2

    .line 221
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v7}, Lcom/localytics/android/BasePushManager;->logNotification(Landroid/app/Notification;Landroid/app/PendingIntent;)V

    .line 223
    :cond_2
    move-wide/from16 v0, p3

    long-to-int v15, v0

    invoke-virtual {v11, v15, v10}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 224
    return-void

    .line 163
    .end local v6    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .end local v7    # "contentIntent":Landroid/app/PendingIntent;
    .end local v8    # "devListener":Lcom/localytics/android/MessagingListener;
    .end local v10    # "notification":Landroid/app/Notification;
    .end local v11    # "notificationManager":Landroid/app/NotificationManager;
    .end local v12    # "publicMessage":Ljava/lang/String;
    .end local v13    # "soundUri":Landroid/net/Uri;
    .end local v14    # "trackingIntent":Landroid/content/Intent;
    .restart local p5    # "campaign":Lcom/localytics/android/Campaign;
    :catch_0
    move-exception v9

    .line 165
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v15, "Failed to get application name"

    invoke-static {v15}, Lcom/localytics/android/Localytics$Log;->w(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 187
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6    # "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    .restart local v7    # "contentIntent":Landroid/app/PendingIntent;
    .restart local v13    # "soundUri":Landroid/net/Uri;
    .restart local v14    # "trackingIntent":Landroid/content/Intent;
    :cond_3
    const/4 v15, -0x1

    invoke-virtual {v6, v15}, Landroid/support/v4/app/NotificationCompat$Builder;->setDefaults(I)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_1

    .line 208
    .restart local v8    # "devListener":Lcom/localytics/android/MessagingListener;
    .restart local v12    # "publicMessage":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p5

    instance-of v15, v0, Lcom/localytics/android/PushCampaign;

    if-eqz v15, :cond_1

    .line 210
    check-cast p5, Lcom/localytics/android/PushCampaign;

    .end local p5    # "campaign":Lcom/localytics/android/Campaign;
    move-object/from16 v0, p5

    invoke-interface {v8, v6, v0}, Lcom/localytics/android/MessagingListener;->localyticsWillShowPushNotification(Landroid/support/v4/app/NotificationCompat$Builder;Lcom/localytics/android/PushCampaign;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    goto :goto_2
.end method

.method _tagPushReceived(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/util/Map;)Z
    .locals 9
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "campaignId"    # J
    .param p5, "creativeId"    # Ljava/lang/String;
    .param p6, "serverSchemaVersion"    # Ljava/lang/String;
    .param p7, "creativeType"    # Ljava/lang/String;
    .param p8, "killSwitch"    # I
    .param p9, "testMode"    # I
    .param p10, "pip"    # Ljava/lang/String;
    .param p11    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 66
    .local p11, "extraAttributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->areNotificationsDisabled()Z

    move-result v5

    .line 67
    .local v5, "notificationsDisabled":Z
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    const/4 v4, 0x1

    .line 69
    .local v4, "hasMessage":Z
    :goto_0
    move-object/from16 v0, p7

    invoke-virtual {p0, v0, p2}, Lcom/localytics/android/BasePushManager;->_creativeTypeForMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p7

    .line 70
    const-string v1, "Not Available"

    .line 71
    .local v1, "appContext":Ljava/lang/String;
    iget-object v6, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->isAutoIntegrate()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 73
    iget-object v6, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->isAppInForeground()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 75
    const-string v1, "Foreground"

    .line 84
    :cond_0
    :goto_1
    if-eqz v4, :cond_6

    .line 86
    if-eqz v5, :cond_5

    const-string v3, "No"

    .line 93
    .local v3, "creativeDisplayed":Ljava/lang/String;
    :goto_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 94
    .local v2, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "Campaign ID"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v6, "Creative ID"

    invoke-virtual {v2, v6, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v6, "Creative Type"

    move-object/from16 v0, p7

    invoke-virtual {v2, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const-string v6, "Creative Displayed"

    invoke-virtual {v2, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    const-string v7, "Push Notifications Enabled"

    if-eqz v5, :cond_7

    const-string v6, "No"

    :goto_3
    invoke-virtual {v2, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    const-string v6, "App Context"

    invoke-virtual {v2, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    const-string v6, "Schema Version - Client"

    const/4 v7, 0x5

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    const-string v6, "Schema Version - Server"

    invoke-virtual {v2, v6, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    if-eqz p11, :cond_1

    .line 104
    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 107
    :cond_1
    if-nez p8, :cond_8

    if-nez p9, :cond_8

    .line 109
    iget-object v6, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6, p1, v2}, Lcom/localytics/android/LocalyticsDao;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 110
    iget-object v6, p0, Lcom/localytics/android/BasePushManager;->mLocalyticsDao:Lcom/localytics/android/LocalyticsDao;

    invoke-interface {v6}, Lcom/localytics/android/LocalyticsDao;->upload()V

    .line 112
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 114
    move-object/from16 v0, p10

    invoke-virtual {p0, v0}, Lcom/localytics/android/BasePushManager;->_handlePushIntegrationReceivedVerification(Ljava/lang/String;)V

    .line 117
    :cond_2
    const/4 v6, 0x1

    .line 120
    :goto_4
    return v6

    .line 67
    .end local v1    # "appContext":Ljava/lang/String;
    .end local v2    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "creativeDisplayed":Ljava/lang/String;
    .end local v4    # "hasMessage":Z
    :cond_3
    const/4 v4, 0x0

    goto :goto_0

    .line 79
    .restart local v1    # "appContext":Ljava/lang/String;
    .restart local v4    # "hasMessage":Z
    :cond_4
    const-string v1, "Background"

    goto :goto_1

    .line 86
    :cond_5
    const-string v3, "Yes"

    goto :goto_2

    .line 90
    :cond_6
    const-string v3, "Not Applicable"

    .restart local v3    # "creativeDisplayed":Ljava/lang/String;
    goto :goto_2

    .line 98
    .restart local v2    # "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_7
    const-string v6, "Yes"

    goto :goto_3

    .line 120
    :cond_8
    const/4 v6, 0x0

    goto :goto_4
.end method
