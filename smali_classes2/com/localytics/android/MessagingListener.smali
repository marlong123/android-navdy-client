.class public interface abstract Lcom/localytics/android/MessagingListener;
.super Ljava/lang/Object;
.source "MessagingListener.java"


# virtual methods
.method public abstract localyticsDidDismissInAppMessage()V
.end method

.method public abstract localyticsDidDisplayInAppMessage()V
.end method

.method public abstract localyticsShouldShowPlacesPushNotification(Lcom/localytics/android/PlacesCampaign;)Z
    .param p1    # Lcom/localytics/android/PlacesCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract localyticsShouldShowPushNotification(Lcom/localytics/android/PushCampaign;)Z
    .param p1    # Lcom/localytics/android/PushCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract localyticsWillDismissInAppMessage()V
.end method

.method public abstract localyticsWillDisplayInAppMessage()V
.end method

.method public abstract localyticsWillShowPlacesPushNotification(Landroid/support/v4/app/NotificationCompat$Builder;Lcom/localytics/android/PlacesCampaign;)Landroid/support/v4/app/NotificationCompat$Builder;
    .param p1    # Landroid/support/v4/app/NotificationCompat$Builder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/localytics/android/PlacesCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract localyticsWillShowPushNotification(Landroid/support/v4/app/NotificationCompat$Builder;Lcom/localytics/android/PushCampaign;)Landroid/support/v4/app/NotificationCompat$Builder;
    .param p1    # Landroid/support/v4/app/NotificationCompat$Builder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/localytics/android/PushCampaign;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
