.class public final Lcom/google/maps/android/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/maps/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final cameraBearing:I = 0x7f010179

.field public static final cameraTargetLat:I = 0x7f01017a

.field public static final cameraTargetLng:I = 0x7f01017b

.field public static final cameraTilt:I = 0x7f01017c

.field public static final cameraZoom:I = 0x7f01017d

.field public static final circleCrop:I = 0x7f010177

.field public static final imageAspectRatio:I = 0x7f010176

.field public static final imageAspectRatioAdjust:I = 0x7f010175

.field public static final liteMode:I = 0x7f01017e

.field public static final mapType:I = 0x7f010178

.field public static final uiCompass:I = 0x7f01017f

.field public static final uiMapToolbar:I = 0x7f010187

.field public static final uiRotateGestures:I = 0x7f010180

.field public static final uiScrollGestures:I = 0x7f010181

.field public static final uiTiltGestures:I = 0x7f010182

.field public static final uiZoomControls:I = 0x7f010183

.field public static final uiZoomGestures:I = 0x7f010184

.field public static final useViewLifecycle:I = 0x7f010185

.field public static final zOrderOnTop:I = 0x7f010186


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
