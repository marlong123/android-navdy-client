.class public Lcom/google/maps/internal/PolylineEncoding;
.super Ljava/lang/Object;
.source "PolylineEncoding.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decode(Ljava/lang/String;)Ljava/util/List;
    .locals 18
    .param p0, "encodedPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/model/LatLng;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 38
    .local v6, "len":I
    new-instance v8, Ljava/util/ArrayList;

    div-int/lit8 v11, v6, 0x2

    invoke-direct {v8, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 39
    .local v8, "path":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/model/LatLng;>;"
    const/4 v3, 0x0

    .line 40
    .local v3, "index":I
    const/4 v5, 0x0

    .line 41
    .local v5, "lat":I
    const/4 v7, 0x0

    .line 43
    .local v7, "lng":I
    :goto_0
    if-ge v3, v6, :cond_2

    .line 44
    const/4 v9, 0x1

    .line 45
    .local v9, "result":I
    const/4 v10, 0x0

    .line 48
    .local v10, "shift":I
    :goto_1
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .local v4, "index":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v11

    add-int/lit8 v11, v11, -0x3f

    add-int/lit8 v2, v11, -0x1

    .line 49
    .local v2, "b":I
    shl-int v11, v2, v10

    add-int/2addr v9, v11

    .line 50
    add-int/lit8 v10, v10, 0x5

    .line 51
    const/16 v11, 0x1f

    if-ge v2, v11, :cond_4

    .line 52
    and-int/lit8 v11, v9, 0x1

    if-eqz v11, :cond_0

    shr-int/lit8 v11, v9, 0x1

    xor-int/lit8 v11, v11, -0x1

    :goto_2
    add-int/2addr v5, v11

    .line 54
    const/4 v9, 0x1

    .line 55
    const/4 v10, 0x0

    move v3, v4

    .line 57
    .end local v4    # "index":I
    .restart local v3    # "index":I
    :goto_3
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .restart local v4    # "index":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v11

    add-int/lit8 v11, v11, -0x3f

    add-int/lit8 v2, v11, -0x1

    .line 58
    shl-int v11, v2, v10

    add-int/2addr v9, v11

    .line 59
    add-int/lit8 v10, v10, 0x5

    .line 60
    const/16 v11, 0x1f

    if-ge v2, v11, :cond_3

    .line 61
    and-int/lit8 v11, v9, 0x1

    if-eqz v11, :cond_1

    shr-int/lit8 v11, v9, 0x1

    xor-int/lit8 v11, v11, -0x1

    :goto_4
    add-int/2addr v7, v11

    .line 63
    new-instance v11, Lcom/google/maps/model/LatLng;

    int-to-double v12, v5

    const-wide v14, 0x3ee4f8b588e368f1L    # 1.0E-5

    mul-double/2addr v12, v14

    int-to-double v14, v7

    const-wide v16, 0x3ee4f8b588e368f1L    # 1.0E-5

    mul-double v14, v14, v16

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/maps/model/LatLng;-><init>(DD)V

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v4

    .line 64
    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_0

    .line 52
    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_0
    shr-int/lit8 v11, v9, 0x1

    goto :goto_2

    .line 61
    :cond_1
    shr-int/lit8 v11, v9, 0x1

    goto :goto_4

    .line 66
    .end local v2    # "b":I
    .end local v4    # "index":I
    .end local v9    # "result":I
    .end local v10    # "shift":I
    .restart local v3    # "index":I
    :cond_2
    return-object v8

    .end local v3    # "index":I
    .restart local v2    # "b":I
    .restart local v4    # "index":I
    .restart local v9    # "result":I
    .restart local v10    # "shift":I
    :cond_3
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :cond_4
    move v3, v4

    .end local v4    # "index":I
    .restart local v3    # "index":I
    goto :goto_1
.end method

.method public static encode(Ljava/util/List;)Ljava/lang/String;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/model/LatLng;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "path":Ljava/util/List;, "Ljava/util/List<Lcom/google/maps/model/LatLng;>;"
    const-wide/16 v8, 0x0

    .line 74
    .local v8, "lastLat":J
    const-wide/16 v10, 0x0

    .line 76
    .local v10, "lastLng":J
    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    .line 78
    .local v16, "result":Ljava/lang/StringBuffer;
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/maps/model/LatLng;

    .line 79
    .local v7, "point":Lcom/google/maps/model/LatLng;
    iget-wide v0, v7, Lcom/google/maps/model/LatLng;->lat:D

    move-wide/from16 v18, v0

    const-wide v20, 0x40f86a0000000000L    # 100000.0

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    .line 80
    .local v12, "lat":J
    iget-wide v0, v7, Lcom/google/maps/model/LatLng;->lng:D

    move-wide/from16 v18, v0

    const-wide v20, 0x40f86a0000000000L    # 100000.0

    mul-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->round(D)J

    move-result-wide v14

    .line 82
    .local v14, "lng":J
    sub-long v2, v12, v8

    .line 83
    .local v2, "dLat":J
    sub-long v4, v14, v10

    .line 85
    .local v4, "dLng":J
    move-object/from16 v0, v16

    invoke-static {v2, v3, v0}, Lcom/google/maps/internal/PolylineEncoding;->encode(JLjava/lang/StringBuffer;)V

    .line 86
    move-object/from16 v0, v16

    invoke-static {v4, v5, v0}, Lcom/google/maps/internal/PolylineEncoding;->encode(JLjava/lang/StringBuffer;)V

    .line 88
    move-wide v8, v12

    .line 89
    move-wide v10, v14

    .line 90
    goto :goto_0

    .line 91
    .end local v2    # "dLat":J
    .end local v4    # "dLng":J
    .end local v7    # "point":Lcom/google/maps/model/LatLng;
    .end local v12    # "lat":J
    .end local v14    # "lng":J
    :cond_0
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    return-object v17
.end method

.method public static encode([Lcom/google/maps/model/LatLng;)Ljava/lang/String;
    .locals 1
    .param p0, "path"    # [Lcom/google/maps/model/LatLng;

    .prologue
    .line 107
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/maps/internal/PolylineEncoding;->encode(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static encode(JLjava/lang/StringBuffer;)V
    .locals 8
    .param p0, "v"    # J
    .param p2, "result"    # Ljava/lang/StringBuffer;

    .prologue
    const-wide/16 v6, 0x3f

    const-wide/16 v4, 0x20

    const/4 v2, 0x1

    .line 95
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    shl-long v0, p0, v2

    const-wide/16 v2, -0x1

    xor-long p0, v0, v2

    .line 96
    :goto_0
    cmp-long v0, p0, v4

    if-ltz v0, :cond_1

    .line 97
    const-wide/16 v0, 0x1f

    and-long/2addr v0, p0

    or-long/2addr v0, v4

    add-long/2addr v0, v6

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 98
    const/4 v0, 0x5

    shr-long/2addr p0, v0

    goto :goto_0

    .line 95
    :cond_0
    shl-long/2addr p0, v2

    goto :goto_0

    .line 100
    :cond_1
    add-long v0, p0, v6

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append([C)Ljava/lang/StringBuffer;

    .line 101
    return-void
.end method
