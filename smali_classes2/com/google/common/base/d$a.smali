.class public final Lcom/google/common/base/d$a;
.super Ljava/lang/Object;
.source "MoreObjects.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/common/base/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/base/d$a$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/common/base/d$a$a;

.field private c:Lcom/google/common/base/d$a$a;

.field private d:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Lcom/google/common/base/d$a$a;

    invoke-direct {v0, v1}, Lcom/google/common/base/d$a$a;-><init>(B)V

    iput-object v0, p0, Lcom/google/common/base/d$a;->b:Lcom/google/common/base/d$a$a;

    .line 140
    iget-object v0, p0, Lcom/google/common/base/d$a;->b:Lcom/google/common/base/d$a$a;

    iput-object v0, p0, Lcom/google/common/base/d$a;->c:Lcom/google/common/base/d$a$a;

    .line 141
    iput-boolean v1, p0, Lcom/google/common/base/d$a;->d:Z

    .line 147
    invoke-static {p1}, Lcom/google/common/base/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/common/base/d$a;->a:Ljava/lang/String;

    .line 148
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0, p1}, Lcom/google/common/base/d$a;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private final b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;
    .locals 2

    .prologue
    .line 372
    .line 1360
    new-instance v1, Lcom/google/common/base/d$a$a;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/common/base/d$a$a;-><init>(B)V

    .line 1361
    iget-object v0, p0, Lcom/google/common/base/d$a;->c:Lcom/google/common/base/d$a$a;

    iput-object v1, v0, Lcom/google/common/base/d$a$a;->c:Lcom/google/common/base/d$a$a;

    iput-object v1, p0, Lcom/google/common/base/d$a;->c:Lcom/google/common/base/d$a$a;

    .line 373
    iput-object p2, v1, Lcom/google/common/base/d$a$a;->b:Ljava/lang/Object;

    .line 374
    invoke-static {p1}, Lcom/google/common/base/g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/common/base/d$a$a;->a:Ljava/lang/String;

    .line 375
    return-object p0
.end method


# virtual methods
.method public final a(Ljava/lang/String;F)Lcom/google/common/base/d$a;
    .locals 1

    .prologue
    .line 210
    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/common/base/d$a;->b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;I)Lcom/google/common/base/d$a;
    .locals 1

    .prologue
    .line 220
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/common/base/d$a;->b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)Lcom/google/common/base/d$a;
    .locals 2

    .prologue
    .line 230
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/common/base/d$a;->b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0, p1, p2}, Lcom/google/common/base/d$a;->b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/common/base/d$a;
    .locals 1

    .prologue
    .line 180
    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/common/base/d$a;->b(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/common/base/d$a;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 334
    const-string v1, ""

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v2, p0, Lcom/google/common/base/d$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 336
    iget-object v0, p0, Lcom/google/common/base/d$a;->b:Lcom/google/common/base/d$a$a;

    iget-object v0, v0, Lcom/google/common/base/d$a$a;->c:Lcom/google/common/base/d$a$a;

    .line 337
    :goto_0
    if-eqz v0, :cond_2

    .line 339
    iget-object v3, v0, Lcom/google/common/base/d$a$a;->b:Ljava/lang/Object;

    .line 341
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string v1, ", "

    .line 344
    iget-object v4, v0, Lcom/google/common/base/d$a$a;->a:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 345
    iget-object v4, v0, Lcom/google/common/base/d$a$a;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 347
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->isArray()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 348
    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    .line 349
    invoke-static {v4}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 350
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 338
    :goto_1
    iget-object v0, v0, Lcom/google/common/base/d$a$a;->c:Lcom/google/common/base/d$a$a;

    goto :goto_0

    .line 352
    :cond_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 356
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
