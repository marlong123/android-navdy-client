.class public Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;
.super Ljava/lang/Object;
.source "EdgeConnectedTriangleTraversal.java"


# instance fields
.field private triQueue:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    .line 48
    return-void
.end method

.method private process(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;)V
    .locals 3
    .param p1, "currTri"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    .param p2, "visitor"    # Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;

    .prologue
    .line 84
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getNeighbours()[Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .line 85
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 86
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;->getEdge(I)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .line 87
    .local v1, "neighTri":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_1
    invoke-interface {p2, p1, v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;->visit(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;ILcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_1

    .line 92
    .end local v1    # "neighTri":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    :cond_2
    return-void
.end method


# virtual methods
.method public init(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;)V
    .locals 1
    .param p1, "tri"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method public init(Ljava/util/Collection;)V
    .locals 1
    .param p1, "tris"    # Ljava/util/Collection;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 61
    return-void
.end method

.method public visitAll(Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;)V
    .locals 2
    .param p1, "visitor"    # Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;

    .prologue
    .line 77
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->triQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    .line 79
    .local v0, "tri":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/EdgeConnectedTriangleTraversal;->process(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;Lcom/vividsolutions/jts/triangulate/quadedge/TraversalVisitor;)V

    goto :goto_0

    .line 81
    .end local v0    # "tri":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
    :cond_0
    return-void
.end method
