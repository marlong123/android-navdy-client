.class Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;
.super Ljava/lang/Object;
.source "QuadEdgeTriangle.java"

# interfaces
.implements Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QuadEdgeTriangleBuilderVisitor"
.end annotation


# instance fields
.field private triangles:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;->triangles:Ljava/util/List;

    .line 347
    return-void
.end method


# virtual methods
.method public getTriangles()Ljava/util/List;
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;->triangles:Ljava/util/List;

    return-object v0
.end method

.method public visit([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 2
    .param p1, "edges"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 350
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle$QuadEdgeTriangleBuilderVisitor;->triangles:Ljava/util/List;

    new-instance v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;

    invoke-direct {v1, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeTriangle;-><init>([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    return-void
.end method
