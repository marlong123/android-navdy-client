.class public Lcom/vividsolutions/jts/triangulate/ConstraintVertex;
.super Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
.source "ConstraintVertex.java"


# instance fields
.field private constraint:Ljava/lang/Object;

.field private isOnConstraint:Z


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->constraint:Ljava/lang/Object;

    .line 57
    return-void
.end method


# virtual methods
.method public getConstraint()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->constraint:Ljava/lang/Object;

    return-object v0
.end method

.method public isOnConstraint()Z
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->isOnConstraint:Z

    return v0
.end method

.method protected merge(Lcom/vividsolutions/jts/triangulate/ConstraintVertex;)V
    .locals 1
    .param p1, "other"    # Lcom/vividsolutions/jts/triangulate/ConstraintVertex;

    .prologue
    .line 104
    iget-boolean v0, p1, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->isOnConstraint:Z

    if-eqz v0, :cond_0

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->isOnConstraint:Z

    .line 106
    iget-object v0, p1, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->constraint:Ljava/lang/Object;

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->constraint:Ljava/lang/Object;

    .line 108
    :cond_0
    return-void
.end method

.method public setConstraint(Ljava/lang/Object;)V
    .locals 1
    .param p1, "constraint"    # Ljava/lang/Object;

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->isOnConstraint:Z

    .line 84
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->constraint:Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public setOnConstraint(Z)V
    .locals 0
    .param p1, "isOnConstraint"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/vividsolutions/jts/triangulate/ConstraintVertex;->isOnConstraint:Z

    .line 66
    return-void
.end method
