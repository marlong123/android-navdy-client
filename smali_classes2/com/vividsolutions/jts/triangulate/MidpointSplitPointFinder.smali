.class public Lcom/vividsolutions/jts/triangulate/MidpointSplitPointFinder;
.super Ljava/lang/Object;
.source "MidpointSplitPointFinder.java"

# interfaces
.implements Lcom/vividsolutions/jts/triangulate/ConstraintSplitPointFinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public findSplitPoint(Lcom/vividsolutions/jts/triangulate/Segment;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 12
    .param p1, "seg"    # Lcom/vividsolutions/jts/triangulate/Segment;
    .param p2, "encroachPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 57
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 58
    .local v0, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 59
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v4, v6

    div-double/2addr v4, v10

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v6, v8

    div-double/2addr v6, v10

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v2
.end method
