.class public Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;
.super Ljava/lang/Object;
.source "DiscreteHausdorffDistance.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MaxPointDistanceFilter"
.end annotation


# instance fields
.field private euclideanDist:Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;

.field private geom:Lcom/vividsolutions/jts/geom/Geometry;

.field private maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

.field private minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .line 159
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .line 160
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->euclideanDist:Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;

    .line 165
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 166
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize()V

    .line 171
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-static {v0, p1, v1}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 172
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 173
    return-void
.end method

.method public getMaxPointDistance()Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    return-object v0
.end method
