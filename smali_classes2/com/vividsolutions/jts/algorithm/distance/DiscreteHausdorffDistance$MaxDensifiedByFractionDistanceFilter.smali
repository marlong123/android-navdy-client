.class public Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;
.super Ljava/lang/Object;
.source "DiscreteHausdorffDistance.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MaxDensifiedByFractionDistanceFilter"
.end annotation


# instance fields
.field private geom:Lcom/vividsolutions/jts/geom/Geometry;

.field private maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

.field private minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

.field private numSubSegs:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 2
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "fraction"    # D

    .prologue
    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .line 182
    new-instance v0, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->numSubSegs:I

    .line 187
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 188
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double/2addr v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->rint(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->numSubSegs:I

    .line 189
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 18
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "index"    # I

    .prologue
    .line 196
    if-nez p2, :cond_1

    .line 215
    :cond_0
    return-void

    .line 199
    :cond_1
    add-int/lit8 v14, p2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 200
    .local v7, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface/range {p1 .. p2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    .line 202
    .local v8, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v14, v8, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->numSubSegs:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v2, v14, v16

    .line 203
    .local v2, "delx":D
    iget-wide v14, v8, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v14, v14, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->numSubSegs:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    div-double v4, v14, v16

    .line 205
    .local v4, "dely":D
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->numSubSegs:I

    if-ge v6, v14, :cond_0

    .line 206
    iget-wide v14, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    int-to-double v0, v6

    move-wide/from16 v16, v0

    mul-double v16, v16, v2

    add-double v10, v14, v16

    .line 207
    .local v10, "x":D
    iget-wide v14, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    int-to-double v0, v6

    move-wide/from16 v16, v0

    mul-double v16, v16, v4

    add-double v12, v14, v16

    .line 208
    .local v12, "y":D
    new-instance v9, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 209
    .local v9, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v14}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->initialize()V

    .line 210
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-static {v14, v9, v15}, Lcom/vividsolutions/jts/algorithm/distance/DistanceToPoint;->computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 211
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    invoke-virtual {v14, v15}, Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;)V

    .line 205
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public getMaxPointDistance()Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/distance/DiscreteHausdorffDistance$MaxDensifiedByFractionDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/algorithm/distance/PointPairDistance;

    return-object v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return v0
.end method

.method public isGeometryChanged()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method
