.class public Lcom/vividsolutions/jts/algorithm/InteriorPointLine;
.super Ljava/lang/Object;
.source "InteriorPointLine.java"


# instance fields
.field private centroid:Lcom/vividsolutions/jts/geom/Coordinate;

.field private interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private minDistance:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->minDistance:D

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 59
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCentroid()Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->centroid:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 60
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addInterior(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 61
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v0, :cond_0

    .line 62
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addEndpoints(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 63
    :cond_0
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "point"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->centroid:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 121
    .local v0, "dist":D
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->minDistance:D

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    .line 122
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v2, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 123
    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->minDistance:D

    .line 125
    :cond_0
    return-void
.end method

.method private addEndpoints(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 102
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_1

    .line 103
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addEndpoints([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 111
    :cond_0
    return-void

    .line 105
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 106
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 107
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 108
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addEndpoints(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 107
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private addEndpoints([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 114
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 115
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 116
    return-void
.end method

.method private addInterior(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 78
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v2, :cond_1

    .line 79
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addInterior([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 87
    :cond_0
    return-void

    .line 81
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 82
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 83
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 84
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->addInterior(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private addInterior([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 90
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 91
    aget-object v1, p1, v0

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method public getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->interiorPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method
