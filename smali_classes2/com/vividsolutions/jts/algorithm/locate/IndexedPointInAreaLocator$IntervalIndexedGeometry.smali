.class Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;
.super Ljava/lang/Object;
.source "IndexedPointInAreaLocator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IntervalIndexedGeometry"
.end annotation


# instance fields
.field private index:Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->index:Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;

    .line 118
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->init(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 119
    return-void
.end method

.method private addLine([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 12
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 133
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 134
    new-instance v6, Lcom/vividsolutions/jts/geom/LineSegment;

    add-int/lit8 v1, v0, -0x1

    aget-object v1, p1, v1

    aget-object v7, p1, v0

    invoke-direct {v6, v1, v7}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 135
    .local v6, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v1, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 136
    .local v2, "min":D
    iget-object v1, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    .line 137
    .local v4, "max":D
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->index:Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;

    invoke-virtual/range {v1 .. v6}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->insert(DDLjava/lang/Object;)V

    .line 133
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    .end local v2    # "min":D
    .end local v4    # "max":D
    .end local v6    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    :cond_0
    return-void
.end method

.method private init(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 5
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 123
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 124
    .local v2, "lines":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 125
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 126
    .local v1, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 127
    .local v3, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->addLine([Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0

    .line 129
    .end local v1    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v3    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-void
.end method


# virtual methods
.method public query(DD)Ljava/util/List;
    .locals 7
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 143
    new-instance v6, Lcom/vividsolutions/jts/index/ArrayListVisitor;

    invoke-direct {v6}, Lcom/vividsolutions/jts/index/ArrayListVisitor;-><init>()V

    .line 144
    .local v6, "visitor":Lcom/vividsolutions/jts/index/ArrayListVisitor;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->index:Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v6}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 145
    invoke-virtual {v6}, Lcom/vividsolutions/jts/index/ArrayListVisitor;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V
    .locals 7
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "visitor"    # Lcom/vividsolutions/jts/index/ItemVisitor;

    .prologue
    .line 150
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator$IntervalIndexedGeometry;->index:Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/vividsolutions/jts/index/intervalrtree/SortedPackedIntervalRTree;->query(DDLcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 151
    return-void
.end method
