.class public Lcom/vividsolutions/jts/operation/buffer/BufferParameters;
.super Ljava/lang/Object;
.source "BufferParameters.java"


# static fields
.field public static final CAP_FLAT:I = 0x2

.field public static final CAP_ROUND:I = 0x1

.field public static final CAP_SQUARE:I = 0x3

.field public static final DEFAULT_MITRE_LIMIT:D = 5.0

.field public static final DEFAULT_QUADRANT_SEGMENTS:I = 0x8

.field public static final JOIN_BEVEL:I = 0x3

.field public static final JOIN_MITRE:I = 0x2

.field public static final JOIN_ROUND:I = 0x1


# instance fields
.field private endCapStyle:I

.field private isSingleSided:Z

.field private joinStyle:I

.field private mitreLimit:D

.field private quadrantSegments:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/16 v0, 0x8

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 95
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    .line 96
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 97
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    .line 105
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "quadrantSegments"    # I

    .prologue
    const/4 v1, 0x1

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/16 v0, 0x8

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 95
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    .line 96
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 97
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    .line 115
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setQuadrantSegments(I)V

    .line 116
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "quadrantSegments"    # I
    .param p2, "endCapStyle"    # I

    .prologue
    const/4 v1, 0x1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/16 v0, 0x8

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 95
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    .line 96
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 97
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    .line 128
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setQuadrantSegments(I)V

    .line 129
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setEndCapStyle(I)V

    .line 130
    return-void
.end method

.method public constructor <init>(IIID)V
    .locals 2
    .param p1, "quadrantSegments"    # I
    .param p2, "endCapStyle"    # I
    .param p3, "joinStyle"    # I
    .param p4, "mitreLimit"    # D

    .prologue
    const/4 v1, 0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/16 v0, 0x8

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 95
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    .line 96
    iput v1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 97
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    .line 146
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setQuadrantSegments(I)V

    .line 147
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setEndCapStyle(I)V

    .line 148
    invoke-virtual {p0, p3}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setJoinStyle(I)V

    .line 149
    invoke-virtual {p0, p4, p5}, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->setMitreLimit(D)V

    .line 150
    return-void
.end method

.method public static bufferDistanceError(I)D
    .locals 6
    .param p0, "quadSegs"    # I

    .prologue
    .line 228
    const-wide v2, 0x3ff921fb54442d18L    # 1.5707963267948966

    int-to-double v4, p0

    div-double v0, v2, v4

    .line 229
    .local v0, "alpha":D
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    return-wide v2
.end method


# virtual methods
.method public getEndCapStyle()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    return v0
.end method

.method public getJoinStyle()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    return v0
.end method

.method public getMitreLimit()D
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    return-wide v0
.end method

.method public getQuadrantSegments()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    return v0
.end method

.method public isSingleSided()Z
    .locals 1

    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    return v0
.end method

.method public setEndCapStyle(I)V
    .locals 0
    .param p1, "endCapStyle"    # I

    .prologue
    .line 251
    iput p1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->endCapStyle:I

    .line 252
    return-void
.end method

.method public setJoinStyle(I)V
    .locals 0
    .param p1, "joinStyle"    # I

    .prologue
    .line 273
    iput p1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 274
    return-void
.end method

.method public setMitreLimit(D)V
    .locals 1
    .param p1, "mitreLimit"    # D

    .prologue
    .line 301
    iput-wide p1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 302
    return-void
.end method

.method public setQuadrantSegments(I)V
    .locals 3
    .param p1, "quadSegs"    # I

    .prologue
    const/4 v2, 0x1

    .line 187
    iput p1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 199
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x3

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 201
    :cond_0
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    if-gez v0, :cond_1

    .line 202
    const/4 v0, 0x2

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    .line 203
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->mitreLimit:D

    .line 206
    :cond_1
    if-gtz p1, :cond_2

    .line 207
    iput v2, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 214
    :cond_2
    iget v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->joinStyle:I

    if-eq v0, v2, :cond_3

    .line 215
    const/16 v0, 0x8

    iput v0, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->quadrantSegments:I

    .line 217
    :cond_3
    return-void
.end method

.method public setSingleSided(Z)V
    .locals 0
    .param p1, "isSingleSided"    # Z

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/vividsolutions/jts/operation/buffer/BufferParameters;->isSingleSided:Z

    .line 325
    return-void
.end method
