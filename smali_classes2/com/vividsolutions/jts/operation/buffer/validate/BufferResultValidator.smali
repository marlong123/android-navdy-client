.class public Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;
.super Ljava/lang/Object;
.source "BufferResultValidator.java"


# static fields
.field private static final MAX_ENV_DIFF_FRAC:D = 0.012

.field private static VERBOSE:Z


# instance fields
.field private distance:D

.field private errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

.field private errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

.field private errorMsg:Ljava/lang/String;

.field private input:Lcom/vividsolutions/jts/geom/Geometry;

.field private isValid:Z

.field private result:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->VERBOSE:Z

    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "input"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "distance"    # D
    .param p4, "result"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 92
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 94
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 98
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    .line 99
    iput-wide p2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    .line 100
    iput-object p4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    .line 101
    return-void
.end method

.method private checkArea()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    .line 198
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getArea()D

    move-result-wide v0

    .line 199
    .local v0, "inputArea":D
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getArea()D

    move-result-wide v2

    .line 201
    .local v2, "resultArea":D
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    cmpl-double v4, v4, v6

    if-lez v4, :cond_0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    .line 203
    iput-boolean v8, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 204
    const-string v4, "Area of positive buffer is smaller than input"

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 205
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 207
    :cond_0
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    cmpg-double v4, v0, v2

    if-gez v4, :cond_1

    .line 209
    iput-boolean v8, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 210
    const-string v4, "Area of negative buffer is larger than input"

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 211
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 213
    :cond_1
    const-string v4, "Area"

    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->report(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method private checkDistance()V
    .locals 5

    .prologue
    .line 218
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    iget-wide v2, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)V

    .line 219
    .local v0, "distValid":Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 220
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 221
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 222
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->getErrorLocation()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 223
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferDistanceValidator;->getErrorIndicator()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 225
    :cond_0
    const-string v1, "Distance"

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->report(Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method private checkEnvelope()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 177
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    cmpg-double v4, v4, v8

    if-gez v4, :cond_0

    .line 194
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    const-wide v6, 0x3f889374bc6a7efaL    # 0.012

    mul-double v2, v4, v6

    .line 180
    .local v2, "padding":D
    cmpl-double v4, v2, v8

    if-nez v4, :cond_1

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    .line 182
    :cond_1
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 183
    .local v1, "expectedEnv":Lcom/vividsolutions/jts/geom/Envelope;
    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    invoke-virtual {v1, v4, v5}, Lcom/vividsolutions/jts/geom/Envelope;->expandBy(D)V

    .line 185
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 186
    .local v0, "bufEnv":Lcom/vividsolutions/jts/geom/Envelope;
    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->expandBy(D)V

    .line 188
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 189
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 190
    const-string v4, "Buffer envelope is incorrect"

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 191
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometry(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 193
    :cond_2
    const-string v4, "Envelope"

    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->report(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkExpectedEmpty()V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->input:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iget-wide v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->distance:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 169
    const-string v0, "Result is non-empty"

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 172
    :cond_2
    const-string v0, "ExpectedEmpty"

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->report(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkPolygonal()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/Polygon;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-nez v0, :cond_0

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 154
    :cond_0
    const-string v0, "Result is not polygonal"

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->result:Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    .line 156
    const-string v0, "Polygonal"

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->report(Ljava/lang/String;)V

    .line 157
    return-void
.end method

.method public static isValid(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "distance"    # D
    .param p3, "result"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 64
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)V

    .line 65
    .local v0, "validator":Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x1

    .line 67
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidMsg(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)Ljava/lang/String;
    .locals 3
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "distance"    # D
    .param p3, "result"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 82
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;DLcom/vividsolutions/jts/geom/Geometry;)V

    .line 83
    .local v0, "validator":Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    .line 85
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private report(Ljava/lang/String;)V
    .locals 3
    .param p1, "checkName"    # Ljava/lang/String;

    .prologue
    .line 144
    sget-boolean v0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->VERBOSE:Z

    if-nez v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Check "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    if-eqz v0, :cond_1

    const-string v0, "passed"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "FAILED"

    goto :goto_1
.end method


# virtual methods
.method public getErrorIndicator()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorIndicator:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method public getErrorLocation()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorLocation:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->errorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->checkPolygonal()V

    .line 106
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    .line 114
    :goto_0
    return v0

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->checkExpectedEmpty()V

    .line 108
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    goto :goto_0

    .line 109
    :cond_1
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->checkEnvelope()V

    .line 110
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    goto :goto_0

    .line 111
    :cond_2
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->checkArea()V

    .line 112
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    goto :goto_0

    .line 113
    :cond_3
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->checkDistance()V

    .line 114
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferResultValidator;->isValid:Z

    goto :goto_0
.end method
