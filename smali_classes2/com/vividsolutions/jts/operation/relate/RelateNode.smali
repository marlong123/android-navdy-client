.class public Lcom/vividsolutions/jts/operation/relate/RelateNode;
.super Lcom/vividsolutions/jts/geomgraph/Node;
.source "RelateNode.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;)V
    .locals 0
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "edges"    # Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geomgraph/Node;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;)V

    .line 63
    return-void
.end method


# virtual methods
.method protected computeIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 4
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    const/4 v3, 0x0

    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateNode;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v0

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/RelateNode;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    invoke-virtual {p1, v0, v1, v3}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeastIfValid(III)V

    .line 72
    return-void
.end method

.method updateIMFromEdges(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 1
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateNode;->edges:Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    check-cast v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundleStar;->updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 79
    return-void
.end method
