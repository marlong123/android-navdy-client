.class public Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;
.super Ljava/lang/Object;
.source "ConnectedInteriorTester.java"


# instance fields
.field private disconnectedRingcoord:Lcom/vividsolutions/jts/geom/Coordinate;

.field private geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 1
    .param p1, "geomGraph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 78
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 79
    return-void
.end method

.method private buildEdgeRings(Ljava/util/Collection;)Ljava/util/List;
    .locals 6
    .param p1, "dirEdges"    # Ljava/util/Collection;

    .prologue
    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v1, "edgeRings":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 131
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 133
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInResult()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getEdgeRing()Lcom/vividsolutions/jts/geomgraph/EdgeRing;

    move-result-object v5

    if-nez v5, :cond_0

    .line 135
    new-instance v2, Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v2, v0, v5}, Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;-><init>(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 137
    .local v2, "er":Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;->linkDirectedEdgesForMinimalEdgeRings()V

    .line 138
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;->buildMinimalRings()Ljava/util/List;

    move-result-object v4

    .line 139
    .local v4, "minEdgeRings":Ljava/util/List;
    invoke-interface {v1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 142
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v2    # "er":Lcom/vividsolutions/jts/operation/overlay/MaximalEdgeRing;
    .end local v4    # "minEdgeRings":Ljava/util/List;
    :cond_1
    return-object v1
.end method

.method public static findDifferentPoint([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "coord"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 63
    aget-object v1, p0, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    aget-object v1, p0, v0

    .line 66
    :goto_1
    return-object v1

    .line 62
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private hasUnvisitedShellEdge(Ljava/util/List;)Z
    .locals 8
    .param p1, "edgeRings"    # Ljava/util/List;

    .prologue
    const/4 v5, 0x0

    .line 212
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 213
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeRing;

    .line 215
    .local v2, "er":Lcom/vividsolutions/jts/geomgraph/EdgeRing;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/EdgeRing;->isHole()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 212
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 217
    :cond_1
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/EdgeRing;->getEdges()Ljava/util/List;

    move-result-object v1

    .line 218
    .local v1, "edges":Ljava/util/List;
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 221
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    const/4 v7, 0x2

    invoke-virtual {v6, v5, v7}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v6

    if-nez v6, :cond_0

    .line 227
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 228
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 230
    .restart local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isVisited()Z

    move-result v6

    if-nez v6, :cond_3

    .line 232
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->disconnectedRingcoord:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 233
    const/4 v5, 0x1

    .line 237
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v1    # "edges":Ljava/util/List;
    .end local v2    # "er":Lcom/vividsolutions/jts/geomgraph/EdgeRing;
    .end local v4    # "j":I
    :cond_2
    return v5

    .line 227
    .restart local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .restart local v1    # "edges":Ljava/util/List;
    .restart local v2    # "er":Lcom/vividsolutions/jts/geomgraph/EdgeRing;
    .restart local v4    # "j":I
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private setInteriorEdgesInResult(Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V
    .locals 5
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 116
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v2

    if-nez v2, :cond_0

    .line 117
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setInResult(Z)V

    goto :goto_0

    .line 120
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    :cond_1
    return-void
.end method

.method private visitInteriorRing(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V
    .locals 9
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    .prologue
    const/4 v8, 0x2

    const/4 v6, 0x0

    .line 168
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 169
    .local v5, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v3, v5, v6

    .line 174
    .local v3, "pt0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v5, v3}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->findDifferentPoint([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 175
    .local v4, "pt1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2, v3, v4}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->findEdgeInSameDirection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    .line 176
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->findEdgeEnd(Lcom/vividsolutions/jts/geomgraph/Edge;)Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 177
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    const/4 v2, 0x0

    .line 178
    .local v2, "intDe":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v7

    invoke-virtual {v7, v6, v8}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v7

    if-nez v7, :cond_2

    .line 179
    move-object v2, v0

    .line 184
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    const/4 v6, 0x1

    :cond_1
    const-string v7, "unable to find dirEdge with Interior on RHS"

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 186
    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->visitLinkedDirectedEdges(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;)V

    .line 187
    return-void

    .line 181
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    move-result-object v7

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v7

    invoke-virtual {v7, v6, v8}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v7

    if-nez v7, :cond_0

    .line 182
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    move-result-object v2

    goto :goto_0
.end method

.method private visitShellInteriors(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    .prologue
    .line 153
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_0

    move-object v2, p1

    .line 154
    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 155
    .local v2, "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->visitInteriorRing(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V

    .line 157
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_0
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v3, :cond_1

    move-object v1, p1

    .line 158
    check-cast v1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .line 159
    .local v1, "mp":Lcom/vividsolutions/jts/geom/MultiPolygon;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 160
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 161
    .restart local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->visitInteriorRing(Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    .end local v0    # "i":I
    .end local v1    # "mp":Lcom/vividsolutions/jts/geom/MultiPolygon;
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_1
    return-void
.end method


# virtual methods
.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->disconnectedRingcoord:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public isInteriorsConnected()Z
    .locals 4

    .prologue
    .line 86
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 87
    .local v2, "splitEdges":Ljava/util/List;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSplitEdges(Ljava/util/List;)V

    .line 90
    new-instance v1, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    new-instance v3, Lcom/vividsolutions/jts/operation/overlay/OverlayNodeFactory;

    invoke-direct {v3}, Lcom/vividsolutions/jts/operation/overlay/OverlayNodeFactory;-><init>()V

    invoke-direct {v1, v3}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    .line 91
    .local v1, "graph":Lcom/vividsolutions/jts/geomgraph/PlanarGraph;
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->addEdges(Ljava/util/List;)V

    .line 92
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->setInteriorEdgesInResult(Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V

    .line 93
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->linkResultDirectedEdges()V

    .line 94
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->buildEdgeRings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 100
    .local v0, "edgeRings":Ljava/util/List;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-direct {p0, v3, v1}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->visitShellInteriors(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V

    .line 109
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->hasUnvisitedShellEdge(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected visitLinkedDirectedEdges(Lcom/vividsolutions/jts/geomgraph/DirectedEdge;)V
    .locals 5
    .param p1, "start"    # Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .prologue
    const/4 v3, 0x1

    .line 191
    move-object v1, p1

    .line 192
    .local v1, "startDe":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    move-object v0, p1

    .line 194
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    :cond_0
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    const-string v4, "found null Directed Edge"

    invoke-static {v2, v4}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 195
    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setVisited(Z)V

    .line 196
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getNext()Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    move-result-object v0

    .line 197
    if-ne v0, v1, :cond_0

    .line 198
    return-void

    .line 194
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
