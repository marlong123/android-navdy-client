.class public Lcom/vividsolutions/jts/operation/valid/IsValidOp;
.super Ljava/lang/Object;
.source "IsValidOp.java"


# instance fields
.field private isSelfTouchingRingFormingHoleValid:Z

.field private parentGeometry:Lcom/vividsolutions/jts/geom/Geometry;

.field private validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "parentGeometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isSelfTouchingRingFormingHoleValid:Z

    .line 113
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->parentGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    .line 114
    return-void
.end method

.method private checkClosedRing(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 3
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 340
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 341
    const/4 v0, 0x0

    .line 342
    .local v0, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getNumPoints()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 343
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 344
    :cond_0
    new-instance v1, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/16 v2, 0xb

    invoke-direct {v1, v2, v0}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 348
    .end local v0    # "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    return-void
.end method

.method private checkClosedRings(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 2
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 330
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkClosedRing(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 331
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v1, :cond_1

    .line 336
    :cond_0
    return-void

    .line 332
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 333
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkClosedRing(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 334
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 332
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkConnectedInteriors(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 4
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 611
    new-instance v0, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;-><init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 612
    .local v0, "cit":Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->isInteriorsConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 613
    new-instance v1, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/4 v2, 0x4

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConnectedInteriorTester;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 616
    :cond_0
    return-void
.end method

.method private checkConsistentArea(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 5
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 370
    new-instance v0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;-><init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 371
    .local v0, "cat":Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->isNodeConsistentArea()Z

    move-result v1

    .line 372
    .local v1, "isValidArea":Z
    if-nez v1, :cond_1

    .line 373
    new-instance v2, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/4 v3, 0x5

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->getInvalidPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 383
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->hasDuplicateRings()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 379
    new-instance v2, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/16 v3, 0x8

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->getInvalidPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    goto :goto_0
.end method

.method private checkHolesInShell(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 8
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 443
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 447
    .local v5, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    new-instance v4, Lcom/vividsolutions/jts/algorithm/MCPointInRing;

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/algorithm/MCPointInRing;-><init>(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 449
    .local v4, "pir":Lcom/vividsolutions/jts/algorithm/PointInRing;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 451
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 452
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-static {v6, v5, p2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 458
    .local v1, "holePt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v1, :cond_1

    .line 468
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v1    # "holePt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    :goto_1
    return-void

    .line 460
    .restart local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    .restart local v1    # "holePt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    invoke-interface {v4, v1}, Lcom/vividsolutions/jts/algorithm/PointInRing;->isInside(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v3, 0x1

    .line 461
    .local v3, "outside":Z
    :goto_2
    if-eqz v3, :cond_3

    .line 462
    new-instance v6, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/4 v7, 0x2

    invoke-direct {v6, v7, v1}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v6, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    goto :goto_1

    .line 460
    .end local v3    # "outside":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    .line 449
    .restart local v3    # "outside":Z
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private checkHolesNotNested(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 7
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 484
    new-instance v3, Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;

    invoke-direct {v3, p2}, Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;-><init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 488
    .local v3, "nestedTester":Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 489
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 490
    .local v1, "innerHole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v3, v1}, Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;->add(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 488
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 492
    .end local v1    # "innerHole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_0
    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;->isNonNested()Z

    move-result v2

    .line 493
    .local v2, "isNonNested":Z
    if-nez v2, :cond_1

    .line 494
    new-instance v4, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/4 v5, 0x3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/valid/IndexedNestedRingTester;->getNestedPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 498
    :cond_1
    return-void
.end method

.method private checkInvalidCoordinates(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 2
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 320
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 321
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v1, :cond_1

    .line 326
    :cond_0
    return-void

    .line 322
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 323
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 324
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "coords"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 308
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 309
    aget-object v1, p1, v0

    invoke-static {v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isValid(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 310
    new-instance v1, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/16 v2, 0xa

    aget-object v3, p1, v0

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 316
    :cond_0
    return-void

    .line 308
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkNoSelfIntersectingRing(Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;)V
    .locals 7
    .param p1, "eiList"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    .prologue
    .line 409
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    .line 410
    .local v3, "nodeSet":Ljava/util/Set;
    const/4 v2, 0x1

    .line 411
    .local v2, "isFirst":Z
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 412
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 413
    .local v0, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    if-eqz v2, :cond_0

    .line 414
    const/4 v2, 0x0

    .line 415
    goto :goto_0

    .line 417
    :cond_0
    iget-object v4, v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 418
    new-instance v4, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/4 v5, 0x6

    iget-object v6, v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v4, v5, v6}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 427
    .end local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_1
    return-void

    .line 424
    .restart local v0    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_2
    iget-object v4, v0, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private checkNoSelfIntersectingRings(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 3
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 394
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 395
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 396
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkNoSelfIntersectingRing(Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;)V

    .line 397
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v2, :cond_0

    .line 400
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_1
    return-void
.end method

.method private checkShellInsideHole(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "shell"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "hole"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p3, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    const/4 v6, 0x0

    .line 585
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 586
    .local v5, "shellPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 588
    .local v1, "holePts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v5, p2, p3}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 590
    .local v4, "shellPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v4, :cond_0

    .line 591
    invoke-static {v4, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    .line 592
    .local v2, "insideHole":Z
    if-nez v2, :cond_0

    .line 606
    .end local v2    # "insideHole":Z
    .end local v4    # "shellPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object v4

    .line 596
    .restart local v4    # "shellPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    invoke-static {v1, p1, p3}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 598
    .local v0, "holePt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v0, :cond_2

    .line 599
    invoke-static {v0, v5}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    .line 600
    .local v3, "insideShell":Z
    if-eqz v3, :cond_1

    move-object v4, v0

    .line 601
    goto :goto_0

    :cond_1
    move-object v4, v6

    .line 603
    goto :goto_0

    .line 605
    .end local v3    # "insideShell":Z
    :cond_2
    const-string v7, "points in shell and hole appear to be equal"

    invoke-static {v7}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    move-object v4, v6

    .line 606
    goto :goto_0
.end method

.method private checkShellNotNested(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 10
    .param p1, "shell"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "p"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p3, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    const/4 v9, 0x7

    .line 537
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v7

    .line 539
    .local v7, "shellPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 540
    .local v5, "polyShell":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    .line 541
    .local v4, "polyPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v7, v5, p3}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 543
    .local v6, "shellPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v6, :cond_1

    .line 572
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    invoke-static {v6, v4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v3

    .line 546
    .local v3, "insidePolyShell":Z
    if-eqz v3, :cond_0

    .line 549
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v8

    if-gtz v8, :cond_2

    .line 550
    new-instance v8, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    invoke-direct {v8, v9, v6}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v8, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    goto :goto_0

    .line 562
    :cond_2
    const/4 v0, 0x0

    .line 563
    .local v0, "badNestedPt":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v8

    if-ge v2, v8, :cond_3

    .line 564
    invoke-virtual {p2, v2}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 565
    .local v1, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-direct {p0, p1, v1, p3}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkShellInsideHole(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 566
    if-eqz v0, :cond_0

    .line 563
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 569
    .end local v1    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_3
    new-instance v8, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    invoke-direct {v8, v9, v0}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v8, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    goto :goto_0
.end method

.method private checkShellsNotNested(Lcom/vividsolutions/jts/geom/MultiPolygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 6
    .param p1, "mp"    # Lcom/vividsolutions/jts/geom/MultiPolygon;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 514
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 515
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 516
    .local v2, "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 517
    .local v4, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 518
    if-ne v0, v1, :cond_1

    .line 517
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 519
    :cond_1
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Polygon;

    .line 520
    .local v3, "p2":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-direct {p0, v4, v3, p2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkShellNotNested(Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 521
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v5, :cond_0

    .line 524
    .end local v1    # "j":I
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    .end local v3    # "p2":Lcom/vividsolutions/jts/geom/Polygon;
    .end local v4    # "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_2
    return-void

    .line 514
    .restart local v1    # "j":I
    .restart local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    .restart local v4    # "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private checkTooFewPoints(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 3
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 352
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->hasTooFewPoints()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    new-instance v0, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    const/16 v1, 0x9

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getInvalidPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;-><init>(ILcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 358
    :cond_0
    return-void
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    .line 176
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return-void

    .line 178
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/Point;)V

    goto :goto_0

    .line 179
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPoint;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/MultiPoint;)V

    goto :goto_0

    .line 181
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/vividsolutions/jts/geom/LinearRing;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/LinearRing;)V

    goto :goto_0

    .line 182
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/LineString;)V

    goto :goto_0

    .line 183
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/Polygon;)V

    goto :goto_0

    .line 184
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_5
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    if-eqz v0, :cond_6

    check-cast p1, Lcom/vividsolutions/jts/geom/MultiPolygon;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/MultiPolygon;)V

    goto :goto_0

    .line 185
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_6
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_7

    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/GeometryCollection;)V

    goto :goto_0

    .line 186
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/GeometryCollection;)V
    .locals 3
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;

    .prologue
    .line 299
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 300
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 301
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 302
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v2, :cond_1

    .line 304
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void

    .line 299
    .restart local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 209
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 210
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v1, :cond_0

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 212
    .local v0, "graph":Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkTooFewPoints(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_0
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 3
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 220
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v2, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkClosedRing(Lcom/vividsolutions/jts/geom/LinearRing;)V

    .line 222
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v2, :cond_0

    .line 224
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v2, 0x0

    invoke-direct {v0, v2, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 225
    .local v0, "graph":Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkTooFewPoints(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 226
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v2, :cond_0

    .line 227
    new-instance v1, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v1}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    .line 228
    .local v1, "li":Lcom/vividsolutions/jts/algorithm/LineIntersector;
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 229
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkNoSelfIntersectingRings(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_0
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/MultiPoint;)V
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/MultiPoint;

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPoint;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 202
    return-void
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/MultiPolygon;)V
    .locals 4
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/MultiPolygon;

    .prologue
    .line 264
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 265
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 266
    .local v2, "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 267
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v3, :cond_1

    .line 295
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_0
    :goto_1
    return-void

    .line 268
    .restart local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_1
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkClosedRings(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 269
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 264
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 272
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v3, 0x0

    invoke-direct {v0, v3, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 274
    .local v0, "graph":Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkTooFewPoints(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 275
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 276
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkConsistentArea(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 277
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 278
    iget-boolean v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isSelfTouchingRingFormingHoleValid:Z

    if-nez v3, :cond_3

    .line 279
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkNoSelfIntersectingRings(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 280
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 282
    :cond_3
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 283
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 284
    .restart local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-direct {p0, v2, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkHolesInShell(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 285
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 287
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_4
    const/4 v1, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 288
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/MultiPolygon;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 289
    .restart local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-direct {p0, v2, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkHolesNotNested(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 290
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 287
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 292
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkShellsNotNested(Lcom/vividsolutions/jts/geom/MultiPolygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 293
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v3, :cond_0

    .line 294
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkConnectedInteriors(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_1
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/Point;)V
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Point;

    .prologue
    .line 194
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 195
    return-void
.end method

.method private checkValid(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 238
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkInvalidCoordinates(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 239
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-eqz v1, :cond_1

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkClosedRings(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 241
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 243
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 245
    .local v0, "graph":Lcom/vividsolutions/jts/geomgraph/GeometryGraph;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkTooFewPoints(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 246
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 247
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkConsistentArea(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 248
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 250
    iget-boolean v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isSelfTouchingRingFormingHoleValid:Z

    if-nez v1, :cond_2

    .line 251
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkNoSelfIntersectingRings(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 252
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 254
    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkHolesInShell(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 255
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 257
    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkHolesNotNested(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 258
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v1, :cond_0

    .line 259
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkConnectedInteriors(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_0
.end method

.method public static findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p0, "testCoords"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "searchRing"    # Lcom/vividsolutions/jts/geom/LinearRing;
    .param p2, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 91
    invoke-virtual {p2, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->findEdge(Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v3

    .line 93
    .local v3, "searchEdge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v0

    .line 95
    .local v0, "eiList":Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p0

    if-ge v1, v4, :cond_1

    .line 96
    aget-object v2, p0, v1

    .line 97
    .local v2, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->isIntersection(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 100
    .end local v2    # "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    return-object v2

    .line 95
    .restart local v2    # "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    .end local v2    # "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static isValid(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 4
    .param p0, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 73
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 74
    :cond_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v1

    if-nez v1, :cond_0

    .line 75
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isValid(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 59
    new-instance v0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 60
    .local v0, "isValidOp":Lcom/vividsolutions/jts/operation/valid/IsValidOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isValid()Z

    move-result v1

    return v1
.end method


# virtual methods
.method public getValidationError()Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->parentGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 168
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->parentGeometry:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->checkValid(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 154
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->validErr:Lcom/vividsolutions/jts/operation/valid/TopologyValidationError;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelfTouchingRingFormingHoleValid(Z)V
    .locals 0
    .param p1, "isValid"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isSelfTouchingRingFormingHoleValid:Z

    .line 143
    return-void
.end method
