.class public Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;
.super Ljava/lang/Object;
.source "ConsistentAreaTester.java"


# instance fields
.field private geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private final li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private nodeGraph:Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 1
    .param p1, "geomGraph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 70
    new-instance v0, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->nodeGraph:Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;

    .line 82
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 83
    return-void
.end method

.method private isNodeEdgeAreaLabelsConsistent()Z
    .locals 4

    .prologue
    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->nodeGraph:Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->getNodeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeIt":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 122
    .local v0, "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;->isAreaLabelsConsistent(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Coordinate;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 124
    const/4 v2, 0x0

    .line 127
    .end local v0    # "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getInvalidPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public hasDuplicateRings()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 147
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->nodeGraph:Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->getNodeIterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "nodeIt":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 148
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 149
    .local v2, "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 150
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;

    .line 151
    .local v0, "eeb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->getEdgeEnds()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v4, :cond_1

    .line 152
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 157
    .end local v0    # "eeb":Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
    .end local v1    # "i":Ljava/util/Iterator;
    .end local v2    # "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :goto_0
    return v4

    :cond_2
    move v4, v5

    goto :goto_0
.end method

.method public isNodeConsistentArea()Z
    .locals 4

    .prologue
    .line 101
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    move-result-object v0

    .line 102
    .local v0, "intersector":Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperIntersection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->getProperIntersectionPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    iput-object v1, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 104
    const/4 v1, 0x0

    .line 109
    :goto_0
    return v1

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->nodeGraph:Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->geomGraph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/operation/relate/RelateNodeGraph;->build(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    .line 109
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/valid/ConsistentAreaTester;->isNodeEdgeAreaLabelsConsistent()Z

    move-result v1

    goto :goto_0
.end method
