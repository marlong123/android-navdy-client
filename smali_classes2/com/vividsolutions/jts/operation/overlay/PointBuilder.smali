.class public Lcom/vividsolutions/jts/operation/overlay/PointBuilder;
.super Ljava/lang/Object;
.source "PointBuilder.java"


# instance fields
.field private geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

.field private resultPointList:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/operation/overlay/OverlayOp;Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/algorithm/PointLocator;)V
    .locals 1
    .param p1, "op"    # Lcom/vividsolutions/jts/operation/overlay/OverlayOp;
    .param p2, "geometryFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;
    .param p3, "ptLocator"    # Lcom/vividsolutions/jts/algorithm/PointLocator;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->resultPointList:Ljava/util/List;

    .line 50
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    .line 51
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 53
    return-void
.end method

.method private extractNonCoveredResultNodes(I)V
    .locals 4
    .param p1, "opCode"    # I

    .prologue
    .line 85
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getGraph()Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "nodeit":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 86
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 89
    .local v1, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->isInResult()Z

    move-result v3

    if-nez v3, :cond_0

    .line 92
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->isIncidentEdgeInResult()Z

    move-result v3

    if-nez v3, :cond_0

    .line 94
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;->getDegree()I

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    .line 100
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 101
    .local v0, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-static {v0, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isResultOfOp(Lcom/vividsolutions/jts/geomgraph/Label;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 102
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->filterCoveredNodeToPoint(Lcom/vividsolutions/jts/geomgraph/Node;)V

    goto :goto_0

    .line 107
    .end local v0    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_2
    return-void
.end method

.method private filterCoveredNodeToPoint(Lcom/vividsolutions/jts/geomgraph/Node;)V
    .locals 3
    .param p1, "n"    # Lcom/vividsolutions/jts/geomgraph/Node;

    .prologue
    .line 120
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 121
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->op:Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isCoveredByLA(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v1

    .line 123
    .local v1, "pt":Lcom/vividsolutions/jts/geom/Point;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->resultPointList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    .end local v1    # "pt":Lcom/vividsolutions/jts/geom/Point;
    :cond_0
    return-void
.end method


# virtual methods
.method public build(I)Ljava/util/List;
    .locals 1
    .param p1, "opCode"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->extractNonCoveredResultNodes(I)V

    .line 69
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->resultPointList:Ljava/util/List;

    return-object v0
.end method
