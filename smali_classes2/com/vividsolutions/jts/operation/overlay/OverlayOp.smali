.class public Lcom/vividsolutions/jts/operation/overlay/OverlayOp;
.super Lcom/vividsolutions/jts/operation/GeometryGraphOperation;
.source "OverlayOp.java"


# static fields
.field public static final DIFFERENCE:I = 0x3

.field public static final INTERSECTION:I = 0x1

.field public static final SYMDIFFERENCE:I = 0x4

.field public static final UNION:I = 0x2


# instance fields
.field private edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

.field private final ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

.field private resultGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private resultLineList:Ljava/util/List;

.field private resultPointList:Ljava/util/List;

.field private resultPolyList:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 103
    new-instance v0, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 108
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPolyList:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultLineList:Ljava/util/List;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPointList:Ljava/util/List;

    .line 116
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/OverlayNodeFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/operation/overlay/OverlayNodeFactory;-><init>()V

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    .line 122
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 123
    return-void
.end method

.method private cancelDuplicateResultEdges()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 491
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 492
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 493
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getSym()Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    move-result-object v2

    .line 494
    .local v2, "sym":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInResult()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInResult()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 495
    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setInResult(Z)V

    .line 496
    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setInResult(Z)V

    goto :goto_0

    .line 500
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v2    # "sym":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    :cond_1
    return-void
.end method

.method private computeGeometry(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "resultPointList"    # Ljava/util/List;
    .param p2, "resultLineList"    # Ljava/util/List;
    .param p3, "resultPolyList"    # Ljava/util/List;
    .param p4, "opcode"    # I

    .prologue
    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 543
    .local v0, "geomList":Ljava/util/List;
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 544
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 545
    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 548
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 549
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-static {p4, v1, v2, v3}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 553
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    goto :goto_0
.end method

.method private computeLabelling()V
    .locals 4

    .prologue
    .line 370
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 371
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 373
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;->computeLabelling([Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_0

    .line 375
    .end local v0    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->mergeSymLabels()V

    .line 376
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->updateNodeLabelling()V

    .line 377
    return-void
.end method

.method private computeLabelsFromDepths()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x2

    const/4 v6, 0x1

    .line 287
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 288
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 289
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v4

    .line 290
    .local v4, "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getDepth()Lcom/vividsolutions/jts/geomgraph/Depth;

    move-result-object v0

    .line 296
    .local v0, "depth":Lcom/vividsolutions/jts/geomgraph/Depth;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Depth;->isNull()Z

    move-result v5

    if-nez v5, :cond_0

    .line 297
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Depth;->normalize()V

    .line 298
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v9, :cond_0

    .line 299
    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geomgraph/Depth;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 306
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geomgraph/Depth;->getDelta(I)I

    move-result v5

    if-nez v5, :cond_2

    .line 307
    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->toLine(I)V

    .line 298
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 316
    :cond_2
    invoke-virtual {v0, v2, v6}, Lcom/vividsolutions/jts/geomgraph/Depth;->isNull(II)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_2
    const-string v8, "depth of LEFT side has not been initialized"

    invoke-static {v5, v8}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 317
    invoke-virtual {v0, v2, v6}, Lcom/vividsolutions/jts/geomgraph/Depth;->getLocation(II)I

    move-result v5

    invoke-virtual {v4, v2, v6, v5}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(III)V

    .line 318
    invoke-virtual {v0, v2, v9}, Lcom/vividsolutions/jts/geomgraph/Depth;->isNull(II)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v6

    :goto_3
    const-string v8, "depth of RIGHT side has not been initialized"

    invoke-static {v5, v8}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 319
    invoke-virtual {v0, v2, v9}, Lcom/vividsolutions/jts/geomgraph/Depth;->getLocation(II)I

    move-result v5

    invoke-virtual {v4, v2, v9, v5}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(III)V

    goto :goto_1

    :cond_3
    move v5, v7

    .line 316
    goto :goto_2

    :cond_4
    move v5, v7

    .line 318
    goto :goto_3

    .line 325
    .end local v0    # "depth":Lcom/vividsolutions/jts/geomgraph/Depth;
    .end local v1    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v2    # "i":I
    .end local v4    # "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    :cond_5
    return-void
.end method

.method private computeOverlay(I)V
    .locals 10
    .param p1, "opCode"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 138
    invoke-direct {p0, v8}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->copyPoints(I)V

    .line 139
    invoke-direct {p0, v9}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->copyPoints(I)V

    .line 142
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v8}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 143
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v9

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v8}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 146
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v6, v6, v9

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v7, v9}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeEdgeIntersections(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v0, "baseSplitEdges":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    invoke-virtual {v5, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSplitEdges(Ljava/util/List;)V

    .line 150
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v9

    invoke-virtual {v5, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSplitEdges(Ljava/util/List;)V

    .line 151
    move-object v4, v0

    .line 153
    .local v4, "splitEdges":Ljava/util/List;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->insertUniqueEdges(Ljava/util/List;)V

    .line 155
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->computeLabelsFromDepths()V

    .line 156
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->replaceCollapsedEdges()V

    .line 170
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->getEdges()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/vividsolutions/jts/geomgraph/EdgeNodingValidator;->checkValid(Ljava/util/Collection;)V

    .line 172
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->getEdges()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->addEdges(Ljava/util/List;)V

    .line 173
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->computeLabelling()V

    .line 175
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->labelIncompleteNodes()V

    .line 185
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->findResultAreaEdges(I)V

    .line 186
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->cancelDuplicateResultEdges()V

    .line 188
    new-instance v3, Lcom/vividsolutions/jts/operation/overlay/PolygonBuilder;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v3, v5}, Lcom/vividsolutions/jts/operation/overlay/PolygonBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 189
    .local v3, "polyBuilder":Lcom/vividsolutions/jts/operation/overlay/PolygonBuilder;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v3, v5}, Lcom/vividsolutions/jts/operation/overlay/PolygonBuilder;->add(Lcom/vividsolutions/jts/geomgraph/PlanarGraph;)V

    .line 190
    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/overlay/PolygonBuilder;->getPolygons()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPolyList:Ljava/util/List;

    .line 192
    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v1, p0, v5, v6}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;-><init>(Lcom/vividsolutions/jts/operation/overlay/OverlayOp;Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/algorithm/PointLocator;)V

    .line 193
    .local v1, "lineBuilder":Lcom/vividsolutions/jts/operation/overlay/LineBuilder;
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/operation/overlay/LineBuilder;->build(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultLineList:Ljava/util/List;

    .line 195
    new-instance v2, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;

    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v2, p0, v5, v6}, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;-><init>(Lcom/vividsolutions/jts/operation/overlay/OverlayOp;Lcom/vividsolutions/jts/geom/GeometryFactory;Lcom/vividsolutions/jts/algorithm/PointLocator;)V

    .line 196
    .local v2, "pointBuilder":Lcom/vividsolutions/jts/operation/overlay/PointBuilder;
    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/operation/overlay/PointBuilder;->build(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPointList:Ljava/util/List;

    .line 199
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPointList:Ljava/util/List;

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultLineList:Ljava/util/List;

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPolyList:Ljava/util/List;

    invoke-direct {p0, v5, v6, v7, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->computeGeometry(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 200
    return-void
.end method

.method private copyPoints(I)V
    .locals 5
    .param p1, "argIndex"    # I

    .prologue
    .line 354
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getNodeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 355
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 356
    .local v0, "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v2

    .line 357
    .local v2, "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/vividsolutions/jts/geomgraph/Node;->setLabel(II)V

    goto :goto_0

    .line 359
    .end local v0    # "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    .end local v2    # "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public static createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "opCode"    # I
    .param p1, "a"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "b"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const/4 v1, 0x0

    .line 579
    const/4 v0, 0x0

    .line 580
    .local v0, "result":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultDimension(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 594
    :goto_0
    return-object v0

    .line 582
    :pswitch_0
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p3, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v0

    .line 583
    goto :goto_0

    .line 585
    :pswitch_1
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p3, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    .line 586
    goto :goto_0

    .line 588
    :pswitch_2
    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p3, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 589
    goto :goto_0

    .line 591
    :pswitch_3
    invoke-virtual {p3, v1, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    goto :goto_0

    .line 580
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private findResultAreaEdges(I)V
    .locals 7
    .param p1, "opCode"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 468
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getEdgeEnds()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 469
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;

    .line 471
    .local v0, "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    .line 472
    .local v2, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->isInteriorAreaEdge()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v6}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v3

    invoke-virtual {v2, v5, v6}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v4

    invoke-static {v3, v4, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isResultOfOp(III)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 478
    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geomgraph/DirectedEdge;->setInResult(Z)V

    goto :goto_0

    .line 482
    .end local v0    # "de":Lcom/vividsolutions/jts/geomgraph/DirectedEdge;
    .end local v2    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    :cond_1
    return-void
.end method

.method private insertUniqueEdges(Ljava/util/List;)V
    .locals 3
    .param p1, "edges"    # Ljava/util/List;

    .prologue
    .line 204
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 205
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 206
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->insertUniqueEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    goto :goto_0

    .line 208
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method private isCovered(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/List;)Z
    .locals 4
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geomList"    # Ljava/util/List;

    .prologue
    .line 528
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 529
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 530
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {v3, p1, v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v2

    .line 531
    .local v2, "loc":I
    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    const/4 v3, 0x1

    .line 533
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v2    # "loc":I
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isResultOfOp(III)Z
    .locals 2
    .param p0, "loc0"    # I
    .param p1, "loc1"    # I
    .param p2, "opCode"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 84
    if-ne p0, v0, :cond_0

    const/4 p0, 0x0

    .line 85
    :cond_0
    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    .line 86
    :cond_1
    packed-switch p2, :pswitch_data_0

    move v0, v1

    .line 100
    :cond_2
    :goto_0
    return v0

    .line 88
    :pswitch_0
    if-nez p0, :cond_3

    if-eqz p1, :cond_2

    :cond_3
    move v0, v1

    goto :goto_0

    .line 91
    :pswitch_1
    if-eqz p0, :cond_4

    if-nez p1, :cond_5

    :cond_4
    move v1, v0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 94
    :pswitch_2
    if-nez p0, :cond_6

    if-nez p1, :cond_2

    :cond_6
    move v0, v1

    goto :goto_0

    .line 97
    :pswitch_3
    if-nez p0, :cond_7

    if-nez p1, :cond_2

    :cond_7
    if-eqz p0, :cond_8

    if-eqz p1, :cond_2

    :cond_8
    move v0, v1

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static isResultOfOp(Lcom/vividsolutions/jts/geomgraph/Label;I)Z
    .locals 3
    .param p0, "label"    # Lcom/vividsolutions/jts/geomgraph/Label;
    .param p1, "opCode"    # I

    .prologue
    .line 72
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v0

    .line 73
    .local v0, "loc0":I
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    .line 74
    .local v1, "loc1":I
    invoke-static {v0, v1, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isResultOfOp(III)Z

    move-result v2

    return v2
.end method

.method private labelIncompleteNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V
    .locals 4
    .param p1, "n"    # Lcom/vividsolutions/jts/geomgraph/Node;
    .param p2, "targetIndex"    # I

    .prologue
    .line 451
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v3, v3, p2

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    .line 455
    .local v0, "loc":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(II)V

    .line 456
    return-void
.end method

.method private labelIncompleteNodes()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 422
    const/4 v3, 0x0

    .line 423
    .local v3, "nodeCount":I
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "ni":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 424
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 425
    .local v1, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 426
    .local v0, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->isIsolated()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 427
    add-int/lit8 v3, v3, 0x1

    .line 428
    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 429
    invoke-direct {p0, v1, v5}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->labelIncompleteNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V

    .line 434
    :cond_0
    :goto_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v4

    check-cast v4, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v4, v0}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->updateLabelling(Lcom/vividsolutions/jts/geomgraph/Label;)V

    goto :goto_0

    .line 431
    :cond_1
    const/4 v4, 0x1

    invoke-direct {p0, v1, v4}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->labelIncompleteNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V

    goto :goto_1

    .line 444
    .end local v0    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_2
    return-void
.end method

.method private mergeSymLabels()V
    .locals 3

    .prologue
    .line 386
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 387
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 388
    .local v0, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->mergeSymLabels()V

    goto :goto_0

    .line 391
    .end local v0    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method public static overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geom0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "geom1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "opCode"    # I

    .prologue
    .line 65
    new-instance v1, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;

    invoke-direct {v1, p0, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 66
    .local v1, "gov":Lcom/vividsolutions/jts/operation/overlay/OverlayOp;
    invoke-virtual {v1, p2}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->getResultGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 67
    .local v0, "geomOv":Lcom/vividsolutions/jts/geom/Geometry;
    return-object v0
.end method

.method private replaceCollapsedEdges()V
    .locals 4

    .prologue
    .line 332
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 333
    .local v2, "newEdges":Ljava/util/List;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 334
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 335
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->isCollapsed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 337
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 338
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCollapsedEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 341
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_1
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->addAll(Ljava/util/Collection;)V

    .line 342
    return-void
.end method

.method private static resultDimension(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)I
    .locals 3
    .param p0, "opCode"    # I
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 599
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v0

    .line 600
    .local v0, "dim0":I
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    .line 602
    .local v1, "dim1":I
    const/4 v2, -0x1

    .line 603
    .local v2, "resultDimension":I
    packed-switch p0, :pswitch_data_0

    .line 624
    :goto_0
    return v2

    .line 605
    :pswitch_0
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 606
    goto :goto_0

    .line 608
    :pswitch_1
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 609
    goto :goto_0

    .line 611
    :pswitch_2
    move v2, v0

    .line 612
    goto :goto_0

    .line 621
    :pswitch_3
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateNodeLabelling()V
    .locals 4

    .prologue
    .line 398
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/PlanarGraph;->getNodes()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "nodeit":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 399
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 400
    .local v1, "node":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/DirectedEdgeStar;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 401
    .local v0, "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geomgraph/Label;->merge(Lcom/vividsolutions/jts/geomgraph/Label;)V

    goto :goto_0

    .line 403
    .end local v0    # "lbl":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v1    # "node":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method


# virtual methods
.method public getGraph()Lcom/vividsolutions/jts/geomgraph/PlanarGraph;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->graph:Lcom/vividsolutions/jts/geomgraph/PlanarGraph;

    return-object v0
.end method

.method public getResultGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "funcCode"    # I

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->computeOverlay(I)V

    .line 128
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultGeom:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method protected insertUniqueEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)V
    .locals 5
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;

    .prologue
    .line 220
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->findEqualEdge(Lcom/vividsolutions/jts/geomgraph/Edge;)Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v1

    .line 223
    .local v1, "existingEdge":Lcom/vividsolutions/jts/geomgraph/Edge;
    if-eqz v1, :cond_2

    .line 224
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v2

    .line 226
    .local v2, "existingLabel":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    .line 229
    .local v3, "labelToMerge":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->isPointwiseEqual(Lcom/vividsolutions/jts/geomgraph/Edge;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 230
    new-instance v3, Lcom/vividsolutions/jts/geomgraph/Label;

    .end local v3    # "labelToMerge":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 231
    .restart local v3    # "labelToMerge":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/Label;->flip()V

    .line 233
    :cond_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getDepth()Lcom/vividsolutions/jts/geomgraph/Depth;

    move-result-object v0

    .line 236
    .local v0, "depth":Lcom/vividsolutions/jts/geomgraph/Depth;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Depth;->isNull()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 237
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geomgraph/Depth;->add(Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 240
    :cond_1
    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geomgraph/Depth;->add(Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 241
    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geomgraph/Label;->merge(Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 252
    .end local v0    # "depth":Lcom/vividsolutions/jts/geomgraph/Depth;
    .end local v2    # "existingLabel":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v3    # "labelToMerge":Lcom/vividsolutions/jts/geomgraph/Label;
    :goto_0
    return-void

    .line 250
    :cond_2
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->edgeList:Lcom/vividsolutions/jts/geomgraph/EdgeList;

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeList;->add(Lcom/vividsolutions/jts/geomgraph/Edge;)V

    goto :goto_0
.end method

.method public isCoveredByA(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 519
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPolyList:Ljava/util/List;

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isCovered(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 520
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCoveredByLA(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x1

    .line 508
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultLineList:Ljava/util/List;

    invoke-direct {p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isCovered(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 510
    :cond_0
    :goto_0
    return v0

    .line 509
    :cond_1
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->resultPolyList:Ljava/util/List;

    invoke-direct {p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->isCovered(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 510
    const/4 v0, 0x0

    goto :goto_0
.end method
