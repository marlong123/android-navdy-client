.class public Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;
.super Ljava/lang/Object;
.source "LineStringSnapper.java"


# instance fields
.field private allowSnappingToSourceVertices:Z

.field private isClosed:Z

.field private seg:Lcom/vividsolutions/jts/geom/LineSegment;

.field private snapTolerance:D

.field private srcPts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineString;D)V
    .locals 2
    .param p1, "srcLine"    # Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "snapTolerance"    # D

    .prologue
    .line 66
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;D)V

    .line 67
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;D)V
    .locals 4
    .param p1, "srcPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "snapTolerance"    # D

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapTolerance:D

    .line 53
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 54
    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->allowSnappingToSourceVertices:Z

    .line 55
    iput-boolean v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->isClosed:Z

    .line 78
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->srcPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->isClosed([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->isClosed:Z

    .line 80
    iput-wide p2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapTolerance:D

    .line 81
    return-void
.end method

.method private findSegmentIndexToSnap(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/CoordinateList;)I
    .locals 8
    .param p1, "snapPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "srcCoords"    # Lcom/vividsolutions/jts/geom/CoordinateList;

    .prologue
    .line 209
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 210
    .local v4, "minDist":D
    const/4 v3, -0x1

    .line 211
    .local v3, "snapIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_3

    .line 212
    iget-object v7, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {p2, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v6, v7, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 213
    iget-object v7, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p2, v6}, Lcom/vividsolutions/jts/geom/CoordinateList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v6, v7, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 220
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v6, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v6, v6, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 221
    :cond_0
    iget-boolean v6, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->allowSnappingToSourceVertices:Z

    if-eqz v6, :cond_2

    .line 211
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 224
    :cond_2
    const/4 v3, -0x1

    .line 233
    .end local v3    # "snapIndex":I
    :cond_3
    return v3

    .line 227
    .restart local v3    # "snapIndex":I
    :cond_4
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->seg:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 228
    .local v0, "dist":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapTolerance:D

    cmpg-double v6, v0, v6

    if-gez v6, :cond_1

    cmpg-double v6, v0, v4

    if-gez v6, :cond_1

    .line 229
    move-wide v4, v0

    .line 230
    move v3, v2

    goto :goto_1
.end method

.method private findSnapForVertex(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v1, 0x0

    .line 136
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 138
    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    :cond_0
    :goto_1
    return-object v1

    .line 140
    :cond_1
    aget-object v2, p2, v0

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    iget-wide v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapTolerance:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_2

    .line 141
    aget-object v1, p2, v0

    goto :goto_1

    .line 136
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static isClosed([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 3
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 89
    array-length v1, p0

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    .line 90
    :goto_0
    return v0

    :cond_0
    aget-object v0, p0, v0

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p0, v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    goto :goto_0
.end method

.method private snapSegments(Lcom/vividsolutions/jts/geom/CoordinateList;[Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 7
    .param p1, "srcCoords"    # Lcom/vividsolutions/jts/geom/CoordinateList;
    .param p2, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v6, 0x0

    .line 163
    array-length v4, p2

    if-nez v4, :cond_1

    .line 185
    :cond_0
    return-void

    .line 165
    :cond_1
    array-length v0, p2

    .line 169
    .local v0, "distinctPtCount":I
    aget-object v4, p2, v6

    array-length v5, p2

    add-int/lit8 v5, v5, -0x1

    aget-object v5, p2, v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    array-length v4, p2

    add-int/lit8 v0, v4, -0x1

    .line 172
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 173
    aget-object v3, p2, v1

    .line 174
    .local v3, "snapPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v3, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->findSegmentIndexToSnap(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/CoordinateList;)I

    move-result v2

    .line 181
    .local v2, "index":I
    if-ltz v2, :cond_3

    .line 182
    add-int/lit8 v4, v2, 0x1

    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v5, v3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {p1, v4, v5, v6}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(ILcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 172
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private snapVertices(Lcom/vividsolutions/jts/geom/CoordinateList;[Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 6
    .param p1, "srcCoords"    # Lcom/vividsolutions/jts/geom/CoordinateList;
    .param p2, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 120
    iget-boolean v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->isClosed:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 121
    .local v0, "end":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 122
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/CoordinateList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 123
    .local v3, "srcPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v3, p2}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->findSnapForVertex(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 124
    .local v2, "snapVert":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v2, :cond_0

    .line 126
    new-instance v4, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v4, v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {p1, v1, v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 128
    if-nez v1, :cond_0

    iget-boolean v4, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->isClosed:Z

    if-eqz v4, :cond_0

    .line 129
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v5, v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {p1, v4, v5}, Lcom/vividsolutions/jts/geom/CoordinateList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 121
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    .end local v0    # "end":I
    .end local v1    # "i":I
    .end local v2    # "snapVert":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "srcPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v0

    goto :goto_0

    .line 132
    .restart local v0    # "end":I
    .restart local v1    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method public setAllowSnappingToSourceVertices(Z)V
    .locals 0
    .param p1, "allowSnappingToSourceVertices"    # Z

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->allowSnappingToSourceVertices:Z

    .line 86
    return-void
.end method

.method public snapTo([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "snapPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 101
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->srcPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 103
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapVertices(Lcom/vividsolutions/jts/geom/CoordinateList;[Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 104
    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/operation/overlay/snap/LineStringSnapper;->snapSegments(Lcom/vividsolutions/jts/geom/CoordinateList;[Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 106
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 107
    .local v1, "newPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v1
.end method
