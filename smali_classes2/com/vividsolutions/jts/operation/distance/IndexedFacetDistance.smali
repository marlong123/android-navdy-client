.class public Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;
.super Ljava/lang/Object;
.source "IndexedFacetDistance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$1;,
        Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$FacetSequenceDistance;
    }
.end annotation


# instance fields
.field private cachedTree:Lcom/vividsolutions/jts/index/strtree/STRtree;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->build(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/index/strtree/STRtree;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;->cachedTree:Lcom/vividsolutions/jts/index/strtree/STRtree;

    .line 109
    return-void
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 88
    new-instance v0, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 89
    .local v0, "dist":Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;->getDistance(Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v2

    return-wide v2
.end method

.method private static facetDistance([Ljava/lang/Object;)D
    .locals 4
    .param p0, "obj"    # [Ljava/lang/Object;

    .prologue
    .line 129
    const/4 v2, 0x0

    aget-object v0, p0, v2

    .line 130
    .local v0, "o1":Ljava/lang/Object;
    const/4 v2, 0x1

    aget-object v1, p0, v2

    .line 131
    .local v1, "o2":Ljava/lang/Object;
    check-cast v0, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .end local v0    # "o1":Ljava/lang/Object;
    check-cast v1, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    .end local v1    # "o2":Ljava/lang/Object;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/distance/FacetSequence;->distance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v2

    return-wide v2
.end method


# virtual methods
.method public getDistance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 5
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 121
    invoke-static {p1}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->build(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/index/strtree/STRtree;

    move-result-object v1

    .line 122
    .local v1, "tree2":Lcom/vividsolutions/jts/index/strtree/STRtree;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;->cachedTree:Lcom/vividsolutions/jts/index/strtree/STRtree;

    new-instance v3, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$FacetSequenceDistance;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$FacetSequenceDistance;-><init>(Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance$1;)V

    invoke-virtual {v2, v1, v3}, Lcom/vividsolutions/jts/index/strtree/STRtree;->nearestNeighbour(Lcom/vividsolutions/jts/index/strtree/STRtree;Lcom/vividsolutions/jts/index/strtree/ItemDistance;)[Ljava/lang/Object;

    move-result-object v0

    .line 124
    .local v0, "obj":[Ljava/lang/Object;
    invoke-static {v0}, Lcom/vividsolutions/jts/operation/distance/IndexedFacetDistance;->facetDistance([Ljava/lang/Object;)D

    move-result-wide v2

    return-wide v2
.end method
