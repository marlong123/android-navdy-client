.class public Lcom/vividsolutions/jts/operation/GeometryGraphOperation;
.super Ljava/lang/Object;
.source "GeometryGraphOperation.java"


# instance fields
.field protected arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field protected final li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field protected resultPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v2, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 78
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->setComputationPrecision(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 80
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 81
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    new-instance v1, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v1, v2, p1}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;)V

    aput-object v1, v0, v2

    .line 82
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 58
    sget-object v0, Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;->OGC_SFS_BOUNDARY_RULE:Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    invoke-direct {p0, p1, p2, v0}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 4
    .param p1, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p3, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 67
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/PrecisionModel;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 68
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->setComputationPrecision(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 72
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 73
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    new-instance v1, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v1, v2, p1, p3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    aput-object v1, v0, v2

    .line 74
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    new-instance v1, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-direct {v1, v3, p2, p3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;-><init>(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    aput-object v1, v0, v3

    .line 75
    return-void

    .line 70
    :cond_0
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->setComputationPrecision(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    goto :goto_0
.end method


# virtual methods
.method public getArgGeometry(I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method protected setComputationPrecision(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 2
    .param p1, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->resultPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 89
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/GeometryGraphOperation;->resultPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->setPrecisionModel(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 90
    return-void
.end method
