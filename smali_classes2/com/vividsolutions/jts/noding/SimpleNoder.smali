.class public Lcom/vividsolutions/jts/noding/SimpleNoder;
.super Lcom/vividsolutions/jts/noding/SinglePassNoder;
.source "SimpleNoder.java"


# instance fields
.field private nodedSegStrings:Ljava/util/Collection;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/vividsolutions/jts/noding/SinglePassNoder;-><init>()V

    .line 54
    return-void
.end method

.method private computeIntersects(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V
    .locals 5
    .param p1, "e0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "e1"    # Lcom/vividsolutions/jts/noding/SegmentString;

    .prologue
    .line 75
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 76
    .local v2, "pts0":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p2}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 77
    .local v3, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i0":I
    :goto_0
    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 78
    const/4 v1, 0x0

    .local v1, "i1":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 79
    iget-object v4, p0, Lcom/vividsolutions/jts/noding/SimpleNoder;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    invoke-interface {v4, p1, v0, p2, v1}, Lcom/vividsolutions/jts/noding/SegmentIntersector;->processIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V

    .line 78
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    .end local v1    # "i1":I
    :cond_1
    return-void
.end method


# virtual methods
.method public computeNodes(Ljava/util/Collection;)V
    .locals 5
    .param p1, "inputSegStrings"    # Ljava/util/Collection;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/SimpleNoder;->nodedSegStrings:Ljava/util/Collection;

    .line 64
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i0":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 66
    .local v0, "edge0":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i1":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 67
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 68
    .local v1, "edge1":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/noding/SimpleNoder;->computeIntersects(Lcom/vividsolutions/jts/noding/SegmentString;Lcom/vividsolutions/jts/noding/SegmentString;)V

    goto :goto_0

    .line 71
    .end local v0    # "edge0":Lcom/vividsolutions/jts/noding/SegmentString;
    .end local v1    # "edge1":Lcom/vividsolutions/jts/noding/SegmentString;
    .end local v3    # "i1":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public getNodedSubstrings()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/SimpleNoder;->nodedSegStrings:Ljava/util/Collection;

    invoke-static {v0}, Lcom/vividsolutions/jts/noding/NodedSegmentString;->getNodedSubstrings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
