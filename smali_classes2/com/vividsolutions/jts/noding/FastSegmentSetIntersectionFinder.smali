.class public Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;
.super Ljava/lang/Object;
.source "FastSegmentSetIntersectionFinder.java"


# static fields
.field private static li:Lcom/vividsolutions/jts/algorithm/LineIntersector;


# instance fields
.field private segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0
    .param p1, "baseSegStrings"    # Ljava/util/Collection;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->init(Ljava/util/Collection;)V

    .line 56
    return-void
.end method

.method private init(Ljava/util/Collection;)V
    .locals 1
    .param p1, "baseSegStrings"    # Ljava/util/Collection;

    .prologue
    .line 60
    new-instance v0, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/noding/MCIndexSegmentSetMutualIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    .line 64
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->setBaseSegments(Ljava/util/Collection;)V

    .line 65
    return-void
.end method


# virtual methods
.method public getSegmentSetIntersector()Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    return-object v0
.end method

.method public intersects(Ljava/util/Collection;)Z
    .locals 2
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 82
    new-instance v0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;

    sget-object v1, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;-><init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V

    .line 83
    .local v0, "intFinder":Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->setSegmentIntersector(Lcom/vividsolutions/jts/noding/SegmentIntersector;)V

    .line 85
    iget-object v1, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->process(Ljava/util/Collection;)V

    .line 86
    invoke-virtual {v0}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection()Z

    move-result v1

    return v1
.end method

.method public intersects(Ljava/util/Collection;Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;)Z
    .locals 1
    .param p1, "segStrings"    # Ljava/util/Collection;
    .param p2, "intDetector"    # Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->setSegmentIntersector(Lcom/vividsolutions/jts/noding/SegmentIntersector;)V

    .line 93
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->segSetMutInt:Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->process(Ljava/util/Collection;)V

    .line 94
    invoke-virtual {p2}, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection()Z

    move-result v0

    return v0
.end method
