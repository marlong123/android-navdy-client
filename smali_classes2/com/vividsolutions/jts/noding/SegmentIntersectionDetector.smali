.class public Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;
.super Ljava/lang/Object;
.source "SegmentIntersectionDetector.java"

# interfaces
.implements Lcom/vividsolutions/jts/noding/SegmentIntersector;


# instance fields
.field private findAllTypes:Z

.field private findProper:Z

.field private hasIntersection:Z

.field private hasNonProperIntersection:Z

.field private hasProperIntersection:Z

.field private intPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/LineIntersector;)V
    .locals 2
    .param p1, "li"    # Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findProper:Z

    .line 56
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findAllTypes:Z

    .line 58
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection:Z

    .line 59
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection:Z

    .line 60
    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasNonProperIntersection:Z

    .line 62
    iput-object v1, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 63
    iput-object v1, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 72
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 73
    return-void
.end method


# virtual methods
.method public getIntersection()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getIntersectionSegments()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public hasIntersection()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection:Z

    return v0
.end method

.method public hasNonProperIntersection()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasNonProperIntersection:Z

    return v0
.end method

.method public hasProperIntersection()Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findAllTypes:Z

    if-eqz v0, :cond_1

    .line 203
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasNonProperIntersection:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 212
    :goto_0
    return v0

    .line 203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findProper:Z

    if-eqz v0, :cond_2

    .line 210
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection:Z

    goto :goto_0

    .line 212
    :cond_2
    iget-boolean v0, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection:Z

    goto :goto_0
.end method

.method public processIntersections(Lcom/vividsolutions/jts/noding/SegmentString;ILcom/vividsolutions/jts/noding/SegmentString;I)V
    .locals 10
    .param p1, "e0"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p2, "segIndex0"    # I
    .param p3, "e1"    # Lcom/vividsolutions/jts/noding/SegmentString;
    .param p4, "segIndex1"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 151
    if-ne p1, p3, :cond_1

    if-ne p2, p4, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    aget-object v1, v6, p2

    .line 154
    .local v1, "p00":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p1}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    add-int/lit8 v7, p2, 0x1

    aget-object v2, v6, v7

    .line 155
    .local v2, "p01":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    aget-object v3, v6, p4

    .line 156
    .local v3, "p10":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p3}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    add-int/lit8 v7, p4, 0x1

    aget-object v4, v6, v7

    .line 158
    .local v4, "p11":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v6, v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 161
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 165
    iput-boolean v8, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasIntersection:Z

    .line 167
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v6}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isProper()Z

    move-result v0

    .line 168
    .local v0, "isProper":Z
    if-eqz v0, :cond_2

    .line 169
    iput-boolean v8, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasProperIntersection:Z

    .line 170
    :cond_2
    if-nez v0, :cond_3

    .line 171
    iput-boolean v8, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->hasNonProperIntersection:Z

    .line 178
    :cond_3
    const/4 v5, 0x1

    .line 179
    .local v5, "saveLocation":Z
    iget-boolean v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findProper:Z

    if-eqz v6, :cond_4

    if-nez v0, :cond_4

    const/4 v5, 0x0

    .line 181
    :cond_4
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intPt:Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v6, :cond_5

    if-eqz v5, :cond_0

    .line 184
    :cond_5
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v6, v9}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    iput-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 187
    const/4 v6, 0x4

    new-array v6, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 188
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v1, v6, v9

    .line 189
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v2, v6, v8

    .line 190
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x2

    aput-object v3, v6, v7

    .line 191
    iget-object v6, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->intSegments:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v7, 0x3

    aput-object v4, v6, v7

    goto :goto_0
.end method

.method public setFindAllIntersectionTypes(Z)V
    .locals 0
    .param p1, "findAllTypes"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findAllTypes:Z

    .line 83
    return-void
.end method

.method public setFindProper(Z)V
    .locals 0
    .param p1, "findProper"    # Z

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/SegmentIntersectionDetector;->findProper:Z

    .line 78
    return-void
.end method
