.class public Lcom/vividsolutions/jts/index/bintree/Key;
.super Ljava/lang/Object;
.source "Key.java"


# instance fields
.field private interval:Lcom/vividsolutions/jts/index/bintree/Interval;

.field private level:I

.field private pt:D


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/index/bintree/Interval;)V
    .locals 2
    .param p1, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->pt:D

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    .line 66
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/bintree/Key;->computeKey(Lcom/vividsolutions/jts/index/bintree/Interval;)V

    .line 67
    return-void
.end method

.method private computeInterval(ILcom/vividsolutions/jts/index/bintree/Interval;)V
    .locals 8
    .param p1, "level"    # I
    .param p2, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 91
    invoke-static {p1}, Lcom/vividsolutions/jts/index/quadtree/DoubleBits;->powerOf2(I)D

    move-result-wide v0

    .line 93
    .local v0, "size":D
    invoke-virtual {p2}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMin()D

    move-result-wide v2

    div-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Key;->pt:D

    .line 94
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/Key;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    iget-wide v4, p0, Lcom/vividsolutions/jts/index/bintree/Key;->pt:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/index/bintree/Key;->pt:D

    add-double/2addr v6, v0

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/vividsolutions/jts/index/bintree/Interval;->init(DD)V

    .line 95
    return-void
.end method

.method public static computeLevel(Lcom/vividsolutions/jts/index/bintree/Interval;)I
    .locals 4
    .param p0, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/bintree/Interval;->getWidth()D

    move-result-wide v0

    .line 53
    .local v0, "dx":D
    invoke-static {v0, v1}, Lcom/vividsolutions/jts/index/quadtree/DoubleBits;->exponent(D)I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 54
    .local v2, "level":I
    return v2
.end method


# virtual methods
.method public computeKey(Lcom/vividsolutions/jts/index/bintree/Interval;)V
    .locals 1
    .param p1, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 79
    invoke-static {p1}, Lcom/vividsolutions/jts/index/bintree/Key;->computeLevel(Lcom/vividsolutions/jts/index/bintree/Interval;)I

    move-result v0

    iput v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    .line 80
    new-instance v0, Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/bintree/Interval;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    .line 81
    iget v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/index/bintree/Key;->computeInterval(ILcom/vividsolutions/jts/index/bintree/Interval;)V

    .line 83
    :goto_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/bintree/Interval;->contains(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    .line 85
    iget v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/index/bintree/Key;->computeInterval(ILcom/vividsolutions/jts/index/bintree/Interval;)V

    goto :goto_0

    .line 87
    :cond_0
    return-void
.end method

.method public getInterval()Lcom/vividsolutions/jts/index/bintree/Interval;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->interval:Lcom/vividsolutions/jts/index/bintree/Interval;

    return-object v0
.end method

.method public getLevel()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->level:I

    return v0
.end method

.method public getPoint()D
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/bintree/Key;->pt:D

    return-wide v0
.end method
