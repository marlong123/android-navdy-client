.class Lcom/vividsolutions/jts/index/strtree/SIRtree$2;
.super Ljava/lang/Object;
.source "SIRtree.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/strtree/AbstractSTRtree$IntersectsOp;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/index/strtree/SIRtree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/index/strtree/SIRtree;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/index/strtree/SIRtree;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/vividsolutions/jts/index/strtree/SIRtree$2;->this$0:Lcom/vividsolutions/jts/index/strtree/SIRtree;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public intersects(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "aBounds"    # Ljava/lang/Object;
    .param p2, "bBounds"    # Ljava/lang/Object;

    .prologue
    .line 61
    check-cast p1, Lcom/vividsolutions/jts/index/strtree/Interval;

    .end local p1    # "aBounds":Ljava/lang/Object;
    check-cast p2, Lcom/vividsolutions/jts/index/strtree/Interval;

    .end local p2    # "bBounds":Ljava/lang/Object;
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/index/strtree/Interval;->intersects(Lcom/vividsolutions/jts/index/strtree/Interval;)Z

    move-result v0

    return v0
.end method
