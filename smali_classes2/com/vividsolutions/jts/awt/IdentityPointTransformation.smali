.class public Lcom/vividsolutions/jts/awt/IdentityPointTransformation;
.super Ljava/lang/Object;
.source "IdentityPointTransformation.java"

# interfaces
.implements Lcom/vividsolutions/jts/awt/PointTransformation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transform(Lcom/vividsolutions/jts/geom/Coordinate;Ljava/awt/geom/Point2D;)V
    .locals 4
    .param p1, "model"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "view"    # Ljava/awt/geom/Point2D;

    .prologue
    .line 49
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p2, v0, v1, v2, v3}, Ljava/awt/geom/Point2D;->setLocation(DD)V

    .line 50
    return-void
.end method
