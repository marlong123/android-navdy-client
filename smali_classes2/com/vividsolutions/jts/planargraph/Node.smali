.class public Lcom/vividsolutions/jts/planargraph/Node;
.super Lcom/vividsolutions/jts/planargraph/GraphComponent;
.source "Node.java"


# instance fields
.field protected deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

.field protected pt:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 76
    new-instance v0, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    invoke-direct {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/planargraph/Node;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;)V
    .locals 0
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "deStar"    # Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/vividsolutions/jts/planargraph/GraphComponent;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/vividsolutions/jts/planargraph/Node;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 85
    iput-object p2, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    .line 86
    return-void
.end method

.method public static getEdgesBetween(Lcom/vividsolutions/jts/planargraph/Node;Lcom/vividsolutions/jts/planargraph/Node;)Ljava/util/Collection;
    .locals 4
    .param p0, "node0"    # Lcom/vividsolutions/jts/planargraph/Node;
    .param p1, "node1"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->toEdges(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 59
    .local v1, "edges0":Ljava/util/List;
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 60
    .local v0, "commonEdges":Ljava/util/Set;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getEdges()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/vividsolutions/jts/planargraph/DirectedEdge;->toEdges(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 61
    .local v2, "edges1":Ljava/util/List;
    invoke-interface {v0, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 62
    return-object v0
.end method


# virtual methods
.method public addOutEdge(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->add(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 99
    return-void
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getDegree()I
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getDegree()I

    move-result v0

    return v0
.end method

.method public getIndex(Lcom/vividsolutions/jts/planargraph/Edge;)I
    .locals 1
    .param p1, "edge"    # Lcom/vividsolutions/jts/planargraph/Edge;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->getIndex(Lcom/vividsolutions/jts/planargraph/Edge;)I

    move-result v0

    return v0
.end method

.method public getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    return-object v0
.end method

.method public isRemoved()Z
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method remove()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 132
    return-void
.end method

.method public remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V
    .locals 1
    .param p1, "de"    # Lcom/vividsolutions/jts/planargraph/DirectedEdge;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/vividsolutions/jts/planargraph/Node;->deStar:Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->remove(Lcom/vividsolutions/jts/planargraph/DirectedEdge;)V

    .line 125
    return-void
.end method
