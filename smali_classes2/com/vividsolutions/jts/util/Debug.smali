.class public Lcom/vividsolutions/jts/util/Debug;
.super Ljava/lang/Object;
.source "Debug.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;
    }
.end annotation


# static fields
.field private static final DEBUG_LINE_TAG:Ljava/lang/String; = "D! "

.field public static DEBUG_PROPERTY_NAME:Ljava/lang/String;

.field public static DEBUG_PROPERTY_VALUE_ON:Ljava/lang/String;

.field public static DEBUG_PROPERTY_VALUE_TRUE:Ljava/lang/String;

.field private static final debug:Lcom/vividsolutions/jts/util/Debug;

.field private static debugOn:Z

.field private static final fact:Lcom/vividsolutions/jts/geom/GeometryFactory;


# instance fields
.field private args:[Ljava/lang/Object;

.field private out:Ljava/io/PrintStream;

.field private printArgs:[Ljava/lang/Class;

.field private watchObj:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    const-string v1, "jts.debug"

    sput-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_NAME:Ljava/lang/String;

    .line 60
    const-string v1, "on"

    sput-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_VALUE_ON:Ljava/lang/String;

    .line 61
    const-string v1, "true"

    sput-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_VALUE_TRUE:Ljava/lang/String;

    .line 63
    const/4 v1, 0x0

    sput-boolean v1, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    .line 66
    sget-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_NAME:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "debugValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 68
    sget-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_VALUE_ON:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/vividsolutions/jts/util/Debug;->DEBUG_PROPERTY_VALUE_TRUE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    .line 86
    :cond_1
    new-instance v1, Lcom/vividsolutions/jts/util/Debug;

    invoke-direct {v1}, Lcom/vividsolutions/jts/util/Debug;-><init>()V

    sput-object v1, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    .line 87
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    sput-object v1, Lcom/vividsolutions/jts/util/Debug;->fact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    .line 93
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->args:[Ljava/lang/Object;

    .line 225
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    .line 226
    new-array v0, v1, [Ljava/lang/Class;

    iput-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->printArgs:[Ljava/lang/Class;

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->printArgs:[Ljava/lang/Class;

    const/4 v1, 0x0

    const-string v2, "java.io.PrintStream"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static addWatch(Ljava/lang/Object;)V
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 153
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/util/Debug;->instanceAddWatch(Ljava/lang/Object;)V

    .line 154
    return-void
.end method

.method public static breakIf(Z)V
    .locals 0
    .param p0, "cond"    # Z

    .prologue
    .line 166
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/vividsolutions/jts/util/Debug;->doBreak()V

    .line 167
    :cond_0
    return-void
.end method

.method public static breakIfEqual(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)V
    .locals 2
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "tolerance"    # D

    .prologue
    .line 176
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    cmpg-double v0, v0, p2

    if-gtz v0, :cond_0

    invoke-static {}, Lcom/vividsolutions/jts/util/Debug;->doBreak()V

    .line 177
    :cond_0
    return-void
.end method

.method public static breakIfEqual(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p0, "o1"    # Ljava/lang/Object;
    .param p1, "o2"    # Ljava/lang/Object;

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/vividsolutions/jts/util/Debug;->doBreak()V

    .line 172
    :cond_0
    return-void
.end method

.method private static doBreak()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public static equals(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)Z
    .locals 2
    .param p0, "c1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "c2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "tolerance"    # D

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    cmpg-double v0, v0, p2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasSegment(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 187
    new-instance v0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;

    invoke-direct {v0, p1, p2}, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 188
    .local v0, "filter":Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 189
    invoke-virtual {v0}, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->hasSegment()Z

    move-result v1

    return v1
.end method

.method private instanceAddWatch(Ljava/lang/Object;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    .line 293
    return-void
.end method

.method private instancePrint(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 296
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    const-string v1, "D! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 297
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method public static isDebugging()Z
    .locals 1

    .prologue
    .line 95
    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    return v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 3
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 82
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JTS Debugging is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    if-eqz v0, :cond_0

    const-string v0, "ON"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 84
    return-void

    .line 82
    :cond_0
    const-string v0, "OFF"

    goto :goto_0
.end method

.method public static print(Ljava/lang/Object;)V
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 123
    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 124
    :cond_0
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static print(Ljava/lang/String;)V
    .locals 1
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 110
    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 113
    :cond_0
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static print(ZLjava/lang/Object;)V
    .locals 1
    .param p0, "isTrue"    # Z
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 128
    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    if-eqz p0, :cond_0

    .line 130
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static printIfWatch(Ljava/lang/Object;)V
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 161
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/util/Debug;->instancePrintIfWatch(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public static printWatch()V
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/util/Debug;->instancePrintWatch()V

    .line 158
    return-void
.end method

.method public static println(Ljava/lang/Object;)V
    .locals 1
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 134
    sget-boolean v0, Lcom/vividsolutions/jts/util/Debug;->debugOn:Z

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/Object;)V

    .line 138
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->debug:Lcom/vividsolutions/jts/util/Debug;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/util/Debug;->println()V

    goto :goto_0
.end method

.method public static toLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 3
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 98
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->fact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public static toLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 3
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 102
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->fact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public static toLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 3
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p3"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 106
    sget-object v0, Lcom/vividsolutions/jts/util/Debug;->fact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    const/4 v2, 0x3

    aput-object p3, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public instancePrint(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 249
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    .line 250
    check-cast p1, Ljava/util/Collection;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/util/Iterator;)V

    .line 258
    :goto_0
    return-void

    .line 252
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Ljava/util/Iterator;

    if-eqz v0, :cond_1

    .line 253
    check-cast p1, Ljava/util/Iterator;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/util/Iterator;)V

    goto :goto_0

    .line 256
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/util/Debug;->instancePrintObject(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public instancePrint(Ljava/util/Iterator;)V
    .locals 2
    .param p1, "it"    # Ljava/util/Iterator;

    .prologue
    .line 262
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 264
    .local v0, "obj":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/util/Debug;->instancePrintObject(Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    .end local v0    # "obj":Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public instancePrintIfWatch(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    if-eq p1, v0, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public instancePrintObject(Ljava/lang/Object;)V
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 269
    const/4 v2, 0x0

    .line 271
    .local v2, "printMethod":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 273
    .local v0, "cls":Ljava/lang/Class;
    :try_start_1
    const-string v3, "print"

    iget-object v4, p0, Lcom/vividsolutions/jts/util/Debug;->printArgs:[Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 274
    iget-object v3, p0, Lcom/vividsolutions/jts/util/Debug;->args:[Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    aput-object v5, v3, v4

    .line 275
    iget-object v3, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    const-string v4, "D! "

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 276
    iget-object v3, p0, Lcom/vividsolutions/jts/util/Debug;->args:[Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    .end local v0    # "cls":Ljava/lang/Class;
    :goto_0
    return-void

    .line 278
    .restart local v0    # "cls":Ljava/lang/Class;
    :catch_0
    move-exception v1

    .line 279
    .local v1, "ex":Ljava/lang/NoSuchMethodException;
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 282
    .end local v0    # "cls":Ljava/lang/Class;
    .end local v1    # "ex":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 283
    .local v1, "ex":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v3}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V

    goto :goto_0
.end method

.method public instancePrintWatch()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->watchObj:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/util/Debug;->instancePrint(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public println()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 289
    return-void
.end method
