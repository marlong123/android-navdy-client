.class Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;
.super Ljava/lang/Object;
.source "Debug.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/util/Debug;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SegmentFindingFilter"
.end annotation


# instance fields
.field private hasSegment:Z

.field private p0:Lcom/vividsolutions/jts/geom/Coordinate;

.field private p1:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->hasSegment:Z

    .line 200
    iput-object p1, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 201
    iput-object p2, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 202
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 2
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "i"    # I

    .prologue
    .line 208
    if-nez p2, :cond_0

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {p1, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {p1, p2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->hasSegment:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hasSegment()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->hasSegment:Z

    return v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/vividsolutions/jts/util/Debug$SegmentFindingFilter;->hasSegment:Z

    return v0
.end method

.method public isGeometryChanged()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method
