.class Lcom/vividsolutions/jts/simplify/LineSegmentIndex;
.super Ljava/lang/Object;
.source "LineSegmentIndex.java"


# instance fields
.field private index:Lcom/vividsolutions/jts/index/quadtree/Quadtree;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    invoke-direct {v0}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->index:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    .line 53
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 4
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->index:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v2, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {v0, v1, p1}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public add(Lcom/vividsolutions/jts/simplify/TaggedLineString;)V
    .locals 4
    .param p1, "line"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getSegments()[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    move-result-object v2

    .line 57
    .local v2, "segs":[Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 58
    aget-object v1, v2, v0

    .line 59
    .local v1, "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->add(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    :cond_0
    return-void
.end method

.method public query(Lcom/vividsolutions/jts/geom/LineSegment;)Ljava/util/List;
    .locals 5
    .param p1, "querySeg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 75
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, v3, v4}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 77
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    new-instance v2, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;

    invoke-direct {v2, p1}, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;-><init>(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 78
    .local v2, "visitor":Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->index:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    invoke-virtual {v3, v0, v2}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->query(Lcom/vividsolutions/jts/geom/Envelope;Lcom/vividsolutions/jts/index/ItemVisitor;)V

    .line 79
    invoke-virtual {v2}, Lcom/vividsolutions/jts/simplify/LineSegmentVisitor;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 86
    .local v1, "itemsFound":Ljava/util/List;
    return-object v1
.end method

.method public remove(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 4
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->index:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v2, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {v0, v1, p1}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->remove(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)Z

    .line 71
    return-void
.end method
