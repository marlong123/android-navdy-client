.class Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;
.super Ljava/lang/Object;
.source "TopologyPreservingSimplifier.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LineStringMapBuilderFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;


# direct methods
.method constructor <init>(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;->this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 169
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 170
    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 171
    .local v0, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x4

    .line 173
    .local v1, "minSize":I
    :goto_0
    new-instance v2, Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-direct {v2, v0, v1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;-><init>(Lcom/vividsolutions/jts/geom/LineString;I)V

    .line 174
    .local v2, "taggedLine":Lcom/vividsolutions/jts/simplify/TaggedLineString;
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier$LineStringMapBuilderFilter;->this$0:Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;

    invoke-static {v3}, Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;->access$000(Lcom/vividsolutions/jts/simplify/TopologyPreservingSimplifier;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    .end local v0    # "line":Lcom/vividsolutions/jts/geom/LineString;
    .end local v1    # "minSize":I
    .end local v2    # "taggedLine":Lcom/vividsolutions/jts/simplify/TaggedLineString;
    :cond_0
    return-void

    .line 171
    .restart local v0    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_1
    const/4 v1, 0x2

    goto :goto_0
.end method
