.class public Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;
.super Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;
.source "SimpleSweepLineIntersector.java"


# instance fields
.field events:Ljava/util/List;

.field nOverlaps:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/index/EdgeSetIntersector;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    .line 62
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V
    .locals 14
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "edgeSet"    # Ljava/lang/Object;

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v13

    .line 100
    .local v13, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_0
    array-length v1, v13

    add-int/lit8 v1, v1, -0x1

    if-ge v12, v1, :cond_0

    .line 101
    new-instance v5, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;

    invoke-direct {v5, p1, v12}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;I)V

    .line 102
    .local v5, "ss":Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->getMinX()D

    move-result-wide v2

    const/4 v4, 0x0

    move-object/from16 v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;-><init>(Ljava/lang/Object;DLcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Ljava/lang/Object;)V

    .line 103
    .local v0, "insertEvent":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v1, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    new-instance v6, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->getMaxX()D

    move-result-wide v8

    move-object/from16 v7, p2

    move-object v10, v0

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;-><init>(Ljava/lang/Object;DLcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Ljava/lang/Object;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "insertEvent":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .end local v5    # "ss":Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    :cond_0
    return-void
.end method

.method private add(Ljava/util/List;)V
    .locals 3
    .param p1, "edges"    # Ljava/util/List;

    .prologue
    .line 82
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 85
    .local v0, "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-direct {p0, v0, v0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    .end local v0    # "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method private add(Ljava/util/List;Ljava/lang/Object;)V
    .locals 3
    .param p1, "edges"    # Ljava/util/List;
    .param p2, "edgeSet"    # Ljava/lang/Object;

    .prologue
    .line 90
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 92
    .local v0, "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-direct {p0, v0, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/lang/Object;)V

    goto :goto_0

    .line 94
    .end local v0    # "edge":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-void
.end method

.method private computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 3
    .param p1, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 127
    const/4 v2, 0x0

    iput v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->nOverlaps:I

    .line 128
    invoke-direct {p0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->prepareEvents()V

    .line 130
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 133
    .local v0, "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isInsert()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getDeleteEventIndex()I

    move-result v2

    invoke-direct {p0, v1, v2, v0, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->processOverlaps(IILcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 130
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_1
    return-void
.end method

.method private prepareEvents()V
    .locals 3

    .prologue
    .line 115
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 116
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 119
    .local v0, "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isDelete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 120
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getInsertEvent()Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->setDeleteEventIndex(I)V

    .line 116
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 123
    .end local v0    # "ev":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_1
    return-void
.end method

.method private processOverlaps(IILcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "ev0"    # Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    .param p4, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 141
    invoke-virtual {p3}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getObject()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;

    .line 147
    .local v2, "ss0":Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    move v1, p1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_2

    .line 148
    iget-object v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->events:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;

    .line 149
    .local v0, "ev1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->isInsert()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 150
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->getObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;

    .line 151
    .local v3, "ss1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    iget-object v4, p3, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    if-eqz v4, :cond_0

    iget-object v4, p3, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    iget-object v5, v0, Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;->edgeSet:Ljava/lang/Object;

    if-eq v4, v5, :cond_1

    .line 152
    :cond_0
    invoke-virtual {v2, v3, p4}, Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 153
    iget v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->nOverlaps:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->nOverlaps:I

    .line 147
    .end local v3    # "ss1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineSegment;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    .end local v0    # "ev1":Lcom/vividsolutions/jts/geomgraph/index/SweepLineEvent;
    :cond_2
    return-void
.end method


# virtual methods
.method public computeIntersections(Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Z)V
    .locals 1
    .param p1, "edges"    # Ljava/util/List;
    .param p2, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .param p3, "testAllSegments"    # Z

    .prologue
    .line 66
    if-eqz p3, :cond_0

    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 70
    :goto_0
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 71
    return-void

    .line 69
    :cond_0
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Ljava/util/List;)V

    goto :goto_0
.end method

.method public computeIntersections(Ljava/util/List;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V
    .locals 0
    .param p1, "edges0"    # Ljava/util/List;
    .param p2, "edges1"    # Ljava/util/List;
    .param p3, "si"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .prologue
    .line 75
    invoke-direct {p0, p1, p1}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 76
    invoke-direct {p0, p2, p2}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->add(Ljava/util/List;Ljava/lang/Object;)V

    .line 77
    invoke-direct {p0, p3}, Lcom/vividsolutions/jts/geomgraph/index/SimpleSweepLineIntersector;->computeIntersections(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;)V

    .line 78
    return-void
.end method
