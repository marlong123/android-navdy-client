.class public Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;
.super Ljava/lang/Object;
.source "SimpleGeometryPrecisionReducer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$1;,
        Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;
    }
.end annotation


# instance fields
.field private changePrecisionModel:Z

.field private newPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

.field private removeCollapsed:Z


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 1
    .param p1, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->removeCollapsed:Z

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->changePrecisionModel:Z

    .line 78
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->newPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 79
    return-void
.end method

.method static synthetic access$100(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)Lcom/vividsolutions/jts/geom/PrecisionModel;
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->newPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->removeCollapsed:Z

    return v0
.end method

.method public static reduce(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "precModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 68
    new-instance v0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 69
    .local v0, "reducer":Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->reduce(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public reduce(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 112
    iget-boolean v2, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->changePrecisionModel:Z

    if-eqz v2, :cond_0

    .line 113
    new-instance v1, Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->newPrecisionModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getSRID()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    .line 114
    .local v1, "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 120
    .end local v1    # "newFactory":Lcom/vividsolutions/jts/geom/GeometryFactory;
    .local v0, "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    :goto_0
    new-instance v2, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$PrecisionReducerCoordinateOperation;-><init>(Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer$1;)V

    invoke-virtual {v0, p1, v2}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;->edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    return-object v2

    .line 118
    .end local v0    # "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryEditor;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/util/GeometryEditor;-><init>()V

    .restart local v0    # "geomEdit":Lcom/vividsolutions/jts/geom/util/GeometryEditor;
    goto :goto_0
.end method

.method public setChangePrecisionModel(Z)V
    .locals 0
    .param p1, "changePrecisionModel"    # Z

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->changePrecisionModel:Z

    .line 107
    return-void
.end method

.method public setRemoveCollapsedComponents(Z)V
    .locals 0
    .param p1, "removeCollapsed"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/vividsolutions/jts/precision/SimpleGeometryPrecisionReducer;->removeCollapsed:Z

    .line 92
    return-void
.end method
