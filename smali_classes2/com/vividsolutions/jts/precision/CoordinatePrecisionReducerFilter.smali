.class public Lcom/vividsolutions/jts/precision/CoordinatePrecisionReducerFilter;
.super Ljava/lang/Object;
.source "CoordinatePrecisionReducerFilter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;


# instance fields
.field private precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 0
    .param p1, "precModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/CoordinatePrecisionReducerFilter;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 62
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 6
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "i"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 69
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CoordinatePrecisionReducerFilter;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-interface {p1, p2, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(D)D

    move-result-wide v0

    invoke-interface {p1, p2, v4, v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 70
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/CoordinatePrecisionReducerFilter;->precModel:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-interface {p1, p2, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(D)D

    move-result-wide v0

    invoke-interface {p1, p2, v5, v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->setOrdinate(IID)V

    .line 71
    return-void
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public isGeometryChanged()Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method
