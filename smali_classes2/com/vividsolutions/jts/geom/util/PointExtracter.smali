.class public Lcom/vividsolutions/jts/geom/util/PointExtracter;
.super Ljava/lang/Object;
.source "PointExtracter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryFilter;


# instance fields
.field private pts:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "pts"    # Ljava/util/List;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/PointExtracter;->pts:Ljava/util/List;

    .line 87
    return-void
.end method

.method public static getPoints(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, v0}, Lcom/vividsolutions/jts/geom/util/PointExtracter;->getPoints(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getPoints(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "list"    # Ljava/util/List;

    .prologue
    .line 58
    instance-of v0, p0, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_1

    .line 59
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_0
    :goto_0
    return-object p1

    .line 61
    :cond_1
    instance-of v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_0

    .line 62
    new-instance v0, Lcom/vividsolutions/jts/geom/util/PointExtracter;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/util/PointExtracter;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    goto :goto_0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 91
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/util/PointExtracter;->pts:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    return-void
.end method
