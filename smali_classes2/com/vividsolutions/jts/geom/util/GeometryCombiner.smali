.class public Lcom/vividsolutions/jts/geom/util/GeometryCombiner;
.super Ljava/lang/Object;
.source "GeometryCombiner.java"


# instance fields
.field private geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private inputGeoms:Ljava/util/Collection;

.field private skipEmpty:Z


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->skipEmpty:Z

    .line 135
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->extractFactory(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 136
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->inputGeoms:Ljava/util/Collection;

    .line 137
    return-void
.end method

.method public static combine(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 75
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;

    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->createList(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;-><init>(Ljava/util/Collection;)V

    .line 76
    .local v0, "combiner":Lcom/vividsolutions/jts/geom/util/GeometryCombiner;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static combine(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g0"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "g1"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "g2"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 89
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;

    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->createList(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;-><init>(Ljava/util/Collection;)V

    .line 90
    .local v0, "combiner":Lcom/vividsolutions/jts/geom/util/GeometryCombiner;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static combine(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 62
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;-><init>(Ljava/util/Collection;)V

    .line 63
    .local v0, "combiner":Lcom/vividsolutions/jts/geom/util/GeometryCombiner;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->combine()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private static createList(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p0, "obj0"    # Ljava/lang/Object;
    .param p1, "obj1"    # Ljava/lang/Object;

    .prologue
    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v0, "list":Ljava/util/List;
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    return-object v0
.end method

.method private static createList(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p0, "obj0"    # Ljava/lang/Object;
    .param p1, "obj1"    # Ljava/lang/Object;
    .param p2, "obj2"    # Ljava/lang/Object;

    .prologue
    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "list":Ljava/util/List;
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    return-object v0
.end method

.method private extractElements(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "elems"    # Ljava/util/List;

    .prologue
    .line 178
    if-nez p1, :cond_1

    .line 187
    :cond_0
    return-void

    .line 181
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 182
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 183
    .local v0, "elemGeom":Lcom/vividsolutions/jts/geom/Geometry;
    iget-boolean v2, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->skipEmpty:Z

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 181
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_2
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static extractFactory(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/GeometryFactory;
    .locals 1
    .param p0, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 146
    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public combine()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 160
    .local v0, "elems":Ljava/util/List;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->inputGeoms:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Geometry;

    .line 162
    .local v1, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v1, v0}, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->extractElements(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)V

    goto :goto_0

    .line 165
    .end local v1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 166
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-eqz v4, :cond_1

    .line 168
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v3

    .line 173
    :cond_1
    :goto_1
    return-object v3

    :cond_2
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/util/GeometryCombiner;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    goto :goto_1
.end method
