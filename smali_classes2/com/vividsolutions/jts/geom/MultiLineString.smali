.class public Lcom/vividsolutions/jts/geom/MultiLineString;
.super Lcom/vividsolutions/jts/geom/GeometryCollection;
.source "MultiLineString.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/Lineal;


# static fields
.field private static final serialVersionUID:J = 0x7155d2ab4afa7f8dL


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "lineStrings"    # [Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 77
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/LineString;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "lineStrings"    # [Lcom/vividsolutions/jts/geom/LineString;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 63
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 64
    return-void
.end method


# virtual methods
.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    .line 138
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/MultiLineString;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryCollection;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v0

    goto :goto_0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/vividsolutions/jts/operation/BoundaryOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/BoundaryOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->getBoundary()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getBoundaryDimension()I
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiLineString;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, -0x1

    .line 87
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    return v0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    const-string v0, "MultiLineString"

    return-object v0
.end method

.method public isClosed()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiLineString;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 103
    :goto_0
    return v1

    .line 98
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/MultiLineString;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 99
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/MultiLineString;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    .line 100
    goto :goto_0

    .line 98
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 103
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public reverse()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5

    .prologue
    .line 129
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiLineString;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v3

    .line 130
    .local v1, "nLines":I
    new-array v2, v1, [Lcom/vividsolutions/jts/geom/LineString;

    .line 131
    .local v2, "revLines":[Lcom/vividsolutions/jts/geom/LineString;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiLineString;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 132
    add-int/lit8 v3, v1, -0x1

    sub-int v4, v3, v0

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/MultiLineString;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/LineString;

    aput-object v3, v2, v4

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiLineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiLineString([Lcom/vividsolutions/jts/geom/LineString;)Lcom/vividsolutions/jts/geom/MultiLineString;

    move-result-object v3

    return-object v3
.end method
