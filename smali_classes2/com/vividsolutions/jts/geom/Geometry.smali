.class public abstract Lcom/vividsolutions/jts/geom/Geometry;
.super Ljava/lang/Object;
.source "Geometry.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# static fields
.field private static final geometryChangedFilter:Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

.field private static final serialVersionUID:J = 0x799ea46522854a3eL

.field private static sortedClasses:[Ljava/lang/Class;


# instance fields
.field protected SRID:I

.field protected envelope:Lcom/vividsolutions/jts/geom/Envelope;

.field protected final factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private userData:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/vividsolutions/jts/geom/Geometry$1;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Geometry$1;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/geom/Geometry;->geometryChangedFilter:Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->userData:Ljava/lang/Object;

    .line 210
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 211
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getSRID()I

    move-result v0

    iput v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->SRID:I

    .line 212
    return-void
.end method

.method private createPointFromInternalCoord(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Point;
    .locals 1
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "exemplar"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1900
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 1901
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    return-object v0
.end method

.method private getClassSortIndex()I
    .locals 3

    .prologue
    .line 1874
    sget-object v1, Lcom/vividsolutions/jts/geom/Geometry;->sortedClasses:[Ljava/lang/Class;

    if-nez v1, :cond_0

    .line 1875
    invoke-static {}, Lcom/vividsolutions/jts/geom/Geometry;->initSortedClasses()V

    .line 1877
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/vividsolutions/jts/geom/Geometry;->sortedClasses:[Ljava/lang/Class;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1878
    sget-object v1, Lcom/vividsolutions/jts/geom/Geometry;->sortedClasses:[Ljava/lang/Class;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1882
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 1877
    .restart local v0    # "i":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1881
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Class not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere(Ljava/lang/String;)V

    .line 1882
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected static hasNonEmptyElements([Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "geometries"    # [Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 230
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 231
    aget-object v1, p0, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    const/4 v1, 0x1

    .line 235
    :goto_1
    return v1

    .line 230
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected static hasNullElements([Ljava/lang/Object;)Z
    .locals 2
    .param p0, "array"    # [Ljava/lang/Object;

    .prologue
    .line 246
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 247
    aget-object v1, p0, v0

    if-nez v1, :cond_0

    .line 248
    const/4 v1, 0x1

    .line 251
    :goto_1
    return v1

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 251
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static initSortedClasses()V
    .locals 3

    .prologue
    .line 1887
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/vividsolutions/jts/geom/Point;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/vividsolutions/jts/geom/MultiPoint;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/vividsolutions/jts/geom/LineString;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Lcom/vividsolutions/jts/geom/LinearRing;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Lcom/vividsolutions/jts/geom/MultiLineString;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Lcom/vividsolutions/jts/geom/Polygon;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-class v2, Lcom/vividsolutions/jts/geom/MultiPolygon;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-class v2, Lcom/vividsolutions/jts/geom/GeometryCollection;

    aput-object v2, v0, v1

    sput-object v0, Lcom/vividsolutions/jts/geom/Geometry;->sortedClasses:[Ljava/lang/Class;

    .line 1896
    return-void
.end method


# virtual methods
.method public abstract apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V
.end method

.method public abstract apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V
.end method

.method public abstract apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V
.end method

.method public abstract apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V
.end method

.method public buffer(D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "distance"    # D

    .prologue
    .line 1180
    invoke-static {p0, p1, p2}, Lcom/vividsolutions/jts/operation/buffer/BufferOp;->bufferOp(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public buffer(DI)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "distance"    # D
    .param p3, "quadrantSegments"    # I

    .prologue
    .line 1213
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/buffer/BufferOp;->bufferOp(Lcom/vividsolutions/jts/geom/Geometry;DI)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public buffer(DII)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "distance"    # D
    .param p3, "quadrantSegments"    # I
    .param p4, "endCapStyle"    # I

    .prologue
    .line 1251
    invoke-static {p0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/operation/buffer/BufferOp;->bufferOp(Lcom/vividsolutions/jts/geom/Geometry;DII)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method protected checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1781
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.vividsolutions.jts.geom.GeometryCollection"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This method does not support GeometryCollection arguments"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784
    :cond_0
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1624
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1625
    .local v0, "clone":Lcom/vividsolutions/jts/geom/Geometry;
    iget-object v2, v0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    if-eqz v2, :cond_0

    new-instance v2, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    iput-object v2, v0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1630
    .end local v0    # "clone":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return-object v0

    .line 1628
    :catch_0
    move-exception v1

    .line 1629
    .local v1, "e":Ljava/lang/CloneNotSupportedException;
    invoke-static {}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere()V

    .line 1630
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected compare(Ljava/util/Collection;Ljava/util/Collection;)I
    .locals 6
    .param p1, "a"    # Ljava/util/Collection;
    .param p2, "b"    # Ljava/util/Collection;

    .prologue
    .line 1849
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1850
    .local v3, "i":Ljava/util/Iterator;
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1851
    .local v4, "j":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1852
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Comparable;

    .line 1853
    .local v0, "aElement":Ljava/lang/Comparable;
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Comparable;

    .line 1854
    .local v1, "bElement":Ljava/lang/Comparable;
    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v2

    .line 1855
    .local v2, "comparison":I
    if-eqz v2, :cond_0

    .line 1865
    .end local v0    # "aElement":Ljava/lang/Comparable;
    .end local v1    # "bElement":Ljava/lang/Comparable;
    .end local v2    # "comparison":I
    :goto_0
    return v2

    .line 1859
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1860
    const/4 v2, 0x1

    goto :goto_0

    .line 1862
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1863
    const/4 v2, -0x1

    goto :goto_0

    .line 1865
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1692
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1693
    .local v0, "other":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v1

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1694
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v1

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1705
    :goto_0
    return v1

    .line 1696
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1697
    const/4 v1, 0x0

    goto :goto_0

    .line 1699
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1700
    const/4 v1, -0x1

    goto :goto_0

    .line 1702
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1703
    const/4 v1, 0x1

    goto :goto_0

    .line 1705
    :cond_3
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->compareToSameClass(Ljava/lang/Object;)I

    move-result v1

    goto :goto_0
.end method

.method public compareTo(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "comp"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;

    .prologue
    .line 1739
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1740
    .local v0, "other":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v1

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1741
    invoke-direct {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v1

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getClassSortIndex()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1752
    :goto_0
    return v1

    .line 1743
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1744
    const/4 v1, 0x0

    goto :goto_0

    .line 1746
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1747
    const/4 v1, -0x1

    goto :goto_0

    .line 1749
    :cond_2
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1750
    const/4 v1, 0x1

    goto :goto_0

    .line 1752
    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/geom/Geometry;->compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I

    move-result v1

    goto :goto_0
.end method

.method protected abstract compareToSameClass(Ljava/lang/Object;)I
.end method

.method protected abstract compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I
.end method

.method protected abstract computeEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;
.end method

.method public contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 882
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 883
    const/4 v0, 0x0

    .line 889
    .end local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_0
    return v0

    .line 885
    .restart local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isRectangle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 886
    check-cast p0, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->contains(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 889
    .restart local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isContains()Z

    move-result v0

    goto :goto_0
.end method

.method public convexHull()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 1285
    new-instance v0, Lcom/vividsolutions/jts/algorithm/ConvexHull;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/algorithm/ConvexHull;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->getConvexHull()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public coveredBy(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 995
    invoke-virtual {p1, p0}, Lcom/vividsolutions/jts/geom/Geometry;->covers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public covers(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 955
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->covers(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 956
    const/4 v0, 0x0

    .line 962
    :goto_0
    return v0

    .line 958
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isRectangle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 960
    const/4 v0, 0x1

    goto :goto_0

    .line 962
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isCovers()Z

    move-result v0

    goto :goto_0
.end method

.method public crosses(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 816
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 817
    const/4 v0, 0x0

    .line 818
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isCrosses(II)Z

    move-result v0

    goto :goto_0
.end method

.method public difference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x3

    .line 1418
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-static {v1, p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 1423
    :goto_0
    return-object v0

    .line 1419
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 1421
    :cond_1
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1422
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1423
    invoke-static {p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapIfNeededOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method public disjoint(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 702
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public distance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 455
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->distance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v0

    return-wide v0
.end method

.method protected equal(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)Z
    .locals 3
    .param p1, "a"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "b"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "tolerance"    # D

    .prologue
    .line 1869
    const-wide/16 v0, 0x0

    cmpl-double v0, p3, v0

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1870
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    cmpg-double v0, v0, p3

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1056
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->equalsTopo(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1122
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Geometry;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 1124
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 1123
    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1124
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    goto :goto_0
.end method

.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1544
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v0

    return v0
.end method

.method public abstract equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
.end method

.method public equalsNorm(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1563
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1564
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->norm()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->norm()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method

.method public equalsTopo(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1086
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1087
    const/4 v0, 0x0

    .line 1088
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isEquals(II)Z

    move-result v0

    goto :goto_0
.end method

.method public geometryChanged()V
    .locals 1

    .prologue
    .line 669
    sget-object v0, Lcom/vividsolutions/jts/geom/Geometry;->geometryChangedFilter:Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 670
    return-void
.end method

.method protected geometryChangedAction()V
    .locals 1

    .prologue
    .line 680
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    .line 681
    return-void
.end method

.method public getArea()D
    .locals 2

    .prologue
    .line 496
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public abstract getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
.end method

.method public abstract getBoundaryDimension()I
.end method

.method public getCentroid()Lcom/vividsolutions/jts/geom/Point;
    .locals 5

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 527
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v3, 0x0

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    .line 545
    :goto_0
    return-object v3

    .line 528
    :cond_0
    const/4 v1, 0x0

    .line 529
    .local v1, "centPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    .line 530
    .local v2, "dim":I
    if-nez v2, :cond_1

    .line 531
    new-instance v0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/CentroidPoint;-><init>()V

    .line 532
    .local v0, "cent":Lcom/vividsolutions/jts/algorithm/CentroidPoint;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 533
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 545
    .end local v0    # "cent":Lcom/vividsolutions/jts/algorithm/CentroidPoint;
    :goto_1
    invoke-direct {p0, v1, p0}, Lcom/vividsolutions/jts/geom/Geometry;->createPointFromInternalCoord(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    goto :goto_0

    .line 535
    :cond_1
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 536
    new-instance v0, Lcom/vividsolutions/jts/algorithm/CentroidLine;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/CentroidLine;-><init>()V

    .line 537
    .local v0, "cent":Lcom/vividsolutions/jts/algorithm/CentroidLine;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 538
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 539
    goto :goto_1

    .line 541
    .end local v0    # "cent":Lcom/vividsolutions/jts/algorithm/CentroidLine;
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/algorithm/CentroidArea;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/CentroidArea;-><init>()V

    .line 542
    .local v0, "cent":Lcom/vividsolutions/jts/algorithm/CentroidArea;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 543
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    goto :goto_1
.end method

.method public abstract getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
.end method

.method public abstract getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
.end method

.method public abstract getDimension()I
.end method

.method public getEnvelope()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometry(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 2

    .prologue
    .line 655
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    if-nez v0, :cond_0

    .line 656
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->computeEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    .line 658
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Geometry;->envelope:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    return-object v0
.end method

.method public getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    return-object v0
.end method

.method public getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 0
    .param p1, "n"    # I

    .prologue
    .line 322
    return-object p0
.end method

.method public abstract getGeometryType()Ljava/lang/String;
.end method

.method public getInteriorPoint()Lcom/vividsolutions/jts/geom/Point;
    .locals 5

    .prologue
    .line 561
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 562
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v3, 0x0

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    .line 577
    :goto_0
    return-object v3

    .line 563
    :cond_0
    const/4 v2, 0x0

    .line 564
    .local v2, "interiorPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v0

    .line 565
    .local v0, "dim":I
    if-nez v0, :cond_1

    .line 566
    new-instance v1, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 567
    .local v1, "intPt":Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;->getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 577
    .end local v1    # "intPt":Lcom/vividsolutions/jts/algorithm/InteriorPointPoint;
    :goto_1
    invoke-direct {p0, v2, p0}, Lcom/vividsolutions/jts/geom/Geometry;->createPointFromInternalCoord(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v3

    goto :goto_0

    .line 569
    :cond_1
    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 570
    new-instance v1, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 571
    .local v1, "intPt":Lcom/vividsolutions/jts/algorithm/InteriorPointLine;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/InteriorPointLine;->getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 572
    goto :goto_1

    .line 574
    .end local v1    # "intPt":Lcom/vividsolutions/jts/algorithm/InteriorPointLine;
    :cond_2
    new-instance v1, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 575
    .local v1, "intPt":Lcom/vividsolutions/jts/algorithm/InteriorPointArea;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/algorithm/InteriorPointArea;->getInteriorPoint()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    goto :goto_1
.end method

.method public getLength()D
    .locals 2

    .prologue
    .line 510
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getNumGeometries()I
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x1

    return v0
.end method

.method public abstract getNumPoints()I
.end method

.method public getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v0

    return-object v0
.end method

.method public getSRID()I
    .locals 1

    .prologue
    .line 269
    iget v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->SRID:I

    return v0
.end method

.method public getUserData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->userData:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1134
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->hashCode()I

    move-result v0

    return v0
.end method

.method public intersection(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v2, 0x1

    .line 1323
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1324
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-static {v2, p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 1342
    :goto_0
    return-object v1

    .line 1327
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isGeometryCollection()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1328
    move-object v0, p1

    .local v0, "g2":Lcom/vividsolutions/jts/geom/Geometry;
    move-object v1, p0

    .line 1329
    check-cast v1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    new-instance v2, Lcom/vividsolutions/jts/geom/Geometry$2;

    invoke-direct {v2, p0, v0}, Lcom/vividsolutions/jts/geom/Geometry$2;-><init>(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)V

    invoke-static {v1, v2}, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;->map(Lcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v1

    goto :goto_0

    .line 1340
    .end local v0    # "g2":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1341
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1342
    invoke-static {p0, p1, v2}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapIfNeededOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    goto :goto_0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 761
    const/4 v0, 0x0

    .line 787
    :goto_0
    return v0

    .line 780
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isRectangle()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 781
    check-cast p0, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->intersects(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 783
    .restart local p0    # "this":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isRectangle()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 784
    check-cast p1, Lcom/vividsolutions/jts/geom/Polygon;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {p1, p0}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->intersects(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 787
    .restart local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isIntersects()Z

    move-result v0

    goto :goto_0
.end method

.method public abstract isEmpty()Z
.end method

.method protected isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1768
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected isGeometryCollection()Z
    .locals 2

    .prologue
    .line 1794
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isRectangle()Z
    .locals 1

    .prologue
    .line 483
    const/4 v0, 0x0

    return v0
.end method

.method public isSimple()Z
    .locals 2

    .prologue
    .line 417
    new-instance v0, Lcom/vividsolutions/jts/operation/IsSimpleOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 418
    .local v0, "op":Lcom/vividsolutions/jts/operation/IsSimpleOp;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/IsSimpleOp;->isSimple()Z

    move-result v1

    return v1
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 433
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->isValid(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public isWithinDistance(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "distance"    # D

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->distance(Lcom/vividsolutions/jts/geom/Envelope;)D

    move-result-wide v0

    .line 469
    .local v0, "envDist":D
    cmpl-double v2, v0, p2

    if-lez v2, :cond_0

    .line 470
    const/4 v2, 0x0

    .line 471
    :goto_0
    return v2

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/operation/distance/DistanceOp;->isWithinDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v2

    goto :goto_0
.end method

.method public norm()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 1660
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 1661
    .local v0, "copy":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->normalize()V

    .line 1662
    return-object v0
.end method

.method public abstract normalize()V
.end method

.method public overlaps(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 914
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 915
    const/4 v0, 0x0

    .line 916
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isOverlaps(II)Z

    move-result v0

    goto :goto_0
.end method

.method public relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 1033
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1034
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1035
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/operation/relate/RelateOp;->relate(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    return-object v0
.end method

.method public relate(Lcom/vividsolutions/jts/geom/Geometry;Ljava/lang/String;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "intersectionPattern"    # Ljava/lang/String;

    .prologue
    .line 1022
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->matches(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public abstract reverse()Lcom/vividsolutions/jts/geom/Geometry;
.end method

.method public setSRID(I)V
    .locals 0
    .param p1, "SRID"    # I

    .prologue
    .line 283
    iput p1, p0, Lcom/vividsolutions/jts/geom/Geometry;->SRID:I

    .line 284
    return-void
.end method

.method public setUserData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "userData"    # Ljava/lang/Object;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/Geometry;->userData:Ljava/lang/Object;

    .line 338
    return-void
.end method

.method public symDifference(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x4

    .line 1447
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1449
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1450
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-static {v1, p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 1459
    :goto_0
    return-object v0

    .line 1453
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 1454
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 1457
    :cond_3
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1458
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1459
    invoke-static {p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapIfNeededOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->toText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1149
    new-instance v0, Lcom/vividsolutions/jts/io/WKTWriter;

    invoke-direct {v0}, Lcom/vividsolutions/jts/io/WKTWriter;-><init>()V

    .line 1150
    .local v0, "writer":Lcom/vividsolutions/jts/io/WKTWriter;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/io/WKTWriter;->write(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public touches(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 729
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    const/4 v0, 0x0

    .line 731
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->relate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->isTouches(II)Z

    move-result v0

    goto :goto_0
.end method

.method public union()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 1484
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/union/UnaryUnionOp;->union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public union(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v1, 0x2

    .line 1382
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1383
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1384
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/Geometry;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-static {v1, p0, p1, v0}, Lcom/vividsolutions/jts/operation/overlay/OverlayOp;->createEmptyResult(ILcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 1395
    :goto_0
    return-object v0

    .line 1387
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 1388
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    goto :goto_0

    .line 1393
    :cond_3
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1394
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Geometry;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 1395
    invoke-static {p0, p1, v1}, Lcom/vividsolutions/jts/operation/overlay/snap/SnapIfNeededOverlayOp;->overlayOp(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    goto :goto_0
.end method

.method public within(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 850
    invoke-virtual {p1, p0}, Lcom/vividsolutions/jts/geom/Geometry;->contains(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method
