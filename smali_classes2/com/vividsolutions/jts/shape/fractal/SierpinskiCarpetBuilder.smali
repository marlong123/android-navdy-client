.class public Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;
.super Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;
.source "SierpinskiCarpetBuilder.java"


# instance fields
.field private coordList:Lcom/vividsolutions/jts/geom/CoordinateList;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 44
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->coordList:Lcom/vividsolutions/jts/geom/CoordinateList;

    .line 49
    return-void
.end method

.method private addHoles(IDDDLjava/util/List;)V
    .locals 22
    .param p1, "n"    # I
    .param p2, "originX"    # D
    .param p4, "originY"    # D
    .param p6, "width"    # D
    .param p8, "holeList"    # Ljava/util/List;

    .prologue
    .line 80
    if-gez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 81
    :cond_0
    add-int/lit8 v3, p1, -0x1

    .line 82
    .local v3, "n2":I
    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double v8, p6, v4

    .line 83
    .local v8, "widthThird":D
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double v4, v4, p6

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    div-double v20, v4, v6

    .line 84
    .local v20, "widthTwoThirds":D
    const-wide/high16 v4, 0x4022000000000000L    # 9.0

    div-double v18, p6, v4

    .local v18, "widthNinth":D
    move-object/from16 v2, p0

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v10, p8

    .line 85
    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 86
    add-double v4, p2, v8

    move-object/from16 v2, p0

    move-wide/from16 v6, p4

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 87
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v8

    add-double v4, v4, p2

    move-object/from16 v2, p0

    move-wide/from16 v6, p4

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 89
    add-double v6, p4, v8

    move-object/from16 v2, p0

    move-wide/from16 v4, p2

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 90
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v8

    add-double v4, v4, p2

    add-double v6, p4, v8

    move-object/from16 v2, p0

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 92
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v8

    add-double v6, p4, v4

    move-object/from16 v2, p0

    move-wide/from16 v4, p2

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 93
    add-double v4, p2, v8

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    add-double v6, v6, p4

    move-object/from16 v2, p0

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 94
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double/2addr v4, v8

    add-double v4, v4, p2

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    add-double v6, v6, p4

    move-object/from16 v2, p0

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v10}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 97
    add-double v12, p2, v8

    add-double v14, p4, v8

    move-object/from16 v11, p0

    move-wide/from16 v16, v8

    invoke-direct/range {v11 .. v17}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->createSquareHole(DDD)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v2

    move-object/from16 v0, p8

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private createSquareHole(DDD)Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 9
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "width"    # D

    .prologue
    .line 102
    const/4 v1, 0x5

    new-array v0, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    add-double v4, p1, p5

    invoke-direct {v2, v4, v5, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    add-double v4, p1, p5

    add-double v6, p3, p5

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    add-double v4, p3, p5

    invoke-direct {v2, p1, p2, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    aput-object v2, v0, v1

    .line 109
    .local v0, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    return-object v1
.end method

.method private getHoles(IDDD)[Lcom/vividsolutions/jts/geom/LinearRing;
    .locals 10
    .param p1, "n"    # I
    .param p2, "originX"    # D
    .param p4, "originY"    # D
    .param p6, "width"    # D

    .prologue
    .line 71
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .local v8, "holeList":Ljava/util/List;
    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    .line 73
    invoke-direct/range {v0 .. v8}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->addHoles(IDDDLjava/util/List;)V

    .line 75
    invoke-static {v8}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toLinearRingArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    return-object v0
.end method

.method public static recursionLevelForSize(I)I
    .locals 8
    .param p0, "numPts"    # I

    .prologue
    .line 53
    div-int/lit8 v4, p0, 0x3

    int-to-double v2, v4

    .line 54
    .local v2, "pow4":D
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double v0, v4, v6

    .line 55
    .local v0, "exp":D
    double-to-int v4, v0

    return v4
.end method


# virtual methods
.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 12

    .prologue
    .line 60
    iget v0, p0, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->numPts:I

    invoke-static {v0}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->recursionLevelForSize(I)I

    move-result v1

    .line 61
    .local v1, "level":I
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->getSquareBaseLine()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v8

    .line 62
    .local v8, "baseLine":Lcom/vividsolutions/jts/geom/LineSegment;
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    .line 63
    .local v10, "origin":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, v10, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->getDiameter()D

    move-result-wide v6

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->getHoles(IDDD)[Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v9

    .line 64
    .local v9, "holes":[Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->getSquareExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometry(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v11

    check-cast v11, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 65
    .local v11, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/fractal/SierpinskiCarpetBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v0, v11, v9}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v0

    return-object v0
.end method
