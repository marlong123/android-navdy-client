.class public Lnet/minidev/json/reader/JsonWriter;
.super Ljava/lang/Object;
.source "JsonWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/minidev/json/reader/JsonWriter$WriterByInterface;
    }
.end annotation


# static fields
.field public static final EnumWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Ljava/lang/Enum",
            "<*>;>;"
        }
    .end annotation
.end field

.field public static final JSONIterableWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final JSONJSONAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Lnet/minidev/json/JSONAwareEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final JSONJSONAwareWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Lnet/minidev/json/JSONAware;",
            ">;"
        }
    .end annotation
.end field

.field public static final JSONMapWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final JSONStreamAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Lnet/minidev/json/JSONStreamAwareEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final JSONStreamAwareWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Lnet/minidev/json/JSONStreamAwareEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final arrayWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final beansWriter:Lnet/minidev/json/reader/JsonWriterI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lnet/minidev/json/reader/JsonWriterI",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private data:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lnet/minidev/json/reader/JsonWriterI",
            "<*>;>;"
        }
    .end annotation
.end field

.field private writerInterfaces:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lnet/minidev/json/reader/JsonWriter$WriterByInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$1;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$1;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONStreamAwareWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 66
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$2;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$2;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONStreamAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 73
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$3;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$3;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONJSONAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 80
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$4;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$4;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONJSONAwareWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 87
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$5;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$5;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONIterableWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 109
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$6;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$6;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->EnumWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 118
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$7;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$7;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->JSONMapWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 175
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$8;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$8;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->beansWriter:Lnet/minidev/json/reader/JsonWriterI;

    .line 227
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$9;

    invoke-direct {v0}, Lnet/minidev/json/reader/JsonWriter$9;-><init>()V

    sput-object v0, Lnet/minidev/json/reader/JsonWriter;->arrayWriter:Lnet/minidev/json/reader/JsonWriterI;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/reader/JsonWriter;->data:Ljava/util/HashMap;

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/reader/JsonWriter;->writerInterfaces:Ljava/util/LinkedList;

    .line 32
    invoke-virtual {p0}, Lnet/minidev/json/reader/JsonWriter;->init()V

    .line 33
    return-void
.end method

.method public static writeJSONKV(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "out"    # Ljava/lang/Appendable;
    .param p3, "compression"    # Lnet/minidev/json/JSONStyle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v1, 0x22

    .line 419
    if-nez p0, :cond_0

    .line 420
    const-string v0, "null"

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 428
    :goto_0
    invoke-virtual {p3, p2}, Lnet/minidev/json/JSONStyle;->objectEndOfKey(Ljava/lang/Appendable;)V

    .line 429
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 430
    check-cast p1, Ljava/lang/String;

    .end local p1    # "value":Ljava/lang/Object;
    invoke-virtual {p3, p2, p1}, Lnet/minidev/json/JSONStyle;->writeString(Ljava/lang/Appendable;Ljava/lang/String;)V

    .line 433
    :goto_1
    invoke-virtual {p3, p2}, Lnet/minidev/json/JSONStyle;->objectElmStop(Ljava/lang/Appendable;)V

    .line 434
    return-void

    .line 421
    .restart local p1    # "value":Ljava/lang/Object;
    :cond_0
    invoke-virtual {p3, p0}, Lnet/minidev/json/JSONStyle;->mustProtectKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    invoke-interface {p2, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    .line 424
    :cond_1
    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 425
    invoke-static {p0, p2, p3}, Lnet/minidev/json/JSONValue;->escape(Ljava/lang/String;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    .line 426
    invoke-interface {p2, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_0

    .line 432
    :cond_2
    invoke-static {p1, p2, p3}, Lnet/minidev/json/JSONValue;->writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V

    goto :goto_1
.end method


# virtual methods
.method public addInterfaceWriterFirst(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lnet/minidev/json/reader/JsonWriterI",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 403
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "writer":Lnet/minidev/json/reader/JsonWriterI;, "Lnet/minidev/json/reader/JsonWriterI<*>;"
    iget-object v0, p0, Lnet/minidev/json/reader/JsonWriter;->writerInterfaces:Ljava/util/LinkedList;

    new-instance v1, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;

    invoke-direct {v1, p1, p2}, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;-><init>(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 404
    return-void
.end method

.method public addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lnet/minidev/json/reader/JsonWriterI",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 407
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p2, "writer":Lnet/minidev/json/reader/JsonWriterI;, "Lnet/minidev/json/reader/JsonWriterI<*>;"
    iget-object v0, p0, Lnet/minidev/json/reader/JsonWriter;->writerInterfaces:Ljava/util/LinkedList;

    new-instance v1, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;

    invoke-direct {v1, p1, p2}, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;-><init>(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 408
    return-void
.end method

.method public getWrite(Ljava/lang/Class;)Lnet/minidev/json/reader/JsonWriterI;
    .locals 1
    .param p1, "cls"    # Ljava/lang/Class;

    .prologue
    .line 56
    iget-object v0, p0, Lnet/minidev/json/reader/JsonWriter;->data:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/minidev/json/reader/JsonWriterI;

    return-object v0
.end method

.method public getWriterByInterface(Ljava/lang/Class;)Lnet/minidev/json/reader/JsonWriterI;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Lnet/minidev/json/reader/JsonWriterI;"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v2, p0, Lnet/minidev/json/reader/JsonWriter;->writerInterfaces:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;

    .line 48
    .local v1, "w":Lnet/minidev/json/reader/JsonWriter$WriterByInterface;
    iget-object v2, v1, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;->_interface:Ljava/lang/Class;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    iget-object v2, v1, Lnet/minidev/json/reader/JsonWriter$WriterByInterface;->_writer:Lnet/minidev/json/reader/JsonWriterI;

    .line 51
    .end local v1    # "w":Lnet/minidev/json/reader/JsonWriter$WriterByInterface;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 243
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$10;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$10;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 249
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$11;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$11;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 255
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$12;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$12;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Double;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 264
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$13;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$13;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/util/Date;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 272
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$14;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$14;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Float;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 281
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$15;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$15;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Integer;

    aput-object v2, v1, v4

    const-class v2, Ljava/lang/Long;

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-class v3, Ljava/lang/Byte;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Ljava/lang/Short;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Ljava/math/BigInteger;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 287
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$16;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$16;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 293
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$17;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$17;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 303
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$18;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$18;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [I

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 318
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$19;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$19;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [S

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 333
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$20;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$20;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [J

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 348
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$21;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$21;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [F

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 363
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$22;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$22;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [D

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 378
    new-instance v0, Lnet/minidev/json/reader/JsonWriter$23;

    invoke-direct {v0, p0}, Lnet/minidev/json/reader/JsonWriter$23;-><init>(Lnet/minidev/json/reader/JsonWriter;)V

    new-array v1, v5, [Ljava/lang/Class;

    const-class v2, [Z

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V

    .line 393
    const-class v0, Lnet/minidev/json/JSONStreamAwareEx;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONStreamAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 394
    const-class v0, Lnet/minidev/json/JSONStreamAware;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONStreamAwareWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 395
    const-class v0, Lnet/minidev/json/JSONAwareEx;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONJSONAwareExWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 396
    const-class v0, Lnet/minidev/json/JSONAware;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONJSONAwareWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 397
    const-class v0, Ljava/util/Map;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONMapWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 398
    const-class v0, Ljava/lang/Iterable;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->JSONIterableWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 399
    const-class v0, Ljava/lang/Enum;

    sget-object v1, Lnet/minidev/json/reader/JsonWriter;->EnumWriter:Lnet/minidev/json/reader/JsonWriterI;

    invoke-virtual {p0, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->addInterfaceWriterLast(Ljava/lang/Class;Lnet/minidev/json/reader/JsonWriterI;)V

    .line 400
    return-void
.end method

.method public varargs registerWriter(Lnet/minidev/json/reader/JsonWriterI;[Ljava/lang/Class;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lnet/minidev/json/reader/JsonWriterI",
            "<TT;>;[",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 411
    .local p1, "writer":Lnet/minidev/json/reader/JsonWriterI;, "Lnet/minidev/json/reader/JsonWriterI<TT;>;"
    .local p2, "cls":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 412
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v4, p0, Lnet/minidev/json/reader/JsonWriter;->data:Ljava/util/HashMap;

    invoke-virtual {v4, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 413
    .end local v1    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_0
    return-void
.end method
