.class final Lnet/minidev/json/reader/JsonWriter$8;
.super Ljava/lang/Object;
.source "JsonWriter.java"

# interfaces
.implements Lnet/minidev/json/reader/JsonWriterI;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/minidev/json/reader/JsonWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lnet/minidev/json/reader/JsonWriterI",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public writeJSONString(Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    .locals 16
    .param p2, "out"    # Ljava/lang/Appendable;
    .param p3, "compression"    # Lnet/minidev/json/JSONStyle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;",
            "Ljava/lang/Appendable;",
            "Lnet/minidev/json/JSONStyle;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 178
    .local p1, "value":Ljava/lang/Object;, "TE;"
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    .line 179
    .local v13, "nextClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v12, 0x0

    .line 180
    .local v12, "needSep":Z
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lnet/minidev/json/JSONStyle;->objectStart(Ljava/lang/Appendable;)V

    .line 181
    :goto_0
    const-class v15, Ljava/lang/Object;

    if-eq v13, v15, :cond_8

    .line 182
    invoke-virtual {v13}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v6

    .line 183
    .local v6, "fields":[Ljava/lang/reflect/Field;
    move-object v2, v6

    .local v2, "arr$":[Ljava/lang/reflect/Field;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v9, :cond_7

    aget-object v5, v2, v8

    .line 184
    .local v5, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v10

    .line 185
    .local v10, "m":I
    and-int/lit16 v15, v10, 0x98

    if-lez v15, :cond_1

    .line 183
    :cond_0
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 187
    :cond_1
    const/4 v14, 0x0

    .line 188
    .local v14, "v":Ljava/lang/Object;
    and-int/lit8 v15, v10, 0x1

    if-lez v15, :cond_3

    .line 189
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    .line 209
    :goto_3
    if-nez v14, :cond_2

    invoke-virtual/range {p3 .. p3}, Lnet/minidev/json/JSONStyle;->ignoreNull()Z

    move-result v15

    if-nez v15, :cond_0

    .line 211
    :cond_2
    if-eqz v12, :cond_6

    .line 212
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lnet/minidev/json/JSONStyle;->objectNext(Ljava/lang/Appendable;)V

    .line 215
    :goto_4
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v15, v14, v0, v1}, Lnet/minidev/json/reader/JsonWriter;->writeJSONKV(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Appendable;Lnet/minidev/json/JSONStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 221
    .end local v2    # "arr$":[Ljava/lang/reflect/Field;
    .end local v5    # "field":Ljava/lang/reflect/Field;
    .end local v6    # "fields":[Ljava/lang/reflect/Field;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "m":I
    .end local v12    # "needSep":Z
    .end local v13    # "nextClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v14    # "v":Ljava/lang/Object;
    :catch_0
    move-exception v4

    .line 222
    .local v4, "e":Ljava/lang/Exception;
    new-instance v15, Ljava/lang/RuntimeException;

    invoke-direct {v15, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v15

    .line 191
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v5    # "field":Ljava/lang/reflect/Field;
    .restart local v6    # "fields":[Ljava/lang/reflect/Field;
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    .restart local v10    # "m":I
    .restart local v12    # "needSep":Z
    .restart local v13    # "nextClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v14    # "v":Ljava/lang/Object;
    :cond_3
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lnet/minidev/json/JSONUtil;->getGetterName(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    .line 192
    .local v7, "g":Ljava/lang/String;
    const/4 v11, 0x0

    .line 195
    .local v11, "mtd":Ljava/lang/reflect/Method;
    const/4 v15, 0x0

    :try_start_2
    new-array v15, v15, [Ljava/lang/Class;

    invoke-virtual {v13, v7, v15}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v11

    .line 198
    :goto_5
    if-nez v11, :cond_5

    .line 199
    :try_start_3
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v3

    .line 200
    .local v3, "c2":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v15, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v3, v15, :cond_4

    const-class v15, Ljava/lang/Boolean;

    if-ne v3, v15, :cond_5

    .line 201
    :cond_4
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lnet/minidev/json/JSONUtil;->getIsName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 202
    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Class;

    invoke-virtual {v13, v7, v15}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 205
    .end local v3    # "c2":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_5
    if-eqz v11, :cond_0

    .line 207
    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    goto :goto_3

    .line 214
    .end local v7    # "g":Ljava/lang/String;
    .end local v11    # "mtd":Ljava/lang/reflect/Method;
    :cond_6
    const/4 v12, 0x1

    goto :goto_4

    .line 218
    .end local v5    # "field":Ljava/lang/reflect/Field;
    .end local v10    # "m":I
    .end local v14    # "v":Ljava/lang/Object;
    :cond_7
    invoke-virtual {v13}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v13

    .line 219
    goto/16 :goto_0

    .line 220
    .end local v2    # "arr$":[Ljava/lang/reflect/Field;
    .end local v6    # "fields":[Ljava/lang/reflect/Field;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :cond_8
    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lnet/minidev/json/JSONStyle;->objectStop(Ljava/lang/Appendable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 224
    return-void

    .line 196
    .restart local v2    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v5    # "field":Ljava/lang/reflect/Field;
    .restart local v6    # "fields":[Ljava/lang/reflect/Field;
    .restart local v7    # "g":Ljava/lang/String;
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    .restart local v10    # "m":I
    .restart local v11    # "mtd":Ljava/lang/reflect/Method;
    .restart local v14    # "v":Ljava/lang/Object;
    :catch_1
    move-exception v15

    goto :goto_5
.end method
