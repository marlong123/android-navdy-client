.class public abstract Lorg/droidparts/persist/sql/AbstractDBOpenHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "AbstractDBOpenHelper.java"

# interfaces
.implements Lorg/droidparts/contract/SQL$DDL;


# instance fields
.field private final ctx:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "version"    # I

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1, p3}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->ctx:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method protected final varargs addMissingColumns(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/Class;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "[",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 73
    .local p2, "entityClasses":[Ljava/lang/Class;, "[Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v5, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 76
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    :try_start_0
    invoke-static {p1, v1}, Lorg/droidparts/inner/PersistUtils;->getAddMissingColumns(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    :catch_0
    move-exception v2

    .line 78
    .local v2, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 81
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {p0, p1, v5}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z

    move-result v6

    return v6
.end method

.method protected final varargs createIndex(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table"    # Ljava/lang/String;
    .param p3, "unique"    # Z
    .param p4, "firstColumn"    # Ljava/lang/String;
    .param p5, "otherColumns"    # [Ljava/lang/String;

    .prologue
    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v0, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p2, p3, p4, p5}, Lorg/droidparts/inner/PersistUtils;->getCreateIndex(Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-virtual {p0, p1, v0}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z

    move-result v1

    return v1
.end method

.method protected final varargs createTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/Class;)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "[",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 62
    .local p2, "entityClasses":[Ljava/lang/Class;, "[Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v5, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/Class;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 64
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    invoke-static {v1}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableName(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1}, Lorg/droidparts/inner/ClassSpecRegistry;->getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/droidparts/inner/PersistUtils;->getSQLCreate(Ljava/lang/String;[Lorg/droidparts/inner/ann/FieldSpec;)Ljava/lang/String;

    move-result-object v4

    .line 66
    .local v4, "query":Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 68
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    .end local v4    # "query":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p1, v5}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z

    move-result v6

    return v6
.end method

.method protected final varargs dropTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Z
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "optionalTableNames"    # [Ljava/lang/String;

    .prologue
    .line 86
    invoke-static {p1, p2}, Lorg/droidparts/inner/PersistUtils;->getDropTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 88
    .local v0, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0, p1, v0}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z

    move-result v1

    return v1
.end method

.method protected final executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "statements":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lorg/droidparts/inner/PersistUtils;->executeStatements(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->onOpen(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 101
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->onCreateTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 102
    return-void
.end method

.method protected abstract onCreateTables(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 106
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    const-string v0, "PRAGMA foreign_keys = ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 109
    :cond_0
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/AbstractDBOpenHelper;->onOpenExtra(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 110
    return-void
.end method

.method protected onOpenExtra(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 48
    return-void
.end method
