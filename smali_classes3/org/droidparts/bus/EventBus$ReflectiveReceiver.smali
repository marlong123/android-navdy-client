.class Lorg/droidparts/bus/EventBus$ReflectiveReceiver;
.super Ljava/lang/Object;
.source "EventBus.java"

# interfaces
.implements Lorg/droidparts/bus/EventReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/bus/EventBus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReflectiveReceiver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/droidparts/bus/EventReceiver",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final objectRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final spec:Lorg/droidparts/inner/ann/MethodSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/droidparts/inner/ann/MethodSpec",
            "<",
            "Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lorg/droidparts/inner/ann/MethodSpec;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lorg/droidparts/inner/ann/MethodSpec",
            "<",
            "Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    .local p2, "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->objectRef:Ljava/lang/ref/WeakReference;

    .line 197
    iput-object p2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    .line 198
    return-void
.end method


# virtual methods
.method public onEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 203
    :try_start_0
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->objectRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    .line 204
    .local v1, "obj":Ljava/lang/Object;
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->paramTypes:[Ljava/lang/Class;

    array-length v2, v2

    packed-switch v2, :pswitch_data_0

    .line 216
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    :goto_0
    return-void

    .line 206
    :pswitch_0
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 218
    .end local v1    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    throw v0

    .line 209
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "obj":Ljava/lang/Object;
    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->paramTypes:[Ljava/lang/Class;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-class v3, Ljava/lang/String;

    if-ne v2, v3, :cond_0

    .line 210
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 220
    .end local v1    # "obj":Ljava/lang/Object;
    :catch_1
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 212
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "obj":Ljava/lang/Object;
    :cond_0
    :try_start_2
    iget-object v2, p0, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->spec:Lorg/droidparts/inner/ann/MethodSpec;

    iget-object v2, v2, Lorg/droidparts/inner/ann/MethodSpec;->method:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v2, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
