.class public Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
.super Lorg/droidparts/adapter/widget/SpinnerAdapter;
.source "StringSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/adapter/widget/SpinnerAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/widget/Spinner;I)V
    .locals 1
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "stringArrResId"    # I

    .prologue
    .line 26
    invoke-virtual {p1}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;[Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/widget/Spinner;Ljava/util/List;)V
    .locals 0
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/Spinner;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/adapter/widget/SpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/widget/Spinner;[Ljava/lang/String;)V
    .locals 1
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "arr"    # [Ljava/lang/String;

    .prologue
    .line 31
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 32
    return-void
.end method
