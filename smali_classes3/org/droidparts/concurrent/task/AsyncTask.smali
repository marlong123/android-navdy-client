.class public abstract Lorg/droidparts/concurrent/task/AsyncTask;
.super Landroid/os/AsyncTask;
.source "AsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;",
        "Landroid/util/Pair",
        "<",
        "Ljava/lang/Exception;",
        "TResult;>;>;"
    }
.end annotation


# instance fields
.field private final ctx:Landroid/content/Context;

.field private final resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/droidparts/concurrent/task/AsyncTaskResultListener",
            "<TResult;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 31
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/droidparts/concurrent/task/AsyncTask;-><init>(Landroid/content/Context;Lorg/droidparts/concurrent/task/AsyncTaskResultListener;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/concurrent/task/AsyncTaskResultListener;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/droidparts/concurrent/task/AsyncTaskResultListener",
            "<TResult;>;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    .local p2, "resultListener":Lorg/droidparts/concurrent/task/AsyncTaskResultListener;, "Lorg/droidparts/concurrent/task/AsyncTaskResultListener<TResult;>;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 35
    invoke-static {p1, p0}, Lorg/droidparts/Injector;->inject(Landroid/content/Context;Ljava/lang/Object;)V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/concurrent/task/AsyncTask;->ctx:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lorg/droidparts/concurrent/task/AsyncTask;->resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;

    .line 38
    return-void
.end method


# virtual methods
.method protected final varargs doInBackground([Ljava/lang/Object;)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Exception;",
            "TResult;>;"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "params":[Ljava/lang/Object;, "[TParams;"
    const/4 v2, 0x0

    .line 47
    .local v2, "res":Ljava/lang/Object;, "TResult;"
    const/4 v1, 0x0

    .line 49
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 50
    .local v4, "start":J
    invoke-virtual {p0, p1}, Lorg/droidparts/concurrent/task/AsyncTask;->onExecute([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 51
    const-string v3, "Executed %s in %d ms."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .end local v2    # "res":Ljava/lang/Object;, "TResult;"
    .end local v4    # "start":J
    :goto_0
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 55
    move-object v1, v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/concurrent/task/AsyncTask;->doInBackground([Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lorg/droidparts/concurrent/task/AsyncTask;->ctx:Landroid/content/Context;

    return-object v0
.end method

.method protected varargs abstract onExecute([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected final onPostExecute(Landroid/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Exception;",
            "TResult;>;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Exception;TResult;>;"
    :try_start_0
    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_1

    .line 65
    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Exception;

    invoke-virtual {p0, v1}, Lorg/droidparts/concurrent/task/AsyncTask;->onPostExecuteFailure(Ljava/lang/Exception;)V

    .line 66
    iget-object v1, p0, Lorg/droidparts/concurrent/task/AsyncTask;->resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;

    if-eqz v1, :cond_0

    .line 67
    iget-object v2, p0, Lorg/droidparts/concurrent/task/AsyncTask;->resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Exception;

    invoke-interface {v2, v1}, Lorg/droidparts/concurrent/task/AsyncTaskResultListener;->onAsyncTaskFailure(Ljava/lang/Exception;)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lorg/droidparts/concurrent/task/AsyncTask;->onPostExecuteSuccess(Ljava/lang/Object;)V

    .line 71
    iget-object v1, p0, Lorg/droidparts/concurrent/task/AsyncTask;->resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;

    if-eqz v1, :cond_0

    .line 72
    iget-object v1, p0, Lorg/droidparts/concurrent/task/AsyncTask;->resultListener:Lorg/droidparts/concurrent/task/AsyncTaskResultListener;

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lorg/droidparts/concurrent/task/AsyncTaskResultListener;->onAsyncTaskSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "t":Ljava/lang/Throwable;
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/concurrent/task/AsyncTask;->onPostExecute(Landroid/util/Pair;)V

    return-void
.end method

.method protected onPostExecuteFailure(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 86
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    return-void
.end method

.method protected onPostExecuteSuccess(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lorg/droidparts/concurrent/task/AsyncTask;, "Lorg/droidparts/concurrent/task/AsyncTask<TParams;TProgress;TResult;>;"
    .local p1, "result":Ljava/lang/Object;, "TResult;"
    return-void
.end method
