.class Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;
.super Ljava/lang/Object;
.source "ImageFetcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/droidparts/net/image/ImageFetcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ImageViewSpec"
.end annotation


# instance fields
.field final cacheKey:Ljava/lang/String;

.field final configHint:Landroid/graphics/Bitmap$Config;

.field final crossFadeMillis:I

.field final heightHint:I

.field final imgUrl:Ljava/lang/String;

.field private final imgViewHash:I

.field final imgViewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field final inBitmapRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field final listener:Lorg/droidparts/net/image/ImageFetchListener;

.field final reshaper:Lorg/droidparts/net/image/ImageReshaper;

.field final widthHint:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Ljava/lang/String;Landroid/graphics/Bitmap;ILorg/droidparts/net/image/ImageReshaper;Lorg/droidparts/net/image/ImageFetchListener;)V
    .locals 2
    .param p1, "imgView"    # Landroid/widget/ImageView;
    .param p2, "imgUrl"    # Ljava/lang/String;
    .param p3, "inBitmap"    # Landroid/graphics/Bitmap;
    .param p4, "crossFadeMillis"    # I
    .param p5, "reshaper"    # Lorg/droidparts/net/image/ImageReshaper;
    .param p6, "listener"    # Lorg/droidparts/net/image/ImageFetchListener;

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewRef:Ljava/lang/ref/WeakReference;

    .line 336
    iput-object p2, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    .line 337
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->inBitmapRef:Ljava/lang/ref/WeakReference;

    .line 338
    iput p4, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->crossFadeMillis:I

    .line 339
    iput-object p5, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    .line 340
    iput-object p6, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->listener:Lorg/droidparts/net/image/ImageFetchListener;

    .line 341
    invoke-direct {p0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->getCacheKey()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->cacheKey:Ljava/lang/String;

    .line 342
    invoke-direct {p0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->getConfigHint()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    iput-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->configHint:Landroid/graphics/Bitmap$Config;

    .line 343
    invoke-direct {p0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->getSizeHint()Landroid/graphics/Point;

    move-result-object v0

    .line 344
    .local v0, "p":Landroid/graphics/Point;
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->widthHint:I

    .line 345
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->heightHint:I

    .line 346
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iput v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewHash:I

    .line 347
    return-void
.end method

.method private getCacheKey()Ljava/lang/String;
    .locals 3

    .prologue
    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 351
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    if-eqz v2, :cond_0

    .line 353
    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    invoke-interface {v2}, Lorg/droidparts/net/image/ImageReshaper;->getCacheId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    :cond_0
    invoke-direct {p0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->getSizeHint()Landroid/graphics/Point;

    move-result-object v0

    .line 357
    .local v0, "p":Landroid/graphics/Point;
    iget v2, v0, Landroid/graphics/Point;->x:I

    if-gtz v2, :cond_1

    iget v2, v0, Landroid/graphics/Point;->y:I

    if-lez v2, :cond_2

    .line 358
    :cond_1
    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 360
    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    iget v2, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 363
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getConfigHint()Landroid/graphics/Bitmap$Config;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    invoke-interface {v0}, Lorg/droidparts/net/image/ImageReshaper;->getBitmapConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSizeHint()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 371
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 372
    .local v0, "p":Landroid/graphics/Point;
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    invoke-interface {v1}, Lorg/droidparts/net/image/ImageReshaper;->getImageWidthHint()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 374
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->reshaper:Lorg/droidparts/net/image/ImageReshaper;

    invoke-interface {v1}, Lorg/droidparts/net/image/ImageReshaper;->getImageHeightHint()I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 376
    :cond_0
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-gtz v1, :cond_1

    iget v1, v0, Landroid/graphics/Point;->y:I

    if-gtz v1, :cond_1

    .line 377
    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v1}, Lorg/droidparts/inner/BitmapFactoryUtils;->calcDecodeSizeHint(Landroid/widget/ImageView;)Landroid/graphics/Point;

    move-result-object v0

    .line 379
    :cond_1
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 384
    if-ne p0, p1, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v0

    .line 386
    :cond_1
    instance-of v2, p1, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    if-eqz v2, :cond_2

    .line 387
    invoke-virtual {p0}, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 389
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgViewHash:I

    return v0
.end method
